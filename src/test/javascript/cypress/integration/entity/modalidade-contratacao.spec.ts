import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('ModalidadeContratacao e2e test', () => {
  const modalidadeContratacaoPageUrl = '/modalidade-contratacao';
  const modalidadeContratacaoPageUrlPattern = new RegExp('/modalidade-contratacao(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const modalidadeContratacaoSample = { nome: 'e-commerce Calças' };

  let modalidadeContratacao: any;

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/modalidade-contratacaos+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/modalidade-contratacaos').as('postEntityRequest');
    cy.intercept('DELETE', '/api/modalidade-contratacaos/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (modalidadeContratacao) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/modalidade-contratacaos/${modalidadeContratacao.id}`,
      }).then(() => {
        modalidadeContratacao = undefined;
      });
    }
  });

  it('ModalidadeContratacaos menu should load ModalidadeContratacaos page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('modalidade-contratacao');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('ModalidadeContratacao').should('exist');
    cy.url().should('match', modalidadeContratacaoPageUrlPattern);
  });

  describe('ModalidadeContratacao page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(modalidadeContratacaoPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create ModalidadeContratacao page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/modalidade-contratacao/new$'));
        cy.getEntityCreateUpdateHeading('ModalidadeContratacao');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', modalidadeContratacaoPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/modalidade-contratacaos',
          body: modalidadeContratacaoSample,
        }).then(({ body }) => {
          modalidadeContratacao = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/modalidade-contratacaos+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [modalidadeContratacao],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(modalidadeContratacaoPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details ModalidadeContratacao page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('modalidadeContratacao');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', modalidadeContratacaoPageUrlPattern);
      });

      it('edit button click should load edit ModalidadeContratacao page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('ModalidadeContratacao');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', modalidadeContratacaoPageUrlPattern);
      });

      it('last delete button click should delete instance of ModalidadeContratacao', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('modalidadeContratacao').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', modalidadeContratacaoPageUrlPattern);

        modalidadeContratacao = undefined;
      });
    });
  });

  describe('new ModalidadeContratacao page', () => {
    beforeEach(() => {
      cy.visit(`${modalidadeContratacaoPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('ModalidadeContratacao');
    });

    it('should create an instance of ModalidadeContratacao', () => {
      cy.get(`[data-cy="nome"]`).type('Suave Designer').should('have.value', 'Suave Designer');

      cy.get(`[data-cy="categoria"]`).type('maximize Right-sized Future').should('have.value', 'maximize Right-sized Future');

      cy.get(`[data-cy="descricao"]`).type('Lustroso bandwidth driver').should('have.value', 'Lustroso bandwidth driver');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        modalidadeContratacao = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', modalidadeContratacaoPageUrlPattern);
    });
  });
});
