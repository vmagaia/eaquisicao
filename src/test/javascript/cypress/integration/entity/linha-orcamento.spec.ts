import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('LinhaOrcamento e2e test', () => {
  const linhaOrcamentoPageUrl = '/linha-orcamento';
  const linhaOrcamentoPageUrlPattern = new RegExp('/linha-orcamento(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const linhaOrcamentoSample = { nome: 'hack Toalhas Brand', rubrica: 'Rústico systemic magnetic', valor: 40620 };

  let linhaOrcamento: any;

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/linha-orcamentos+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/linha-orcamentos').as('postEntityRequest');
    cy.intercept('DELETE', '/api/linha-orcamentos/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (linhaOrcamento) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/linha-orcamentos/${linhaOrcamento.id}`,
      }).then(() => {
        linhaOrcamento = undefined;
      });
    }
  });

  it('LinhaOrcamentos menu should load LinhaOrcamentos page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('linha-orcamento');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('LinhaOrcamento').should('exist');
    cy.url().should('match', linhaOrcamentoPageUrlPattern);
  });

  describe('LinhaOrcamento page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(linhaOrcamentoPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create LinhaOrcamento page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/linha-orcamento/new$'));
        cy.getEntityCreateUpdateHeading('LinhaOrcamento');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', linhaOrcamentoPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/linha-orcamentos',
          body: linhaOrcamentoSample,
        }).then(({ body }) => {
          linhaOrcamento = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/linha-orcamentos+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [linhaOrcamento],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(linhaOrcamentoPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details LinhaOrcamento page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('linhaOrcamento');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', linhaOrcamentoPageUrlPattern);
      });

      it('edit button click should load edit LinhaOrcamento page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('LinhaOrcamento');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', linhaOrcamentoPageUrlPattern);
      });

      it('last delete button click should delete instance of LinhaOrcamento', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('linhaOrcamento').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', linhaOrcamentoPageUrlPattern);

        linhaOrcamento = undefined;
      });
    });
  });

  describe('new LinhaOrcamento page', () => {
    beforeEach(() => {
      cy.visit(`${linhaOrcamentoPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('LinhaOrcamento');
    });

    it('should create an instance of LinhaOrcamento', () => {
      cy.get(`[data-cy="nome"]`).type('SCSI feed orchestration').should('have.value', 'SCSI feed orchestration');

      cy.get(`[data-cy="entidadeFinanceira"]`).type('rich Account').should('have.value', 'rich Account');

      cy.get(`[data-cy="rubrica"]`).type('Card').should('have.value', 'Card');

      cy.get(`[data-cy="descricaoRubrica"]`).type('Bermuda').should('have.value', 'Bermuda');

      cy.get(`[data-cy="valor"]`).type('18245').should('have.value', '18245');

      cy.get(`[data-cy="valorExecutado"]`).type('35224').should('have.value', '35224');

      cy.get(`[data-cy="saldo"]`).type('60169').should('have.value', '60169');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        linhaOrcamento = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', linhaOrcamentoPageUrlPattern);
    });
  });
});
