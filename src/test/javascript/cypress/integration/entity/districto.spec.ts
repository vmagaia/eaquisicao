import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Districto e2e test', () => {
  const districtoPageUrl = '/districto';
  const districtoPageUrlPattern = new RegExp('/districto(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const districtoSample = {};

  let districto: any;

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/districtos+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/districtos').as('postEntityRequest');
    cy.intercept('DELETE', '/api/districtos/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (districto) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/districtos/${districto.id}`,
      }).then(() => {
        districto = undefined;
      });
    }
  });

  it('Districtos menu should load Districtos page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('districto');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Districto').should('exist');
    cy.url().should('match', districtoPageUrlPattern);
  });

  describe('Districto page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(districtoPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Districto page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/districto/new$'));
        cy.getEntityCreateUpdateHeading('Districto');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', districtoPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/districtos',
          body: districtoSample,
        }).then(({ body }) => {
          districto = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/districtos+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [districto],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(districtoPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Districto page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('districto');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', districtoPageUrlPattern);
      });

      it('edit button click should load edit Districto page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Districto');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', districtoPageUrlPattern);
      });

      it('last delete button click should delete instance of Districto', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('districto').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', districtoPageUrlPattern);

        districto = undefined;
      });
    });
  });

  describe('new Districto page', () => {
    beforeEach(() => {
      cy.visit(`${districtoPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Districto');
    });

    it('should create an instance of Districto', () => {
      cy.get(`[data-cy="nome"]`).type('Investor Mobility').should('have.value', 'Investor Mobility');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        districto = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', districtoPageUrlPattern);
    });
  });
});
