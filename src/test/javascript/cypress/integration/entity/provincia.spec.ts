import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Provincia e2e test', () => {
  const provinciaPageUrl = '/provincia';
  const provinciaPageUrlPattern = new RegExp('/provincia(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const provinciaSample = {};

  let provincia: any;

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/provincias+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/provincias').as('postEntityRequest');
    cy.intercept('DELETE', '/api/provincias/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (provincia) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/provincias/${provincia.id}`,
      }).then(() => {
        provincia = undefined;
      });
    }
  });

  it('Provincias menu should load Provincias page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('provincia');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Provincia').should('exist');
    cy.url().should('match', provinciaPageUrlPattern);
  });

  describe('Provincia page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(provinciaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Provincia page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/provincia/new$'));
        cy.getEntityCreateUpdateHeading('Provincia');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', provinciaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/provincias',
          body: provinciaSample,
        }).then(({ body }) => {
          provincia = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/provincias+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [provincia],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(provinciaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Provincia page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('provincia');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', provinciaPageUrlPattern);
      });

      it('edit button click should load edit Provincia page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Provincia');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', provinciaPageUrlPattern);
      });

      it('last delete button click should delete instance of Provincia', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('provincia').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', provinciaPageUrlPattern);

        provincia = undefined;
      });
    });
  });

  describe('new Provincia page', () => {
    beforeEach(() => {
      cy.visit(`${provinciaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Provincia');
    });

    it('should create an instance of Provincia', () => {
      cy.get(`[data-cy="nome"]`).type('Croácia Product pixel').should('have.value', 'Croácia Product pixel');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        provincia = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', provinciaPageUrlPattern);
    });
  });
});
