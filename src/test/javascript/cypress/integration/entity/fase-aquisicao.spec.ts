import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('FaseAquisicao e2e test', () => {
  const faseAquisicaoPageUrl = '/fase-aquisicao';
  const faseAquisicaoPageUrlPattern = new RegExp('/fase-aquisicao(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const faseAquisicaoSample = { nome: 'deposit Division', diasExecucao: 54474, sequencia: 89255 };

  let faseAquisicao: any;

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/fase-aquisicaos+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/fase-aquisicaos').as('postEntityRequest');
    cy.intercept('DELETE', '/api/fase-aquisicaos/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (faseAquisicao) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/fase-aquisicaos/${faseAquisicao.id}`,
      }).then(() => {
        faseAquisicao = undefined;
      });
    }
  });

  it('FaseAquisicaos menu should load FaseAquisicaos page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('fase-aquisicao');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('FaseAquisicao').should('exist');
    cy.url().should('match', faseAquisicaoPageUrlPattern);
  });

  describe('FaseAquisicao page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(faseAquisicaoPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create FaseAquisicao page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/fase-aquisicao/new$'));
        cy.getEntityCreateUpdateHeading('FaseAquisicao');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', faseAquisicaoPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/fase-aquisicaos',
          body: faseAquisicaoSample,
        }).then(({ body }) => {
          faseAquisicao = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/fase-aquisicaos+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [faseAquisicao],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(faseAquisicaoPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details FaseAquisicao page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('faseAquisicao');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', faseAquisicaoPageUrlPattern);
      });

      it('edit button click should load edit FaseAquisicao page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('FaseAquisicao');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', faseAquisicaoPageUrlPattern);
      });

      it('last delete button click should delete instance of FaseAquisicao', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('faseAquisicao').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', faseAquisicaoPageUrlPattern);

        faseAquisicao = undefined;
      });
    });
  });

  describe('new FaseAquisicao page', () => {
    beforeEach(() => {
      cy.visit(`${faseAquisicaoPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('FaseAquisicao');
    });

    it('should create an instance of FaseAquisicao', () => {
      cy.get(`[data-cy="nome"]`).type('Loti panel').should('have.value', 'Loti panel');

      cy.get(`[data-cy="diasExecucao"]`).type('53005').should('have.value', '53005');

      cy.get(`[data-cy="sequencia"]`).type('1013').should('have.value', '1013');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        faseAquisicao = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', faseAquisicaoPageUrlPattern);
    });
  });
});
