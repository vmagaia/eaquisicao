import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('PropostaFinanceira e2e test', () => {
  const propostaFinanceiraPageUrl = '/proposta-financeira';
  const propostaFinanceiraPageUrlPattern = new RegExp('/proposta-financeira(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const propostaFinanceiraSample = {
    valor: 13247,
    descricao: '24/7',
    anexo: 'Li4vZmFrZS1kYXRhL2Jsb2IvaGlwc3Rlci5wbmc=',
    anexoContentType: 'unknown',
  };

  let propostaFinanceira: any;

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/proposta-financeiras+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/proposta-financeiras').as('postEntityRequest');
    cy.intercept('DELETE', '/api/proposta-financeiras/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (propostaFinanceira) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/proposta-financeiras/${propostaFinanceira.id}`,
      }).then(() => {
        propostaFinanceira = undefined;
      });
    }
  });

  it('PropostaFinanceiras menu should load PropostaFinanceiras page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('proposta-financeira');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('PropostaFinanceira').should('exist');
    cy.url().should('match', propostaFinanceiraPageUrlPattern);
  });

  describe('PropostaFinanceira page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(propostaFinanceiraPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create PropostaFinanceira page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/proposta-financeira/new$'));
        cy.getEntityCreateUpdateHeading('PropostaFinanceira');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', propostaFinanceiraPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/proposta-financeiras',
          body: propostaFinanceiraSample,
        }).then(({ body }) => {
          propostaFinanceira = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/proposta-financeiras+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [propostaFinanceira],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(propostaFinanceiraPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details PropostaFinanceira page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('propostaFinanceira');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', propostaFinanceiraPageUrlPattern);
      });

      it('edit button click should load edit PropostaFinanceira page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('PropostaFinanceira');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', propostaFinanceiraPageUrlPattern);
      });

      it('last delete button click should delete instance of PropostaFinanceira', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('propostaFinanceira').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', propostaFinanceiraPageUrlPattern);

        propostaFinanceira = undefined;
      });
    });
  });

  describe('new PropostaFinanceira page', () => {
    beforeEach(() => {
      cy.visit(`${propostaFinanceiraPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('PropostaFinanceira');
    });

    it('should create an instance of PropostaFinanceira', () => {
      cy.get(`[data-cy="valor"]`).type('98372').should('have.value', '98372');

      cy.get(`[data-cy="descricao"]`).type('Bicicleta feed').should('have.value', 'Bicicleta feed');

      cy.setFieldImageAsBytesOfEntity('anexo', 'integration-test.png', 'image/png');

      cy.get(`[data-cy="dataRegisto"]`).type('2022-01-24').should('have.value', '2022-01-24');

      cy.get(`[data-cy="estadoProposta"]`).select('Reprovada');

      // since cypress clicks submit too fast before the blob fields are validated
      cy.wait(200); // eslint-disable-line cypress/no-unnecessary-waiting
      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        propostaFinanceira = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', propostaFinanceiraPageUrlPattern);
    });
  });
});
