import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Fornecedor e2e test', () => {
  const fornecedorPageUrl = '/fornecedor';
  const fornecedorPageUrlPattern = new RegExp('/fornecedor(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const fornecedorSample = { nome: 'B2C', endereco: 'Frango Irlanda motivating', nuit: 95925, areaActividade: 'Teclado' };

  let fornecedor: any;

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/fornecedors+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/fornecedors').as('postEntityRequest');
    cy.intercept('DELETE', '/api/fornecedors/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (fornecedor) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/fornecedors/${fornecedor.id}`,
      }).then(() => {
        fornecedor = undefined;
      });
    }
  });

  it('Fornecedors menu should load Fornecedors page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('fornecedor');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Fornecedor').should('exist');
    cy.url().should('match', fornecedorPageUrlPattern);
  });

  describe('Fornecedor page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(fornecedorPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Fornecedor page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/fornecedor/new$'));
        cy.getEntityCreateUpdateHeading('Fornecedor');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fornecedorPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/fornecedors',
          body: fornecedorSample,
        }).then(({ body }) => {
          fornecedor = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/fornecedors+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [fornecedor],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(fornecedorPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Fornecedor page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('fornecedor');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fornecedorPageUrlPattern);
      });

      it('edit button click should load edit Fornecedor page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Fornecedor');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fornecedorPageUrlPattern);
      });

      it('last delete button click should delete instance of Fornecedor', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('fornecedor').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', fornecedorPageUrlPattern);

        fornecedor = undefined;
      });
    });
  });

  describe('new Fornecedor page', () => {
    beforeEach(() => {
      cy.visit(`${fornecedorPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Fornecedor');
    });

    it('should create an instance of Fornecedor', () => {
      cy.get(`[data-cy="nome"]`).type('Cuban generating').should('have.value', 'Cuban generating');

      cy.get(`[data-cy="endereco"]`).type('Atum Peixe').should('have.value', 'Atum Peixe');

      cy.get(`[data-cy="nuit"]`).type('10100').should('have.value', '10100');

      cy.get(`[data-cy="areaActividade"]`).type('Mountain Colon').should('have.value', 'Mountain Colon');

      cy.get(`[data-cy="saldo"]`).type('32534').should('have.value', '32534');

      cy.get(`[data-cy="dataRegisto"]`).type('2022-01-23').should('have.value', '2022-01-23');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        fornecedor = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', fornecedorPageUrlPattern);
    });
  });
});
