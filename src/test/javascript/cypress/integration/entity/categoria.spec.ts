import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Categoria e2e test', () => {
  const categoriaPageUrl = '/categoria';
  const categoriaPageUrlPattern = new RegExp('/categoria(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const categoriaSample = { nome: 'e-tailers Ergonómico' };

  let categoria: any;

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/categorias+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/categorias').as('postEntityRequest');
    cy.intercept('DELETE', '/api/categorias/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (categoria) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/categorias/${categoria.id}`,
      }).then(() => {
        categoria = undefined;
      });
    }
  });

  it('Categorias menu should load Categorias page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('categoria');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Categoria').should('exist');
    cy.url().should('match', categoriaPageUrlPattern);
  });

  describe('Categoria page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(categoriaPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Categoria page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/categoria/new$'));
        cy.getEntityCreateUpdateHeading('Categoria');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', categoriaPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/categorias',
          body: categoriaSample,
        }).then(({ body }) => {
          categoria = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/categorias+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [categoria],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(categoriaPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Categoria page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('categoria');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', categoriaPageUrlPattern);
      });

      it('edit button click should load edit Categoria page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Categoria');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', categoriaPageUrlPattern);
      });

      it('last delete button click should delete instance of Categoria', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('categoria').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', categoriaPageUrlPattern);

        categoria = undefined;
      });
    });
  });

  describe('new Categoria page', () => {
    beforeEach(() => {
      cy.visit(`${categoriaPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Categoria');
    });

    it('should create an instance of Categoria', () => {
      cy.get(`[data-cy="nome"]`).type('Electrónica input').should('have.value', 'Electrónica input');

      cy.get(`[data-cy="descricao"]`).type('Optimization Timor Berkshire').should('have.value', 'Optimization Timor Berkshire');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        categoria = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', categoriaPageUrlPattern);
    });
  });
});
