import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Aquisicao e2e test', () => {
  const aquisicaoPageUrl = '/aquisicao';
  const aquisicaoPageUrlPattern = new RegExp('/aquisicao(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'admin';
  const password = Cypress.env('E2E_PASSWORD') ?? 'admin';
  const aquisicaoSample = { valorPrevisto: 48676 };

  let aquisicao: any;

  before(() => {
    cy.window().then(win => {
      win.sessionStorage.clear();
    });
    cy.visit('');
    cy.login(username, password);
    cy.get(entityItemSelector).should('exist');
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/aquisicaos+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/aquisicaos').as('postEntityRequest');
    cy.intercept('DELETE', '/api/aquisicaos/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (aquisicao) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/aquisicaos/${aquisicao.id}`,
      }).then(() => {
        aquisicao = undefined;
      });
    }
  });

  it('Aquisicaos menu should load Aquisicaos page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('aquisicao');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Aquisicao').should('exist');
    cy.url().should('match', aquisicaoPageUrlPattern);
  });

  describe('Aquisicao page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(aquisicaoPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Aquisicao page', () => {
        cy.get(entityCreateButtonSelector).click({ force: true });
        cy.url().should('match', new RegExp('/aquisicao/new$'));
        cy.getEntityCreateUpdateHeading('Aquisicao');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', aquisicaoPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/aquisicaos',
          body: aquisicaoSample,
        }).then(({ body }) => {
          aquisicao = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/aquisicaos+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [aquisicao],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(aquisicaoPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Aquisicao page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('aquisicao');
        cy.get(entityDetailsBackButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', aquisicaoPageUrlPattern);
      });

      it('edit button click should load edit Aquisicao page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Aquisicao');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click({ force: true });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', aquisicaoPageUrlPattern);
      });

      it('last delete button click should delete instance of Aquisicao', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('aquisicao').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click({ force: true });
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', aquisicaoPageUrlPattern);

        aquisicao = undefined;
      });
    });
  });

  describe('new Aquisicao page', () => {
    beforeEach(() => {
      cy.visit(`${aquisicaoPageUrl}`);
      cy.get(entityCreateButtonSelector).click({ force: true });
      cy.getEntityCreateUpdateHeading('Aquisicao');
    });

    it('should create an instance of Aquisicao', () => {
      cy.get(`[data-cy="descricao"]`).type('Marca visualize').should('have.value', 'Marca visualize');

      cy.get(`[data-cy="valorPrevisto"]`).type('919').should('have.value', '919');

      cy.get(`[data-cy="dataCriacao"]`).type('2022-01-24T02:40').should('have.value', '2022-01-24T02:40');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        aquisicao = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', aquisicaoPageUrlPattern);
    });
  });
});
