package mz.co.eaquisicao.domain;

import static org.assertj.core.api.Assertions.assertThat;

import mz.co.eaquisicao.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DistrictoTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Districto.class);
        Districto districto1 = new Districto();
        districto1.setId(1L);
        Districto districto2 = new Districto();
        districto2.setId(districto1.getId());
        assertThat(districto1).isEqualTo(districto2);
        districto2.setId(2L);
        assertThat(districto1).isNotEqualTo(districto2);
        districto1.setId(null);
        assertThat(districto1).isNotEqualTo(districto2);
    }
}
