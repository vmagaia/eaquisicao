package mz.co.eaquisicao.domain;

import static org.assertj.core.api.Assertions.assertThat;

import mz.co.eaquisicao.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AquisicaoTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Aquisicao.class);
        Aquisicao aquisicao1 = new Aquisicao();
        aquisicao1.setId(1L);
        Aquisicao aquisicao2 = new Aquisicao();
        aquisicao2.setId(aquisicao1.getId());
        assertThat(aquisicao1).isEqualTo(aquisicao2);
        aquisicao2.setId(2L);
        assertThat(aquisicao1).isNotEqualTo(aquisicao2);
        aquisicao1.setId(null);
        assertThat(aquisicao1).isNotEqualTo(aquisicao2);
    }
}
