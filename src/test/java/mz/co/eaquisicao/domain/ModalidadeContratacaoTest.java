package mz.co.eaquisicao.domain;

import static org.assertj.core.api.Assertions.assertThat;

import mz.co.eaquisicao.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ModalidadeContratacaoTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModalidadeContratacao.class);
        ModalidadeContratacao modalidadeContratacao1 = new ModalidadeContratacao();
        modalidadeContratacao1.setId(1L);
        ModalidadeContratacao modalidadeContratacao2 = new ModalidadeContratacao();
        modalidadeContratacao2.setId(modalidadeContratacao1.getId());
        assertThat(modalidadeContratacao1).isEqualTo(modalidadeContratacao2);
        modalidadeContratacao2.setId(2L);
        assertThat(modalidadeContratacao1).isNotEqualTo(modalidadeContratacao2);
        modalidadeContratacao1.setId(null);
        assertThat(modalidadeContratacao1).isNotEqualTo(modalidadeContratacao2);
    }
}
