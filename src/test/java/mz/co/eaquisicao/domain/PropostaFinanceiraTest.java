package mz.co.eaquisicao.domain;

import static org.assertj.core.api.Assertions.assertThat;

import mz.co.eaquisicao.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PropostaFinanceiraTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PropostaFinanceira.class);
        PropostaFinanceira propostaFinanceira1 = new PropostaFinanceira();
        propostaFinanceira1.setId(1L);
        PropostaFinanceira propostaFinanceira2 = new PropostaFinanceira();
        propostaFinanceira2.setId(propostaFinanceira1.getId());
        assertThat(propostaFinanceira1).isEqualTo(propostaFinanceira2);
        propostaFinanceira2.setId(2L);
        assertThat(propostaFinanceira1).isNotEqualTo(propostaFinanceira2);
        propostaFinanceira1.setId(null);
        assertThat(propostaFinanceira1).isNotEqualTo(propostaFinanceira2);
    }
}
