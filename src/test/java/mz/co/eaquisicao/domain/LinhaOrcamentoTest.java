package mz.co.eaquisicao.domain;

import static org.assertj.core.api.Assertions.assertThat;

import mz.co.eaquisicao.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class LinhaOrcamentoTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LinhaOrcamento.class);
        LinhaOrcamento linhaOrcamento1 = new LinhaOrcamento();
        linhaOrcamento1.setId(1L);
        LinhaOrcamento linhaOrcamento2 = new LinhaOrcamento();
        linhaOrcamento2.setId(linhaOrcamento1.getId());
        assertThat(linhaOrcamento1).isEqualTo(linhaOrcamento2);
        linhaOrcamento2.setId(2L);
        assertThat(linhaOrcamento1).isNotEqualTo(linhaOrcamento2);
        linhaOrcamento1.setId(null);
        assertThat(linhaOrcamento1).isNotEqualTo(linhaOrcamento2);
    }
}
