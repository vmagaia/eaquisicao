package mz.co.eaquisicao.domain;

import static org.assertj.core.api.Assertions.assertThat;

import mz.co.eaquisicao.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class FaseAquisicaoTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FaseAquisicao.class);
        FaseAquisicao faseAquisicao1 = new FaseAquisicao();
        faseAquisicao1.setId(1L);
        FaseAquisicao faseAquisicao2 = new FaseAquisicao();
        faseAquisicao2.setId(faseAquisicao1.getId());
        assertThat(faseAquisicao1).isEqualTo(faseAquisicao2);
        faseAquisicao2.setId(2L);
        assertThat(faseAquisicao1).isNotEqualTo(faseAquisicao2);
        faseAquisicao1.setId(null);
        assertThat(faseAquisicao1).isNotEqualTo(faseAquisicao2);
    }
}
