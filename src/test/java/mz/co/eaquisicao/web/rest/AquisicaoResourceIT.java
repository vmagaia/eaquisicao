package mz.co.eaquisicao.web.rest;

import static mz.co.eaquisicao.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mz.co.eaquisicao.IntegrationTest;
import mz.co.eaquisicao.domain.Aquisicao;
import mz.co.eaquisicao.domain.Categoria;
import mz.co.eaquisicao.domain.Contrato;
import mz.co.eaquisicao.domain.FaseAquisicao;
import mz.co.eaquisicao.domain.LinhaOrcamento;
import mz.co.eaquisicao.domain.ModalidadeContratacao;
import mz.co.eaquisicao.domain.PropostaFinanceira;
import mz.co.eaquisicao.repository.AquisicaoRepository;
import mz.co.eaquisicao.service.criteria.AquisicaoCriteria;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AquisicaoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AquisicaoResourceIT {

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_VALOR_PREVISTO = new BigDecimal(0);
    private static final BigDecimal UPDATED_VALOR_PREVISTO = new BigDecimal(1);
    private static final BigDecimal SMALLER_VALOR_PREVISTO = new BigDecimal(0 - 1);

    private static final Instant DEFAULT_DATA_CRIACAO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA_CRIACAO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/aquisicaos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AquisicaoRepository aquisicaoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAquisicaoMockMvc;

    private Aquisicao aquisicao;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Aquisicao createEntity(EntityManager em) {
        Aquisicao aquisicao = new Aquisicao()
            .descricao(DEFAULT_DESCRICAO)
            .valorPrevisto(DEFAULT_VALOR_PREVISTO)
            .dataCriacao(DEFAULT_DATA_CRIACAO);
        return aquisicao;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Aquisicao createUpdatedEntity(EntityManager em) {
        Aquisicao aquisicao = new Aquisicao()
            .descricao(UPDATED_DESCRICAO)
            .valorPrevisto(UPDATED_VALOR_PREVISTO)
            .dataCriacao(UPDATED_DATA_CRIACAO);
        return aquisicao;
    }

    @BeforeEach
    public void initTest() {
        aquisicao = createEntity(em);
    }

    @Test
    @Transactional
    void createAquisicao() throws Exception {
        int databaseSizeBeforeCreate = aquisicaoRepository.findAll().size();
        // Create the Aquisicao
        restAquisicaoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(aquisicao)))
            .andExpect(status().isCreated());

        // Validate the Aquisicao in the database
        List<Aquisicao> aquisicaoList = aquisicaoRepository.findAll();
        assertThat(aquisicaoList).hasSize(databaseSizeBeforeCreate + 1);
        Aquisicao testAquisicao = aquisicaoList.get(aquisicaoList.size() - 1);
        assertThat(testAquisicao.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
        assertThat(testAquisicao.getValorPrevisto()).isEqualByComparingTo(DEFAULT_VALOR_PREVISTO);
        assertThat(testAquisicao.getDataCriacao()).isEqualTo(DEFAULT_DATA_CRIACAO);
    }

    @Test
    @Transactional
    void createAquisicaoWithExistingId() throws Exception {
        // Create the Aquisicao with an existing ID
        aquisicao.setId(1L);

        int databaseSizeBeforeCreate = aquisicaoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAquisicaoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(aquisicao)))
            .andExpect(status().isBadRequest());

        // Validate the Aquisicao in the database
        List<Aquisicao> aquisicaoList = aquisicaoRepository.findAll();
        assertThat(aquisicaoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkValorPrevistoIsRequired() throws Exception {
        int databaseSizeBeforeTest = aquisicaoRepository.findAll().size();
        // set the field null
        aquisicao.setValorPrevisto(null);

        // Create the Aquisicao, which fails.

        restAquisicaoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(aquisicao)))
            .andExpect(status().isBadRequest());

        List<Aquisicao> aquisicaoList = aquisicaoRepository.findAll();
        assertThat(aquisicaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllAquisicaos() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList
        restAquisicaoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aquisicao.getId().intValue())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)))
            .andExpect(jsonPath("$.[*].valorPrevisto").value(hasItem(sameNumber(DEFAULT_VALOR_PREVISTO))))
            .andExpect(jsonPath("$.[*].dataCriacao").value(hasItem(DEFAULT_DATA_CRIACAO.toString())));
    }

    @Test
    @Transactional
    void getAquisicao() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get the aquisicao
        restAquisicaoMockMvc
            .perform(get(ENTITY_API_URL_ID, aquisicao.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(aquisicao.getId().intValue()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO))
            .andExpect(jsonPath("$.valorPrevisto").value(sameNumber(DEFAULT_VALOR_PREVISTO)))
            .andExpect(jsonPath("$.dataCriacao").value(DEFAULT_DATA_CRIACAO.toString()));
    }

    @Test
    @Transactional
    void getAquisicaosByIdFiltering() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        Long id = aquisicao.getId();

        defaultAquisicaoShouldBeFound("id.equals=" + id);
        defaultAquisicaoShouldNotBeFound("id.notEquals=" + id);

        defaultAquisicaoShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultAquisicaoShouldNotBeFound("id.greaterThan=" + id);

        defaultAquisicaoShouldBeFound("id.lessThanOrEqual=" + id);
        defaultAquisicaoShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllAquisicaosByDescricaoIsEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where descricao equals to DEFAULT_DESCRICAO
        defaultAquisicaoShouldBeFound("descricao.equals=" + DEFAULT_DESCRICAO);

        // Get all the aquisicaoList where descricao equals to UPDATED_DESCRICAO
        defaultAquisicaoShouldNotBeFound("descricao.equals=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByDescricaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where descricao not equals to DEFAULT_DESCRICAO
        defaultAquisicaoShouldNotBeFound("descricao.notEquals=" + DEFAULT_DESCRICAO);

        // Get all the aquisicaoList where descricao not equals to UPDATED_DESCRICAO
        defaultAquisicaoShouldBeFound("descricao.notEquals=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByDescricaoIsInShouldWork() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where descricao in DEFAULT_DESCRICAO or UPDATED_DESCRICAO
        defaultAquisicaoShouldBeFound("descricao.in=" + DEFAULT_DESCRICAO + "," + UPDATED_DESCRICAO);

        // Get all the aquisicaoList where descricao equals to UPDATED_DESCRICAO
        defaultAquisicaoShouldNotBeFound("descricao.in=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByDescricaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where descricao is not null
        defaultAquisicaoShouldBeFound("descricao.specified=true");

        // Get all the aquisicaoList where descricao is null
        defaultAquisicaoShouldNotBeFound("descricao.specified=false");
    }

    @Test
    @Transactional
    void getAllAquisicaosByDescricaoContainsSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where descricao contains DEFAULT_DESCRICAO
        defaultAquisicaoShouldBeFound("descricao.contains=" + DEFAULT_DESCRICAO);

        // Get all the aquisicaoList where descricao contains UPDATED_DESCRICAO
        defaultAquisicaoShouldNotBeFound("descricao.contains=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByDescricaoNotContainsSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where descricao does not contain DEFAULT_DESCRICAO
        defaultAquisicaoShouldNotBeFound("descricao.doesNotContain=" + DEFAULT_DESCRICAO);

        // Get all the aquisicaoList where descricao does not contain UPDATED_DESCRICAO
        defaultAquisicaoShouldBeFound("descricao.doesNotContain=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByValorPrevistoIsEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where valorPrevisto equals to DEFAULT_VALOR_PREVISTO
        defaultAquisicaoShouldBeFound("valorPrevisto.equals=" + DEFAULT_VALOR_PREVISTO);

        // Get all the aquisicaoList where valorPrevisto equals to UPDATED_VALOR_PREVISTO
        defaultAquisicaoShouldNotBeFound("valorPrevisto.equals=" + UPDATED_VALOR_PREVISTO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByValorPrevistoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where valorPrevisto not equals to DEFAULT_VALOR_PREVISTO
        defaultAquisicaoShouldNotBeFound("valorPrevisto.notEquals=" + DEFAULT_VALOR_PREVISTO);

        // Get all the aquisicaoList where valorPrevisto not equals to UPDATED_VALOR_PREVISTO
        defaultAquisicaoShouldBeFound("valorPrevisto.notEquals=" + UPDATED_VALOR_PREVISTO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByValorPrevistoIsInShouldWork() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where valorPrevisto in DEFAULT_VALOR_PREVISTO or UPDATED_VALOR_PREVISTO
        defaultAquisicaoShouldBeFound("valorPrevisto.in=" + DEFAULT_VALOR_PREVISTO + "," + UPDATED_VALOR_PREVISTO);

        // Get all the aquisicaoList where valorPrevisto equals to UPDATED_VALOR_PREVISTO
        defaultAquisicaoShouldNotBeFound("valorPrevisto.in=" + UPDATED_VALOR_PREVISTO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByValorPrevistoIsNullOrNotNull() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where valorPrevisto is not null
        defaultAquisicaoShouldBeFound("valorPrevisto.specified=true");

        // Get all the aquisicaoList where valorPrevisto is null
        defaultAquisicaoShouldNotBeFound("valorPrevisto.specified=false");
    }

    @Test
    @Transactional
    void getAllAquisicaosByValorPrevistoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where valorPrevisto is greater than or equal to DEFAULT_VALOR_PREVISTO
        defaultAquisicaoShouldBeFound("valorPrevisto.greaterThanOrEqual=" + DEFAULT_VALOR_PREVISTO);

        // Get all the aquisicaoList where valorPrevisto is greater than or equal to UPDATED_VALOR_PREVISTO
        defaultAquisicaoShouldNotBeFound("valorPrevisto.greaterThanOrEqual=" + UPDATED_VALOR_PREVISTO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByValorPrevistoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where valorPrevisto is less than or equal to DEFAULT_VALOR_PREVISTO
        defaultAquisicaoShouldBeFound("valorPrevisto.lessThanOrEqual=" + DEFAULT_VALOR_PREVISTO);

        // Get all the aquisicaoList where valorPrevisto is less than or equal to SMALLER_VALOR_PREVISTO
        defaultAquisicaoShouldNotBeFound("valorPrevisto.lessThanOrEqual=" + SMALLER_VALOR_PREVISTO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByValorPrevistoIsLessThanSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where valorPrevisto is less than DEFAULT_VALOR_PREVISTO
        defaultAquisicaoShouldNotBeFound("valorPrevisto.lessThan=" + DEFAULT_VALOR_PREVISTO);

        // Get all the aquisicaoList where valorPrevisto is less than UPDATED_VALOR_PREVISTO
        defaultAquisicaoShouldBeFound("valorPrevisto.lessThan=" + UPDATED_VALOR_PREVISTO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByValorPrevistoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where valorPrevisto is greater than DEFAULT_VALOR_PREVISTO
        defaultAquisicaoShouldNotBeFound("valorPrevisto.greaterThan=" + DEFAULT_VALOR_PREVISTO);

        // Get all the aquisicaoList where valorPrevisto is greater than SMALLER_VALOR_PREVISTO
        defaultAquisicaoShouldBeFound("valorPrevisto.greaterThan=" + SMALLER_VALOR_PREVISTO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByDataCriacaoIsEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where dataCriacao equals to DEFAULT_DATA_CRIACAO
        defaultAquisicaoShouldBeFound("dataCriacao.equals=" + DEFAULT_DATA_CRIACAO);

        // Get all the aquisicaoList where dataCriacao equals to UPDATED_DATA_CRIACAO
        defaultAquisicaoShouldNotBeFound("dataCriacao.equals=" + UPDATED_DATA_CRIACAO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByDataCriacaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where dataCriacao not equals to DEFAULT_DATA_CRIACAO
        defaultAquisicaoShouldNotBeFound("dataCriacao.notEquals=" + DEFAULT_DATA_CRIACAO);

        // Get all the aquisicaoList where dataCriacao not equals to UPDATED_DATA_CRIACAO
        defaultAquisicaoShouldBeFound("dataCriacao.notEquals=" + UPDATED_DATA_CRIACAO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByDataCriacaoIsInShouldWork() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where dataCriacao in DEFAULT_DATA_CRIACAO or UPDATED_DATA_CRIACAO
        defaultAquisicaoShouldBeFound("dataCriacao.in=" + DEFAULT_DATA_CRIACAO + "," + UPDATED_DATA_CRIACAO);

        // Get all the aquisicaoList where dataCriacao equals to UPDATED_DATA_CRIACAO
        defaultAquisicaoShouldNotBeFound("dataCriacao.in=" + UPDATED_DATA_CRIACAO);
    }

    @Test
    @Transactional
    void getAllAquisicaosByDataCriacaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        // Get all the aquisicaoList where dataCriacao is not null
        defaultAquisicaoShouldBeFound("dataCriacao.specified=true");

        // Get all the aquisicaoList where dataCriacao is null
        defaultAquisicaoShouldNotBeFound("dataCriacao.specified=false");
    }

    @Test
    @Transactional
    void getAllAquisicaosByContratoIsEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);
        Contrato contrato;
        if (TestUtil.findAll(em, Contrato.class).isEmpty()) {
            contrato = ContratoResourceIT.createEntity(em);
            em.persist(contrato);
            em.flush();
        } else {
            contrato = TestUtil.findAll(em, Contrato.class).get(0);
        }
        em.persist(contrato);
        em.flush();
        aquisicao.setContrato(contrato);
        aquisicaoRepository.saveAndFlush(aquisicao);
        Long contratoId = contrato.getId();

        // Get all the aquisicaoList where contrato equals to contratoId
        defaultAquisicaoShouldBeFound("contratoId.equals=" + contratoId);

        // Get all the aquisicaoList where contrato equals to (contratoId + 1)
        defaultAquisicaoShouldNotBeFound("contratoId.equals=" + (contratoId + 1));
    }

    @Test
    @Transactional
    void getAllAquisicaosByPropostaFinanceiraIsEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);
        PropostaFinanceira propostaFinanceira;
        if (TestUtil.findAll(em, PropostaFinanceira.class).isEmpty()) {
            propostaFinanceira = PropostaFinanceiraResourceIT.createEntity(em);
            em.persist(propostaFinanceira);
            em.flush();
        } else {
            propostaFinanceira = TestUtil.findAll(em, PropostaFinanceira.class).get(0);
        }
        em.persist(propostaFinanceira);
        em.flush();
        aquisicao.addPropostaFinanceira(propostaFinanceira);
        aquisicaoRepository.saveAndFlush(aquisicao);
        Long propostaFinanceiraId = propostaFinanceira.getId();

        // Get all the aquisicaoList where propostaFinanceira equals to propostaFinanceiraId
        defaultAquisicaoShouldBeFound("propostaFinanceiraId.equals=" + propostaFinanceiraId);

        // Get all the aquisicaoList where propostaFinanceira equals to (propostaFinanceiraId + 1)
        defaultAquisicaoShouldNotBeFound("propostaFinanceiraId.equals=" + (propostaFinanceiraId + 1));
    }

    @Test
    @Transactional
    void getAllAquisicaosByCategoriaIsEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);
        Categoria categoria;
        if (TestUtil.findAll(em, Categoria.class).isEmpty()) {
            categoria = CategoriaResourceIT.createEntity(em);
            em.persist(categoria);
            em.flush();
        } else {
            categoria = TestUtil.findAll(em, Categoria.class).get(0);
        }
        em.persist(categoria);
        em.flush();
        aquisicao.setCategoria(categoria);
        aquisicaoRepository.saveAndFlush(aquisicao);
        Long categoriaId = categoria.getId();

        // Get all the aquisicaoList where categoria equals to categoriaId
        defaultAquisicaoShouldBeFound("categoriaId.equals=" + categoriaId);

        // Get all the aquisicaoList where categoria equals to (categoriaId + 1)
        defaultAquisicaoShouldNotBeFound("categoriaId.equals=" + (categoriaId + 1));
    }

    @Test
    @Transactional
    void getAllAquisicaosByModalidadeContratacaoIsEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);
        ModalidadeContratacao modalidadeContratacao;
        if (TestUtil.findAll(em, ModalidadeContratacao.class).isEmpty()) {
            modalidadeContratacao = ModalidadeContratacaoResourceIT.createEntity(em);
            em.persist(modalidadeContratacao);
            em.flush();
        } else {
            modalidadeContratacao = TestUtil.findAll(em, ModalidadeContratacao.class).get(0);
        }
        em.persist(modalidadeContratacao);
        em.flush();
        aquisicao.setModalidadeContratacao(modalidadeContratacao);
        aquisicaoRepository.saveAndFlush(aquisicao);
        Long modalidadeContratacaoId = modalidadeContratacao.getId();

        // Get all the aquisicaoList where modalidadeContratacao equals to modalidadeContratacaoId
        defaultAquisicaoShouldBeFound("modalidadeContratacaoId.equals=" + modalidadeContratacaoId);

        // Get all the aquisicaoList where modalidadeContratacao equals to (modalidadeContratacaoId + 1)
        defaultAquisicaoShouldNotBeFound("modalidadeContratacaoId.equals=" + (modalidadeContratacaoId + 1));
    }

    @Test
    @Transactional
    void getAllAquisicaosByFaseAquisicaoIsEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);
        FaseAquisicao faseAquisicao;
        if (TestUtil.findAll(em, FaseAquisicao.class).isEmpty()) {
            faseAquisicao = FaseAquisicaoResourceIT.createEntity(em);
            em.persist(faseAquisicao);
            em.flush();
        } else {
            faseAquisicao = TestUtil.findAll(em, FaseAquisicao.class).get(0);
        }
        em.persist(faseAquisicao);
        em.flush();
        aquisicao.setFaseAquisicao(faseAquisicao);
        aquisicaoRepository.saveAndFlush(aquisicao);
        Long faseAquisicaoId = faseAquisicao.getId();

        // Get all the aquisicaoList where faseAquisicao equals to faseAquisicaoId
        defaultAquisicaoShouldBeFound("faseAquisicaoId.equals=" + faseAquisicaoId);

        // Get all the aquisicaoList where faseAquisicao equals to (faseAquisicaoId + 1)
        defaultAquisicaoShouldNotBeFound("faseAquisicaoId.equals=" + (faseAquisicaoId + 1));
    }

    @Test
    @Transactional
    void getAllAquisicaosByLinhaOrcamentoIsEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);
        LinhaOrcamento linhaOrcamento;
        if (TestUtil.findAll(em, LinhaOrcamento.class).isEmpty()) {
            linhaOrcamento = LinhaOrcamentoResourceIT.createEntity(em);
            em.persist(linhaOrcamento);
            em.flush();
        } else {
            linhaOrcamento = TestUtil.findAll(em, LinhaOrcamento.class).get(0);
        }
        em.persist(linhaOrcamento);
        em.flush();
        aquisicao.setLinhaOrcamento(linhaOrcamento);
        aquisicaoRepository.saveAndFlush(aquisicao);
        Long linhaOrcamentoId = linhaOrcamento.getId();

        // Get all the aquisicaoList where linhaOrcamento equals to linhaOrcamentoId
        defaultAquisicaoShouldBeFound("linhaOrcamentoId.equals=" + linhaOrcamentoId);

        // Get all the aquisicaoList where linhaOrcamento equals to (linhaOrcamentoId + 1)
        defaultAquisicaoShouldNotBeFound("linhaOrcamentoId.equals=" + (linhaOrcamentoId + 1));
    }

    @Test
    @Transactional
    void getAllAquisicaosByContratoIsEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);
        Contrato contrato;
        if (TestUtil.findAll(em, Contrato.class).isEmpty()) {
            contrato = ContratoResourceIT.createEntity(em);
            em.persist(contrato);
            em.flush();
        } else {
            contrato = TestUtil.findAll(em, Contrato.class).get(0);
        }
        em.persist(contrato);
        em.flush();
        aquisicao.setContrato(contrato);
        aquisicaoRepository.saveAndFlush(aquisicao);
        Long contratoId = contrato.getId();

        // Get all the aquisicaoList where contrato equals to contratoId
        defaultAquisicaoShouldBeFound("contratoId.equals=" + contratoId);

        // Get all the aquisicaoList where contrato equals to (contratoId + 1)
        defaultAquisicaoShouldNotBeFound("contratoId.equals=" + (contratoId + 1));
    }

    @Test
    @Transactional
    void getAllAquisicaosByPropostaFinanceiraIsEqualToSomething() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);
        PropostaFinanceira propostaFinanceira;
        if (TestUtil.findAll(em, PropostaFinanceira.class).isEmpty()) {
            propostaFinanceira = PropostaFinanceiraResourceIT.createEntity(em);
            em.persist(propostaFinanceira);
            em.flush();
        } else {
            propostaFinanceira = TestUtil.findAll(em, PropostaFinanceira.class).get(0);
        }
        em.persist(propostaFinanceira);
        em.flush();
        aquisicao.addPropostaFinanceira(propostaFinanceira);
        aquisicaoRepository.saveAndFlush(aquisicao);
        Long propostaFinanceiraId = propostaFinanceira.getId();

        // Get all the aquisicaoList where propostaFinanceira equals to propostaFinanceiraId
        defaultAquisicaoShouldBeFound("propostaFinanceiraId.equals=" + propostaFinanceiraId);

        // Get all the aquisicaoList where propostaFinanceira equals to (propostaFinanceiraId + 1)
        defaultAquisicaoShouldNotBeFound("propostaFinanceiraId.equals=" + (propostaFinanceiraId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAquisicaoShouldBeFound(String filter) throws Exception {
        restAquisicaoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aquisicao.getId().intValue())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)))
            .andExpect(jsonPath("$.[*].valorPrevisto").value(hasItem(sameNumber(DEFAULT_VALOR_PREVISTO))))
            .andExpect(jsonPath("$.[*].dataCriacao").value(hasItem(DEFAULT_DATA_CRIACAO.toString())));

        // Check, that the count call also returns 1
        restAquisicaoMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAquisicaoShouldNotBeFound(String filter) throws Exception {
        restAquisicaoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAquisicaoMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingAquisicao() throws Exception {
        // Get the aquisicao
        restAquisicaoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewAquisicao() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        int databaseSizeBeforeUpdate = aquisicaoRepository.findAll().size();

        // Update the aquisicao
        Aquisicao updatedAquisicao = aquisicaoRepository.findById(aquisicao.getId()).get();
        // Disconnect from session so that the updates on updatedAquisicao are not directly saved in db
        em.detach(updatedAquisicao);
        updatedAquisicao.descricao(UPDATED_DESCRICAO).valorPrevisto(UPDATED_VALOR_PREVISTO).dataCriacao(UPDATED_DATA_CRIACAO);

        restAquisicaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedAquisicao.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedAquisicao))
            )
            .andExpect(status().isOk());

        // Validate the Aquisicao in the database
        List<Aquisicao> aquisicaoList = aquisicaoRepository.findAll();
        assertThat(aquisicaoList).hasSize(databaseSizeBeforeUpdate);
        Aquisicao testAquisicao = aquisicaoList.get(aquisicaoList.size() - 1);
        assertThat(testAquisicao.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testAquisicao.getValorPrevisto()).isEqualTo(UPDATED_VALOR_PREVISTO);
        assertThat(testAquisicao.getDataCriacao()).isEqualTo(UPDATED_DATA_CRIACAO);
    }

    @Test
    @Transactional
    void putNonExistingAquisicao() throws Exception {
        int databaseSizeBeforeUpdate = aquisicaoRepository.findAll().size();
        aquisicao.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAquisicaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, aquisicao.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(aquisicao))
            )
            .andExpect(status().isBadRequest());

        // Validate the Aquisicao in the database
        List<Aquisicao> aquisicaoList = aquisicaoRepository.findAll();
        assertThat(aquisicaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAquisicao() throws Exception {
        int databaseSizeBeforeUpdate = aquisicaoRepository.findAll().size();
        aquisicao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAquisicaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(aquisicao))
            )
            .andExpect(status().isBadRequest());

        // Validate the Aquisicao in the database
        List<Aquisicao> aquisicaoList = aquisicaoRepository.findAll();
        assertThat(aquisicaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAquisicao() throws Exception {
        int databaseSizeBeforeUpdate = aquisicaoRepository.findAll().size();
        aquisicao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAquisicaoMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(aquisicao)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Aquisicao in the database
        List<Aquisicao> aquisicaoList = aquisicaoRepository.findAll();
        assertThat(aquisicaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAquisicaoWithPatch() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        int databaseSizeBeforeUpdate = aquisicaoRepository.findAll().size();

        // Update the aquisicao using partial update
        Aquisicao partialUpdatedAquisicao = new Aquisicao();
        partialUpdatedAquisicao.setId(aquisicao.getId());

        partialUpdatedAquisicao.valorPrevisto(UPDATED_VALOR_PREVISTO);

        restAquisicaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAquisicao.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAquisicao))
            )
            .andExpect(status().isOk());

        // Validate the Aquisicao in the database
        List<Aquisicao> aquisicaoList = aquisicaoRepository.findAll();
        assertThat(aquisicaoList).hasSize(databaseSizeBeforeUpdate);
        Aquisicao testAquisicao = aquisicaoList.get(aquisicaoList.size() - 1);
        assertThat(testAquisicao.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
        assertThat(testAquisicao.getValorPrevisto()).isEqualByComparingTo(UPDATED_VALOR_PREVISTO);
        assertThat(testAquisicao.getDataCriacao()).isEqualTo(DEFAULT_DATA_CRIACAO);
    }

    @Test
    @Transactional
    void fullUpdateAquisicaoWithPatch() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        int databaseSizeBeforeUpdate = aquisicaoRepository.findAll().size();

        // Update the aquisicao using partial update
        Aquisicao partialUpdatedAquisicao = new Aquisicao();
        partialUpdatedAquisicao.setId(aquisicao.getId());

        partialUpdatedAquisicao.descricao(UPDATED_DESCRICAO).valorPrevisto(UPDATED_VALOR_PREVISTO).dataCriacao(UPDATED_DATA_CRIACAO);

        restAquisicaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAquisicao.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAquisicao))
            )
            .andExpect(status().isOk());

        // Validate the Aquisicao in the database
        List<Aquisicao> aquisicaoList = aquisicaoRepository.findAll();
        assertThat(aquisicaoList).hasSize(databaseSizeBeforeUpdate);
        Aquisicao testAquisicao = aquisicaoList.get(aquisicaoList.size() - 1);
        assertThat(testAquisicao.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testAquisicao.getValorPrevisto()).isEqualByComparingTo(UPDATED_VALOR_PREVISTO);
        assertThat(testAquisicao.getDataCriacao()).isEqualTo(UPDATED_DATA_CRIACAO);
    }

    @Test
    @Transactional
    void patchNonExistingAquisicao() throws Exception {
        int databaseSizeBeforeUpdate = aquisicaoRepository.findAll().size();
        aquisicao.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAquisicaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, aquisicao.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(aquisicao))
            )
            .andExpect(status().isBadRequest());

        // Validate the Aquisicao in the database
        List<Aquisicao> aquisicaoList = aquisicaoRepository.findAll();
        assertThat(aquisicaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAquisicao() throws Exception {
        int databaseSizeBeforeUpdate = aquisicaoRepository.findAll().size();
        aquisicao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAquisicaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(aquisicao))
            )
            .andExpect(status().isBadRequest());

        // Validate the Aquisicao in the database
        List<Aquisicao> aquisicaoList = aquisicaoRepository.findAll();
        assertThat(aquisicaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAquisicao() throws Exception {
        int databaseSizeBeforeUpdate = aquisicaoRepository.findAll().size();
        aquisicao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAquisicaoMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(aquisicao))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Aquisicao in the database
        List<Aquisicao> aquisicaoList = aquisicaoRepository.findAll();
        assertThat(aquisicaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAquisicao() throws Exception {
        // Initialize the database
        aquisicaoRepository.saveAndFlush(aquisicao);

        int databaseSizeBeforeDelete = aquisicaoRepository.findAll().size();

        // Delete the aquisicao
        restAquisicaoMockMvc
            .perform(delete(ENTITY_API_URL_ID, aquisicao.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Aquisicao> aquisicaoList = aquisicaoRepository.findAll();
        assertThat(aquisicaoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
