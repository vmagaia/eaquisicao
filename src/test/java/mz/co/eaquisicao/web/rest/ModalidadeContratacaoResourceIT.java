package mz.co.eaquisicao.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mz.co.eaquisicao.IntegrationTest;
import mz.co.eaquisicao.domain.ModalidadeContratacao;
import mz.co.eaquisicao.repository.ModalidadeContratacaoRepository;
import mz.co.eaquisicao.service.criteria.ModalidadeContratacaoCriteria;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ModalidadeContratacaoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ModalidadeContratacaoResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_CATEGORIA = "AAAAAAAAAA";
    private static final String UPDATED_CATEGORIA = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/modalidade-contratacaos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ModalidadeContratacaoRepository modalidadeContratacaoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restModalidadeContratacaoMockMvc;

    private ModalidadeContratacao modalidadeContratacao;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModalidadeContratacao createEntity(EntityManager em) {
        ModalidadeContratacao modalidadeContratacao = new ModalidadeContratacao()
            .nome(DEFAULT_NOME)
            .categoria(DEFAULT_CATEGORIA)
            .descricao(DEFAULT_DESCRICAO);
        return modalidadeContratacao;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModalidadeContratacao createUpdatedEntity(EntityManager em) {
        ModalidadeContratacao modalidadeContratacao = new ModalidadeContratacao()
            .nome(UPDATED_NOME)
            .categoria(UPDATED_CATEGORIA)
            .descricao(UPDATED_DESCRICAO);
        return modalidadeContratacao;
    }

    @BeforeEach
    public void initTest() {
        modalidadeContratacao = createEntity(em);
    }

    @Test
    @Transactional
    void createModalidadeContratacao() throws Exception {
        int databaseSizeBeforeCreate = modalidadeContratacaoRepository.findAll().size();
        // Create the ModalidadeContratacao
        restModalidadeContratacaoMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modalidadeContratacao))
            )
            .andExpect(status().isCreated());

        // Validate the ModalidadeContratacao in the database
        List<ModalidadeContratacao> modalidadeContratacaoList = modalidadeContratacaoRepository.findAll();
        assertThat(modalidadeContratacaoList).hasSize(databaseSizeBeforeCreate + 1);
        ModalidadeContratacao testModalidadeContratacao = modalidadeContratacaoList.get(modalidadeContratacaoList.size() - 1);
        assertThat(testModalidadeContratacao.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testModalidadeContratacao.getCategoria()).isEqualTo(DEFAULT_CATEGORIA);
        assertThat(testModalidadeContratacao.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void createModalidadeContratacaoWithExistingId() throws Exception {
        // Create the ModalidadeContratacao with an existing ID
        modalidadeContratacao.setId(1L);

        int databaseSizeBeforeCreate = modalidadeContratacaoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restModalidadeContratacaoMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modalidadeContratacao))
            )
            .andExpect(status().isBadRequest());

        // Validate the ModalidadeContratacao in the database
        List<ModalidadeContratacao> modalidadeContratacaoList = modalidadeContratacaoRepository.findAll();
        assertThat(modalidadeContratacaoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = modalidadeContratacaoRepository.findAll().size();
        // set the field null
        modalidadeContratacao.setNome(null);

        // Create the ModalidadeContratacao, which fails.

        restModalidadeContratacaoMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modalidadeContratacao))
            )
            .andExpect(status().isBadRequest());

        List<ModalidadeContratacao> modalidadeContratacaoList = modalidadeContratacaoRepository.findAll();
        assertThat(modalidadeContratacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaos() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList
        restModalidadeContratacaoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modalidadeContratacao.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].categoria").value(hasItem(DEFAULT_CATEGORIA)))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)));
    }

    @Test
    @Transactional
    void getModalidadeContratacao() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get the modalidadeContratacao
        restModalidadeContratacaoMockMvc
            .perform(get(ENTITY_API_URL_ID, modalidadeContratacao.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(modalidadeContratacao.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME))
            .andExpect(jsonPath("$.categoria").value(DEFAULT_CATEGORIA))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO));
    }

    @Test
    @Transactional
    void getModalidadeContratacaosByIdFiltering() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        Long id = modalidadeContratacao.getId();

        defaultModalidadeContratacaoShouldBeFound("id.equals=" + id);
        defaultModalidadeContratacaoShouldNotBeFound("id.notEquals=" + id);

        defaultModalidadeContratacaoShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultModalidadeContratacaoShouldNotBeFound("id.greaterThan=" + id);

        defaultModalidadeContratacaoShouldBeFound("id.lessThanOrEqual=" + id);
        defaultModalidadeContratacaoShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByNomeIsEqualToSomething() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where nome equals to DEFAULT_NOME
        defaultModalidadeContratacaoShouldBeFound("nome.equals=" + DEFAULT_NOME);

        // Get all the modalidadeContratacaoList where nome equals to UPDATED_NOME
        defaultModalidadeContratacaoShouldNotBeFound("nome.equals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByNomeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where nome not equals to DEFAULT_NOME
        defaultModalidadeContratacaoShouldNotBeFound("nome.notEquals=" + DEFAULT_NOME);

        // Get all the modalidadeContratacaoList where nome not equals to UPDATED_NOME
        defaultModalidadeContratacaoShouldBeFound("nome.notEquals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByNomeIsInShouldWork() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where nome in DEFAULT_NOME or UPDATED_NOME
        defaultModalidadeContratacaoShouldBeFound("nome.in=" + DEFAULT_NOME + "," + UPDATED_NOME);

        // Get all the modalidadeContratacaoList where nome equals to UPDATED_NOME
        defaultModalidadeContratacaoShouldNotBeFound("nome.in=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByNomeIsNullOrNotNull() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where nome is not null
        defaultModalidadeContratacaoShouldBeFound("nome.specified=true");

        // Get all the modalidadeContratacaoList where nome is null
        defaultModalidadeContratacaoShouldNotBeFound("nome.specified=false");
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByNomeContainsSomething() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where nome contains DEFAULT_NOME
        defaultModalidadeContratacaoShouldBeFound("nome.contains=" + DEFAULT_NOME);

        // Get all the modalidadeContratacaoList where nome contains UPDATED_NOME
        defaultModalidadeContratacaoShouldNotBeFound("nome.contains=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByNomeNotContainsSomething() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where nome does not contain DEFAULT_NOME
        defaultModalidadeContratacaoShouldNotBeFound("nome.doesNotContain=" + DEFAULT_NOME);

        // Get all the modalidadeContratacaoList where nome does not contain UPDATED_NOME
        defaultModalidadeContratacaoShouldBeFound("nome.doesNotContain=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByCategoriaIsEqualToSomething() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where categoria equals to DEFAULT_CATEGORIA
        defaultModalidadeContratacaoShouldBeFound("categoria.equals=" + DEFAULT_CATEGORIA);

        // Get all the modalidadeContratacaoList where categoria equals to UPDATED_CATEGORIA
        defaultModalidadeContratacaoShouldNotBeFound("categoria.equals=" + UPDATED_CATEGORIA);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByCategoriaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where categoria not equals to DEFAULT_CATEGORIA
        defaultModalidadeContratacaoShouldNotBeFound("categoria.notEquals=" + DEFAULT_CATEGORIA);

        // Get all the modalidadeContratacaoList where categoria not equals to UPDATED_CATEGORIA
        defaultModalidadeContratacaoShouldBeFound("categoria.notEquals=" + UPDATED_CATEGORIA);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByCategoriaIsInShouldWork() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where categoria in DEFAULT_CATEGORIA or UPDATED_CATEGORIA
        defaultModalidadeContratacaoShouldBeFound("categoria.in=" + DEFAULT_CATEGORIA + "," + UPDATED_CATEGORIA);

        // Get all the modalidadeContratacaoList where categoria equals to UPDATED_CATEGORIA
        defaultModalidadeContratacaoShouldNotBeFound("categoria.in=" + UPDATED_CATEGORIA);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByCategoriaIsNullOrNotNull() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where categoria is not null
        defaultModalidadeContratacaoShouldBeFound("categoria.specified=true");

        // Get all the modalidadeContratacaoList where categoria is null
        defaultModalidadeContratacaoShouldNotBeFound("categoria.specified=false");
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByCategoriaContainsSomething() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where categoria contains DEFAULT_CATEGORIA
        defaultModalidadeContratacaoShouldBeFound("categoria.contains=" + DEFAULT_CATEGORIA);

        // Get all the modalidadeContratacaoList where categoria contains UPDATED_CATEGORIA
        defaultModalidadeContratacaoShouldNotBeFound("categoria.contains=" + UPDATED_CATEGORIA);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByCategoriaNotContainsSomething() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where categoria does not contain DEFAULT_CATEGORIA
        defaultModalidadeContratacaoShouldNotBeFound("categoria.doesNotContain=" + DEFAULT_CATEGORIA);

        // Get all the modalidadeContratacaoList where categoria does not contain UPDATED_CATEGORIA
        defaultModalidadeContratacaoShouldBeFound("categoria.doesNotContain=" + UPDATED_CATEGORIA);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByDescricaoIsEqualToSomething() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where descricao equals to DEFAULT_DESCRICAO
        defaultModalidadeContratacaoShouldBeFound("descricao.equals=" + DEFAULT_DESCRICAO);

        // Get all the modalidadeContratacaoList where descricao equals to UPDATED_DESCRICAO
        defaultModalidadeContratacaoShouldNotBeFound("descricao.equals=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByDescricaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where descricao not equals to DEFAULT_DESCRICAO
        defaultModalidadeContratacaoShouldNotBeFound("descricao.notEquals=" + DEFAULT_DESCRICAO);

        // Get all the modalidadeContratacaoList where descricao not equals to UPDATED_DESCRICAO
        defaultModalidadeContratacaoShouldBeFound("descricao.notEquals=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByDescricaoIsInShouldWork() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where descricao in DEFAULT_DESCRICAO or UPDATED_DESCRICAO
        defaultModalidadeContratacaoShouldBeFound("descricao.in=" + DEFAULT_DESCRICAO + "," + UPDATED_DESCRICAO);

        // Get all the modalidadeContratacaoList where descricao equals to UPDATED_DESCRICAO
        defaultModalidadeContratacaoShouldNotBeFound("descricao.in=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByDescricaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where descricao is not null
        defaultModalidadeContratacaoShouldBeFound("descricao.specified=true");

        // Get all the modalidadeContratacaoList where descricao is null
        defaultModalidadeContratacaoShouldNotBeFound("descricao.specified=false");
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByDescricaoContainsSomething() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where descricao contains DEFAULT_DESCRICAO
        defaultModalidadeContratacaoShouldBeFound("descricao.contains=" + DEFAULT_DESCRICAO);

        // Get all the modalidadeContratacaoList where descricao contains UPDATED_DESCRICAO
        defaultModalidadeContratacaoShouldNotBeFound("descricao.contains=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllModalidadeContratacaosByDescricaoNotContainsSomething() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        // Get all the modalidadeContratacaoList where descricao does not contain DEFAULT_DESCRICAO
        defaultModalidadeContratacaoShouldNotBeFound("descricao.doesNotContain=" + DEFAULT_DESCRICAO);

        // Get all the modalidadeContratacaoList where descricao does not contain UPDATED_DESCRICAO
        defaultModalidadeContratacaoShouldBeFound("descricao.doesNotContain=" + UPDATED_DESCRICAO);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultModalidadeContratacaoShouldBeFound(String filter) throws Exception {
        restModalidadeContratacaoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modalidadeContratacao.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].categoria").value(hasItem(DEFAULT_CATEGORIA)))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)));

        // Check, that the count call also returns 1
        restModalidadeContratacaoMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultModalidadeContratacaoShouldNotBeFound(String filter) throws Exception {
        restModalidadeContratacaoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restModalidadeContratacaoMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingModalidadeContratacao() throws Exception {
        // Get the modalidadeContratacao
        restModalidadeContratacaoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewModalidadeContratacao() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        int databaseSizeBeforeUpdate = modalidadeContratacaoRepository.findAll().size();

        // Update the modalidadeContratacao
        ModalidadeContratacao updatedModalidadeContratacao = modalidadeContratacaoRepository.findById(modalidadeContratacao.getId()).get();
        // Disconnect from session so that the updates on updatedModalidadeContratacao are not directly saved in db
        em.detach(updatedModalidadeContratacao);
        updatedModalidadeContratacao.nome(UPDATED_NOME).categoria(UPDATED_CATEGORIA).descricao(UPDATED_DESCRICAO);

        restModalidadeContratacaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedModalidadeContratacao.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedModalidadeContratacao))
            )
            .andExpect(status().isOk());

        // Validate the ModalidadeContratacao in the database
        List<ModalidadeContratacao> modalidadeContratacaoList = modalidadeContratacaoRepository.findAll();
        assertThat(modalidadeContratacaoList).hasSize(databaseSizeBeforeUpdate);
        ModalidadeContratacao testModalidadeContratacao = modalidadeContratacaoList.get(modalidadeContratacaoList.size() - 1);
        assertThat(testModalidadeContratacao.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testModalidadeContratacao.getCategoria()).isEqualTo(UPDATED_CATEGORIA);
        assertThat(testModalidadeContratacao.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void putNonExistingModalidadeContratacao() throws Exception {
        int databaseSizeBeforeUpdate = modalidadeContratacaoRepository.findAll().size();
        modalidadeContratacao.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModalidadeContratacaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, modalidadeContratacao.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modalidadeContratacao))
            )
            .andExpect(status().isBadRequest());

        // Validate the ModalidadeContratacao in the database
        List<ModalidadeContratacao> modalidadeContratacaoList = modalidadeContratacaoRepository.findAll();
        assertThat(modalidadeContratacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchModalidadeContratacao() throws Exception {
        int databaseSizeBeforeUpdate = modalidadeContratacaoRepository.findAll().size();
        modalidadeContratacao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModalidadeContratacaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modalidadeContratacao))
            )
            .andExpect(status().isBadRequest());

        // Validate the ModalidadeContratacao in the database
        List<ModalidadeContratacao> modalidadeContratacaoList = modalidadeContratacaoRepository.findAll();
        assertThat(modalidadeContratacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamModalidadeContratacao() throws Exception {
        int databaseSizeBeforeUpdate = modalidadeContratacaoRepository.findAll().size();
        modalidadeContratacao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModalidadeContratacaoMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(modalidadeContratacao))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ModalidadeContratacao in the database
        List<ModalidadeContratacao> modalidadeContratacaoList = modalidadeContratacaoRepository.findAll();
        assertThat(modalidadeContratacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateModalidadeContratacaoWithPatch() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        int databaseSizeBeforeUpdate = modalidadeContratacaoRepository.findAll().size();

        // Update the modalidadeContratacao using partial update
        ModalidadeContratacao partialUpdatedModalidadeContratacao = new ModalidadeContratacao();
        partialUpdatedModalidadeContratacao.setId(modalidadeContratacao.getId());

        restModalidadeContratacaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedModalidadeContratacao.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedModalidadeContratacao))
            )
            .andExpect(status().isOk());

        // Validate the ModalidadeContratacao in the database
        List<ModalidadeContratacao> modalidadeContratacaoList = modalidadeContratacaoRepository.findAll();
        assertThat(modalidadeContratacaoList).hasSize(databaseSizeBeforeUpdate);
        ModalidadeContratacao testModalidadeContratacao = modalidadeContratacaoList.get(modalidadeContratacaoList.size() - 1);
        assertThat(testModalidadeContratacao.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testModalidadeContratacao.getCategoria()).isEqualTo(DEFAULT_CATEGORIA);
        assertThat(testModalidadeContratacao.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void fullUpdateModalidadeContratacaoWithPatch() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        int databaseSizeBeforeUpdate = modalidadeContratacaoRepository.findAll().size();

        // Update the modalidadeContratacao using partial update
        ModalidadeContratacao partialUpdatedModalidadeContratacao = new ModalidadeContratacao();
        partialUpdatedModalidadeContratacao.setId(modalidadeContratacao.getId());

        partialUpdatedModalidadeContratacao.nome(UPDATED_NOME).categoria(UPDATED_CATEGORIA).descricao(UPDATED_DESCRICAO);

        restModalidadeContratacaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedModalidadeContratacao.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedModalidadeContratacao))
            )
            .andExpect(status().isOk());

        // Validate the ModalidadeContratacao in the database
        List<ModalidadeContratacao> modalidadeContratacaoList = modalidadeContratacaoRepository.findAll();
        assertThat(modalidadeContratacaoList).hasSize(databaseSizeBeforeUpdate);
        ModalidadeContratacao testModalidadeContratacao = modalidadeContratacaoList.get(modalidadeContratacaoList.size() - 1);
        assertThat(testModalidadeContratacao.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testModalidadeContratacao.getCategoria()).isEqualTo(UPDATED_CATEGORIA);
        assertThat(testModalidadeContratacao.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void patchNonExistingModalidadeContratacao() throws Exception {
        int databaseSizeBeforeUpdate = modalidadeContratacaoRepository.findAll().size();
        modalidadeContratacao.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModalidadeContratacaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, modalidadeContratacao.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(modalidadeContratacao))
            )
            .andExpect(status().isBadRequest());

        // Validate the ModalidadeContratacao in the database
        List<ModalidadeContratacao> modalidadeContratacaoList = modalidadeContratacaoRepository.findAll();
        assertThat(modalidadeContratacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchModalidadeContratacao() throws Exception {
        int databaseSizeBeforeUpdate = modalidadeContratacaoRepository.findAll().size();
        modalidadeContratacao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModalidadeContratacaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(modalidadeContratacao))
            )
            .andExpect(status().isBadRequest());

        // Validate the ModalidadeContratacao in the database
        List<ModalidadeContratacao> modalidadeContratacaoList = modalidadeContratacaoRepository.findAll();
        assertThat(modalidadeContratacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamModalidadeContratacao() throws Exception {
        int databaseSizeBeforeUpdate = modalidadeContratacaoRepository.findAll().size();
        modalidadeContratacao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restModalidadeContratacaoMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(modalidadeContratacao))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ModalidadeContratacao in the database
        List<ModalidadeContratacao> modalidadeContratacaoList = modalidadeContratacaoRepository.findAll();
        assertThat(modalidadeContratacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteModalidadeContratacao() throws Exception {
        // Initialize the database
        modalidadeContratacaoRepository.saveAndFlush(modalidadeContratacao);

        int databaseSizeBeforeDelete = modalidadeContratacaoRepository.findAll().size();

        // Delete the modalidadeContratacao
        restModalidadeContratacaoMockMvc
            .perform(delete(ENTITY_API_URL_ID, modalidadeContratacao.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ModalidadeContratacao> modalidadeContratacaoList = modalidadeContratacaoRepository.findAll();
        assertThat(modalidadeContratacaoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
