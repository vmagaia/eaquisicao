package mz.co.eaquisicao.web.rest;

import static mz.co.eaquisicao.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mz.co.eaquisicao.IntegrationTest;
import mz.co.eaquisicao.domain.LinhaOrcamento;
import mz.co.eaquisicao.repository.LinhaOrcamentoRepository;
import mz.co.eaquisicao.service.criteria.LinhaOrcamentoCriteria;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link LinhaOrcamentoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class LinhaOrcamentoResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_ENTIDADE_FINANCEIRA = "AAAAAAAAAA";
    private static final String UPDATED_ENTIDADE_FINANCEIRA = "BBBBBBBBBB";

    private static final String DEFAULT_RUBRICA = "AAAAAAAAAA";
    private static final String UPDATED_RUBRICA = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRICAO_RUBRICA = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO_RUBRICA = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_VALOR = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALOR = new BigDecimal(2);
    private static final BigDecimal SMALLER_VALOR = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_VALOR_EXECUTADO = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALOR_EXECUTADO = new BigDecimal(2);
    private static final BigDecimal SMALLER_VALOR_EXECUTADO = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_SALDO = new BigDecimal(1);
    private static final BigDecimal UPDATED_SALDO = new BigDecimal(2);
    private static final BigDecimal SMALLER_SALDO = new BigDecimal(1 - 1);

    private static final String ENTITY_API_URL = "/api/linha-orcamentos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private LinhaOrcamentoRepository linhaOrcamentoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLinhaOrcamentoMockMvc;

    private LinhaOrcamento linhaOrcamento;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LinhaOrcamento createEntity(EntityManager em) {
        LinhaOrcamento linhaOrcamento = new LinhaOrcamento()
            .nome(DEFAULT_NOME)
            .entidadeFinanceira(DEFAULT_ENTIDADE_FINANCEIRA)
            .rubrica(DEFAULT_RUBRICA)
            .descricaoRubrica(DEFAULT_DESCRICAO_RUBRICA)
            .valor(DEFAULT_VALOR)
            .valorExecutado(DEFAULT_VALOR_EXECUTADO)
            .saldo(DEFAULT_SALDO);
        return linhaOrcamento;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LinhaOrcamento createUpdatedEntity(EntityManager em) {
        LinhaOrcamento linhaOrcamento = new LinhaOrcamento()
            .nome(UPDATED_NOME)
            .entidadeFinanceira(UPDATED_ENTIDADE_FINANCEIRA)
            .rubrica(UPDATED_RUBRICA)
            .descricaoRubrica(UPDATED_DESCRICAO_RUBRICA)
            .valor(UPDATED_VALOR)
            .valorExecutado(UPDATED_VALOR_EXECUTADO)
            .saldo(UPDATED_SALDO);
        return linhaOrcamento;
    }

    @BeforeEach
    public void initTest() {
        linhaOrcamento = createEntity(em);
    }

    @Test
    @Transactional
    void createLinhaOrcamento() throws Exception {
        int databaseSizeBeforeCreate = linhaOrcamentoRepository.findAll().size();
        // Create the LinhaOrcamento
        restLinhaOrcamentoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(linhaOrcamento))
            )
            .andExpect(status().isCreated());

        // Validate the LinhaOrcamento in the database
        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeCreate + 1);
        LinhaOrcamento testLinhaOrcamento = linhaOrcamentoList.get(linhaOrcamentoList.size() - 1);
        assertThat(testLinhaOrcamento.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testLinhaOrcamento.getEntidadeFinanceira()).isEqualTo(DEFAULT_ENTIDADE_FINANCEIRA);
        assertThat(testLinhaOrcamento.getRubrica()).isEqualTo(DEFAULT_RUBRICA);
        assertThat(testLinhaOrcamento.getDescricaoRubrica()).isEqualTo(DEFAULT_DESCRICAO_RUBRICA);
        assertThat(testLinhaOrcamento.getValor()).isEqualByComparingTo(DEFAULT_VALOR);
        assertThat(testLinhaOrcamento.getValorExecutado()).isEqualByComparingTo(DEFAULT_VALOR_EXECUTADO);
        assertThat(testLinhaOrcamento.getSaldo()).isEqualByComparingTo(DEFAULT_SALDO);
    }

    @Test
    @Transactional
    void createLinhaOrcamentoWithExistingId() throws Exception {
        // Create the LinhaOrcamento with an existing ID
        linhaOrcamento.setId(1L);

        int databaseSizeBeforeCreate = linhaOrcamentoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restLinhaOrcamentoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(linhaOrcamento))
            )
            .andExpect(status().isBadRequest());

        // Validate the LinhaOrcamento in the database
        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = linhaOrcamentoRepository.findAll().size();
        // set the field null
        linhaOrcamento.setNome(null);

        // Create the LinhaOrcamento, which fails.

        restLinhaOrcamentoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(linhaOrcamento))
            )
            .andExpect(status().isBadRequest());

        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRubricaIsRequired() throws Exception {
        int databaseSizeBeforeTest = linhaOrcamentoRepository.findAll().size();
        // set the field null
        linhaOrcamento.setRubrica(null);

        // Create the LinhaOrcamento, which fails.

        restLinhaOrcamentoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(linhaOrcamento))
            )
            .andExpect(status().isBadRequest());

        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkValorIsRequired() throws Exception {
        int databaseSizeBeforeTest = linhaOrcamentoRepository.findAll().size();
        // set the field null
        linhaOrcamento.setValor(null);

        // Create the LinhaOrcamento, which fails.

        restLinhaOrcamentoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(linhaOrcamento))
            )
            .andExpect(status().isBadRequest());

        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentos() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList
        restLinhaOrcamentoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(linhaOrcamento.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].entidadeFinanceira").value(hasItem(DEFAULT_ENTIDADE_FINANCEIRA)))
            .andExpect(jsonPath("$.[*].rubrica").value(hasItem(DEFAULT_RUBRICA)))
            .andExpect(jsonPath("$.[*].descricaoRubrica").value(hasItem(DEFAULT_DESCRICAO_RUBRICA)))
            .andExpect(jsonPath("$.[*].valor").value(hasItem(sameNumber(DEFAULT_VALOR))))
            .andExpect(jsonPath("$.[*].valorExecutado").value(hasItem(sameNumber(DEFAULT_VALOR_EXECUTADO))))
            .andExpect(jsonPath("$.[*].saldo").value(hasItem(sameNumber(DEFAULT_SALDO))));
    }

    @Test
    @Transactional
    void getLinhaOrcamento() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get the linhaOrcamento
        restLinhaOrcamentoMockMvc
            .perform(get(ENTITY_API_URL_ID, linhaOrcamento.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(linhaOrcamento.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME))
            .andExpect(jsonPath("$.entidadeFinanceira").value(DEFAULT_ENTIDADE_FINANCEIRA))
            .andExpect(jsonPath("$.rubrica").value(DEFAULT_RUBRICA))
            .andExpect(jsonPath("$.descricaoRubrica").value(DEFAULT_DESCRICAO_RUBRICA))
            .andExpect(jsonPath("$.valor").value(sameNumber(DEFAULT_VALOR)))
            .andExpect(jsonPath("$.valorExecutado").value(sameNumber(DEFAULT_VALOR_EXECUTADO)))
            .andExpect(jsonPath("$.saldo").value(sameNumber(DEFAULT_SALDO)));
    }

    @Test
    @Transactional
    void getLinhaOrcamentosByIdFiltering() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        Long id = linhaOrcamento.getId();

        defaultLinhaOrcamentoShouldBeFound("id.equals=" + id);
        defaultLinhaOrcamentoShouldNotBeFound("id.notEquals=" + id);

        defaultLinhaOrcamentoShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLinhaOrcamentoShouldNotBeFound("id.greaterThan=" + id);

        defaultLinhaOrcamentoShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLinhaOrcamentoShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByNomeIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where nome equals to DEFAULT_NOME
        defaultLinhaOrcamentoShouldBeFound("nome.equals=" + DEFAULT_NOME);

        // Get all the linhaOrcamentoList where nome equals to UPDATED_NOME
        defaultLinhaOrcamentoShouldNotBeFound("nome.equals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByNomeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where nome not equals to DEFAULT_NOME
        defaultLinhaOrcamentoShouldNotBeFound("nome.notEquals=" + DEFAULT_NOME);

        // Get all the linhaOrcamentoList where nome not equals to UPDATED_NOME
        defaultLinhaOrcamentoShouldBeFound("nome.notEquals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByNomeIsInShouldWork() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where nome in DEFAULT_NOME or UPDATED_NOME
        defaultLinhaOrcamentoShouldBeFound("nome.in=" + DEFAULT_NOME + "," + UPDATED_NOME);

        // Get all the linhaOrcamentoList where nome equals to UPDATED_NOME
        defaultLinhaOrcamentoShouldNotBeFound("nome.in=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByNomeIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where nome is not null
        defaultLinhaOrcamentoShouldBeFound("nome.specified=true");

        // Get all the linhaOrcamentoList where nome is null
        defaultLinhaOrcamentoShouldNotBeFound("nome.specified=false");
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByNomeContainsSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where nome contains DEFAULT_NOME
        defaultLinhaOrcamentoShouldBeFound("nome.contains=" + DEFAULT_NOME);

        // Get all the linhaOrcamentoList where nome contains UPDATED_NOME
        defaultLinhaOrcamentoShouldNotBeFound("nome.contains=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByNomeNotContainsSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where nome does not contain DEFAULT_NOME
        defaultLinhaOrcamentoShouldNotBeFound("nome.doesNotContain=" + DEFAULT_NOME);

        // Get all the linhaOrcamentoList where nome does not contain UPDATED_NOME
        defaultLinhaOrcamentoShouldBeFound("nome.doesNotContain=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByEntidadeFinanceiraIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where entidadeFinanceira equals to DEFAULT_ENTIDADE_FINANCEIRA
        defaultLinhaOrcamentoShouldBeFound("entidadeFinanceira.equals=" + DEFAULT_ENTIDADE_FINANCEIRA);

        // Get all the linhaOrcamentoList where entidadeFinanceira equals to UPDATED_ENTIDADE_FINANCEIRA
        defaultLinhaOrcamentoShouldNotBeFound("entidadeFinanceira.equals=" + UPDATED_ENTIDADE_FINANCEIRA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByEntidadeFinanceiraIsNotEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where entidadeFinanceira not equals to DEFAULT_ENTIDADE_FINANCEIRA
        defaultLinhaOrcamentoShouldNotBeFound("entidadeFinanceira.notEquals=" + DEFAULT_ENTIDADE_FINANCEIRA);

        // Get all the linhaOrcamentoList where entidadeFinanceira not equals to UPDATED_ENTIDADE_FINANCEIRA
        defaultLinhaOrcamentoShouldBeFound("entidadeFinanceira.notEquals=" + UPDATED_ENTIDADE_FINANCEIRA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByEntidadeFinanceiraIsInShouldWork() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where entidadeFinanceira in DEFAULT_ENTIDADE_FINANCEIRA or UPDATED_ENTIDADE_FINANCEIRA
        defaultLinhaOrcamentoShouldBeFound("entidadeFinanceira.in=" + DEFAULT_ENTIDADE_FINANCEIRA + "," + UPDATED_ENTIDADE_FINANCEIRA);

        // Get all the linhaOrcamentoList where entidadeFinanceira equals to UPDATED_ENTIDADE_FINANCEIRA
        defaultLinhaOrcamentoShouldNotBeFound("entidadeFinanceira.in=" + UPDATED_ENTIDADE_FINANCEIRA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByEntidadeFinanceiraIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where entidadeFinanceira is not null
        defaultLinhaOrcamentoShouldBeFound("entidadeFinanceira.specified=true");

        // Get all the linhaOrcamentoList where entidadeFinanceira is null
        defaultLinhaOrcamentoShouldNotBeFound("entidadeFinanceira.specified=false");
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByEntidadeFinanceiraContainsSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where entidadeFinanceira contains DEFAULT_ENTIDADE_FINANCEIRA
        defaultLinhaOrcamentoShouldBeFound("entidadeFinanceira.contains=" + DEFAULT_ENTIDADE_FINANCEIRA);

        // Get all the linhaOrcamentoList where entidadeFinanceira contains UPDATED_ENTIDADE_FINANCEIRA
        defaultLinhaOrcamentoShouldNotBeFound("entidadeFinanceira.contains=" + UPDATED_ENTIDADE_FINANCEIRA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByEntidadeFinanceiraNotContainsSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where entidadeFinanceira does not contain DEFAULT_ENTIDADE_FINANCEIRA
        defaultLinhaOrcamentoShouldNotBeFound("entidadeFinanceira.doesNotContain=" + DEFAULT_ENTIDADE_FINANCEIRA);

        // Get all the linhaOrcamentoList where entidadeFinanceira does not contain UPDATED_ENTIDADE_FINANCEIRA
        defaultLinhaOrcamentoShouldBeFound("entidadeFinanceira.doesNotContain=" + UPDATED_ENTIDADE_FINANCEIRA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByRubricaIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where rubrica equals to DEFAULT_RUBRICA
        defaultLinhaOrcamentoShouldBeFound("rubrica.equals=" + DEFAULT_RUBRICA);

        // Get all the linhaOrcamentoList where rubrica equals to UPDATED_RUBRICA
        defaultLinhaOrcamentoShouldNotBeFound("rubrica.equals=" + UPDATED_RUBRICA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByRubricaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where rubrica not equals to DEFAULT_RUBRICA
        defaultLinhaOrcamentoShouldNotBeFound("rubrica.notEquals=" + DEFAULT_RUBRICA);

        // Get all the linhaOrcamentoList where rubrica not equals to UPDATED_RUBRICA
        defaultLinhaOrcamentoShouldBeFound("rubrica.notEquals=" + UPDATED_RUBRICA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByRubricaIsInShouldWork() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where rubrica in DEFAULT_RUBRICA or UPDATED_RUBRICA
        defaultLinhaOrcamentoShouldBeFound("rubrica.in=" + DEFAULT_RUBRICA + "," + UPDATED_RUBRICA);

        // Get all the linhaOrcamentoList where rubrica equals to UPDATED_RUBRICA
        defaultLinhaOrcamentoShouldNotBeFound("rubrica.in=" + UPDATED_RUBRICA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByRubricaIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where rubrica is not null
        defaultLinhaOrcamentoShouldBeFound("rubrica.specified=true");

        // Get all the linhaOrcamentoList where rubrica is null
        defaultLinhaOrcamentoShouldNotBeFound("rubrica.specified=false");
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByRubricaContainsSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where rubrica contains DEFAULT_RUBRICA
        defaultLinhaOrcamentoShouldBeFound("rubrica.contains=" + DEFAULT_RUBRICA);

        // Get all the linhaOrcamentoList where rubrica contains UPDATED_RUBRICA
        defaultLinhaOrcamentoShouldNotBeFound("rubrica.contains=" + UPDATED_RUBRICA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByRubricaNotContainsSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where rubrica does not contain DEFAULT_RUBRICA
        defaultLinhaOrcamentoShouldNotBeFound("rubrica.doesNotContain=" + DEFAULT_RUBRICA);

        // Get all the linhaOrcamentoList where rubrica does not contain UPDATED_RUBRICA
        defaultLinhaOrcamentoShouldBeFound("rubrica.doesNotContain=" + UPDATED_RUBRICA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByDescricaoRubricaIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where descricaoRubrica equals to DEFAULT_DESCRICAO_RUBRICA
        defaultLinhaOrcamentoShouldBeFound("descricaoRubrica.equals=" + DEFAULT_DESCRICAO_RUBRICA);

        // Get all the linhaOrcamentoList where descricaoRubrica equals to UPDATED_DESCRICAO_RUBRICA
        defaultLinhaOrcamentoShouldNotBeFound("descricaoRubrica.equals=" + UPDATED_DESCRICAO_RUBRICA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByDescricaoRubricaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where descricaoRubrica not equals to DEFAULT_DESCRICAO_RUBRICA
        defaultLinhaOrcamentoShouldNotBeFound("descricaoRubrica.notEquals=" + DEFAULT_DESCRICAO_RUBRICA);

        // Get all the linhaOrcamentoList where descricaoRubrica not equals to UPDATED_DESCRICAO_RUBRICA
        defaultLinhaOrcamentoShouldBeFound("descricaoRubrica.notEquals=" + UPDATED_DESCRICAO_RUBRICA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByDescricaoRubricaIsInShouldWork() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where descricaoRubrica in DEFAULT_DESCRICAO_RUBRICA or UPDATED_DESCRICAO_RUBRICA
        defaultLinhaOrcamentoShouldBeFound("descricaoRubrica.in=" + DEFAULT_DESCRICAO_RUBRICA + "," + UPDATED_DESCRICAO_RUBRICA);

        // Get all the linhaOrcamentoList where descricaoRubrica equals to UPDATED_DESCRICAO_RUBRICA
        defaultLinhaOrcamentoShouldNotBeFound("descricaoRubrica.in=" + UPDATED_DESCRICAO_RUBRICA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByDescricaoRubricaIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where descricaoRubrica is not null
        defaultLinhaOrcamentoShouldBeFound("descricaoRubrica.specified=true");

        // Get all the linhaOrcamentoList where descricaoRubrica is null
        defaultLinhaOrcamentoShouldNotBeFound("descricaoRubrica.specified=false");
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByDescricaoRubricaContainsSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where descricaoRubrica contains DEFAULT_DESCRICAO_RUBRICA
        defaultLinhaOrcamentoShouldBeFound("descricaoRubrica.contains=" + DEFAULT_DESCRICAO_RUBRICA);

        // Get all the linhaOrcamentoList where descricaoRubrica contains UPDATED_DESCRICAO_RUBRICA
        defaultLinhaOrcamentoShouldNotBeFound("descricaoRubrica.contains=" + UPDATED_DESCRICAO_RUBRICA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByDescricaoRubricaNotContainsSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where descricaoRubrica does not contain DEFAULT_DESCRICAO_RUBRICA
        defaultLinhaOrcamentoShouldNotBeFound("descricaoRubrica.doesNotContain=" + DEFAULT_DESCRICAO_RUBRICA);

        // Get all the linhaOrcamentoList where descricaoRubrica does not contain UPDATED_DESCRICAO_RUBRICA
        defaultLinhaOrcamentoShouldBeFound("descricaoRubrica.doesNotContain=" + UPDATED_DESCRICAO_RUBRICA);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valor equals to DEFAULT_VALOR
        defaultLinhaOrcamentoShouldBeFound("valor.equals=" + DEFAULT_VALOR);

        // Get all the linhaOrcamentoList where valor equals to UPDATED_VALOR
        defaultLinhaOrcamentoShouldNotBeFound("valor.equals=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorIsNotEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valor not equals to DEFAULT_VALOR
        defaultLinhaOrcamentoShouldNotBeFound("valor.notEquals=" + DEFAULT_VALOR);

        // Get all the linhaOrcamentoList where valor not equals to UPDATED_VALOR
        defaultLinhaOrcamentoShouldBeFound("valor.notEquals=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorIsInShouldWork() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valor in DEFAULT_VALOR or UPDATED_VALOR
        defaultLinhaOrcamentoShouldBeFound("valor.in=" + DEFAULT_VALOR + "," + UPDATED_VALOR);

        // Get all the linhaOrcamentoList where valor equals to UPDATED_VALOR
        defaultLinhaOrcamentoShouldNotBeFound("valor.in=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valor is not null
        defaultLinhaOrcamentoShouldBeFound("valor.specified=true");

        // Get all the linhaOrcamentoList where valor is null
        defaultLinhaOrcamentoShouldNotBeFound("valor.specified=false");
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valor is greater than or equal to DEFAULT_VALOR
        defaultLinhaOrcamentoShouldBeFound("valor.greaterThanOrEqual=" + DEFAULT_VALOR);

        // Get all the linhaOrcamentoList where valor is greater than or equal to UPDATED_VALOR
        defaultLinhaOrcamentoShouldNotBeFound("valor.greaterThanOrEqual=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valor is less than or equal to DEFAULT_VALOR
        defaultLinhaOrcamentoShouldBeFound("valor.lessThanOrEqual=" + DEFAULT_VALOR);

        // Get all the linhaOrcamentoList where valor is less than or equal to SMALLER_VALOR
        defaultLinhaOrcamentoShouldNotBeFound("valor.lessThanOrEqual=" + SMALLER_VALOR);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorIsLessThanSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valor is less than DEFAULT_VALOR
        defaultLinhaOrcamentoShouldNotBeFound("valor.lessThan=" + DEFAULT_VALOR);

        // Get all the linhaOrcamentoList where valor is less than UPDATED_VALOR
        defaultLinhaOrcamentoShouldBeFound("valor.lessThan=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valor is greater than DEFAULT_VALOR
        defaultLinhaOrcamentoShouldNotBeFound("valor.greaterThan=" + DEFAULT_VALOR);

        // Get all the linhaOrcamentoList where valor is greater than SMALLER_VALOR
        defaultLinhaOrcamentoShouldBeFound("valor.greaterThan=" + SMALLER_VALOR);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorExecutadoIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valorExecutado equals to DEFAULT_VALOR_EXECUTADO
        defaultLinhaOrcamentoShouldBeFound("valorExecutado.equals=" + DEFAULT_VALOR_EXECUTADO);

        // Get all the linhaOrcamentoList where valorExecutado equals to UPDATED_VALOR_EXECUTADO
        defaultLinhaOrcamentoShouldNotBeFound("valorExecutado.equals=" + UPDATED_VALOR_EXECUTADO);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorExecutadoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valorExecutado not equals to DEFAULT_VALOR_EXECUTADO
        defaultLinhaOrcamentoShouldNotBeFound("valorExecutado.notEquals=" + DEFAULT_VALOR_EXECUTADO);

        // Get all the linhaOrcamentoList where valorExecutado not equals to UPDATED_VALOR_EXECUTADO
        defaultLinhaOrcamentoShouldBeFound("valorExecutado.notEquals=" + UPDATED_VALOR_EXECUTADO);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorExecutadoIsInShouldWork() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valorExecutado in DEFAULT_VALOR_EXECUTADO or UPDATED_VALOR_EXECUTADO
        defaultLinhaOrcamentoShouldBeFound("valorExecutado.in=" + DEFAULT_VALOR_EXECUTADO + "," + UPDATED_VALOR_EXECUTADO);

        // Get all the linhaOrcamentoList where valorExecutado equals to UPDATED_VALOR_EXECUTADO
        defaultLinhaOrcamentoShouldNotBeFound("valorExecutado.in=" + UPDATED_VALOR_EXECUTADO);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorExecutadoIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valorExecutado is not null
        defaultLinhaOrcamentoShouldBeFound("valorExecutado.specified=true");

        // Get all the linhaOrcamentoList where valorExecutado is null
        defaultLinhaOrcamentoShouldNotBeFound("valorExecutado.specified=false");
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorExecutadoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valorExecutado is greater than or equal to DEFAULT_VALOR_EXECUTADO
        defaultLinhaOrcamentoShouldBeFound("valorExecutado.greaterThanOrEqual=" + DEFAULT_VALOR_EXECUTADO);

        // Get all the linhaOrcamentoList where valorExecutado is greater than or equal to UPDATED_VALOR_EXECUTADO
        defaultLinhaOrcamentoShouldNotBeFound("valorExecutado.greaterThanOrEqual=" + UPDATED_VALOR_EXECUTADO);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorExecutadoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valorExecutado is less than or equal to DEFAULT_VALOR_EXECUTADO
        defaultLinhaOrcamentoShouldBeFound("valorExecutado.lessThanOrEqual=" + DEFAULT_VALOR_EXECUTADO);

        // Get all the linhaOrcamentoList where valorExecutado is less than or equal to SMALLER_VALOR_EXECUTADO
        defaultLinhaOrcamentoShouldNotBeFound("valorExecutado.lessThanOrEqual=" + SMALLER_VALOR_EXECUTADO);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorExecutadoIsLessThanSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valorExecutado is less than DEFAULT_VALOR_EXECUTADO
        defaultLinhaOrcamentoShouldNotBeFound("valorExecutado.lessThan=" + DEFAULT_VALOR_EXECUTADO);

        // Get all the linhaOrcamentoList where valorExecutado is less than UPDATED_VALOR_EXECUTADO
        defaultLinhaOrcamentoShouldBeFound("valorExecutado.lessThan=" + UPDATED_VALOR_EXECUTADO);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosByValorExecutadoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where valorExecutado is greater than DEFAULT_VALOR_EXECUTADO
        defaultLinhaOrcamentoShouldNotBeFound("valorExecutado.greaterThan=" + DEFAULT_VALOR_EXECUTADO);

        // Get all the linhaOrcamentoList where valorExecutado is greater than SMALLER_VALOR_EXECUTADO
        defaultLinhaOrcamentoShouldBeFound("valorExecutado.greaterThan=" + SMALLER_VALOR_EXECUTADO);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosBySaldoIsEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where saldo equals to DEFAULT_SALDO
        defaultLinhaOrcamentoShouldBeFound("saldo.equals=" + DEFAULT_SALDO);

        // Get all the linhaOrcamentoList where saldo equals to UPDATED_SALDO
        defaultLinhaOrcamentoShouldNotBeFound("saldo.equals=" + UPDATED_SALDO);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosBySaldoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where saldo not equals to DEFAULT_SALDO
        defaultLinhaOrcamentoShouldNotBeFound("saldo.notEquals=" + DEFAULT_SALDO);

        // Get all the linhaOrcamentoList where saldo not equals to UPDATED_SALDO
        defaultLinhaOrcamentoShouldBeFound("saldo.notEquals=" + UPDATED_SALDO);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosBySaldoIsInShouldWork() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where saldo in DEFAULT_SALDO or UPDATED_SALDO
        defaultLinhaOrcamentoShouldBeFound("saldo.in=" + DEFAULT_SALDO + "," + UPDATED_SALDO);

        // Get all the linhaOrcamentoList where saldo equals to UPDATED_SALDO
        defaultLinhaOrcamentoShouldNotBeFound("saldo.in=" + UPDATED_SALDO);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosBySaldoIsNullOrNotNull() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where saldo is not null
        defaultLinhaOrcamentoShouldBeFound("saldo.specified=true");

        // Get all the linhaOrcamentoList where saldo is null
        defaultLinhaOrcamentoShouldNotBeFound("saldo.specified=false");
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosBySaldoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where saldo is greater than or equal to DEFAULT_SALDO
        defaultLinhaOrcamentoShouldBeFound("saldo.greaterThanOrEqual=" + DEFAULT_SALDO);

        // Get all the linhaOrcamentoList where saldo is greater than or equal to UPDATED_SALDO
        defaultLinhaOrcamentoShouldNotBeFound("saldo.greaterThanOrEqual=" + UPDATED_SALDO);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosBySaldoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where saldo is less than or equal to DEFAULT_SALDO
        defaultLinhaOrcamentoShouldBeFound("saldo.lessThanOrEqual=" + DEFAULT_SALDO);

        // Get all the linhaOrcamentoList where saldo is less than or equal to SMALLER_SALDO
        defaultLinhaOrcamentoShouldNotBeFound("saldo.lessThanOrEqual=" + SMALLER_SALDO);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosBySaldoIsLessThanSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where saldo is less than DEFAULT_SALDO
        defaultLinhaOrcamentoShouldNotBeFound("saldo.lessThan=" + DEFAULT_SALDO);

        // Get all the linhaOrcamentoList where saldo is less than UPDATED_SALDO
        defaultLinhaOrcamentoShouldBeFound("saldo.lessThan=" + UPDATED_SALDO);
    }

    @Test
    @Transactional
    void getAllLinhaOrcamentosBySaldoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        // Get all the linhaOrcamentoList where saldo is greater than DEFAULT_SALDO
        defaultLinhaOrcamentoShouldNotBeFound("saldo.greaterThan=" + DEFAULT_SALDO);

        // Get all the linhaOrcamentoList where saldo is greater than SMALLER_SALDO
        defaultLinhaOrcamentoShouldBeFound("saldo.greaterThan=" + SMALLER_SALDO);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLinhaOrcamentoShouldBeFound(String filter) throws Exception {
        restLinhaOrcamentoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(linhaOrcamento.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].entidadeFinanceira").value(hasItem(DEFAULT_ENTIDADE_FINANCEIRA)))
            .andExpect(jsonPath("$.[*].rubrica").value(hasItem(DEFAULT_RUBRICA)))
            .andExpect(jsonPath("$.[*].descricaoRubrica").value(hasItem(DEFAULT_DESCRICAO_RUBRICA)))
            .andExpect(jsonPath("$.[*].valor").value(hasItem(sameNumber(DEFAULT_VALOR))))
            .andExpect(jsonPath("$.[*].valorExecutado").value(hasItem(sameNumber(DEFAULT_VALOR_EXECUTADO))))
            .andExpect(jsonPath("$.[*].saldo").value(hasItem(sameNumber(DEFAULT_SALDO))));

        // Check, that the count call also returns 1
        restLinhaOrcamentoMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLinhaOrcamentoShouldNotBeFound(String filter) throws Exception {
        restLinhaOrcamentoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLinhaOrcamentoMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingLinhaOrcamento() throws Exception {
        // Get the linhaOrcamento
        restLinhaOrcamentoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewLinhaOrcamento() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        int databaseSizeBeforeUpdate = linhaOrcamentoRepository.findAll().size();

        // Update the linhaOrcamento
        LinhaOrcamento updatedLinhaOrcamento = linhaOrcamentoRepository.findById(linhaOrcamento.getId()).get();
        // Disconnect from session so that the updates on updatedLinhaOrcamento are not directly saved in db
        em.detach(updatedLinhaOrcamento);
        updatedLinhaOrcamento
            .nome(UPDATED_NOME)
            .entidadeFinanceira(UPDATED_ENTIDADE_FINANCEIRA)
            .rubrica(UPDATED_RUBRICA)
            .descricaoRubrica(UPDATED_DESCRICAO_RUBRICA)
            .valor(UPDATED_VALOR)
            .valorExecutado(UPDATED_VALOR_EXECUTADO)
            .saldo(UPDATED_SALDO);

        restLinhaOrcamentoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedLinhaOrcamento.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedLinhaOrcamento))
            )
            .andExpect(status().isOk());

        // Validate the LinhaOrcamento in the database
        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeUpdate);
        LinhaOrcamento testLinhaOrcamento = linhaOrcamentoList.get(linhaOrcamentoList.size() - 1);
        assertThat(testLinhaOrcamento.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testLinhaOrcamento.getEntidadeFinanceira()).isEqualTo(UPDATED_ENTIDADE_FINANCEIRA);
        assertThat(testLinhaOrcamento.getRubrica()).isEqualTo(UPDATED_RUBRICA);
        assertThat(testLinhaOrcamento.getDescricaoRubrica()).isEqualTo(UPDATED_DESCRICAO_RUBRICA);
        assertThat(testLinhaOrcamento.getValor()).isEqualTo(UPDATED_VALOR);
        assertThat(testLinhaOrcamento.getValorExecutado()).isEqualTo(UPDATED_VALOR_EXECUTADO);
        assertThat(testLinhaOrcamento.getSaldo()).isEqualTo(UPDATED_SALDO);
    }

    @Test
    @Transactional
    void putNonExistingLinhaOrcamento() throws Exception {
        int databaseSizeBeforeUpdate = linhaOrcamentoRepository.findAll().size();
        linhaOrcamento.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLinhaOrcamentoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, linhaOrcamento.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(linhaOrcamento))
            )
            .andExpect(status().isBadRequest());

        // Validate the LinhaOrcamento in the database
        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchLinhaOrcamento() throws Exception {
        int databaseSizeBeforeUpdate = linhaOrcamentoRepository.findAll().size();
        linhaOrcamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLinhaOrcamentoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(linhaOrcamento))
            )
            .andExpect(status().isBadRequest());

        // Validate the LinhaOrcamento in the database
        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamLinhaOrcamento() throws Exception {
        int databaseSizeBeforeUpdate = linhaOrcamentoRepository.findAll().size();
        linhaOrcamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLinhaOrcamentoMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(linhaOrcamento)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the LinhaOrcamento in the database
        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateLinhaOrcamentoWithPatch() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        int databaseSizeBeforeUpdate = linhaOrcamentoRepository.findAll().size();

        // Update the linhaOrcamento using partial update
        LinhaOrcamento partialUpdatedLinhaOrcamento = new LinhaOrcamento();
        partialUpdatedLinhaOrcamento.setId(linhaOrcamento.getId());

        partialUpdatedLinhaOrcamento
            .descricaoRubrica(UPDATED_DESCRICAO_RUBRICA)
            .valor(UPDATED_VALOR)
            .valorExecutado(UPDATED_VALOR_EXECUTADO);

        restLinhaOrcamentoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLinhaOrcamento.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLinhaOrcamento))
            )
            .andExpect(status().isOk());

        // Validate the LinhaOrcamento in the database
        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeUpdate);
        LinhaOrcamento testLinhaOrcamento = linhaOrcamentoList.get(linhaOrcamentoList.size() - 1);
        assertThat(testLinhaOrcamento.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testLinhaOrcamento.getEntidadeFinanceira()).isEqualTo(DEFAULT_ENTIDADE_FINANCEIRA);
        assertThat(testLinhaOrcamento.getRubrica()).isEqualTo(DEFAULT_RUBRICA);
        assertThat(testLinhaOrcamento.getDescricaoRubrica()).isEqualTo(UPDATED_DESCRICAO_RUBRICA);
        assertThat(testLinhaOrcamento.getValor()).isEqualByComparingTo(UPDATED_VALOR);
        assertThat(testLinhaOrcamento.getValorExecutado()).isEqualByComparingTo(UPDATED_VALOR_EXECUTADO);
        assertThat(testLinhaOrcamento.getSaldo()).isEqualByComparingTo(DEFAULT_SALDO);
    }

    @Test
    @Transactional
    void fullUpdateLinhaOrcamentoWithPatch() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        int databaseSizeBeforeUpdate = linhaOrcamentoRepository.findAll().size();

        // Update the linhaOrcamento using partial update
        LinhaOrcamento partialUpdatedLinhaOrcamento = new LinhaOrcamento();
        partialUpdatedLinhaOrcamento.setId(linhaOrcamento.getId());

        partialUpdatedLinhaOrcamento
            .nome(UPDATED_NOME)
            .entidadeFinanceira(UPDATED_ENTIDADE_FINANCEIRA)
            .rubrica(UPDATED_RUBRICA)
            .descricaoRubrica(UPDATED_DESCRICAO_RUBRICA)
            .valor(UPDATED_VALOR)
            .valorExecutado(UPDATED_VALOR_EXECUTADO)
            .saldo(UPDATED_SALDO);

        restLinhaOrcamentoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLinhaOrcamento.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLinhaOrcamento))
            )
            .andExpect(status().isOk());

        // Validate the LinhaOrcamento in the database
        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeUpdate);
        LinhaOrcamento testLinhaOrcamento = linhaOrcamentoList.get(linhaOrcamentoList.size() - 1);
        assertThat(testLinhaOrcamento.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testLinhaOrcamento.getEntidadeFinanceira()).isEqualTo(UPDATED_ENTIDADE_FINANCEIRA);
        assertThat(testLinhaOrcamento.getRubrica()).isEqualTo(UPDATED_RUBRICA);
        assertThat(testLinhaOrcamento.getDescricaoRubrica()).isEqualTo(UPDATED_DESCRICAO_RUBRICA);
        assertThat(testLinhaOrcamento.getValor()).isEqualByComparingTo(UPDATED_VALOR);
        assertThat(testLinhaOrcamento.getValorExecutado()).isEqualByComparingTo(UPDATED_VALOR_EXECUTADO);
        assertThat(testLinhaOrcamento.getSaldo()).isEqualByComparingTo(UPDATED_SALDO);
    }

    @Test
    @Transactional
    void patchNonExistingLinhaOrcamento() throws Exception {
        int databaseSizeBeforeUpdate = linhaOrcamentoRepository.findAll().size();
        linhaOrcamento.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLinhaOrcamentoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, linhaOrcamento.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(linhaOrcamento))
            )
            .andExpect(status().isBadRequest());

        // Validate the LinhaOrcamento in the database
        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchLinhaOrcamento() throws Exception {
        int databaseSizeBeforeUpdate = linhaOrcamentoRepository.findAll().size();
        linhaOrcamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLinhaOrcamentoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(linhaOrcamento))
            )
            .andExpect(status().isBadRequest());

        // Validate the LinhaOrcamento in the database
        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamLinhaOrcamento() throws Exception {
        int databaseSizeBeforeUpdate = linhaOrcamentoRepository.findAll().size();
        linhaOrcamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLinhaOrcamentoMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(linhaOrcamento))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the LinhaOrcamento in the database
        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteLinhaOrcamento() throws Exception {
        // Initialize the database
        linhaOrcamentoRepository.saveAndFlush(linhaOrcamento);

        int databaseSizeBeforeDelete = linhaOrcamentoRepository.findAll().size();

        // Delete the linhaOrcamento
        restLinhaOrcamentoMockMvc
            .perform(delete(ENTITY_API_URL_ID, linhaOrcamento.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LinhaOrcamento> linhaOrcamentoList = linhaOrcamentoRepository.findAll();
        assertThat(linhaOrcamentoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
