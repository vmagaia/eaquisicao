package mz.co.eaquisicao.web.rest;

import static mz.co.eaquisicao.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mz.co.eaquisicao.IntegrationTest;
import mz.co.eaquisicao.domain.Contrato;
import mz.co.eaquisicao.domain.LinhaOrcamento;
import mz.co.eaquisicao.domain.PropostaFinanceira;
import mz.co.eaquisicao.repository.ContratoRepository;
import mz.co.eaquisicao.service.criteria.ContratoCriteria;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ContratoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ContratoResourceIT {

    private static final Integer DEFAULT_CODIGO = 1;
    private static final Integer UPDATED_CODIGO = 2;
    private static final Integer SMALLER_CODIGO = 1 - 1;

    private static final BigDecimal DEFAULT_VALOR = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALOR = new BigDecimal(2);
    private static final BigDecimal SMALLER_VALOR = new BigDecimal(1 - 1);

    private static final LocalDate DEFAULT_INICIO_PREVISTO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_INICIO_PREVISTO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_INICIO_PREVISTO = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_TERMINO_PREVISTO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TERMINO_PREVISTO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_TERMINO_PREVISTO = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_INICIO_EFECTIVO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_INICIO_EFECTIVO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_INICIO_EFECTIVO = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_TERMINO_EFECTIVO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TERMINO_EFECTIVO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_TERMINO_EFECTIVO = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_REGISTO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_REGISTO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_REGISTO = LocalDate.ofEpochDay(-1L);

    private static final String ENTITY_API_URL = "/api/contratoes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ContratoRepository contratoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContratoMockMvc;

    private Contrato contrato;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Contrato createEntity(EntityManager em) {
        Contrato contrato = new Contrato()
            .codigo(DEFAULT_CODIGO)
            .valor(DEFAULT_VALOR)
            .inicioPrevisto(DEFAULT_INICIO_PREVISTO)
            .terminoPrevisto(DEFAULT_TERMINO_PREVISTO)
            .inicioEfectivo(DEFAULT_INICIO_EFECTIVO)
            .terminoEfectivo(DEFAULT_TERMINO_EFECTIVO)
            .descricao(DEFAULT_DESCRICAO)
            .dataRegisto(DEFAULT_DATA_REGISTO);
        return contrato;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Contrato createUpdatedEntity(EntityManager em) {
        Contrato contrato = new Contrato()
            .codigo(UPDATED_CODIGO)
            .valor(UPDATED_VALOR)
            .inicioPrevisto(UPDATED_INICIO_PREVISTO)
            .terminoPrevisto(UPDATED_TERMINO_PREVISTO)
            .inicioEfectivo(UPDATED_INICIO_EFECTIVO)
            .terminoEfectivo(UPDATED_TERMINO_EFECTIVO)
            .descricao(UPDATED_DESCRICAO)
            .dataRegisto(UPDATED_DATA_REGISTO);
        return contrato;
    }

    @BeforeEach
    public void initTest() {
        contrato = createEntity(em);
    }

    @Test
    @Transactional
    void createContrato() throws Exception {
        int databaseSizeBeforeCreate = contratoRepository.findAll().size();
        // Create the Contrato
        restContratoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(contrato)))
            .andExpect(status().isCreated());

        // Validate the Contrato in the database
        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeCreate + 1);
        Contrato testContrato = contratoList.get(contratoList.size() - 1);
        assertThat(testContrato.getCodigo()).isEqualTo(DEFAULT_CODIGO);
        assertThat(testContrato.getValor()).isEqualByComparingTo(DEFAULT_VALOR);
        assertThat(testContrato.getInicioPrevisto()).isEqualTo(DEFAULT_INICIO_PREVISTO);
        assertThat(testContrato.getTerminoPrevisto()).isEqualTo(DEFAULT_TERMINO_PREVISTO);
        assertThat(testContrato.getInicioEfectivo()).isEqualTo(DEFAULT_INICIO_EFECTIVO);
        assertThat(testContrato.getTerminoEfectivo()).isEqualTo(DEFAULT_TERMINO_EFECTIVO);
        assertThat(testContrato.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
        assertThat(testContrato.getDataRegisto()).isEqualTo(DEFAULT_DATA_REGISTO);
    }

    @Test
    @Transactional
    void createContratoWithExistingId() throws Exception {
        // Create the Contrato with an existing ID
        contrato.setId(1L);

        int databaseSizeBeforeCreate = contratoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restContratoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(contrato)))
            .andExpect(status().isBadRequest());

        // Validate the Contrato in the database
        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkValorIsRequired() throws Exception {
        int databaseSizeBeforeTest = contratoRepository.findAll().size();
        // set the field null
        contrato.setValor(null);

        // Create the Contrato, which fails.

        restContratoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(contrato)))
            .andExpect(status().isBadRequest());

        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkInicioPrevistoIsRequired() throws Exception {
        int databaseSizeBeforeTest = contratoRepository.findAll().size();
        // set the field null
        contrato.setInicioPrevisto(null);

        // Create the Contrato, which fails.

        restContratoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(contrato)))
            .andExpect(status().isBadRequest());

        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTerminoPrevistoIsRequired() throws Exception {
        int databaseSizeBeforeTest = contratoRepository.findAll().size();
        // set the field null
        contrato.setTerminoPrevisto(null);

        // Create the Contrato, which fails.

        restContratoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(contrato)))
            .andExpect(status().isBadRequest());

        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkInicioEfectivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = contratoRepository.findAll().size();
        // set the field null
        contrato.setInicioEfectivo(null);

        // Create the Contrato, which fails.

        restContratoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(contrato)))
            .andExpect(status().isBadRequest());

        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTerminoEfectivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = contratoRepository.findAll().size();
        // set the field null
        contrato.setTerminoEfectivo(null);

        // Create the Contrato, which fails.

        restContratoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(contrato)))
            .andExpect(status().isBadRequest());

        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllContratoes() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList
        restContratoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contrato.getId().intValue())))
            .andExpect(jsonPath("$.[*].codigo").value(hasItem(DEFAULT_CODIGO)))
            .andExpect(jsonPath("$.[*].valor").value(hasItem(sameNumber(DEFAULT_VALOR))))
            .andExpect(jsonPath("$.[*].inicioPrevisto").value(hasItem(DEFAULT_INICIO_PREVISTO.toString())))
            .andExpect(jsonPath("$.[*].terminoPrevisto").value(hasItem(DEFAULT_TERMINO_PREVISTO.toString())))
            .andExpect(jsonPath("$.[*].inicioEfectivo").value(hasItem(DEFAULT_INICIO_EFECTIVO.toString())))
            .andExpect(jsonPath("$.[*].terminoEfectivo").value(hasItem(DEFAULT_TERMINO_EFECTIVO.toString())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)))
            .andExpect(jsonPath("$.[*].dataRegisto").value(hasItem(DEFAULT_DATA_REGISTO.toString())));
    }

    @Test
    @Transactional
    void getContrato() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get the contrato
        restContratoMockMvc
            .perform(get(ENTITY_API_URL_ID, contrato.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contrato.getId().intValue()))
            .andExpect(jsonPath("$.codigo").value(DEFAULT_CODIGO))
            .andExpect(jsonPath("$.valor").value(sameNumber(DEFAULT_VALOR)))
            .andExpect(jsonPath("$.inicioPrevisto").value(DEFAULT_INICIO_PREVISTO.toString()))
            .andExpect(jsonPath("$.terminoPrevisto").value(DEFAULT_TERMINO_PREVISTO.toString()))
            .andExpect(jsonPath("$.inicioEfectivo").value(DEFAULT_INICIO_EFECTIVO.toString()))
            .andExpect(jsonPath("$.terminoEfectivo").value(DEFAULT_TERMINO_EFECTIVO.toString()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO))
            .andExpect(jsonPath("$.dataRegisto").value(DEFAULT_DATA_REGISTO.toString()));
    }

    @Test
    @Transactional
    void getContratoesByIdFiltering() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        Long id = contrato.getId();

        defaultContratoShouldBeFound("id.equals=" + id);
        defaultContratoShouldNotBeFound("id.notEquals=" + id);

        defaultContratoShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContratoShouldNotBeFound("id.greaterThan=" + id);

        defaultContratoShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContratoShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllContratoesByCodigoIsEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where codigo equals to DEFAULT_CODIGO
        defaultContratoShouldBeFound("codigo.equals=" + DEFAULT_CODIGO);

        // Get all the contratoList where codigo equals to UPDATED_CODIGO
        defaultContratoShouldNotBeFound("codigo.equals=" + UPDATED_CODIGO);
    }

    @Test
    @Transactional
    void getAllContratoesByCodigoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where codigo not equals to DEFAULT_CODIGO
        defaultContratoShouldNotBeFound("codigo.notEquals=" + DEFAULT_CODIGO);

        // Get all the contratoList where codigo not equals to UPDATED_CODIGO
        defaultContratoShouldBeFound("codigo.notEquals=" + UPDATED_CODIGO);
    }

    @Test
    @Transactional
    void getAllContratoesByCodigoIsInShouldWork() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where codigo in DEFAULT_CODIGO or UPDATED_CODIGO
        defaultContratoShouldBeFound("codigo.in=" + DEFAULT_CODIGO + "," + UPDATED_CODIGO);

        // Get all the contratoList where codigo equals to UPDATED_CODIGO
        defaultContratoShouldNotBeFound("codigo.in=" + UPDATED_CODIGO);
    }

    @Test
    @Transactional
    void getAllContratoesByCodigoIsNullOrNotNull() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where codigo is not null
        defaultContratoShouldBeFound("codigo.specified=true");

        // Get all the contratoList where codigo is null
        defaultContratoShouldNotBeFound("codigo.specified=false");
    }

    @Test
    @Transactional
    void getAllContratoesByCodigoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where codigo is greater than or equal to DEFAULT_CODIGO
        defaultContratoShouldBeFound("codigo.greaterThanOrEqual=" + DEFAULT_CODIGO);

        // Get all the contratoList where codigo is greater than or equal to UPDATED_CODIGO
        defaultContratoShouldNotBeFound("codigo.greaterThanOrEqual=" + UPDATED_CODIGO);
    }

    @Test
    @Transactional
    void getAllContratoesByCodigoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where codigo is less than or equal to DEFAULT_CODIGO
        defaultContratoShouldBeFound("codigo.lessThanOrEqual=" + DEFAULT_CODIGO);

        // Get all the contratoList where codigo is less than or equal to SMALLER_CODIGO
        defaultContratoShouldNotBeFound("codigo.lessThanOrEqual=" + SMALLER_CODIGO);
    }

    @Test
    @Transactional
    void getAllContratoesByCodigoIsLessThanSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where codigo is less than DEFAULT_CODIGO
        defaultContratoShouldNotBeFound("codigo.lessThan=" + DEFAULT_CODIGO);

        // Get all the contratoList where codigo is less than UPDATED_CODIGO
        defaultContratoShouldBeFound("codigo.lessThan=" + UPDATED_CODIGO);
    }

    @Test
    @Transactional
    void getAllContratoesByCodigoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where codigo is greater than DEFAULT_CODIGO
        defaultContratoShouldNotBeFound("codigo.greaterThan=" + DEFAULT_CODIGO);

        // Get all the contratoList where codigo is greater than SMALLER_CODIGO
        defaultContratoShouldBeFound("codigo.greaterThan=" + SMALLER_CODIGO);
    }

    @Test
    @Transactional
    void getAllContratoesByValorIsEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where valor equals to DEFAULT_VALOR
        defaultContratoShouldBeFound("valor.equals=" + DEFAULT_VALOR);

        // Get all the contratoList where valor equals to UPDATED_VALOR
        defaultContratoShouldNotBeFound("valor.equals=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllContratoesByValorIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where valor not equals to DEFAULT_VALOR
        defaultContratoShouldNotBeFound("valor.notEquals=" + DEFAULT_VALOR);

        // Get all the contratoList where valor not equals to UPDATED_VALOR
        defaultContratoShouldBeFound("valor.notEquals=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllContratoesByValorIsInShouldWork() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where valor in DEFAULT_VALOR or UPDATED_VALOR
        defaultContratoShouldBeFound("valor.in=" + DEFAULT_VALOR + "," + UPDATED_VALOR);

        // Get all the contratoList where valor equals to UPDATED_VALOR
        defaultContratoShouldNotBeFound("valor.in=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllContratoesByValorIsNullOrNotNull() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where valor is not null
        defaultContratoShouldBeFound("valor.specified=true");

        // Get all the contratoList where valor is null
        defaultContratoShouldNotBeFound("valor.specified=false");
    }

    @Test
    @Transactional
    void getAllContratoesByValorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where valor is greater than or equal to DEFAULT_VALOR
        defaultContratoShouldBeFound("valor.greaterThanOrEqual=" + DEFAULT_VALOR);

        // Get all the contratoList where valor is greater than or equal to UPDATED_VALOR
        defaultContratoShouldNotBeFound("valor.greaterThanOrEqual=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllContratoesByValorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where valor is less than or equal to DEFAULT_VALOR
        defaultContratoShouldBeFound("valor.lessThanOrEqual=" + DEFAULT_VALOR);

        // Get all the contratoList where valor is less than or equal to SMALLER_VALOR
        defaultContratoShouldNotBeFound("valor.lessThanOrEqual=" + SMALLER_VALOR);
    }

    @Test
    @Transactional
    void getAllContratoesByValorIsLessThanSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where valor is less than DEFAULT_VALOR
        defaultContratoShouldNotBeFound("valor.lessThan=" + DEFAULT_VALOR);

        // Get all the contratoList where valor is less than UPDATED_VALOR
        defaultContratoShouldBeFound("valor.lessThan=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllContratoesByValorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where valor is greater than DEFAULT_VALOR
        defaultContratoShouldNotBeFound("valor.greaterThan=" + DEFAULT_VALOR);

        // Get all the contratoList where valor is greater than SMALLER_VALOR
        defaultContratoShouldBeFound("valor.greaterThan=" + SMALLER_VALOR);
    }

    @Test
    @Transactional
    void getAllContratoesByInicioPrevistoIsEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioPrevisto equals to DEFAULT_INICIO_PREVISTO
        defaultContratoShouldBeFound("inicioPrevisto.equals=" + DEFAULT_INICIO_PREVISTO);

        // Get all the contratoList where inicioPrevisto equals to UPDATED_INICIO_PREVISTO
        defaultContratoShouldNotBeFound("inicioPrevisto.equals=" + UPDATED_INICIO_PREVISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByInicioPrevistoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioPrevisto not equals to DEFAULT_INICIO_PREVISTO
        defaultContratoShouldNotBeFound("inicioPrevisto.notEquals=" + DEFAULT_INICIO_PREVISTO);

        // Get all the contratoList where inicioPrevisto not equals to UPDATED_INICIO_PREVISTO
        defaultContratoShouldBeFound("inicioPrevisto.notEquals=" + UPDATED_INICIO_PREVISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByInicioPrevistoIsInShouldWork() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioPrevisto in DEFAULT_INICIO_PREVISTO or UPDATED_INICIO_PREVISTO
        defaultContratoShouldBeFound("inicioPrevisto.in=" + DEFAULT_INICIO_PREVISTO + "," + UPDATED_INICIO_PREVISTO);

        // Get all the contratoList where inicioPrevisto equals to UPDATED_INICIO_PREVISTO
        defaultContratoShouldNotBeFound("inicioPrevisto.in=" + UPDATED_INICIO_PREVISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByInicioPrevistoIsNullOrNotNull() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioPrevisto is not null
        defaultContratoShouldBeFound("inicioPrevisto.specified=true");

        // Get all the contratoList where inicioPrevisto is null
        defaultContratoShouldNotBeFound("inicioPrevisto.specified=false");
    }

    @Test
    @Transactional
    void getAllContratoesByInicioPrevistoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioPrevisto is greater than or equal to DEFAULT_INICIO_PREVISTO
        defaultContratoShouldBeFound("inicioPrevisto.greaterThanOrEqual=" + DEFAULT_INICIO_PREVISTO);

        // Get all the contratoList where inicioPrevisto is greater than or equal to UPDATED_INICIO_PREVISTO
        defaultContratoShouldNotBeFound("inicioPrevisto.greaterThanOrEqual=" + UPDATED_INICIO_PREVISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByInicioPrevistoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioPrevisto is less than or equal to DEFAULT_INICIO_PREVISTO
        defaultContratoShouldBeFound("inicioPrevisto.lessThanOrEqual=" + DEFAULT_INICIO_PREVISTO);

        // Get all the contratoList where inicioPrevisto is less than or equal to SMALLER_INICIO_PREVISTO
        defaultContratoShouldNotBeFound("inicioPrevisto.lessThanOrEqual=" + SMALLER_INICIO_PREVISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByInicioPrevistoIsLessThanSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioPrevisto is less than DEFAULT_INICIO_PREVISTO
        defaultContratoShouldNotBeFound("inicioPrevisto.lessThan=" + DEFAULT_INICIO_PREVISTO);

        // Get all the contratoList where inicioPrevisto is less than UPDATED_INICIO_PREVISTO
        defaultContratoShouldBeFound("inicioPrevisto.lessThan=" + UPDATED_INICIO_PREVISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByInicioPrevistoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioPrevisto is greater than DEFAULT_INICIO_PREVISTO
        defaultContratoShouldNotBeFound("inicioPrevisto.greaterThan=" + DEFAULT_INICIO_PREVISTO);

        // Get all the contratoList where inicioPrevisto is greater than SMALLER_INICIO_PREVISTO
        defaultContratoShouldBeFound("inicioPrevisto.greaterThan=" + SMALLER_INICIO_PREVISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoPrevistoIsEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoPrevisto equals to DEFAULT_TERMINO_PREVISTO
        defaultContratoShouldBeFound("terminoPrevisto.equals=" + DEFAULT_TERMINO_PREVISTO);

        // Get all the contratoList where terminoPrevisto equals to UPDATED_TERMINO_PREVISTO
        defaultContratoShouldNotBeFound("terminoPrevisto.equals=" + UPDATED_TERMINO_PREVISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoPrevistoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoPrevisto not equals to DEFAULT_TERMINO_PREVISTO
        defaultContratoShouldNotBeFound("terminoPrevisto.notEquals=" + DEFAULT_TERMINO_PREVISTO);

        // Get all the contratoList where terminoPrevisto not equals to UPDATED_TERMINO_PREVISTO
        defaultContratoShouldBeFound("terminoPrevisto.notEquals=" + UPDATED_TERMINO_PREVISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoPrevistoIsInShouldWork() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoPrevisto in DEFAULT_TERMINO_PREVISTO or UPDATED_TERMINO_PREVISTO
        defaultContratoShouldBeFound("terminoPrevisto.in=" + DEFAULT_TERMINO_PREVISTO + "," + UPDATED_TERMINO_PREVISTO);

        // Get all the contratoList where terminoPrevisto equals to UPDATED_TERMINO_PREVISTO
        defaultContratoShouldNotBeFound("terminoPrevisto.in=" + UPDATED_TERMINO_PREVISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoPrevistoIsNullOrNotNull() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoPrevisto is not null
        defaultContratoShouldBeFound("terminoPrevisto.specified=true");

        // Get all the contratoList where terminoPrevisto is null
        defaultContratoShouldNotBeFound("terminoPrevisto.specified=false");
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoPrevistoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoPrevisto is greater than or equal to DEFAULT_TERMINO_PREVISTO
        defaultContratoShouldBeFound("terminoPrevisto.greaterThanOrEqual=" + DEFAULT_TERMINO_PREVISTO);

        // Get all the contratoList where terminoPrevisto is greater than or equal to UPDATED_TERMINO_PREVISTO
        defaultContratoShouldNotBeFound("terminoPrevisto.greaterThanOrEqual=" + UPDATED_TERMINO_PREVISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoPrevistoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoPrevisto is less than or equal to DEFAULT_TERMINO_PREVISTO
        defaultContratoShouldBeFound("terminoPrevisto.lessThanOrEqual=" + DEFAULT_TERMINO_PREVISTO);

        // Get all the contratoList where terminoPrevisto is less than or equal to SMALLER_TERMINO_PREVISTO
        defaultContratoShouldNotBeFound("terminoPrevisto.lessThanOrEqual=" + SMALLER_TERMINO_PREVISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoPrevistoIsLessThanSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoPrevisto is less than DEFAULT_TERMINO_PREVISTO
        defaultContratoShouldNotBeFound("terminoPrevisto.lessThan=" + DEFAULT_TERMINO_PREVISTO);

        // Get all the contratoList where terminoPrevisto is less than UPDATED_TERMINO_PREVISTO
        defaultContratoShouldBeFound("terminoPrevisto.lessThan=" + UPDATED_TERMINO_PREVISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoPrevistoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoPrevisto is greater than DEFAULT_TERMINO_PREVISTO
        defaultContratoShouldNotBeFound("terminoPrevisto.greaterThan=" + DEFAULT_TERMINO_PREVISTO);

        // Get all the contratoList where terminoPrevisto is greater than SMALLER_TERMINO_PREVISTO
        defaultContratoShouldBeFound("terminoPrevisto.greaterThan=" + SMALLER_TERMINO_PREVISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByInicioEfectivoIsEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioEfectivo equals to DEFAULT_INICIO_EFECTIVO
        defaultContratoShouldBeFound("inicioEfectivo.equals=" + DEFAULT_INICIO_EFECTIVO);

        // Get all the contratoList where inicioEfectivo equals to UPDATED_INICIO_EFECTIVO
        defaultContratoShouldNotBeFound("inicioEfectivo.equals=" + UPDATED_INICIO_EFECTIVO);
    }

    @Test
    @Transactional
    void getAllContratoesByInicioEfectivoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioEfectivo not equals to DEFAULT_INICIO_EFECTIVO
        defaultContratoShouldNotBeFound("inicioEfectivo.notEquals=" + DEFAULT_INICIO_EFECTIVO);

        // Get all the contratoList where inicioEfectivo not equals to UPDATED_INICIO_EFECTIVO
        defaultContratoShouldBeFound("inicioEfectivo.notEquals=" + UPDATED_INICIO_EFECTIVO);
    }

    @Test
    @Transactional
    void getAllContratoesByInicioEfectivoIsInShouldWork() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioEfectivo in DEFAULT_INICIO_EFECTIVO or UPDATED_INICIO_EFECTIVO
        defaultContratoShouldBeFound("inicioEfectivo.in=" + DEFAULT_INICIO_EFECTIVO + "," + UPDATED_INICIO_EFECTIVO);

        // Get all the contratoList where inicioEfectivo equals to UPDATED_INICIO_EFECTIVO
        defaultContratoShouldNotBeFound("inicioEfectivo.in=" + UPDATED_INICIO_EFECTIVO);
    }

    @Test
    @Transactional
    void getAllContratoesByInicioEfectivoIsNullOrNotNull() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioEfectivo is not null
        defaultContratoShouldBeFound("inicioEfectivo.specified=true");

        // Get all the contratoList where inicioEfectivo is null
        defaultContratoShouldNotBeFound("inicioEfectivo.specified=false");
    }

    @Test
    @Transactional
    void getAllContratoesByInicioEfectivoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioEfectivo is greater than or equal to DEFAULT_INICIO_EFECTIVO
        defaultContratoShouldBeFound("inicioEfectivo.greaterThanOrEqual=" + DEFAULT_INICIO_EFECTIVO);

        // Get all the contratoList where inicioEfectivo is greater than or equal to UPDATED_INICIO_EFECTIVO
        defaultContratoShouldNotBeFound("inicioEfectivo.greaterThanOrEqual=" + UPDATED_INICIO_EFECTIVO);
    }

    @Test
    @Transactional
    void getAllContratoesByInicioEfectivoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioEfectivo is less than or equal to DEFAULT_INICIO_EFECTIVO
        defaultContratoShouldBeFound("inicioEfectivo.lessThanOrEqual=" + DEFAULT_INICIO_EFECTIVO);

        // Get all the contratoList where inicioEfectivo is less than or equal to SMALLER_INICIO_EFECTIVO
        defaultContratoShouldNotBeFound("inicioEfectivo.lessThanOrEqual=" + SMALLER_INICIO_EFECTIVO);
    }

    @Test
    @Transactional
    void getAllContratoesByInicioEfectivoIsLessThanSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioEfectivo is less than DEFAULT_INICIO_EFECTIVO
        defaultContratoShouldNotBeFound("inicioEfectivo.lessThan=" + DEFAULT_INICIO_EFECTIVO);

        // Get all the contratoList where inicioEfectivo is less than UPDATED_INICIO_EFECTIVO
        defaultContratoShouldBeFound("inicioEfectivo.lessThan=" + UPDATED_INICIO_EFECTIVO);
    }

    @Test
    @Transactional
    void getAllContratoesByInicioEfectivoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where inicioEfectivo is greater than DEFAULT_INICIO_EFECTIVO
        defaultContratoShouldNotBeFound("inicioEfectivo.greaterThan=" + DEFAULT_INICIO_EFECTIVO);

        // Get all the contratoList where inicioEfectivo is greater than SMALLER_INICIO_EFECTIVO
        defaultContratoShouldBeFound("inicioEfectivo.greaterThan=" + SMALLER_INICIO_EFECTIVO);
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoEfectivoIsEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoEfectivo equals to DEFAULT_TERMINO_EFECTIVO
        defaultContratoShouldBeFound("terminoEfectivo.equals=" + DEFAULT_TERMINO_EFECTIVO);

        // Get all the contratoList where terminoEfectivo equals to UPDATED_TERMINO_EFECTIVO
        defaultContratoShouldNotBeFound("terminoEfectivo.equals=" + UPDATED_TERMINO_EFECTIVO);
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoEfectivoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoEfectivo not equals to DEFAULT_TERMINO_EFECTIVO
        defaultContratoShouldNotBeFound("terminoEfectivo.notEquals=" + DEFAULT_TERMINO_EFECTIVO);

        // Get all the contratoList where terminoEfectivo not equals to UPDATED_TERMINO_EFECTIVO
        defaultContratoShouldBeFound("terminoEfectivo.notEquals=" + UPDATED_TERMINO_EFECTIVO);
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoEfectivoIsInShouldWork() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoEfectivo in DEFAULT_TERMINO_EFECTIVO or UPDATED_TERMINO_EFECTIVO
        defaultContratoShouldBeFound("terminoEfectivo.in=" + DEFAULT_TERMINO_EFECTIVO + "," + UPDATED_TERMINO_EFECTIVO);

        // Get all the contratoList where terminoEfectivo equals to UPDATED_TERMINO_EFECTIVO
        defaultContratoShouldNotBeFound("terminoEfectivo.in=" + UPDATED_TERMINO_EFECTIVO);
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoEfectivoIsNullOrNotNull() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoEfectivo is not null
        defaultContratoShouldBeFound("terminoEfectivo.specified=true");

        // Get all the contratoList where terminoEfectivo is null
        defaultContratoShouldNotBeFound("terminoEfectivo.specified=false");
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoEfectivoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoEfectivo is greater than or equal to DEFAULT_TERMINO_EFECTIVO
        defaultContratoShouldBeFound("terminoEfectivo.greaterThanOrEqual=" + DEFAULT_TERMINO_EFECTIVO);

        // Get all the contratoList where terminoEfectivo is greater than or equal to UPDATED_TERMINO_EFECTIVO
        defaultContratoShouldNotBeFound("terminoEfectivo.greaterThanOrEqual=" + UPDATED_TERMINO_EFECTIVO);
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoEfectivoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoEfectivo is less than or equal to DEFAULT_TERMINO_EFECTIVO
        defaultContratoShouldBeFound("terminoEfectivo.lessThanOrEqual=" + DEFAULT_TERMINO_EFECTIVO);

        // Get all the contratoList where terminoEfectivo is less than or equal to SMALLER_TERMINO_EFECTIVO
        defaultContratoShouldNotBeFound("terminoEfectivo.lessThanOrEqual=" + SMALLER_TERMINO_EFECTIVO);
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoEfectivoIsLessThanSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoEfectivo is less than DEFAULT_TERMINO_EFECTIVO
        defaultContratoShouldNotBeFound("terminoEfectivo.lessThan=" + DEFAULT_TERMINO_EFECTIVO);

        // Get all the contratoList where terminoEfectivo is less than UPDATED_TERMINO_EFECTIVO
        defaultContratoShouldBeFound("terminoEfectivo.lessThan=" + UPDATED_TERMINO_EFECTIVO);
    }

    @Test
    @Transactional
    void getAllContratoesByTerminoEfectivoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where terminoEfectivo is greater than DEFAULT_TERMINO_EFECTIVO
        defaultContratoShouldNotBeFound("terminoEfectivo.greaterThan=" + DEFAULT_TERMINO_EFECTIVO);

        // Get all the contratoList where terminoEfectivo is greater than SMALLER_TERMINO_EFECTIVO
        defaultContratoShouldBeFound("terminoEfectivo.greaterThan=" + SMALLER_TERMINO_EFECTIVO);
    }

    @Test
    @Transactional
    void getAllContratoesByDescricaoIsEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where descricao equals to DEFAULT_DESCRICAO
        defaultContratoShouldBeFound("descricao.equals=" + DEFAULT_DESCRICAO);

        // Get all the contratoList where descricao equals to UPDATED_DESCRICAO
        defaultContratoShouldNotBeFound("descricao.equals=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllContratoesByDescricaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where descricao not equals to DEFAULT_DESCRICAO
        defaultContratoShouldNotBeFound("descricao.notEquals=" + DEFAULT_DESCRICAO);

        // Get all the contratoList where descricao not equals to UPDATED_DESCRICAO
        defaultContratoShouldBeFound("descricao.notEquals=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllContratoesByDescricaoIsInShouldWork() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where descricao in DEFAULT_DESCRICAO or UPDATED_DESCRICAO
        defaultContratoShouldBeFound("descricao.in=" + DEFAULT_DESCRICAO + "," + UPDATED_DESCRICAO);

        // Get all the contratoList where descricao equals to UPDATED_DESCRICAO
        defaultContratoShouldNotBeFound("descricao.in=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllContratoesByDescricaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where descricao is not null
        defaultContratoShouldBeFound("descricao.specified=true");

        // Get all the contratoList where descricao is null
        defaultContratoShouldNotBeFound("descricao.specified=false");
    }

    @Test
    @Transactional
    void getAllContratoesByDescricaoContainsSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where descricao contains DEFAULT_DESCRICAO
        defaultContratoShouldBeFound("descricao.contains=" + DEFAULT_DESCRICAO);

        // Get all the contratoList where descricao contains UPDATED_DESCRICAO
        defaultContratoShouldNotBeFound("descricao.contains=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllContratoesByDescricaoNotContainsSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where descricao does not contain DEFAULT_DESCRICAO
        defaultContratoShouldNotBeFound("descricao.doesNotContain=" + DEFAULT_DESCRICAO);

        // Get all the contratoList where descricao does not contain UPDATED_DESCRICAO
        defaultContratoShouldBeFound("descricao.doesNotContain=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllContratoesByDataRegistoIsEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where dataRegisto equals to DEFAULT_DATA_REGISTO
        defaultContratoShouldBeFound("dataRegisto.equals=" + DEFAULT_DATA_REGISTO);

        // Get all the contratoList where dataRegisto equals to UPDATED_DATA_REGISTO
        defaultContratoShouldNotBeFound("dataRegisto.equals=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByDataRegistoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where dataRegisto not equals to DEFAULT_DATA_REGISTO
        defaultContratoShouldNotBeFound("dataRegisto.notEquals=" + DEFAULT_DATA_REGISTO);

        // Get all the contratoList where dataRegisto not equals to UPDATED_DATA_REGISTO
        defaultContratoShouldBeFound("dataRegisto.notEquals=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByDataRegistoIsInShouldWork() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where dataRegisto in DEFAULT_DATA_REGISTO or UPDATED_DATA_REGISTO
        defaultContratoShouldBeFound("dataRegisto.in=" + DEFAULT_DATA_REGISTO + "," + UPDATED_DATA_REGISTO);

        // Get all the contratoList where dataRegisto equals to UPDATED_DATA_REGISTO
        defaultContratoShouldNotBeFound("dataRegisto.in=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByDataRegistoIsNullOrNotNull() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where dataRegisto is not null
        defaultContratoShouldBeFound("dataRegisto.specified=true");

        // Get all the contratoList where dataRegisto is null
        defaultContratoShouldNotBeFound("dataRegisto.specified=false");
    }

    @Test
    @Transactional
    void getAllContratoesByDataRegistoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where dataRegisto is greater than or equal to DEFAULT_DATA_REGISTO
        defaultContratoShouldBeFound("dataRegisto.greaterThanOrEqual=" + DEFAULT_DATA_REGISTO);

        // Get all the contratoList where dataRegisto is greater than or equal to UPDATED_DATA_REGISTO
        defaultContratoShouldNotBeFound("dataRegisto.greaterThanOrEqual=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByDataRegistoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where dataRegisto is less than or equal to DEFAULT_DATA_REGISTO
        defaultContratoShouldBeFound("dataRegisto.lessThanOrEqual=" + DEFAULT_DATA_REGISTO);

        // Get all the contratoList where dataRegisto is less than or equal to SMALLER_DATA_REGISTO
        defaultContratoShouldNotBeFound("dataRegisto.lessThanOrEqual=" + SMALLER_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByDataRegistoIsLessThanSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where dataRegisto is less than DEFAULT_DATA_REGISTO
        defaultContratoShouldNotBeFound("dataRegisto.lessThan=" + DEFAULT_DATA_REGISTO);

        // Get all the contratoList where dataRegisto is less than UPDATED_DATA_REGISTO
        defaultContratoShouldBeFound("dataRegisto.lessThan=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByDataRegistoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        // Get all the contratoList where dataRegisto is greater than DEFAULT_DATA_REGISTO
        defaultContratoShouldNotBeFound("dataRegisto.greaterThan=" + DEFAULT_DATA_REGISTO);

        // Get all the contratoList where dataRegisto is greater than SMALLER_DATA_REGISTO
        defaultContratoShouldBeFound("dataRegisto.greaterThan=" + SMALLER_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllContratoesByPropostaFinanceiraIsEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);
        PropostaFinanceira propostaFinanceira;
        if (TestUtil.findAll(em, PropostaFinanceira.class).isEmpty()) {
            propostaFinanceira = PropostaFinanceiraResourceIT.createEntity(em);
            em.persist(propostaFinanceira);
            em.flush();
        } else {
            propostaFinanceira = TestUtil.findAll(em, PropostaFinanceira.class).get(0);
        }
        em.persist(propostaFinanceira);
        em.flush();
        contrato.setPropostaFinanceira(propostaFinanceira);
        contratoRepository.saveAndFlush(contrato);
        Long propostaFinanceiraId = propostaFinanceira.getId();

        // Get all the contratoList where propostaFinanceira equals to propostaFinanceiraId
        defaultContratoShouldBeFound("propostaFinanceiraId.equals=" + propostaFinanceiraId);

        // Get all the contratoList where propostaFinanceira equals to (propostaFinanceiraId + 1)
        defaultContratoShouldNotBeFound("propostaFinanceiraId.equals=" + (propostaFinanceiraId + 1));
    }

    @Test
    @Transactional
    void getAllContratoesByLinhaOrcamentoIsEqualToSomething() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);
        LinhaOrcamento linhaOrcamento;
        if (TestUtil.findAll(em, LinhaOrcamento.class).isEmpty()) {
            linhaOrcamento = LinhaOrcamentoResourceIT.createEntity(em);
            em.persist(linhaOrcamento);
            em.flush();
        } else {
            linhaOrcamento = TestUtil.findAll(em, LinhaOrcamento.class).get(0);
        }
        em.persist(linhaOrcamento);
        em.flush();
        contrato.setLinhaOrcamento(linhaOrcamento);
        contratoRepository.saveAndFlush(contrato);
        Long linhaOrcamentoId = linhaOrcamento.getId();

        // Get all the contratoList where linhaOrcamento equals to linhaOrcamentoId
        defaultContratoShouldBeFound("linhaOrcamentoId.equals=" + linhaOrcamentoId);

        // Get all the contratoList where linhaOrcamento equals to (linhaOrcamentoId + 1)
        defaultContratoShouldNotBeFound("linhaOrcamentoId.equals=" + (linhaOrcamentoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContratoShouldBeFound(String filter) throws Exception {
        restContratoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contrato.getId().intValue())))
            .andExpect(jsonPath("$.[*].codigo").value(hasItem(DEFAULT_CODIGO)))
            .andExpect(jsonPath("$.[*].valor").value(hasItem(sameNumber(DEFAULT_VALOR))))
            .andExpect(jsonPath("$.[*].inicioPrevisto").value(hasItem(DEFAULT_INICIO_PREVISTO.toString())))
            .andExpect(jsonPath("$.[*].terminoPrevisto").value(hasItem(DEFAULT_TERMINO_PREVISTO.toString())))
            .andExpect(jsonPath("$.[*].inicioEfectivo").value(hasItem(DEFAULT_INICIO_EFECTIVO.toString())))
            .andExpect(jsonPath("$.[*].terminoEfectivo").value(hasItem(DEFAULT_TERMINO_EFECTIVO.toString())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)))
            .andExpect(jsonPath("$.[*].dataRegisto").value(hasItem(DEFAULT_DATA_REGISTO.toString())));

        // Check, that the count call also returns 1
        restContratoMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContratoShouldNotBeFound(String filter) throws Exception {
        restContratoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContratoMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingContrato() throws Exception {
        // Get the contrato
        restContratoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewContrato() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        int databaseSizeBeforeUpdate = contratoRepository.findAll().size();

        // Update the contrato
        Contrato updatedContrato = contratoRepository.findById(contrato.getId()).get();
        // Disconnect from session so that the updates on updatedContrato are not directly saved in db
        em.detach(updatedContrato);
        updatedContrato
            .codigo(UPDATED_CODIGO)
            .valor(UPDATED_VALOR)
            .inicioPrevisto(UPDATED_INICIO_PREVISTO)
            .terminoPrevisto(UPDATED_TERMINO_PREVISTO)
            .inicioEfectivo(UPDATED_INICIO_EFECTIVO)
            .terminoEfectivo(UPDATED_TERMINO_EFECTIVO)
            .descricao(UPDATED_DESCRICAO)
            .dataRegisto(UPDATED_DATA_REGISTO);

        restContratoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedContrato.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedContrato))
            )
            .andExpect(status().isOk());

        // Validate the Contrato in the database
        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeUpdate);
        Contrato testContrato = contratoList.get(contratoList.size() - 1);
        assertThat(testContrato.getCodigo()).isEqualTo(UPDATED_CODIGO);
        assertThat(testContrato.getValor()).isEqualTo(UPDATED_VALOR);
        assertThat(testContrato.getInicioPrevisto()).isEqualTo(UPDATED_INICIO_PREVISTO);
        assertThat(testContrato.getTerminoPrevisto()).isEqualTo(UPDATED_TERMINO_PREVISTO);
        assertThat(testContrato.getInicioEfectivo()).isEqualTo(UPDATED_INICIO_EFECTIVO);
        assertThat(testContrato.getTerminoEfectivo()).isEqualTo(UPDATED_TERMINO_EFECTIVO);
        assertThat(testContrato.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testContrato.getDataRegisto()).isEqualTo(UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void putNonExistingContrato() throws Exception {
        int databaseSizeBeforeUpdate = contratoRepository.findAll().size();
        contrato.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContratoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, contrato.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(contrato))
            )
            .andExpect(status().isBadRequest());

        // Validate the Contrato in the database
        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchContrato() throws Exception {
        int databaseSizeBeforeUpdate = contratoRepository.findAll().size();
        contrato.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restContratoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(contrato))
            )
            .andExpect(status().isBadRequest());

        // Validate the Contrato in the database
        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamContrato() throws Exception {
        int databaseSizeBeforeUpdate = contratoRepository.findAll().size();
        contrato.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restContratoMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(contrato)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Contrato in the database
        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateContratoWithPatch() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        int databaseSizeBeforeUpdate = contratoRepository.findAll().size();

        // Update the contrato using partial update
        Contrato partialUpdatedContrato = new Contrato();
        partialUpdatedContrato.setId(contrato.getId());

        partialUpdatedContrato
            .terminoPrevisto(UPDATED_TERMINO_PREVISTO)
            .inicioEfectivo(UPDATED_INICIO_EFECTIVO)
            .descricao(UPDATED_DESCRICAO)
            .dataRegisto(UPDATED_DATA_REGISTO);

        restContratoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedContrato.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedContrato))
            )
            .andExpect(status().isOk());

        // Validate the Contrato in the database
        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeUpdate);
        Contrato testContrato = contratoList.get(contratoList.size() - 1);
        assertThat(testContrato.getCodigo()).isEqualTo(DEFAULT_CODIGO);
        assertThat(testContrato.getValor()).isEqualByComparingTo(DEFAULT_VALOR);
        assertThat(testContrato.getInicioPrevisto()).isEqualTo(DEFAULT_INICIO_PREVISTO);
        assertThat(testContrato.getTerminoPrevisto()).isEqualTo(UPDATED_TERMINO_PREVISTO);
        assertThat(testContrato.getInicioEfectivo()).isEqualTo(UPDATED_INICIO_EFECTIVO);
        assertThat(testContrato.getTerminoEfectivo()).isEqualTo(DEFAULT_TERMINO_EFECTIVO);
        assertThat(testContrato.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testContrato.getDataRegisto()).isEqualTo(UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void fullUpdateContratoWithPatch() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        int databaseSizeBeforeUpdate = contratoRepository.findAll().size();

        // Update the contrato using partial update
        Contrato partialUpdatedContrato = new Contrato();
        partialUpdatedContrato.setId(contrato.getId());

        partialUpdatedContrato
            .codigo(UPDATED_CODIGO)
            .valor(UPDATED_VALOR)
            .inicioPrevisto(UPDATED_INICIO_PREVISTO)
            .terminoPrevisto(UPDATED_TERMINO_PREVISTO)
            .inicioEfectivo(UPDATED_INICIO_EFECTIVO)
            .terminoEfectivo(UPDATED_TERMINO_EFECTIVO)
            .descricao(UPDATED_DESCRICAO)
            .dataRegisto(UPDATED_DATA_REGISTO);

        restContratoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedContrato.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedContrato))
            )
            .andExpect(status().isOk());

        // Validate the Contrato in the database
        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeUpdate);
        Contrato testContrato = contratoList.get(contratoList.size() - 1);
        assertThat(testContrato.getCodigo()).isEqualTo(UPDATED_CODIGO);
        assertThat(testContrato.getValor()).isEqualByComparingTo(UPDATED_VALOR);
        assertThat(testContrato.getInicioPrevisto()).isEqualTo(UPDATED_INICIO_PREVISTO);
        assertThat(testContrato.getTerminoPrevisto()).isEqualTo(UPDATED_TERMINO_PREVISTO);
        assertThat(testContrato.getInicioEfectivo()).isEqualTo(UPDATED_INICIO_EFECTIVO);
        assertThat(testContrato.getTerminoEfectivo()).isEqualTo(UPDATED_TERMINO_EFECTIVO);
        assertThat(testContrato.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testContrato.getDataRegisto()).isEqualTo(UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void patchNonExistingContrato() throws Exception {
        int databaseSizeBeforeUpdate = contratoRepository.findAll().size();
        contrato.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContratoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, contrato.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(contrato))
            )
            .andExpect(status().isBadRequest());

        // Validate the Contrato in the database
        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchContrato() throws Exception {
        int databaseSizeBeforeUpdate = contratoRepository.findAll().size();
        contrato.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restContratoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(contrato))
            )
            .andExpect(status().isBadRequest());

        // Validate the Contrato in the database
        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamContrato() throws Exception {
        int databaseSizeBeforeUpdate = contratoRepository.findAll().size();
        contrato.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restContratoMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(contrato)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Contrato in the database
        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteContrato() throws Exception {
        // Initialize the database
        contratoRepository.saveAndFlush(contrato);

        int databaseSizeBeforeDelete = contratoRepository.findAll().size();

        // Delete the contrato
        restContratoMockMvc
            .perform(delete(ENTITY_API_URL_ID, contrato.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Contrato> contratoList = contratoRepository.findAll();
        assertThat(contratoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
