package mz.co.eaquisicao.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mz.co.eaquisicao.IntegrationTest;
import mz.co.eaquisicao.domain.FaseAquisicao;
import mz.co.eaquisicao.repository.FaseAquisicaoRepository;
import mz.co.eaquisicao.service.criteria.FaseAquisicaoCriteria;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link FaseAquisicaoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class FaseAquisicaoResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final Integer DEFAULT_DIAS_EXECUCAO = 1;
    private static final Integer UPDATED_DIAS_EXECUCAO = 2;
    private static final Integer SMALLER_DIAS_EXECUCAO = 1 - 1;

    private static final Integer DEFAULT_SEQUENCIA = 1;
    private static final Integer UPDATED_SEQUENCIA = 2;
    private static final Integer SMALLER_SEQUENCIA = 1 - 1;

    private static final String ENTITY_API_URL = "/api/fase-aquisicaos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private FaseAquisicaoRepository faseAquisicaoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFaseAquisicaoMockMvc;

    private FaseAquisicao faseAquisicao;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FaseAquisicao createEntity(EntityManager em) {
        FaseAquisicao faseAquisicao = new FaseAquisicao()
            .nome(DEFAULT_NOME)
            .diasExecucao(DEFAULT_DIAS_EXECUCAO)
            .sequencia(DEFAULT_SEQUENCIA);
        return faseAquisicao;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FaseAquisicao createUpdatedEntity(EntityManager em) {
        FaseAquisicao faseAquisicao = new FaseAquisicao()
            .nome(UPDATED_NOME)
            .diasExecucao(UPDATED_DIAS_EXECUCAO)
            .sequencia(UPDATED_SEQUENCIA);
        return faseAquisicao;
    }

    @BeforeEach
    public void initTest() {
        faseAquisicao = createEntity(em);
    }

    @Test
    @Transactional
    void createFaseAquisicao() throws Exception {
        int databaseSizeBeforeCreate = faseAquisicaoRepository.findAll().size();
        // Create the FaseAquisicao
        restFaseAquisicaoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(faseAquisicao)))
            .andExpect(status().isCreated());

        // Validate the FaseAquisicao in the database
        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeCreate + 1);
        FaseAquisicao testFaseAquisicao = faseAquisicaoList.get(faseAquisicaoList.size() - 1);
        assertThat(testFaseAquisicao.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testFaseAquisicao.getDiasExecucao()).isEqualTo(DEFAULT_DIAS_EXECUCAO);
        assertThat(testFaseAquisicao.getSequencia()).isEqualTo(DEFAULT_SEQUENCIA);
    }

    @Test
    @Transactional
    void createFaseAquisicaoWithExistingId() throws Exception {
        // Create the FaseAquisicao with an existing ID
        faseAquisicao.setId(1L);

        int databaseSizeBeforeCreate = faseAquisicaoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restFaseAquisicaoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(faseAquisicao)))
            .andExpect(status().isBadRequest());

        // Validate the FaseAquisicao in the database
        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = faseAquisicaoRepository.findAll().size();
        // set the field null
        faseAquisicao.setNome(null);

        // Create the FaseAquisicao, which fails.

        restFaseAquisicaoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(faseAquisicao)))
            .andExpect(status().isBadRequest());

        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDiasExecucaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = faseAquisicaoRepository.findAll().size();
        // set the field null
        faseAquisicao.setDiasExecucao(null);

        // Create the FaseAquisicao, which fails.

        restFaseAquisicaoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(faseAquisicao)))
            .andExpect(status().isBadRequest());

        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSequenciaIsRequired() throws Exception {
        int databaseSizeBeforeTest = faseAquisicaoRepository.findAll().size();
        // set the field null
        faseAquisicao.setSequencia(null);

        // Create the FaseAquisicao, which fails.

        restFaseAquisicaoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(faseAquisicao)))
            .andExpect(status().isBadRequest());

        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaos() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList
        restFaseAquisicaoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(faseAquisicao.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].diasExecucao").value(hasItem(DEFAULT_DIAS_EXECUCAO)))
            .andExpect(jsonPath("$.[*].sequencia").value(hasItem(DEFAULT_SEQUENCIA)));
    }

    @Test
    @Transactional
    void getFaseAquisicao() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get the faseAquisicao
        restFaseAquisicaoMockMvc
            .perform(get(ENTITY_API_URL_ID, faseAquisicao.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(faseAquisicao.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME))
            .andExpect(jsonPath("$.diasExecucao").value(DEFAULT_DIAS_EXECUCAO))
            .andExpect(jsonPath("$.sequencia").value(DEFAULT_SEQUENCIA));
    }

    @Test
    @Transactional
    void getFaseAquisicaosByIdFiltering() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        Long id = faseAquisicao.getId();

        defaultFaseAquisicaoShouldBeFound("id.equals=" + id);
        defaultFaseAquisicaoShouldNotBeFound("id.notEquals=" + id);

        defaultFaseAquisicaoShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultFaseAquisicaoShouldNotBeFound("id.greaterThan=" + id);

        defaultFaseAquisicaoShouldBeFound("id.lessThanOrEqual=" + id);
        defaultFaseAquisicaoShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosByNomeIsEqualToSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where nome equals to DEFAULT_NOME
        defaultFaseAquisicaoShouldBeFound("nome.equals=" + DEFAULT_NOME);

        // Get all the faseAquisicaoList where nome equals to UPDATED_NOME
        defaultFaseAquisicaoShouldNotBeFound("nome.equals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosByNomeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where nome not equals to DEFAULT_NOME
        defaultFaseAquisicaoShouldNotBeFound("nome.notEquals=" + DEFAULT_NOME);

        // Get all the faseAquisicaoList where nome not equals to UPDATED_NOME
        defaultFaseAquisicaoShouldBeFound("nome.notEquals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosByNomeIsInShouldWork() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where nome in DEFAULT_NOME or UPDATED_NOME
        defaultFaseAquisicaoShouldBeFound("nome.in=" + DEFAULT_NOME + "," + UPDATED_NOME);

        // Get all the faseAquisicaoList where nome equals to UPDATED_NOME
        defaultFaseAquisicaoShouldNotBeFound("nome.in=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosByNomeIsNullOrNotNull() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where nome is not null
        defaultFaseAquisicaoShouldBeFound("nome.specified=true");

        // Get all the faseAquisicaoList where nome is null
        defaultFaseAquisicaoShouldNotBeFound("nome.specified=false");
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosByNomeContainsSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where nome contains DEFAULT_NOME
        defaultFaseAquisicaoShouldBeFound("nome.contains=" + DEFAULT_NOME);

        // Get all the faseAquisicaoList where nome contains UPDATED_NOME
        defaultFaseAquisicaoShouldNotBeFound("nome.contains=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosByNomeNotContainsSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where nome does not contain DEFAULT_NOME
        defaultFaseAquisicaoShouldNotBeFound("nome.doesNotContain=" + DEFAULT_NOME);

        // Get all the faseAquisicaoList where nome does not contain UPDATED_NOME
        defaultFaseAquisicaoShouldBeFound("nome.doesNotContain=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosByDiasExecucaoIsEqualToSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where diasExecucao equals to DEFAULT_DIAS_EXECUCAO
        defaultFaseAquisicaoShouldBeFound("diasExecucao.equals=" + DEFAULT_DIAS_EXECUCAO);

        // Get all the faseAquisicaoList where diasExecucao equals to UPDATED_DIAS_EXECUCAO
        defaultFaseAquisicaoShouldNotBeFound("diasExecucao.equals=" + UPDATED_DIAS_EXECUCAO);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosByDiasExecucaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where diasExecucao not equals to DEFAULT_DIAS_EXECUCAO
        defaultFaseAquisicaoShouldNotBeFound("diasExecucao.notEquals=" + DEFAULT_DIAS_EXECUCAO);

        // Get all the faseAquisicaoList where diasExecucao not equals to UPDATED_DIAS_EXECUCAO
        defaultFaseAquisicaoShouldBeFound("diasExecucao.notEquals=" + UPDATED_DIAS_EXECUCAO);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosByDiasExecucaoIsInShouldWork() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where diasExecucao in DEFAULT_DIAS_EXECUCAO or UPDATED_DIAS_EXECUCAO
        defaultFaseAquisicaoShouldBeFound("diasExecucao.in=" + DEFAULT_DIAS_EXECUCAO + "," + UPDATED_DIAS_EXECUCAO);

        // Get all the faseAquisicaoList where diasExecucao equals to UPDATED_DIAS_EXECUCAO
        defaultFaseAquisicaoShouldNotBeFound("diasExecucao.in=" + UPDATED_DIAS_EXECUCAO);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosByDiasExecucaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where diasExecucao is not null
        defaultFaseAquisicaoShouldBeFound("diasExecucao.specified=true");

        // Get all the faseAquisicaoList where diasExecucao is null
        defaultFaseAquisicaoShouldNotBeFound("diasExecucao.specified=false");
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosByDiasExecucaoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where diasExecucao is greater than or equal to DEFAULT_DIAS_EXECUCAO
        defaultFaseAquisicaoShouldBeFound("diasExecucao.greaterThanOrEqual=" + DEFAULT_DIAS_EXECUCAO);

        // Get all the faseAquisicaoList where diasExecucao is greater than or equal to UPDATED_DIAS_EXECUCAO
        defaultFaseAquisicaoShouldNotBeFound("diasExecucao.greaterThanOrEqual=" + UPDATED_DIAS_EXECUCAO);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosByDiasExecucaoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where diasExecucao is less than or equal to DEFAULT_DIAS_EXECUCAO
        defaultFaseAquisicaoShouldBeFound("diasExecucao.lessThanOrEqual=" + DEFAULT_DIAS_EXECUCAO);

        // Get all the faseAquisicaoList where diasExecucao is less than or equal to SMALLER_DIAS_EXECUCAO
        defaultFaseAquisicaoShouldNotBeFound("diasExecucao.lessThanOrEqual=" + SMALLER_DIAS_EXECUCAO);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosByDiasExecucaoIsLessThanSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where diasExecucao is less than DEFAULT_DIAS_EXECUCAO
        defaultFaseAquisicaoShouldNotBeFound("diasExecucao.lessThan=" + DEFAULT_DIAS_EXECUCAO);

        // Get all the faseAquisicaoList where diasExecucao is less than UPDATED_DIAS_EXECUCAO
        defaultFaseAquisicaoShouldBeFound("diasExecucao.lessThan=" + UPDATED_DIAS_EXECUCAO);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosByDiasExecucaoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where diasExecucao is greater than DEFAULT_DIAS_EXECUCAO
        defaultFaseAquisicaoShouldNotBeFound("diasExecucao.greaterThan=" + DEFAULT_DIAS_EXECUCAO);

        // Get all the faseAquisicaoList where diasExecucao is greater than SMALLER_DIAS_EXECUCAO
        defaultFaseAquisicaoShouldBeFound("diasExecucao.greaterThan=" + SMALLER_DIAS_EXECUCAO);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosBySequenciaIsEqualToSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where sequencia equals to DEFAULT_SEQUENCIA
        defaultFaseAquisicaoShouldBeFound("sequencia.equals=" + DEFAULT_SEQUENCIA);

        // Get all the faseAquisicaoList where sequencia equals to UPDATED_SEQUENCIA
        defaultFaseAquisicaoShouldNotBeFound("sequencia.equals=" + UPDATED_SEQUENCIA);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosBySequenciaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where sequencia not equals to DEFAULT_SEQUENCIA
        defaultFaseAquisicaoShouldNotBeFound("sequencia.notEquals=" + DEFAULT_SEQUENCIA);

        // Get all the faseAquisicaoList where sequencia not equals to UPDATED_SEQUENCIA
        defaultFaseAquisicaoShouldBeFound("sequencia.notEquals=" + UPDATED_SEQUENCIA);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosBySequenciaIsInShouldWork() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where sequencia in DEFAULT_SEQUENCIA or UPDATED_SEQUENCIA
        defaultFaseAquisicaoShouldBeFound("sequencia.in=" + DEFAULT_SEQUENCIA + "," + UPDATED_SEQUENCIA);

        // Get all the faseAquisicaoList where sequencia equals to UPDATED_SEQUENCIA
        defaultFaseAquisicaoShouldNotBeFound("sequencia.in=" + UPDATED_SEQUENCIA);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosBySequenciaIsNullOrNotNull() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where sequencia is not null
        defaultFaseAquisicaoShouldBeFound("sequencia.specified=true");

        // Get all the faseAquisicaoList where sequencia is null
        defaultFaseAquisicaoShouldNotBeFound("sequencia.specified=false");
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosBySequenciaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where sequencia is greater than or equal to DEFAULT_SEQUENCIA
        defaultFaseAquisicaoShouldBeFound("sequencia.greaterThanOrEqual=" + DEFAULT_SEQUENCIA);

        // Get all the faseAquisicaoList where sequencia is greater than or equal to UPDATED_SEQUENCIA
        defaultFaseAquisicaoShouldNotBeFound("sequencia.greaterThanOrEqual=" + UPDATED_SEQUENCIA);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosBySequenciaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where sequencia is less than or equal to DEFAULT_SEQUENCIA
        defaultFaseAquisicaoShouldBeFound("sequencia.lessThanOrEqual=" + DEFAULT_SEQUENCIA);

        // Get all the faseAquisicaoList where sequencia is less than or equal to SMALLER_SEQUENCIA
        defaultFaseAquisicaoShouldNotBeFound("sequencia.lessThanOrEqual=" + SMALLER_SEQUENCIA);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosBySequenciaIsLessThanSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where sequencia is less than DEFAULT_SEQUENCIA
        defaultFaseAquisicaoShouldNotBeFound("sequencia.lessThan=" + DEFAULT_SEQUENCIA);

        // Get all the faseAquisicaoList where sequencia is less than UPDATED_SEQUENCIA
        defaultFaseAquisicaoShouldBeFound("sequencia.lessThan=" + UPDATED_SEQUENCIA);
    }

    @Test
    @Transactional
    void getAllFaseAquisicaosBySequenciaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        // Get all the faseAquisicaoList where sequencia is greater than DEFAULT_SEQUENCIA
        defaultFaseAquisicaoShouldNotBeFound("sequencia.greaterThan=" + DEFAULT_SEQUENCIA);

        // Get all the faseAquisicaoList where sequencia is greater than SMALLER_SEQUENCIA
        defaultFaseAquisicaoShouldBeFound("sequencia.greaterThan=" + SMALLER_SEQUENCIA);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFaseAquisicaoShouldBeFound(String filter) throws Exception {
        restFaseAquisicaoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(faseAquisicao.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].diasExecucao").value(hasItem(DEFAULT_DIAS_EXECUCAO)))
            .andExpect(jsonPath("$.[*].sequencia").value(hasItem(DEFAULT_SEQUENCIA)));

        // Check, that the count call also returns 1
        restFaseAquisicaoMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFaseAquisicaoShouldNotBeFound(String filter) throws Exception {
        restFaseAquisicaoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFaseAquisicaoMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingFaseAquisicao() throws Exception {
        // Get the faseAquisicao
        restFaseAquisicaoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewFaseAquisicao() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        int databaseSizeBeforeUpdate = faseAquisicaoRepository.findAll().size();

        // Update the faseAquisicao
        FaseAquisicao updatedFaseAquisicao = faseAquisicaoRepository.findById(faseAquisicao.getId()).get();
        // Disconnect from session so that the updates on updatedFaseAquisicao are not directly saved in db
        em.detach(updatedFaseAquisicao);
        updatedFaseAquisicao.nome(UPDATED_NOME).diasExecucao(UPDATED_DIAS_EXECUCAO).sequencia(UPDATED_SEQUENCIA);

        restFaseAquisicaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedFaseAquisicao.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedFaseAquisicao))
            )
            .andExpect(status().isOk());

        // Validate the FaseAquisicao in the database
        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeUpdate);
        FaseAquisicao testFaseAquisicao = faseAquisicaoList.get(faseAquisicaoList.size() - 1);
        assertThat(testFaseAquisicao.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testFaseAquisicao.getDiasExecucao()).isEqualTo(UPDATED_DIAS_EXECUCAO);
        assertThat(testFaseAquisicao.getSequencia()).isEqualTo(UPDATED_SEQUENCIA);
    }

    @Test
    @Transactional
    void putNonExistingFaseAquisicao() throws Exception {
        int databaseSizeBeforeUpdate = faseAquisicaoRepository.findAll().size();
        faseAquisicao.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFaseAquisicaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, faseAquisicao.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(faseAquisicao))
            )
            .andExpect(status().isBadRequest());

        // Validate the FaseAquisicao in the database
        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchFaseAquisicao() throws Exception {
        int databaseSizeBeforeUpdate = faseAquisicaoRepository.findAll().size();
        faseAquisicao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFaseAquisicaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(faseAquisicao))
            )
            .andExpect(status().isBadRequest());

        // Validate the FaseAquisicao in the database
        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamFaseAquisicao() throws Exception {
        int databaseSizeBeforeUpdate = faseAquisicaoRepository.findAll().size();
        faseAquisicao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFaseAquisicaoMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(faseAquisicao)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the FaseAquisicao in the database
        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateFaseAquisicaoWithPatch() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        int databaseSizeBeforeUpdate = faseAquisicaoRepository.findAll().size();

        // Update the faseAquisicao using partial update
        FaseAquisicao partialUpdatedFaseAquisicao = new FaseAquisicao();
        partialUpdatedFaseAquisicao.setId(faseAquisicao.getId());

        partialUpdatedFaseAquisicao.nome(UPDATED_NOME).diasExecucao(UPDATED_DIAS_EXECUCAO).sequencia(UPDATED_SEQUENCIA);

        restFaseAquisicaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFaseAquisicao.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFaseAquisicao))
            )
            .andExpect(status().isOk());

        // Validate the FaseAquisicao in the database
        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeUpdate);
        FaseAquisicao testFaseAquisicao = faseAquisicaoList.get(faseAquisicaoList.size() - 1);
        assertThat(testFaseAquisicao.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testFaseAquisicao.getDiasExecucao()).isEqualTo(UPDATED_DIAS_EXECUCAO);
        assertThat(testFaseAquisicao.getSequencia()).isEqualTo(UPDATED_SEQUENCIA);
    }

    @Test
    @Transactional
    void fullUpdateFaseAquisicaoWithPatch() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        int databaseSizeBeforeUpdate = faseAquisicaoRepository.findAll().size();

        // Update the faseAquisicao using partial update
        FaseAquisicao partialUpdatedFaseAquisicao = new FaseAquisicao();
        partialUpdatedFaseAquisicao.setId(faseAquisicao.getId());

        partialUpdatedFaseAquisicao.nome(UPDATED_NOME).diasExecucao(UPDATED_DIAS_EXECUCAO).sequencia(UPDATED_SEQUENCIA);

        restFaseAquisicaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFaseAquisicao.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFaseAquisicao))
            )
            .andExpect(status().isOk());

        // Validate the FaseAquisicao in the database
        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeUpdate);
        FaseAquisicao testFaseAquisicao = faseAquisicaoList.get(faseAquisicaoList.size() - 1);
        assertThat(testFaseAquisicao.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testFaseAquisicao.getDiasExecucao()).isEqualTo(UPDATED_DIAS_EXECUCAO);
        assertThat(testFaseAquisicao.getSequencia()).isEqualTo(UPDATED_SEQUENCIA);
    }

    @Test
    @Transactional
    void patchNonExistingFaseAquisicao() throws Exception {
        int databaseSizeBeforeUpdate = faseAquisicaoRepository.findAll().size();
        faseAquisicao.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFaseAquisicaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, faseAquisicao.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(faseAquisicao))
            )
            .andExpect(status().isBadRequest());

        // Validate the FaseAquisicao in the database
        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchFaseAquisicao() throws Exception {
        int databaseSizeBeforeUpdate = faseAquisicaoRepository.findAll().size();
        faseAquisicao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFaseAquisicaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(faseAquisicao))
            )
            .andExpect(status().isBadRequest());

        // Validate the FaseAquisicao in the database
        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamFaseAquisicao() throws Exception {
        int databaseSizeBeforeUpdate = faseAquisicaoRepository.findAll().size();
        faseAquisicao.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFaseAquisicaoMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(faseAquisicao))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the FaseAquisicao in the database
        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteFaseAquisicao() throws Exception {
        // Initialize the database
        faseAquisicaoRepository.saveAndFlush(faseAquisicao);

        int databaseSizeBeforeDelete = faseAquisicaoRepository.findAll().size();

        // Delete the faseAquisicao
        restFaseAquisicaoMockMvc
            .perform(delete(ENTITY_API_URL_ID, faseAquisicao.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FaseAquisicao> faseAquisicaoList = faseAquisicaoRepository.findAll();
        assertThat(faseAquisicaoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
