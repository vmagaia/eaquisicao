package mz.co.eaquisicao.web.rest;

import static mz.co.eaquisicao.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mz.co.eaquisicao.IntegrationTest;
import mz.co.eaquisicao.domain.Fornecedor;
import mz.co.eaquisicao.domain.Pais;
import mz.co.eaquisicao.repository.FornecedorRepository;
import mz.co.eaquisicao.service.criteria.FornecedorCriteria;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link FornecedorResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class FornecedorResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_ENDERECO = "AAAAAAAAAA";
    private static final String UPDATED_ENDERECO = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUIT = 1;
    private static final Integer UPDATED_NUIT = 2;
    private static final Integer SMALLER_NUIT = 1 - 1;

    private static final String DEFAULT_AREA_ACTIVIDADE = "AAAAAAAAAA";
    private static final String UPDATED_AREA_ACTIVIDADE = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_SALDO = new BigDecimal(1);
    private static final BigDecimal UPDATED_SALDO = new BigDecimal(2);
    private static final BigDecimal SMALLER_SALDO = new BigDecimal(1 - 1);

    private static final LocalDate DEFAULT_DATA_REGISTO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_REGISTO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_REGISTO = LocalDate.ofEpochDay(-1L);

    private static final String ENTITY_API_URL = "/api/fornecedors";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private FornecedorRepository fornecedorRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFornecedorMockMvc;

    private Fornecedor fornecedor;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fornecedor createEntity(EntityManager em) {
        Fornecedor fornecedor = new Fornecedor()
            .nome(DEFAULT_NOME)
            .endereco(DEFAULT_ENDERECO)
            .nuit(DEFAULT_NUIT)
            .areaActividade(DEFAULT_AREA_ACTIVIDADE)
            .saldo(DEFAULT_SALDO)
            .dataRegisto(DEFAULT_DATA_REGISTO);
        return fornecedor;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Fornecedor createUpdatedEntity(EntityManager em) {
        Fornecedor fornecedor = new Fornecedor()
            .nome(UPDATED_NOME)
            .endereco(UPDATED_ENDERECO)
            .nuit(UPDATED_NUIT)
            .areaActividade(UPDATED_AREA_ACTIVIDADE)
            .saldo(UPDATED_SALDO)
            .dataRegisto(UPDATED_DATA_REGISTO);
        return fornecedor;
    }

    @BeforeEach
    public void initTest() {
        fornecedor = createEntity(em);
    }

    @Test
    @Transactional
    void createFornecedor() throws Exception {
        int databaseSizeBeforeCreate = fornecedorRepository.findAll().size();
        // Create the Fornecedor
        restFornecedorMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(fornecedor)))
            .andExpect(status().isCreated());

        // Validate the Fornecedor in the database
        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeCreate + 1);
        Fornecedor testFornecedor = fornecedorList.get(fornecedorList.size() - 1);
        assertThat(testFornecedor.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testFornecedor.getEndereco()).isEqualTo(DEFAULT_ENDERECO);
        assertThat(testFornecedor.getNuit()).isEqualTo(DEFAULT_NUIT);
        assertThat(testFornecedor.getAreaActividade()).isEqualTo(DEFAULT_AREA_ACTIVIDADE);
        assertThat(testFornecedor.getSaldo()).isEqualByComparingTo(DEFAULT_SALDO);
        assertThat(testFornecedor.getDataRegisto()).isEqualTo(DEFAULT_DATA_REGISTO);
    }

    @Test
    @Transactional
    void createFornecedorWithExistingId() throws Exception {
        // Create the Fornecedor with an existing ID
        fornecedor.setId(1L);

        int databaseSizeBeforeCreate = fornecedorRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restFornecedorMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(fornecedor)))
            .andExpect(status().isBadRequest());

        // Validate the Fornecedor in the database
        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = fornecedorRepository.findAll().size();
        // set the field null
        fornecedor.setNome(null);

        // Create the Fornecedor, which fails.

        restFornecedorMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(fornecedor)))
            .andExpect(status().isBadRequest());

        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEnderecoIsRequired() throws Exception {
        int databaseSizeBeforeTest = fornecedorRepository.findAll().size();
        // set the field null
        fornecedor.setEndereco(null);

        // Create the Fornecedor, which fails.

        restFornecedorMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(fornecedor)))
            .andExpect(status().isBadRequest());

        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNuitIsRequired() throws Exception {
        int databaseSizeBeforeTest = fornecedorRepository.findAll().size();
        // set the field null
        fornecedor.setNuit(null);

        // Create the Fornecedor, which fails.

        restFornecedorMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(fornecedor)))
            .andExpect(status().isBadRequest());

        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAreaActividadeIsRequired() throws Exception {
        int databaseSizeBeforeTest = fornecedorRepository.findAll().size();
        // set the field null
        fornecedor.setAreaActividade(null);

        // Create the Fornecedor, which fails.

        restFornecedorMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(fornecedor)))
            .andExpect(status().isBadRequest());

        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllFornecedors() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList
        restFornecedorMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fornecedor.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].endereco").value(hasItem(DEFAULT_ENDERECO)))
            .andExpect(jsonPath("$.[*].nuit").value(hasItem(DEFAULT_NUIT)))
            .andExpect(jsonPath("$.[*].areaActividade").value(hasItem(DEFAULT_AREA_ACTIVIDADE)))
            .andExpect(jsonPath("$.[*].saldo").value(hasItem(sameNumber(DEFAULT_SALDO))))
            .andExpect(jsonPath("$.[*].dataRegisto").value(hasItem(DEFAULT_DATA_REGISTO.toString())));
    }

    @Test
    @Transactional
    void getFornecedor() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get the fornecedor
        restFornecedorMockMvc
            .perform(get(ENTITY_API_URL_ID, fornecedor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fornecedor.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME))
            .andExpect(jsonPath("$.endereco").value(DEFAULT_ENDERECO))
            .andExpect(jsonPath("$.nuit").value(DEFAULT_NUIT))
            .andExpect(jsonPath("$.areaActividade").value(DEFAULT_AREA_ACTIVIDADE))
            .andExpect(jsonPath("$.saldo").value(sameNumber(DEFAULT_SALDO)))
            .andExpect(jsonPath("$.dataRegisto").value(DEFAULT_DATA_REGISTO.toString()));
    }

    @Test
    @Transactional
    void getFornecedorsByIdFiltering() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        Long id = fornecedor.getId();

        defaultFornecedorShouldBeFound("id.equals=" + id);
        defaultFornecedorShouldNotBeFound("id.notEquals=" + id);

        defaultFornecedorShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultFornecedorShouldNotBeFound("id.greaterThan=" + id);

        defaultFornecedorShouldBeFound("id.lessThanOrEqual=" + id);
        defaultFornecedorShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllFornecedorsByNomeIsEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where nome equals to DEFAULT_NOME
        defaultFornecedorShouldBeFound("nome.equals=" + DEFAULT_NOME);

        // Get all the fornecedorList where nome equals to UPDATED_NOME
        defaultFornecedorShouldNotBeFound("nome.equals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllFornecedorsByNomeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where nome not equals to DEFAULT_NOME
        defaultFornecedorShouldNotBeFound("nome.notEquals=" + DEFAULT_NOME);

        // Get all the fornecedorList where nome not equals to UPDATED_NOME
        defaultFornecedorShouldBeFound("nome.notEquals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllFornecedorsByNomeIsInShouldWork() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where nome in DEFAULT_NOME or UPDATED_NOME
        defaultFornecedorShouldBeFound("nome.in=" + DEFAULT_NOME + "," + UPDATED_NOME);

        // Get all the fornecedorList where nome equals to UPDATED_NOME
        defaultFornecedorShouldNotBeFound("nome.in=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllFornecedorsByNomeIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where nome is not null
        defaultFornecedorShouldBeFound("nome.specified=true");

        // Get all the fornecedorList where nome is null
        defaultFornecedorShouldNotBeFound("nome.specified=false");
    }

    @Test
    @Transactional
    void getAllFornecedorsByNomeContainsSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where nome contains DEFAULT_NOME
        defaultFornecedorShouldBeFound("nome.contains=" + DEFAULT_NOME);

        // Get all the fornecedorList where nome contains UPDATED_NOME
        defaultFornecedorShouldNotBeFound("nome.contains=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllFornecedorsByNomeNotContainsSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where nome does not contain DEFAULT_NOME
        defaultFornecedorShouldNotBeFound("nome.doesNotContain=" + DEFAULT_NOME);

        // Get all the fornecedorList where nome does not contain UPDATED_NOME
        defaultFornecedorShouldBeFound("nome.doesNotContain=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllFornecedorsByEnderecoIsEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where endereco equals to DEFAULT_ENDERECO
        defaultFornecedorShouldBeFound("endereco.equals=" + DEFAULT_ENDERECO);

        // Get all the fornecedorList where endereco equals to UPDATED_ENDERECO
        defaultFornecedorShouldNotBeFound("endereco.equals=" + UPDATED_ENDERECO);
    }

    @Test
    @Transactional
    void getAllFornecedorsByEnderecoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where endereco not equals to DEFAULT_ENDERECO
        defaultFornecedorShouldNotBeFound("endereco.notEquals=" + DEFAULT_ENDERECO);

        // Get all the fornecedorList where endereco not equals to UPDATED_ENDERECO
        defaultFornecedorShouldBeFound("endereco.notEquals=" + UPDATED_ENDERECO);
    }

    @Test
    @Transactional
    void getAllFornecedorsByEnderecoIsInShouldWork() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where endereco in DEFAULT_ENDERECO or UPDATED_ENDERECO
        defaultFornecedorShouldBeFound("endereco.in=" + DEFAULT_ENDERECO + "," + UPDATED_ENDERECO);

        // Get all the fornecedorList where endereco equals to UPDATED_ENDERECO
        defaultFornecedorShouldNotBeFound("endereco.in=" + UPDATED_ENDERECO);
    }

    @Test
    @Transactional
    void getAllFornecedorsByEnderecoIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where endereco is not null
        defaultFornecedorShouldBeFound("endereco.specified=true");

        // Get all the fornecedorList where endereco is null
        defaultFornecedorShouldNotBeFound("endereco.specified=false");
    }

    @Test
    @Transactional
    void getAllFornecedorsByEnderecoContainsSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where endereco contains DEFAULT_ENDERECO
        defaultFornecedorShouldBeFound("endereco.contains=" + DEFAULT_ENDERECO);

        // Get all the fornecedorList where endereco contains UPDATED_ENDERECO
        defaultFornecedorShouldNotBeFound("endereco.contains=" + UPDATED_ENDERECO);
    }

    @Test
    @Transactional
    void getAllFornecedorsByEnderecoNotContainsSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where endereco does not contain DEFAULT_ENDERECO
        defaultFornecedorShouldNotBeFound("endereco.doesNotContain=" + DEFAULT_ENDERECO);

        // Get all the fornecedorList where endereco does not contain UPDATED_ENDERECO
        defaultFornecedorShouldBeFound("endereco.doesNotContain=" + UPDATED_ENDERECO);
    }

    @Test
    @Transactional
    void getAllFornecedorsByNuitIsEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where nuit equals to DEFAULT_NUIT
        defaultFornecedorShouldBeFound("nuit.equals=" + DEFAULT_NUIT);

        // Get all the fornecedorList where nuit equals to UPDATED_NUIT
        defaultFornecedorShouldNotBeFound("nuit.equals=" + UPDATED_NUIT);
    }

    @Test
    @Transactional
    void getAllFornecedorsByNuitIsNotEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where nuit not equals to DEFAULT_NUIT
        defaultFornecedorShouldNotBeFound("nuit.notEquals=" + DEFAULT_NUIT);

        // Get all the fornecedorList where nuit not equals to UPDATED_NUIT
        defaultFornecedorShouldBeFound("nuit.notEquals=" + UPDATED_NUIT);
    }

    @Test
    @Transactional
    void getAllFornecedorsByNuitIsInShouldWork() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where nuit in DEFAULT_NUIT or UPDATED_NUIT
        defaultFornecedorShouldBeFound("nuit.in=" + DEFAULT_NUIT + "," + UPDATED_NUIT);

        // Get all the fornecedorList where nuit equals to UPDATED_NUIT
        defaultFornecedorShouldNotBeFound("nuit.in=" + UPDATED_NUIT);
    }

    @Test
    @Transactional
    void getAllFornecedorsByNuitIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where nuit is not null
        defaultFornecedorShouldBeFound("nuit.specified=true");

        // Get all the fornecedorList where nuit is null
        defaultFornecedorShouldNotBeFound("nuit.specified=false");
    }

    @Test
    @Transactional
    void getAllFornecedorsByNuitIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where nuit is greater than or equal to DEFAULT_NUIT
        defaultFornecedorShouldBeFound("nuit.greaterThanOrEqual=" + DEFAULT_NUIT);

        // Get all the fornecedorList where nuit is greater than or equal to UPDATED_NUIT
        defaultFornecedorShouldNotBeFound("nuit.greaterThanOrEqual=" + UPDATED_NUIT);
    }

    @Test
    @Transactional
    void getAllFornecedorsByNuitIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where nuit is less than or equal to DEFAULT_NUIT
        defaultFornecedorShouldBeFound("nuit.lessThanOrEqual=" + DEFAULT_NUIT);

        // Get all the fornecedorList where nuit is less than or equal to SMALLER_NUIT
        defaultFornecedorShouldNotBeFound("nuit.lessThanOrEqual=" + SMALLER_NUIT);
    }

    @Test
    @Transactional
    void getAllFornecedorsByNuitIsLessThanSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where nuit is less than DEFAULT_NUIT
        defaultFornecedorShouldNotBeFound("nuit.lessThan=" + DEFAULT_NUIT);

        // Get all the fornecedorList where nuit is less than UPDATED_NUIT
        defaultFornecedorShouldBeFound("nuit.lessThan=" + UPDATED_NUIT);
    }

    @Test
    @Transactional
    void getAllFornecedorsByNuitIsGreaterThanSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where nuit is greater than DEFAULT_NUIT
        defaultFornecedorShouldNotBeFound("nuit.greaterThan=" + DEFAULT_NUIT);

        // Get all the fornecedorList where nuit is greater than SMALLER_NUIT
        defaultFornecedorShouldBeFound("nuit.greaterThan=" + SMALLER_NUIT);
    }

    @Test
    @Transactional
    void getAllFornecedorsByAreaActividadeIsEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where areaActividade equals to DEFAULT_AREA_ACTIVIDADE
        defaultFornecedorShouldBeFound("areaActividade.equals=" + DEFAULT_AREA_ACTIVIDADE);

        // Get all the fornecedorList where areaActividade equals to UPDATED_AREA_ACTIVIDADE
        defaultFornecedorShouldNotBeFound("areaActividade.equals=" + UPDATED_AREA_ACTIVIDADE);
    }

    @Test
    @Transactional
    void getAllFornecedorsByAreaActividadeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where areaActividade not equals to DEFAULT_AREA_ACTIVIDADE
        defaultFornecedorShouldNotBeFound("areaActividade.notEquals=" + DEFAULT_AREA_ACTIVIDADE);

        // Get all the fornecedorList where areaActividade not equals to UPDATED_AREA_ACTIVIDADE
        defaultFornecedorShouldBeFound("areaActividade.notEquals=" + UPDATED_AREA_ACTIVIDADE);
    }

    @Test
    @Transactional
    void getAllFornecedorsByAreaActividadeIsInShouldWork() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where areaActividade in DEFAULT_AREA_ACTIVIDADE or UPDATED_AREA_ACTIVIDADE
        defaultFornecedorShouldBeFound("areaActividade.in=" + DEFAULT_AREA_ACTIVIDADE + "," + UPDATED_AREA_ACTIVIDADE);

        // Get all the fornecedorList where areaActividade equals to UPDATED_AREA_ACTIVIDADE
        defaultFornecedorShouldNotBeFound("areaActividade.in=" + UPDATED_AREA_ACTIVIDADE);
    }

    @Test
    @Transactional
    void getAllFornecedorsByAreaActividadeIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where areaActividade is not null
        defaultFornecedorShouldBeFound("areaActividade.specified=true");

        // Get all the fornecedorList where areaActividade is null
        defaultFornecedorShouldNotBeFound("areaActividade.specified=false");
    }

    @Test
    @Transactional
    void getAllFornecedorsByAreaActividadeContainsSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where areaActividade contains DEFAULT_AREA_ACTIVIDADE
        defaultFornecedorShouldBeFound("areaActividade.contains=" + DEFAULT_AREA_ACTIVIDADE);

        // Get all the fornecedorList where areaActividade contains UPDATED_AREA_ACTIVIDADE
        defaultFornecedorShouldNotBeFound("areaActividade.contains=" + UPDATED_AREA_ACTIVIDADE);
    }

    @Test
    @Transactional
    void getAllFornecedorsByAreaActividadeNotContainsSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where areaActividade does not contain DEFAULT_AREA_ACTIVIDADE
        defaultFornecedorShouldNotBeFound("areaActividade.doesNotContain=" + DEFAULT_AREA_ACTIVIDADE);

        // Get all the fornecedorList where areaActividade does not contain UPDATED_AREA_ACTIVIDADE
        defaultFornecedorShouldBeFound("areaActividade.doesNotContain=" + UPDATED_AREA_ACTIVIDADE);
    }

    @Test
    @Transactional
    void getAllFornecedorsBySaldoIsEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where saldo equals to DEFAULT_SALDO
        defaultFornecedorShouldBeFound("saldo.equals=" + DEFAULT_SALDO);

        // Get all the fornecedorList where saldo equals to UPDATED_SALDO
        defaultFornecedorShouldNotBeFound("saldo.equals=" + UPDATED_SALDO);
    }

    @Test
    @Transactional
    void getAllFornecedorsBySaldoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where saldo not equals to DEFAULT_SALDO
        defaultFornecedorShouldNotBeFound("saldo.notEquals=" + DEFAULT_SALDO);

        // Get all the fornecedorList where saldo not equals to UPDATED_SALDO
        defaultFornecedorShouldBeFound("saldo.notEquals=" + UPDATED_SALDO);
    }

    @Test
    @Transactional
    void getAllFornecedorsBySaldoIsInShouldWork() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where saldo in DEFAULT_SALDO or UPDATED_SALDO
        defaultFornecedorShouldBeFound("saldo.in=" + DEFAULT_SALDO + "," + UPDATED_SALDO);

        // Get all the fornecedorList where saldo equals to UPDATED_SALDO
        defaultFornecedorShouldNotBeFound("saldo.in=" + UPDATED_SALDO);
    }

    @Test
    @Transactional
    void getAllFornecedorsBySaldoIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where saldo is not null
        defaultFornecedorShouldBeFound("saldo.specified=true");

        // Get all the fornecedorList where saldo is null
        defaultFornecedorShouldNotBeFound("saldo.specified=false");
    }

    @Test
    @Transactional
    void getAllFornecedorsBySaldoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where saldo is greater than or equal to DEFAULT_SALDO
        defaultFornecedorShouldBeFound("saldo.greaterThanOrEqual=" + DEFAULT_SALDO);

        // Get all the fornecedorList where saldo is greater than or equal to UPDATED_SALDO
        defaultFornecedorShouldNotBeFound("saldo.greaterThanOrEqual=" + UPDATED_SALDO);
    }

    @Test
    @Transactional
    void getAllFornecedorsBySaldoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where saldo is less than or equal to DEFAULT_SALDO
        defaultFornecedorShouldBeFound("saldo.lessThanOrEqual=" + DEFAULT_SALDO);

        // Get all the fornecedorList where saldo is less than or equal to SMALLER_SALDO
        defaultFornecedorShouldNotBeFound("saldo.lessThanOrEqual=" + SMALLER_SALDO);
    }

    @Test
    @Transactional
    void getAllFornecedorsBySaldoIsLessThanSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where saldo is less than DEFAULT_SALDO
        defaultFornecedorShouldNotBeFound("saldo.lessThan=" + DEFAULT_SALDO);

        // Get all the fornecedorList where saldo is less than UPDATED_SALDO
        defaultFornecedorShouldBeFound("saldo.lessThan=" + UPDATED_SALDO);
    }

    @Test
    @Transactional
    void getAllFornecedorsBySaldoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where saldo is greater than DEFAULT_SALDO
        defaultFornecedorShouldNotBeFound("saldo.greaterThan=" + DEFAULT_SALDO);

        // Get all the fornecedorList where saldo is greater than SMALLER_SALDO
        defaultFornecedorShouldBeFound("saldo.greaterThan=" + SMALLER_SALDO);
    }

    @Test
    @Transactional
    void getAllFornecedorsByDataRegistoIsEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where dataRegisto equals to DEFAULT_DATA_REGISTO
        defaultFornecedorShouldBeFound("dataRegisto.equals=" + DEFAULT_DATA_REGISTO);

        // Get all the fornecedorList where dataRegisto equals to UPDATED_DATA_REGISTO
        defaultFornecedorShouldNotBeFound("dataRegisto.equals=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllFornecedorsByDataRegistoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where dataRegisto not equals to DEFAULT_DATA_REGISTO
        defaultFornecedorShouldNotBeFound("dataRegisto.notEquals=" + DEFAULT_DATA_REGISTO);

        // Get all the fornecedorList where dataRegisto not equals to UPDATED_DATA_REGISTO
        defaultFornecedorShouldBeFound("dataRegisto.notEquals=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllFornecedorsByDataRegistoIsInShouldWork() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where dataRegisto in DEFAULT_DATA_REGISTO or UPDATED_DATA_REGISTO
        defaultFornecedorShouldBeFound("dataRegisto.in=" + DEFAULT_DATA_REGISTO + "," + UPDATED_DATA_REGISTO);

        // Get all the fornecedorList where dataRegisto equals to UPDATED_DATA_REGISTO
        defaultFornecedorShouldNotBeFound("dataRegisto.in=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllFornecedorsByDataRegistoIsNullOrNotNull() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where dataRegisto is not null
        defaultFornecedorShouldBeFound("dataRegisto.specified=true");

        // Get all the fornecedorList where dataRegisto is null
        defaultFornecedorShouldNotBeFound("dataRegisto.specified=false");
    }

    @Test
    @Transactional
    void getAllFornecedorsByDataRegistoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where dataRegisto is greater than or equal to DEFAULT_DATA_REGISTO
        defaultFornecedorShouldBeFound("dataRegisto.greaterThanOrEqual=" + DEFAULT_DATA_REGISTO);

        // Get all the fornecedorList where dataRegisto is greater than or equal to UPDATED_DATA_REGISTO
        defaultFornecedorShouldNotBeFound("dataRegisto.greaterThanOrEqual=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllFornecedorsByDataRegistoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where dataRegisto is less than or equal to DEFAULT_DATA_REGISTO
        defaultFornecedorShouldBeFound("dataRegisto.lessThanOrEqual=" + DEFAULT_DATA_REGISTO);

        // Get all the fornecedorList where dataRegisto is less than or equal to SMALLER_DATA_REGISTO
        defaultFornecedorShouldNotBeFound("dataRegisto.lessThanOrEqual=" + SMALLER_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllFornecedorsByDataRegistoIsLessThanSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where dataRegisto is less than DEFAULT_DATA_REGISTO
        defaultFornecedorShouldNotBeFound("dataRegisto.lessThan=" + DEFAULT_DATA_REGISTO);

        // Get all the fornecedorList where dataRegisto is less than UPDATED_DATA_REGISTO
        defaultFornecedorShouldBeFound("dataRegisto.lessThan=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllFornecedorsByDataRegistoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        // Get all the fornecedorList where dataRegisto is greater than DEFAULT_DATA_REGISTO
        defaultFornecedorShouldNotBeFound("dataRegisto.greaterThan=" + DEFAULT_DATA_REGISTO);

        // Get all the fornecedorList where dataRegisto is greater than SMALLER_DATA_REGISTO
        defaultFornecedorShouldBeFound("dataRegisto.greaterThan=" + SMALLER_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllFornecedorsByPaisIsEqualToSomething() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);
        Pais pais;
        if (TestUtil.findAll(em, Pais.class).isEmpty()) {
            pais = PaisResourceIT.createEntity(em);
            em.persist(pais);
            em.flush();
        } else {
            pais = TestUtil.findAll(em, Pais.class).get(0);
        }
        em.persist(pais);
        em.flush();
        fornecedor.setPais(pais);
        fornecedorRepository.saveAndFlush(fornecedor);
        Long paisId = pais.getId();

        // Get all the fornecedorList where pais equals to paisId
        defaultFornecedorShouldBeFound("paisId.equals=" + paisId);

        // Get all the fornecedorList where pais equals to (paisId + 1)
        defaultFornecedorShouldNotBeFound("paisId.equals=" + (paisId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFornecedorShouldBeFound(String filter) throws Exception {
        restFornecedorMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fornecedor.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].endereco").value(hasItem(DEFAULT_ENDERECO)))
            .andExpect(jsonPath("$.[*].nuit").value(hasItem(DEFAULT_NUIT)))
            .andExpect(jsonPath("$.[*].areaActividade").value(hasItem(DEFAULT_AREA_ACTIVIDADE)))
            .andExpect(jsonPath("$.[*].saldo").value(hasItem(sameNumber(DEFAULT_SALDO))))
            .andExpect(jsonPath("$.[*].dataRegisto").value(hasItem(DEFAULT_DATA_REGISTO.toString())));

        // Check, that the count call also returns 1
        restFornecedorMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFornecedorShouldNotBeFound(String filter) throws Exception {
        restFornecedorMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFornecedorMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingFornecedor() throws Exception {
        // Get the fornecedor
        restFornecedorMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewFornecedor() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        int databaseSizeBeforeUpdate = fornecedorRepository.findAll().size();

        // Update the fornecedor
        Fornecedor updatedFornecedor = fornecedorRepository.findById(fornecedor.getId()).get();
        // Disconnect from session so that the updates on updatedFornecedor are not directly saved in db
        em.detach(updatedFornecedor);
        updatedFornecedor
            .nome(UPDATED_NOME)
            .endereco(UPDATED_ENDERECO)
            .nuit(UPDATED_NUIT)
            .areaActividade(UPDATED_AREA_ACTIVIDADE)
            .saldo(UPDATED_SALDO)
            .dataRegisto(UPDATED_DATA_REGISTO);

        restFornecedorMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedFornecedor.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedFornecedor))
            )
            .andExpect(status().isOk());

        // Validate the Fornecedor in the database
        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeUpdate);
        Fornecedor testFornecedor = fornecedorList.get(fornecedorList.size() - 1);
        assertThat(testFornecedor.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testFornecedor.getEndereco()).isEqualTo(UPDATED_ENDERECO);
        assertThat(testFornecedor.getNuit()).isEqualTo(UPDATED_NUIT);
        assertThat(testFornecedor.getAreaActividade()).isEqualTo(UPDATED_AREA_ACTIVIDADE);
        assertThat(testFornecedor.getSaldo()).isEqualTo(UPDATED_SALDO);
        assertThat(testFornecedor.getDataRegisto()).isEqualTo(UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void putNonExistingFornecedor() throws Exception {
        int databaseSizeBeforeUpdate = fornecedorRepository.findAll().size();
        fornecedor.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFornecedorMockMvc
            .perform(
                put(ENTITY_API_URL_ID, fornecedor.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fornecedor))
            )
            .andExpect(status().isBadRequest());

        // Validate the Fornecedor in the database
        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchFornecedor() throws Exception {
        int databaseSizeBeforeUpdate = fornecedorRepository.findAll().size();
        fornecedor.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFornecedorMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fornecedor))
            )
            .andExpect(status().isBadRequest());

        // Validate the Fornecedor in the database
        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamFornecedor() throws Exception {
        int databaseSizeBeforeUpdate = fornecedorRepository.findAll().size();
        fornecedor.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFornecedorMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(fornecedor)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Fornecedor in the database
        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateFornecedorWithPatch() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        int databaseSizeBeforeUpdate = fornecedorRepository.findAll().size();

        // Update the fornecedor using partial update
        Fornecedor partialUpdatedFornecedor = new Fornecedor();
        partialUpdatedFornecedor.setId(fornecedor.getId());

        partialUpdatedFornecedor
            .nome(UPDATED_NOME)
            .endereco(UPDATED_ENDERECO)
            .nuit(UPDATED_NUIT)
            .saldo(UPDATED_SALDO)
            .dataRegisto(UPDATED_DATA_REGISTO);

        restFornecedorMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFornecedor.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFornecedor))
            )
            .andExpect(status().isOk());

        // Validate the Fornecedor in the database
        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeUpdate);
        Fornecedor testFornecedor = fornecedorList.get(fornecedorList.size() - 1);
        assertThat(testFornecedor.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testFornecedor.getEndereco()).isEqualTo(UPDATED_ENDERECO);
        assertThat(testFornecedor.getNuit()).isEqualTo(UPDATED_NUIT);
        assertThat(testFornecedor.getAreaActividade()).isEqualTo(DEFAULT_AREA_ACTIVIDADE);
        assertThat(testFornecedor.getSaldo()).isEqualByComparingTo(UPDATED_SALDO);
        assertThat(testFornecedor.getDataRegisto()).isEqualTo(UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void fullUpdateFornecedorWithPatch() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        int databaseSizeBeforeUpdate = fornecedorRepository.findAll().size();

        // Update the fornecedor using partial update
        Fornecedor partialUpdatedFornecedor = new Fornecedor();
        partialUpdatedFornecedor.setId(fornecedor.getId());

        partialUpdatedFornecedor
            .nome(UPDATED_NOME)
            .endereco(UPDATED_ENDERECO)
            .nuit(UPDATED_NUIT)
            .areaActividade(UPDATED_AREA_ACTIVIDADE)
            .saldo(UPDATED_SALDO)
            .dataRegisto(UPDATED_DATA_REGISTO);

        restFornecedorMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFornecedor.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFornecedor))
            )
            .andExpect(status().isOk());

        // Validate the Fornecedor in the database
        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeUpdate);
        Fornecedor testFornecedor = fornecedorList.get(fornecedorList.size() - 1);
        assertThat(testFornecedor.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testFornecedor.getEndereco()).isEqualTo(UPDATED_ENDERECO);
        assertThat(testFornecedor.getNuit()).isEqualTo(UPDATED_NUIT);
        assertThat(testFornecedor.getAreaActividade()).isEqualTo(UPDATED_AREA_ACTIVIDADE);
        assertThat(testFornecedor.getSaldo()).isEqualByComparingTo(UPDATED_SALDO);
        assertThat(testFornecedor.getDataRegisto()).isEqualTo(UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void patchNonExistingFornecedor() throws Exception {
        int databaseSizeBeforeUpdate = fornecedorRepository.findAll().size();
        fornecedor.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFornecedorMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, fornecedor.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(fornecedor))
            )
            .andExpect(status().isBadRequest());

        // Validate the Fornecedor in the database
        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchFornecedor() throws Exception {
        int databaseSizeBeforeUpdate = fornecedorRepository.findAll().size();
        fornecedor.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFornecedorMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(fornecedor))
            )
            .andExpect(status().isBadRequest());

        // Validate the Fornecedor in the database
        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamFornecedor() throws Exception {
        int databaseSizeBeforeUpdate = fornecedorRepository.findAll().size();
        fornecedor.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFornecedorMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(fornecedor))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Fornecedor in the database
        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteFornecedor() throws Exception {
        // Initialize the database
        fornecedorRepository.saveAndFlush(fornecedor);

        int databaseSizeBeforeDelete = fornecedorRepository.findAll().size();

        // Delete the fornecedor
        restFornecedorMockMvc
            .perform(delete(ENTITY_API_URL_ID, fornecedor.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Fornecedor> fornecedorList = fornecedorRepository.findAll();
        assertThat(fornecedorList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
