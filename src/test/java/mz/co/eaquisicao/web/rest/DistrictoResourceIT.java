package mz.co.eaquisicao.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mz.co.eaquisicao.IntegrationTest;
import mz.co.eaquisicao.domain.Districto;
import mz.co.eaquisicao.domain.Provincia;
import mz.co.eaquisicao.repository.DistrictoRepository;
import mz.co.eaquisicao.service.criteria.DistrictoCriteria;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DistrictoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DistrictoResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/districtos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DistrictoRepository districtoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDistrictoMockMvc;

    private Districto districto;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Districto createEntity(EntityManager em) {
        Districto districto = new Districto().nome(DEFAULT_NOME);
        return districto;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Districto createUpdatedEntity(EntityManager em) {
        Districto districto = new Districto().nome(UPDATED_NOME);
        return districto;
    }

    @BeforeEach
    public void initTest() {
        districto = createEntity(em);
    }

    @Test
    @Transactional
    void createDistricto() throws Exception {
        int databaseSizeBeforeCreate = districtoRepository.findAll().size();
        // Create the Districto
        restDistrictoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(districto)))
            .andExpect(status().isCreated());

        // Validate the Districto in the database
        List<Districto> districtoList = districtoRepository.findAll();
        assertThat(districtoList).hasSize(databaseSizeBeforeCreate + 1);
        Districto testDistricto = districtoList.get(districtoList.size() - 1);
        assertThat(testDistricto.getNome()).isEqualTo(DEFAULT_NOME);
    }

    @Test
    @Transactional
    void createDistrictoWithExistingId() throws Exception {
        // Create the Districto with an existing ID
        districto.setId(1L);

        int databaseSizeBeforeCreate = districtoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDistrictoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(districto)))
            .andExpect(status().isBadRequest());

        // Validate the Districto in the database
        List<Districto> districtoList = districtoRepository.findAll();
        assertThat(districtoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllDistrictos() throws Exception {
        // Initialize the database
        districtoRepository.saveAndFlush(districto);

        // Get all the districtoList
        restDistrictoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(districto.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)));
    }

    @Test
    @Transactional
    void getDistricto() throws Exception {
        // Initialize the database
        districtoRepository.saveAndFlush(districto);

        // Get the districto
        restDistrictoMockMvc
            .perform(get(ENTITY_API_URL_ID, districto.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(districto.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME));
    }

    @Test
    @Transactional
    void getDistrictosByIdFiltering() throws Exception {
        // Initialize the database
        districtoRepository.saveAndFlush(districto);

        Long id = districto.getId();

        defaultDistrictoShouldBeFound("id.equals=" + id);
        defaultDistrictoShouldNotBeFound("id.notEquals=" + id);

        defaultDistrictoShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDistrictoShouldNotBeFound("id.greaterThan=" + id);

        defaultDistrictoShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDistrictoShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllDistrictosByNomeIsEqualToSomething() throws Exception {
        // Initialize the database
        districtoRepository.saveAndFlush(districto);

        // Get all the districtoList where nome equals to DEFAULT_NOME
        defaultDistrictoShouldBeFound("nome.equals=" + DEFAULT_NOME);

        // Get all the districtoList where nome equals to UPDATED_NOME
        defaultDistrictoShouldNotBeFound("nome.equals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllDistrictosByNomeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        districtoRepository.saveAndFlush(districto);

        // Get all the districtoList where nome not equals to DEFAULT_NOME
        defaultDistrictoShouldNotBeFound("nome.notEquals=" + DEFAULT_NOME);

        // Get all the districtoList where nome not equals to UPDATED_NOME
        defaultDistrictoShouldBeFound("nome.notEquals=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllDistrictosByNomeIsInShouldWork() throws Exception {
        // Initialize the database
        districtoRepository.saveAndFlush(districto);

        // Get all the districtoList where nome in DEFAULT_NOME or UPDATED_NOME
        defaultDistrictoShouldBeFound("nome.in=" + DEFAULT_NOME + "," + UPDATED_NOME);

        // Get all the districtoList where nome equals to UPDATED_NOME
        defaultDistrictoShouldNotBeFound("nome.in=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllDistrictosByNomeIsNullOrNotNull() throws Exception {
        // Initialize the database
        districtoRepository.saveAndFlush(districto);

        // Get all the districtoList where nome is not null
        defaultDistrictoShouldBeFound("nome.specified=true");

        // Get all the districtoList where nome is null
        defaultDistrictoShouldNotBeFound("nome.specified=false");
    }

    @Test
    @Transactional
    void getAllDistrictosByNomeContainsSomething() throws Exception {
        // Initialize the database
        districtoRepository.saveAndFlush(districto);

        // Get all the districtoList where nome contains DEFAULT_NOME
        defaultDistrictoShouldBeFound("nome.contains=" + DEFAULT_NOME);

        // Get all the districtoList where nome contains UPDATED_NOME
        defaultDistrictoShouldNotBeFound("nome.contains=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllDistrictosByNomeNotContainsSomething() throws Exception {
        // Initialize the database
        districtoRepository.saveAndFlush(districto);

        // Get all the districtoList where nome does not contain DEFAULT_NOME
        defaultDistrictoShouldNotBeFound("nome.doesNotContain=" + DEFAULT_NOME);

        // Get all the districtoList where nome does not contain UPDATED_NOME
        defaultDistrictoShouldBeFound("nome.doesNotContain=" + UPDATED_NOME);
    }

    @Test
    @Transactional
    void getAllDistrictosByProvinciaIsEqualToSomething() throws Exception {
        // Initialize the database
        districtoRepository.saveAndFlush(districto);
        Provincia provincia;
        if (TestUtil.findAll(em, Provincia.class).isEmpty()) {
            provincia = ProvinciaResourceIT.createEntity(em);
            em.persist(provincia);
            em.flush();
        } else {
            provincia = TestUtil.findAll(em, Provincia.class).get(0);
        }
        em.persist(provincia);
        em.flush();
        districto.setProvincia(provincia);
        districtoRepository.saveAndFlush(districto);
        Long provinciaId = provincia.getId();

        // Get all the districtoList where provincia equals to provinciaId
        defaultDistrictoShouldBeFound("provinciaId.equals=" + provinciaId);

        // Get all the districtoList where provincia equals to (provinciaId + 1)
        defaultDistrictoShouldNotBeFound("provinciaId.equals=" + (provinciaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDistrictoShouldBeFound(String filter) throws Exception {
        restDistrictoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(districto.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)));

        // Check, that the count call also returns 1
        restDistrictoMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDistrictoShouldNotBeFound(String filter) throws Exception {
        restDistrictoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDistrictoMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingDistricto() throws Exception {
        // Get the districto
        restDistrictoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewDistricto() throws Exception {
        // Initialize the database
        districtoRepository.saveAndFlush(districto);

        int databaseSizeBeforeUpdate = districtoRepository.findAll().size();

        // Update the districto
        Districto updatedDistricto = districtoRepository.findById(districto.getId()).get();
        // Disconnect from session so that the updates on updatedDistricto are not directly saved in db
        em.detach(updatedDistricto);
        updatedDistricto.nome(UPDATED_NOME);

        restDistrictoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedDistricto.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedDistricto))
            )
            .andExpect(status().isOk());

        // Validate the Districto in the database
        List<Districto> districtoList = districtoRepository.findAll();
        assertThat(districtoList).hasSize(databaseSizeBeforeUpdate);
        Districto testDistricto = districtoList.get(districtoList.size() - 1);
        assertThat(testDistricto.getNome()).isEqualTo(UPDATED_NOME);
    }

    @Test
    @Transactional
    void putNonExistingDistricto() throws Exception {
        int databaseSizeBeforeUpdate = districtoRepository.findAll().size();
        districto.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDistrictoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, districto.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(districto))
            )
            .andExpect(status().isBadRequest());

        // Validate the Districto in the database
        List<Districto> districtoList = districtoRepository.findAll();
        assertThat(districtoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDistricto() throws Exception {
        int databaseSizeBeforeUpdate = districtoRepository.findAll().size();
        districto.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDistrictoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(districto))
            )
            .andExpect(status().isBadRequest());

        // Validate the Districto in the database
        List<Districto> districtoList = districtoRepository.findAll();
        assertThat(districtoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDistricto() throws Exception {
        int databaseSizeBeforeUpdate = districtoRepository.findAll().size();
        districto.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDistrictoMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(districto)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Districto in the database
        List<Districto> districtoList = districtoRepository.findAll();
        assertThat(districtoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDistrictoWithPatch() throws Exception {
        // Initialize the database
        districtoRepository.saveAndFlush(districto);

        int databaseSizeBeforeUpdate = districtoRepository.findAll().size();

        // Update the districto using partial update
        Districto partialUpdatedDistricto = new Districto();
        partialUpdatedDistricto.setId(districto.getId());

        partialUpdatedDistricto.nome(UPDATED_NOME);

        restDistrictoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDistricto.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDistricto))
            )
            .andExpect(status().isOk());

        // Validate the Districto in the database
        List<Districto> districtoList = districtoRepository.findAll();
        assertThat(districtoList).hasSize(databaseSizeBeforeUpdate);
        Districto testDistricto = districtoList.get(districtoList.size() - 1);
        assertThat(testDistricto.getNome()).isEqualTo(UPDATED_NOME);
    }

    @Test
    @Transactional
    void fullUpdateDistrictoWithPatch() throws Exception {
        // Initialize the database
        districtoRepository.saveAndFlush(districto);

        int databaseSizeBeforeUpdate = districtoRepository.findAll().size();

        // Update the districto using partial update
        Districto partialUpdatedDistricto = new Districto();
        partialUpdatedDistricto.setId(districto.getId());

        partialUpdatedDistricto.nome(UPDATED_NOME);

        restDistrictoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDistricto.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDistricto))
            )
            .andExpect(status().isOk());

        // Validate the Districto in the database
        List<Districto> districtoList = districtoRepository.findAll();
        assertThat(districtoList).hasSize(databaseSizeBeforeUpdate);
        Districto testDistricto = districtoList.get(districtoList.size() - 1);
        assertThat(testDistricto.getNome()).isEqualTo(UPDATED_NOME);
    }

    @Test
    @Transactional
    void patchNonExistingDistricto() throws Exception {
        int databaseSizeBeforeUpdate = districtoRepository.findAll().size();
        districto.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDistrictoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, districto.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(districto))
            )
            .andExpect(status().isBadRequest());

        // Validate the Districto in the database
        List<Districto> districtoList = districtoRepository.findAll();
        assertThat(districtoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDistricto() throws Exception {
        int databaseSizeBeforeUpdate = districtoRepository.findAll().size();
        districto.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDistrictoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(districto))
            )
            .andExpect(status().isBadRequest());

        // Validate the Districto in the database
        List<Districto> districtoList = districtoRepository.findAll();
        assertThat(districtoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDistricto() throws Exception {
        int databaseSizeBeforeUpdate = districtoRepository.findAll().size();
        districto.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDistrictoMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(districto))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Districto in the database
        List<Districto> districtoList = districtoRepository.findAll();
        assertThat(districtoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDistricto() throws Exception {
        // Initialize the database
        districtoRepository.saveAndFlush(districto);

        int databaseSizeBeforeDelete = districtoRepository.findAll().size();

        // Delete the districto
        restDistrictoMockMvc
            .perform(delete(ENTITY_API_URL_ID, districto.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Districto> districtoList = districtoRepository.findAll();
        assertThat(districtoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
