package mz.co.eaquisicao.web.rest;

import static mz.co.eaquisicao.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import mz.co.eaquisicao.IntegrationTest;
import mz.co.eaquisicao.domain.Aquisicao;
import mz.co.eaquisicao.domain.Fornecedor;
import mz.co.eaquisicao.domain.PropostaFinanceira;
import mz.co.eaquisicao.domain.enumeration.EstadoProposta;
import mz.co.eaquisicao.repository.PropostaFinanceiraRepository;
import mz.co.eaquisicao.service.criteria.PropostaFinanceiraCriteria;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link PropostaFinanceiraResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PropostaFinanceiraResourceIT {

    private static final BigDecimal DEFAULT_VALOR = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALOR = new BigDecimal(2);
    private static final BigDecimal SMALLER_VALOR = new BigDecimal(1 - 1);

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final byte[] DEFAULT_ANEXO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_ANEXO = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_ANEXO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_ANEXO_CONTENT_TYPE = "image/png";

    private static final LocalDate DEFAULT_DATA_REGISTO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_REGISTO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATA_REGISTO = LocalDate.ofEpochDay(-1L);

    private static final EstadoProposta DEFAULT_ESTADO_PROPOSTA = EstadoProposta.Pendente;
    private static final EstadoProposta UPDATED_ESTADO_PROPOSTA = EstadoProposta.Aprovada;

    private static final String ENTITY_API_URL = "/api/proposta-financeiras";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PropostaFinanceiraRepository propostaFinanceiraRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPropostaFinanceiraMockMvc;

    private PropostaFinanceira propostaFinanceira;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PropostaFinanceira createEntity(EntityManager em) {
        PropostaFinanceira propostaFinanceira = new PropostaFinanceira()
            .valor(DEFAULT_VALOR)
            .descricao(DEFAULT_DESCRICAO)
            .anexo(DEFAULT_ANEXO)
            .anexoContentType(DEFAULT_ANEXO_CONTENT_TYPE)
            .dataRegisto(DEFAULT_DATA_REGISTO)
            .estadoProposta(DEFAULT_ESTADO_PROPOSTA);
        return propostaFinanceira;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PropostaFinanceira createUpdatedEntity(EntityManager em) {
        PropostaFinanceira propostaFinanceira = new PropostaFinanceira()
            .valor(UPDATED_VALOR)
            .descricao(UPDATED_DESCRICAO)
            .anexo(UPDATED_ANEXO)
            .anexoContentType(UPDATED_ANEXO_CONTENT_TYPE)
            .dataRegisto(UPDATED_DATA_REGISTO)
            .estadoProposta(UPDATED_ESTADO_PROPOSTA);
        return propostaFinanceira;
    }

    @BeforeEach
    public void initTest() {
        propostaFinanceira = createEntity(em);
    }

    @Test
    @Transactional
    void createPropostaFinanceira() throws Exception {
        int databaseSizeBeforeCreate = propostaFinanceiraRepository.findAll().size();
        // Create the PropostaFinanceira
        restPropostaFinanceiraMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(propostaFinanceira))
            )
            .andExpect(status().isCreated());

        // Validate the PropostaFinanceira in the database
        List<PropostaFinanceira> propostaFinanceiraList = propostaFinanceiraRepository.findAll();
        assertThat(propostaFinanceiraList).hasSize(databaseSizeBeforeCreate + 1);
        PropostaFinanceira testPropostaFinanceira = propostaFinanceiraList.get(propostaFinanceiraList.size() - 1);
        assertThat(testPropostaFinanceira.getValor()).isEqualByComparingTo(DEFAULT_VALOR);
        assertThat(testPropostaFinanceira.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
        assertThat(testPropostaFinanceira.getAnexo()).isEqualTo(DEFAULT_ANEXO);
        assertThat(testPropostaFinanceira.getAnexoContentType()).isEqualTo(DEFAULT_ANEXO_CONTENT_TYPE);
        assertThat(testPropostaFinanceira.getDataRegisto()).isEqualTo(DEFAULT_DATA_REGISTO);
        assertThat(testPropostaFinanceira.getEstadoProposta()).isEqualTo(DEFAULT_ESTADO_PROPOSTA);
    }

    @Test
    @Transactional
    void createPropostaFinanceiraWithExistingId() throws Exception {
        // Create the PropostaFinanceira with an existing ID
        propostaFinanceira.setId(1L);

        int databaseSizeBeforeCreate = propostaFinanceiraRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPropostaFinanceiraMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(propostaFinanceira))
            )
            .andExpect(status().isBadRequest());

        // Validate the PropostaFinanceira in the database
        List<PropostaFinanceira> propostaFinanceiraList = propostaFinanceiraRepository.findAll();
        assertThat(propostaFinanceiraList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkValorIsRequired() throws Exception {
        int databaseSizeBeforeTest = propostaFinanceiraRepository.findAll().size();
        // set the field null
        propostaFinanceira.setValor(null);

        // Create the PropostaFinanceira, which fails.

        restPropostaFinanceiraMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(propostaFinanceira))
            )
            .andExpect(status().isBadRequest());

        List<PropostaFinanceira> propostaFinanceiraList = propostaFinanceiraRepository.findAll();
        assertThat(propostaFinanceiraList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDescricaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = propostaFinanceiraRepository.findAll().size();
        // set the field null
        propostaFinanceira.setDescricao(null);

        // Create the PropostaFinanceira, which fails.

        restPropostaFinanceiraMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(propostaFinanceira))
            )
            .andExpect(status().isBadRequest());

        List<PropostaFinanceira> propostaFinanceiraList = propostaFinanceiraRepository.findAll();
        assertThat(propostaFinanceiraList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceiras() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList
        restPropostaFinanceiraMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(propostaFinanceira.getId().intValue())))
            .andExpect(jsonPath("$.[*].valor").value(hasItem(sameNumber(DEFAULT_VALOR))))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)))
            .andExpect(jsonPath("$.[*].anexoContentType").value(hasItem(DEFAULT_ANEXO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].anexo").value(hasItem(Base64Utils.encodeToString(DEFAULT_ANEXO))))
            .andExpect(jsonPath("$.[*].dataRegisto").value(hasItem(DEFAULT_DATA_REGISTO.toString())))
            .andExpect(jsonPath("$.[*].estadoProposta").value(hasItem(DEFAULT_ESTADO_PROPOSTA.toString())));
    }

    @Test
    @Transactional
    void getPropostaFinanceira() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get the propostaFinanceira
        restPropostaFinanceiraMockMvc
            .perform(get(ENTITY_API_URL_ID, propostaFinanceira.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(propostaFinanceira.getId().intValue()))
            .andExpect(jsonPath("$.valor").value(sameNumber(DEFAULT_VALOR)))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO))
            .andExpect(jsonPath("$.anexoContentType").value(DEFAULT_ANEXO_CONTENT_TYPE))
            .andExpect(jsonPath("$.anexo").value(Base64Utils.encodeToString(DEFAULT_ANEXO)))
            .andExpect(jsonPath("$.dataRegisto").value(DEFAULT_DATA_REGISTO.toString()))
            .andExpect(jsonPath("$.estadoProposta").value(DEFAULT_ESTADO_PROPOSTA.toString()));
    }

    @Test
    @Transactional
    void getPropostaFinanceirasByIdFiltering() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        Long id = propostaFinanceira.getId();

        defaultPropostaFinanceiraShouldBeFound("id.equals=" + id);
        defaultPropostaFinanceiraShouldNotBeFound("id.notEquals=" + id);

        defaultPropostaFinanceiraShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPropostaFinanceiraShouldNotBeFound("id.greaterThan=" + id);

        defaultPropostaFinanceiraShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPropostaFinanceiraShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByValorIsEqualToSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where valor equals to DEFAULT_VALOR
        defaultPropostaFinanceiraShouldBeFound("valor.equals=" + DEFAULT_VALOR);

        // Get all the propostaFinanceiraList where valor equals to UPDATED_VALOR
        defaultPropostaFinanceiraShouldNotBeFound("valor.equals=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByValorIsNotEqualToSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where valor not equals to DEFAULT_VALOR
        defaultPropostaFinanceiraShouldNotBeFound("valor.notEquals=" + DEFAULT_VALOR);

        // Get all the propostaFinanceiraList where valor not equals to UPDATED_VALOR
        defaultPropostaFinanceiraShouldBeFound("valor.notEquals=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByValorIsInShouldWork() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where valor in DEFAULT_VALOR or UPDATED_VALOR
        defaultPropostaFinanceiraShouldBeFound("valor.in=" + DEFAULT_VALOR + "," + UPDATED_VALOR);

        // Get all the propostaFinanceiraList where valor equals to UPDATED_VALOR
        defaultPropostaFinanceiraShouldNotBeFound("valor.in=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByValorIsNullOrNotNull() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where valor is not null
        defaultPropostaFinanceiraShouldBeFound("valor.specified=true");

        // Get all the propostaFinanceiraList where valor is null
        defaultPropostaFinanceiraShouldNotBeFound("valor.specified=false");
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByValorIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where valor is greater than or equal to DEFAULT_VALOR
        defaultPropostaFinanceiraShouldBeFound("valor.greaterThanOrEqual=" + DEFAULT_VALOR);

        // Get all the propostaFinanceiraList where valor is greater than or equal to UPDATED_VALOR
        defaultPropostaFinanceiraShouldNotBeFound("valor.greaterThanOrEqual=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByValorIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where valor is less than or equal to DEFAULT_VALOR
        defaultPropostaFinanceiraShouldBeFound("valor.lessThanOrEqual=" + DEFAULT_VALOR);

        // Get all the propostaFinanceiraList where valor is less than or equal to SMALLER_VALOR
        defaultPropostaFinanceiraShouldNotBeFound("valor.lessThanOrEqual=" + SMALLER_VALOR);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByValorIsLessThanSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where valor is less than DEFAULT_VALOR
        defaultPropostaFinanceiraShouldNotBeFound("valor.lessThan=" + DEFAULT_VALOR);

        // Get all the propostaFinanceiraList where valor is less than UPDATED_VALOR
        defaultPropostaFinanceiraShouldBeFound("valor.lessThan=" + UPDATED_VALOR);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByValorIsGreaterThanSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where valor is greater than DEFAULT_VALOR
        defaultPropostaFinanceiraShouldNotBeFound("valor.greaterThan=" + DEFAULT_VALOR);

        // Get all the propostaFinanceiraList where valor is greater than SMALLER_VALOR
        defaultPropostaFinanceiraShouldBeFound("valor.greaterThan=" + SMALLER_VALOR);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByDescricaoIsEqualToSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where descricao equals to DEFAULT_DESCRICAO
        defaultPropostaFinanceiraShouldBeFound("descricao.equals=" + DEFAULT_DESCRICAO);

        // Get all the propostaFinanceiraList where descricao equals to UPDATED_DESCRICAO
        defaultPropostaFinanceiraShouldNotBeFound("descricao.equals=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByDescricaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where descricao not equals to DEFAULT_DESCRICAO
        defaultPropostaFinanceiraShouldNotBeFound("descricao.notEquals=" + DEFAULT_DESCRICAO);

        // Get all the propostaFinanceiraList where descricao not equals to UPDATED_DESCRICAO
        defaultPropostaFinanceiraShouldBeFound("descricao.notEquals=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByDescricaoIsInShouldWork() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where descricao in DEFAULT_DESCRICAO or UPDATED_DESCRICAO
        defaultPropostaFinanceiraShouldBeFound("descricao.in=" + DEFAULT_DESCRICAO + "," + UPDATED_DESCRICAO);

        // Get all the propostaFinanceiraList where descricao equals to UPDATED_DESCRICAO
        defaultPropostaFinanceiraShouldNotBeFound("descricao.in=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByDescricaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where descricao is not null
        defaultPropostaFinanceiraShouldBeFound("descricao.specified=true");

        // Get all the propostaFinanceiraList where descricao is null
        defaultPropostaFinanceiraShouldNotBeFound("descricao.specified=false");
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByDescricaoContainsSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where descricao contains DEFAULT_DESCRICAO
        defaultPropostaFinanceiraShouldBeFound("descricao.contains=" + DEFAULT_DESCRICAO);

        // Get all the propostaFinanceiraList where descricao contains UPDATED_DESCRICAO
        defaultPropostaFinanceiraShouldNotBeFound("descricao.contains=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByDescricaoNotContainsSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where descricao does not contain DEFAULT_DESCRICAO
        defaultPropostaFinanceiraShouldNotBeFound("descricao.doesNotContain=" + DEFAULT_DESCRICAO);

        // Get all the propostaFinanceiraList where descricao does not contain UPDATED_DESCRICAO
        defaultPropostaFinanceiraShouldBeFound("descricao.doesNotContain=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByDataRegistoIsEqualToSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where dataRegisto equals to DEFAULT_DATA_REGISTO
        defaultPropostaFinanceiraShouldBeFound("dataRegisto.equals=" + DEFAULT_DATA_REGISTO);

        // Get all the propostaFinanceiraList where dataRegisto equals to UPDATED_DATA_REGISTO
        defaultPropostaFinanceiraShouldNotBeFound("dataRegisto.equals=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByDataRegistoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where dataRegisto not equals to DEFAULT_DATA_REGISTO
        defaultPropostaFinanceiraShouldNotBeFound("dataRegisto.notEquals=" + DEFAULT_DATA_REGISTO);

        // Get all the propostaFinanceiraList where dataRegisto not equals to UPDATED_DATA_REGISTO
        defaultPropostaFinanceiraShouldBeFound("dataRegisto.notEquals=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByDataRegistoIsInShouldWork() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where dataRegisto in DEFAULT_DATA_REGISTO or UPDATED_DATA_REGISTO
        defaultPropostaFinanceiraShouldBeFound("dataRegisto.in=" + DEFAULT_DATA_REGISTO + "," + UPDATED_DATA_REGISTO);

        // Get all the propostaFinanceiraList where dataRegisto equals to UPDATED_DATA_REGISTO
        defaultPropostaFinanceiraShouldNotBeFound("dataRegisto.in=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByDataRegistoIsNullOrNotNull() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where dataRegisto is not null
        defaultPropostaFinanceiraShouldBeFound("dataRegisto.specified=true");

        // Get all the propostaFinanceiraList where dataRegisto is null
        defaultPropostaFinanceiraShouldNotBeFound("dataRegisto.specified=false");
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByDataRegistoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where dataRegisto is greater than or equal to DEFAULT_DATA_REGISTO
        defaultPropostaFinanceiraShouldBeFound("dataRegisto.greaterThanOrEqual=" + DEFAULT_DATA_REGISTO);

        // Get all the propostaFinanceiraList where dataRegisto is greater than or equal to UPDATED_DATA_REGISTO
        defaultPropostaFinanceiraShouldNotBeFound("dataRegisto.greaterThanOrEqual=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByDataRegistoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where dataRegisto is less than or equal to DEFAULT_DATA_REGISTO
        defaultPropostaFinanceiraShouldBeFound("dataRegisto.lessThanOrEqual=" + DEFAULT_DATA_REGISTO);

        // Get all the propostaFinanceiraList where dataRegisto is less than or equal to SMALLER_DATA_REGISTO
        defaultPropostaFinanceiraShouldNotBeFound("dataRegisto.lessThanOrEqual=" + SMALLER_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByDataRegistoIsLessThanSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where dataRegisto is less than DEFAULT_DATA_REGISTO
        defaultPropostaFinanceiraShouldNotBeFound("dataRegisto.lessThan=" + DEFAULT_DATA_REGISTO);

        // Get all the propostaFinanceiraList where dataRegisto is less than UPDATED_DATA_REGISTO
        defaultPropostaFinanceiraShouldBeFound("dataRegisto.lessThan=" + UPDATED_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByDataRegistoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where dataRegisto is greater than DEFAULT_DATA_REGISTO
        defaultPropostaFinanceiraShouldNotBeFound("dataRegisto.greaterThan=" + DEFAULT_DATA_REGISTO);

        // Get all the propostaFinanceiraList where dataRegisto is greater than SMALLER_DATA_REGISTO
        defaultPropostaFinanceiraShouldBeFound("dataRegisto.greaterThan=" + SMALLER_DATA_REGISTO);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByEstadoPropostaIsEqualToSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where estadoProposta equals to DEFAULT_ESTADO_PROPOSTA
        defaultPropostaFinanceiraShouldBeFound("estadoProposta.equals=" + DEFAULT_ESTADO_PROPOSTA);

        // Get all the propostaFinanceiraList where estadoProposta equals to UPDATED_ESTADO_PROPOSTA
        defaultPropostaFinanceiraShouldNotBeFound("estadoProposta.equals=" + UPDATED_ESTADO_PROPOSTA);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByEstadoPropostaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where estadoProposta not equals to DEFAULT_ESTADO_PROPOSTA
        defaultPropostaFinanceiraShouldNotBeFound("estadoProposta.notEquals=" + DEFAULT_ESTADO_PROPOSTA);

        // Get all the propostaFinanceiraList where estadoProposta not equals to UPDATED_ESTADO_PROPOSTA
        defaultPropostaFinanceiraShouldBeFound("estadoProposta.notEquals=" + UPDATED_ESTADO_PROPOSTA);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByEstadoPropostaIsInShouldWork() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where estadoProposta in DEFAULT_ESTADO_PROPOSTA or UPDATED_ESTADO_PROPOSTA
        defaultPropostaFinanceiraShouldBeFound("estadoProposta.in=" + DEFAULT_ESTADO_PROPOSTA + "," + UPDATED_ESTADO_PROPOSTA);

        // Get all the propostaFinanceiraList where estadoProposta equals to UPDATED_ESTADO_PROPOSTA
        defaultPropostaFinanceiraShouldNotBeFound("estadoProposta.in=" + UPDATED_ESTADO_PROPOSTA);
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByEstadoPropostaIsNullOrNotNull() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        // Get all the propostaFinanceiraList where estadoProposta is not null
        defaultPropostaFinanceiraShouldBeFound("estadoProposta.specified=true");

        // Get all the propostaFinanceiraList where estadoProposta is null
        defaultPropostaFinanceiraShouldNotBeFound("estadoProposta.specified=false");
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByFornecedorIsEqualToSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);
        Fornecedor fornecedor;
        if (TestUtil.findAll(em, Fornecedor.class).isEmpty()) {
            fornecedor = FornecedorResourceIT.createEntity(em);
            em.persist(fornecedor);
            em.flush();
        } else {
            fornecedor = TestUtil.findAll(em, Fornecedor.class).get(0);
        }
        em.persist(fornecedor);
        em.flush();
        propostaFinanceira.setFornecedor(fornecedor);
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);
        Long fornecedorId = fornecedor.getId();

        // Get all the propostaFinanceiraList where fornecedor equals to fornecedorId
        defaultPropostaFinanceiraShouldBeFound("fornecedorId.equals=" + fornecedorId);

        // Get all the propostaFinanceiraList where fornecedor equals to (fornecedorId + 1)
        defaultPropostaFinanceiraShouldNotBeFound("fornecedorId.equals=" + (fornecedorId + 1));
    }

    @Test
    @Transactional
    void getAllPropostaFinanceirasByAquisicaoIsEqualToSomething() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);
        Aquisicao aquisicao;
        if (TestUtil.findAll(em, Aquisicao.class).isEmpty()) {
            aquisicao = AquisicaoResourceIT.createEntity(em);
            em.persist(aquisicao);
            em.flush();
        } else {
            aquisicao = TestUtil.findAll(em, Aquisicao.class).get(0);
        }
        em.persist(aquisicao);
        em.flush();
        propostaFinanceira.setAquisicao(aquisicao);
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);
        Long aquisicaoId = aquisicao.getId();

        // Get all the propostaFinanceiraList where aquisicao equals to aquisicaoId
        defaultPropostaFinanceiraShouldBeFound("aquisicaoId.equals=" + aquisicaoId);

        // Get all the propostaFinanceiraList where aquisicao equals to (aquisicaoId + 1)
        defaultPropostaFinanceiraShouldNotBeFound("aquisicaoId.equals=" + (aquisicaoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPropostaFinanceiraShouldBeFound(String filter) throws Exception {
        restPropostaFinanceiraMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(propostaFinanceira.getId().intValue())))
            .andExpect(jsonPath("$.[*].valor").value(hasItem(sameNumber(DEFAULT_VALOR))))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)))
            .andExpect(jsonPath("$.[*].anexoContentType").value(hasItem(DEFAULT_ANEXO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].anexo").value(hasItem(Base64Utils.encodeToString(DEFAULT_ANEXO))))
            .andExpect(jsonPath("$.[*].dataRegisto").value(hasItem(DEFAULT_DATA_REGISTO.toString())))
            .andExpect(jsonPath("$.[*].estadoProposta").value(hasItem(DEFAULT_ESTADO_PROPOSTA.toString())));

        // Check, that the count call also returns 1
        restPropostaFinanceiraMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPropostaFinanceiraShouldNotBeFound(String filter) throws Exception {
        restPropostaFinanceiraMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPropostaFinanceiraMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPropostaFinanceira() throws Exception {
        // Get the propostaFinanceira
        restPropostaFinanceiraMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPropostaFinanceira() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        int databaseSizeBeforeUpdate = propostaFinanceiraRepository.findAll().size();

        // Update the propostaFinanceira
        PropostaFinanceira updatedPropostaFinanceira = propostaFinanceiraRepository.findById(propostaFinanceira.getId()).get();
        // Disconnect from session so that the updates on updatedPropostaFinanceira are not directly saved in db
        em.detach(updatedPropostaFinanceira);
        updatedPropostaFinanceira
            .valor(UPDATED_VALOR)
            .descricao(UPDATED_DESCRICAO)
            .anexo(UPDATED_ANEXO)
            .anexoContentType(UPDATED_ANEXO_CONTENT_TYPE)
            .dataRegisto(UPDATED_DATA_REGISTO)
            .estadoProposta(UPDATED_ESTADO_PROPOSTA);

        restPropostaFinanceiraMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPropostaFinanceira.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPropostaFinanceira))
            )
            .andExpect(status().isOk());

        // Validate the PropostaFinanceira in the database
        List<PropostaFinanceira> propostaFinanceiraList = propostaFinanceiraRepository.findAll();
        assertThat(propostaFinanceiraList).hasSize(databaseSizeBeforeUpdate);
        PropostaFinanceira testPropostaFinanceira = propostaFinanceiraList.get(propostaFinanceiraList.size() - 1);
        assertThat(testPropostaFinanceira.getValor()).isEqualTo(UPDATED_VALOR);
        assertThat(testPropostaFinanceira.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testPropostaFinanceira.getAnexo()).isEqualTo(UPDATED_ANEXO);
        assertThat(testPropostaFinanceira.getAnexoContentType()).isEqualTo(UPDATED_ANEXO_CONTENT_TYPE);
        assertThat(testPropostaFinanceira.getDataRegisto()).isEqualTo(UPDATED_DATA_REGISTO);
        assertThat(testPropostaFinanceira.getEstadoProposta()).isEqualTo(UPDATED_ESTADO_PROPOSTA);
    }

    @Test
    @Transactional
    void putNonExistingPropostaFinanceira() throws Exception {
        int databaseSizeBeforeUpdate = propostaFinanceiraRepository.findAll().size();
        propostaFinanceira.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPropostaFinanceiraMockMvc
            .perform(
                put(ENTITY_API_URL_ID, propostaFinanceira.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(propostaFinanceira))
            )
            .andExpect(status().isBadRequest());

        // Validate the PropostaFinanceira in the database
        List<PropostaFinanceira> propostaFinanceiraList = propostaFinanceiraRepository.findAll();
        assertThat(propostaFinanceiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPropostaFinanceira() throws Exception {
        int databaseSizeBeforeUpdate = propostaFinanceiraRepository.findAll().size();
        propostaFinanceira.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPropostaFinanceiraMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(propostaFinanceira))
            )
            .andExpect(status().isBadRequest());

        // Validate the PropostaFinanceira in the database
        List<PropostaFinanceira> propostaFinanceiraList = propostaFinanceiraRepository.findAll();
        assertThat(propostaFinanceiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPropostaFinanceira() throws Exception {
        int databaseSizeBeforeUpdate = propostaFinanceiraRepository.findAll().size();
        propostaFinanceira.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPropostaFinanceiraMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(propostaFinanceira))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PropostaFinanceira in the database
        List<PropostaFinanceira> propostaFinanceiraList = propostaFinanceiraRepository.findAll();
        assertThat(propostaFinanceiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePropostaFinanceiraWithPatch() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        int databaseSizeBeforeUpdate = propostaFinanceiraRepository.findAll().size();

        // Update the propostaFinanceira using partial update
        PropostaFinanceira partialUpdatedPropostaFinanceira = new PropostaFinanceira();
        partialUpdatedPropostaFinanceira.setId(propostaFinanceira.getId());

        partialUpdatedPropostaFinanceira.valor(UPDATED_VALOR).descricao(UPDATED_DESCRICAO).estadoProposta(UPDATED_ESTADO_PROPOSTA);

        restPropostaFinanceiraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPropostaFinanceira.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPropostaFinanceira))
            )
            .andExpect(status().isOk());

        // Validate the PropostaFinanceira in the database
        List<PropostaFinanceira> propostaFinanceiraList = propostaFinanceiraRepository.findAll();
        assertThat(propostaFinanceiraList).hasSize(databaseSizeBeforeUpdate);
        PropostaFinanceira testPropostaFinanceira = propostaFinanceiraList.get(propostaFinanceiraList.size() - 1);
        assertThat(testPropostaFinanceira.getValor()).isEqualByComparingTo(UPDATED_VALOR);
        assertThat(testPropostaFinanceira.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testPropostaFinanceira.getAnexo()).isEqualTo(DEFAULT_ANEXO);
        assertThat(testPropostaFinanceira.getAnexoContentType()).isEqualTo(DEFAULT_ANEXO_CONTENT_TYPE);
        assertThat(testPropostaFinanceira.getDataRegisto()).isEqualTo(DEFAULT_DATA_REGISTO);
        assertThat(testPropostaFinanceira.getEstadoProposta()).isEqualTo(UPDATED_ESTADO_PROPOSTA);
    }

    @Test
    @Transactional
    void fullUpdatePropostaFinanceiraWithPatch() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        int databaseSizeBeforeUpdate = propostaFinanceiraRepository.findAll().size();

        // Update the propostaFinanceira using partial update
        PropostaFinanceira partialUpdatedPropostaFinanceira = new PropostaFinanceira();
        partialUpdatedPropostaFinanceira.setId(propostaFinanceira.getId());

        partialUpdatedPropostaFinanceira
            .valor(UPDATED_VALOR)
            .descricao(UPDATED_DESCRICAO)
            .anexo(UPDATED_ANEXO)
            .anexoContentType(UPDATED_ANEXO_CONTENT_TYPE)
            .dataRegisto(UPDATED_DATA_REGISTO)
            .estadoProposta(UPDATED_ESTADO_PROPOSTA);

        restPropostaFinanceiraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPropostaFinanceira.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPropostaFinanceira))
            )
            .andExpect(status().isOk());

        // Validate the PropostaFinanceira in the database
        List<PropostaFinanceira> propostaFinanceiraList = propostaFinanceiraRepository.findAll();
        assertThat(propostaFinanceiraList).hasSize(databaseSizeBeforeUpdate);
        PropostaFinanceira testPropostaFinanceira = propostaFinanceiraList.get(propostaFinanceiraList.size() - 1);
        assertThat(testPropostaFinanceira.getValor()).isEqualByComparingTo(UPDATED_VALOR);
        assertThat(testPropostaFinanceira.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testPropostaFinanceira.getAnexo()).isEqualTo(UPDATED_ANEXO);
        assertThat(testPropostaFinanceira.getAnexoContentType()).isEqualTo(UPDATED_ANEXO_CONTENT_TYPE);
        assertThat(testPropostaFinanceira.getDataRegisto()).isEqualTo(UPDATED_DATA_REGISTO);
        assertThat(testPropostaFinanceira.getEstadoProposta()).isEqualTo(UPDATED_ESTADO_PROPOSTA);
    }

    @Test
    @Transactional
    void patchNonExistingPropostaFinanceira() throws Exception {
        int databaseSizeBeforeUpdate = propostaFinanceiraRepository.findAll().size();
        propostaFinanceira.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPropostaFinanceiraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, propostaFinanceira.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(propostaFinanceira))
            )
            .andExpect(status().isBadRequest());

        // Validate the PropostaFinanceira in the database
        List<PropostaFinanceira> propostaFinanceiraList = propostaFinanceiraRepository.findAll();
        assertThat(propostaFinanceiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPropostaFinanceira() throws Exception {
        int databaseSizeBeforeUpdate = propostaFinanceiraRepository.findAll().size();
        propostaFinanceira.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPropostaFinanceiraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(propostaFinanceira))
            )
            .andExpect(status().isBadRequest());

        // Validate the PropostaFinanceira in the database
        List<PropostaFinanceira> propostaFinanceiraList = propostaFinanceiraRepository.findAll();
        assertThat(propostaFinanceiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPropostaFinanceira() throws Exception {
        int databaseSizeBeforeUpdate = propostaFinanceiraRepository.findAll().size();
        propostaFinanceira.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPropostaFinanceiraMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(propostaFinanceira))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PropostaFinanceira in the database
        List<PropostaFinanceira> propostaFinanceiraList = propostaFinanceiraRepository.findAll();
        assertThat(propostaFinanceiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePropostaFinanceira() throws Exception {
        // Initialize the database
        propostaFinanceiraRepository.saveAndFlush(propostaFinanceira);

        int databaseSizeBeforeDelete = propostaFinanceiraRepository.findAll().size();

        // Delete the propostaFinanceira
        restPropostaFinanceiraMockMvc
            .perform(delete(ENTITY_API_URL_ID, propostaFinanceira.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PropostaFinanceira> propostaFinanceiraList = propostaFinanceiraRepository.findAll();
        assertThat(propostaFinanceiraList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
