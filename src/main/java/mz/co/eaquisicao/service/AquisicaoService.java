package mz.co.eaquisicao.service;

import java.util.Optional;
import mz.co.eaquisicao.domain.Aquisicao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link Aquisicao}.
 */
public interface AquisicaoService {
    /**
     * Save a aquisicao.
     *
     * @param aquisicao the entity to save.
     * @return the persisted entity.
     */
    Aquisicao save(Aquisicao aquisicao);

    /**
     * Partially updates a aquisicao.
     *
     * @param aquisicao the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Aquisicao> partialUpdate(Aquisicao aquisicao);

    /**
     * Get all the aquisicaos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Aquisicao> findAll(Pageable pageable);

    /**
     * Get the "id" aquisicao.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Aquisicao> findOne(Long id);

    /**
     * Delete the "id" aquisicao.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
