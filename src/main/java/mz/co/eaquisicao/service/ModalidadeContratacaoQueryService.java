package mz.co.eaquisicao.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import mz.co.eaquisicao.domain.*; // for static metamodels
import mz.co.eaquisicao.domain.ModalidadeContratacao;
import mz.co.eaquisicao.repository.ModalidadeContratacaoRepository;
import mz.co.eaquisicao.service.criteria.ModalidadeContratacaoCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link ModalidadeContratacao} entities in the database.
 * The main input is a {@link ModalidadeContratacaoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ModalidadeContratacao} or a {@link Page} of {@link ModalidadeContratacao} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ModalidadeContratacaoQueryService extends QueryService<ModalidadeContratacao> {

    private final Logger log = LoggerFactory.getLogger(ModalidadeContratacaoQueryService.class);

    private final ModalidadeContratacaoRepository modalidadeContratacaoRepository;

    public ModalidadeContratacaoQueryService(ModalidadeContratacaoRepository modalidadeContratacaoRepository) {
        this.modalidadeContratacaoRepository = modalidadeContratacaoRepository;
    }

    /**
     * Return a {@link List} of {@link ModalidadeContratacao} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ModalidadeContratacao> findByCriteria(ModalidadeContratacaoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ModalidadeContratacao> specification = createSpecification(criteria);
        return modalidadeContratacaoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ModalidadeContratacao} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ModalidadeContratacao> findByCriteria(ModalidadeContratacaoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ModalidadeContratacao> specification = createSpecification(criteria);
        return modalidadeContratacaoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ModalidadeContratacaoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ModalidadeContratacao> specification = createSpecification(criteria);
        return modalidadeContratacaoRepository.count(specification);
    }

    /**
     * Function to convert {@link ModalidadeContratacaoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ModalidadeContratacao> createSpecification(ModalidadeContratacaoCriteria criteria) {
        Specification<ModalidadeContratacao> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ModalidadeContratacao_.id));
            }
            if (criteria.getNome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNome(), ModalidadeContratacao_.nome));
            }
            if (criteria.getDescricao() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescricao(), ModalidadeContratacao_.descricao));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), ModalidadeContratacao_.createdAt));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), ModalidadeContratacao_.createdBy));
            }
            if (criteria.getLastModificationAt() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getLastModificationAt(), ModalidadeContratacao_.lastModificationAt));
            }
            if (criteria.getLastModificationBy() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getLastModificationBy(), ModalidadeContratacao_.lastModificationBy)
                    );
            }
            if (criteria.getAquisicaoId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getAquisicaoId(),
                            root -> root.join(ModalidadeContratacao_.aquisicaos, JoinType.LEFT).get(Aquisicao_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
