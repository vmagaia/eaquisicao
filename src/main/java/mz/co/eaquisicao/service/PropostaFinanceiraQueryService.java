package mz.co.eaquisicao.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import mz.co.eaquisicao.domain.*; // for static metamodels
import mz.co.eaquisicao.domain.PropostaFinanceira;
import mz.co.eaquisicao.repository.PropostaFinanceiraRepository;
import mz.co.eaquisicao.service.criteria.PropostaFinanceiraCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link PropostaFinanceira} entities in the database.
 * The main input is a {@link PropostaFinanceiraCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PropostaFinanceira} or a {@link Page} of {@link PropostaFinanceira} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PropostaFinanceiraQueryService extends QueryService<PropostaFinanceira> {

    private final Logger log = LoggerFactory.getLogger(PropostaFinanceiraQueryService.class);

    private final PropostaFinanceiraRepository propostaFinanceiraRepository;

    public PropostaFinanceiraQueryService(PropostaFinanceiraRepository propostaFinanceiraRepository) {
        this.propostaFinanceiraRepository = propostaFinanceiraRepository;
    }

    /**
     * Return a {@link List} of {@link PropostaFinanceira} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PropostaFinanceira> findByCriteria(PropostaFinanceiraCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PropostaFinanceira> specification = createSpecification(criteria);
        return propostaFinanceiraRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link PropostaFinanceira} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PropostaFinanceira> findByCriteria(PropostaFinanceiraCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PropostaFinanceira> specification = createSpecification(criteria);
        return propostaFinanceiraRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PropostaFinanceiraCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PropostaFinanceira> specification = createSpecification(criteria);
        return propostaFinanceiraRepository.count(specification);
    }

    /**
     * Function to convert {@link PropostaFinanceiraCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PropostaFinanceira> createSpecification(PropostaFinanceiraCriteria criteria) {
        Specification<PropostaFinanceira> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PropostaFinanceira_.id));
            }
            if (criteria.getValor() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValor(), PropostaFinanceira_.valor));
            }
            if (criteria.getDescricao() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescricao(), PropostaFinanceira_.descricao));
            }
            if (criteria.getEstadoProposta() != null) {
                specification = specification.and(buildSpecification(criteria.getEstadoProposta(), PropostaFinanceira_.estadoProposta));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), PropostaFinanceira_.createdAt));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), PropostaFinanceira_.createdBy));
            }
            if (criteria.getLastModificationAt() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getLastModificationAt(), PropostaFinanceira_.lastModificationAt));
            }
            if (criteria.getLastModificationBy() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getLastModificationBy(), PropostaFinanceira_.lastModificationBy));
            }
            if (criteria.getFornecedorId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getFornecedorId(),
                            root -> root.join(PropostaFinanceira_.fornecedor, JoinType.LEFT).get(Fornecedor_.id)
                        )
                    );
            }
            if (criteria.getAquisicaoId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getAquisicaoId(),
                            root -> root.join(PropostaFinanceira_.aquisicao, JoinType.LEFT).get(Aquisicao_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
