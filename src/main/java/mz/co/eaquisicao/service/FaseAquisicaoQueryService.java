package mz.co.eaquisicao.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import mz.co.eaquisicao.domain.*; // for static metamodels
import mz.co.eaquisicao.domain.FaseAquisicao;
import mz.co.eaquisicao.repository.FaseAquisicaoRepository;
import mz.co.eaquisicao.service.criteria.FaseAquisicaoCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link FaseAquisicao} entities in the database.
 * The main input is a {@link FaseAquisicaoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FaseAquisicao} or a {@link Page} of {@link FaseAquisicao} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FaseAquisicaoQueryService extends QueryService<FaseAquisicao> {

    private final Logger log = LoggerFactory.getLogger(FaseAquisicaoQueryService.class);

    private final FaseAquisicaoRepository faseAquisicaoRepository;

    public FaseAquisicaoQueryService(FaseAquisicaoRepository faseAquisicaoRepository) {
        this.faseAquisicaoRepository = faseAquisicaoRepository;
    }

    /**
     * Return a {@link List} of {@link FaseAquisicao} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FaseAquisicao> findByCriteria(FaseAquisicaoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FaseAquisicao> specification = createSpecification(criteria);
        return faseAquisicaoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link FaseAquisicao} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FaseAquisicao> findByCriteria(FaseAquisicaoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<FaseAquisicao> specification = createSpecification(criteria);
        return faseAquisicaoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FaseAquisicaoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<FaseAquisicao> specification = createSpecification(criteria);
        return faseAquisicaoRepository.count(specification);
    }

    /**
     * Function to convert {@link FaseAquisicaoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<FaseAquisicao> createSpecification(FaseAquisicaoCriteria criteria) {
        Specification<FaseAquisicao> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), FaseAquisicao_.id));
            }
            if (criteria.getNome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNome(), FaseAquisicao_.nome));
            }
            if (criteria.getDiasExecucao() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDiasExecucao(), FaseAquisicao_.diasExecucao));
            }
            if (criteria.getSequencia() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSequencia(), FaseAquisicao_.sequencia));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), FaseAquisicao_.createdAt));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), FaseAquisicao_.createdBy));
            }
            if (criteria.getLastModificationAt() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getLastModificationAt(), FaseAquisicao_.lastModificationAt));
            }
            if (criteria.getLastModificationBy() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getLastModificationBy(), FaseAquisicao_.lastModificationBy));
            }
            if (criteria.getAquisicaoId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getAquisicaoId(),
                            root -> root.join(FaseAquisicao_.aquisicaos, JoinType.LEFT).get(Aquisicao_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
