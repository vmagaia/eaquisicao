package mz.co.eaquisicao.service;

import java.util.Optional;
import mz.co.eaquisicao.domain.PropostaFinanceira;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link PropostaFinanceira}.
 */
public interface PropostaFinanceiraService {
    /**
     * Save a propostaFinanceira.
     *
     * @param propostaFinanceira the entity to save.
     * @return the persisted entity.
     */
    PropostaFinanceira save(PropostaFinanceira propostaFinanceira);

    /**
     * Partially updates a propostaFinanceira.
     *
     * @param propostaFinanceira the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PropostaFinanceira> partialUpdate(PropostaFinanceira propostaFinanceira);

    /**
     * Get all the propostaFinanceiras.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PropostaFinanceira> findAll(Pageable pageable);

    /**
     * Get the "id" propostaFinanceira.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PropostaFinanceira> findOne(Long id);

    /**
     * Delete the "id" propostaFinanceira.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
