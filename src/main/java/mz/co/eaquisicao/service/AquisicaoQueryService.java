package mz.co.eaquisicao.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import mz.co.eaquisicao.domain.*; // for static metamodels
import mz.co.eaquisicao.domain.Aquisicao;
import mz.co.eaquisicao.repository.AquisicaoRepository;
import mz.co.eaquisicao.service.criteria.AquisicaoCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Aquisicao} entities in the database.
 * The main input is a {@link AquisicaoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Aquisicao} or a {@link Page} of {@link Aquisicao} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AquisicaoQueryService extends QueryService<Aquisicao> {

    private final Logger log = LoggerFactory.getLogger(AquisicaoQueryService.class);

    private final AquisicaoRepository aquisicaoRepository;

    public AquisicaoQueryService(AquisicaoRepository aquisicaoRepository) {
        this.aquisicaoRepository = aquisicaoRepository;
    }

    /**
     * Return a {@link List} of {@link Aquisicao} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Aquisicao> findByCriteria(AquisicaoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Aquisicao> specification = createSpecification(criteria);
        return aquisicaoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Aquisicao} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Aquisicao> findByCriteria(AquisicaoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Aquisicao> specification = createSpecification(criteria);
        return aquisicaoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AquisicaoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Aquisicao> specification = createSpecification(criteria);
        return aquisicaoRepository.count(specification);
    }

    /**
     * Function to convert {@link AquisicaoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Aquisicao> createSpecification(AquisicaoCriteria criteria) {
        Specification<Aquisicao> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Aquisicao_.id));
            }
            if (criteria.getDescricao() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescricao(), Aquisicao_.descricao));
            }
            if (criteria.getValorPrevisto() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValorPrevisto(), Aquisicao_.valorPrevisto));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), Aquisicao_.createdAt));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Aquisicao_.createdBy));
            }
            if (criteria.getLastModificationAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModificationAt(), Aquisicao_.lastModificationAt));
            }
            if (criteria.getLastModificationBy() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getLastModificationBy(), Aquisicao_.lastModificationBy));
            }
            if (criteria.getContratoId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getContratoId(),
                            root -> root.join(Aquisicao_.contrato, JoinType.LEFT).get(Contrato_.id)
                        )
                    );
            }
            if (criteria.getPropostaFinanceiraId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPropostaFinanceiraId(),
                            root -> root.join(Aquisicao_.propostaFinanceiras, JoinType.LEFT).get(PropostaFinanceira_.id)
                        )
                    );
            }
            if (criteria.getCategoriaId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCategoriaId(),
                            root -> root.join(Aquisicao_.categoria, JoinType.LEFT).get(Categoria_.id)
                        )
                    );
            }
            if (criteria.getFaseAquisicaoId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getFaseAquisicaoId(),
                            root -> root.join(Aquisicao_.faseAquisicao, JoinType.LEFT).get(FaseAquisicao_.id)
                        )
                    );
            }
            if (criteria.getLinhaOrcamentoId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getLinhaOrcamentoId(),
                            root -> root.join(Aquisicao_.linhaOrcamento, JoinType.LEFT).get(LinhaOrcamento_.id)
                        )
                    );
            }
            if (criteria.getModalidadeContratacaoId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getModalidadeContratacaoId(),
                            root -> root.join(Aquisicao_.modalidadeContratacao, JoinType.LEFT).get(ModalidadeContratacao_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
