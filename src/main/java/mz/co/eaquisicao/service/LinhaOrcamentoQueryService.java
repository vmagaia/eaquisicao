package mz.co.eaquisicao.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import mz.co.eaquisicao.domain.*; // for static metamodels
import mz.co.eaquisicao.domain.LinhaOrcamento;
import mz.co.eaquisicao.repository.LinhaOrcamentoRepository;
import mz.co.eaquisicao.service.criteria.LinhaOrcamentoCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link LinhaOrcamento} entities in the database.
 * The main input is a {@link LinhaOrcamentoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LinhaOrcamento} or a {@link Page} of {@link LinhaOrcamento} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LinhaOrcamentoQueryService extends QueryService<LinhaOrcamento> {

    private final Logger log = LoggerFactory.getLogger(LinhaOrcamentoQueryService.class);

    private final LinhaOrcamentoRepository linhaOrcamentoRepository;

    public LinhaOrcamentoQueryService(LinhaOrcamentoRepository linhaOrcamentoRepository) {
        this.linhaOrcamentoRepository = linhaOrcamentoRepository;
    }

    /**
     * Return a {@link List} of {@link LinhaOrcamento} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LinhaOrcamento> findByCriteria(LinhaOrcamentoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LinhaOrcamento> specification = createSpecification(criteria);
        return linhaOrcamentoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link LinhaOrcamento} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LinhaOrcamento> findByCriteria(LinhaOrcamentoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LinhaOrcamento> specification = createSpecification(criteria);
        return linhaOrcamentoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LinhaOrcamentoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LinhaOrcamento> specification = createSpecification(criteria);
        return linhaOrcamentoRepository.count(specification);
    }

    /**
     * Function to convert {@link LinhaOrcamentoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LinhaOrcamento> createSpecification(LinhaOrcamentoCriteria criteria) {
        Specification<LinhaOrcamento> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LinhaOrcamento_.id));
            }
            if (criteria.getNome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNome(), LinhaOrcamento_.nome));
            }
            if (criteria.getEntidadeFinanceira() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getEntidadeFinanceira(), LinhaOrcamento_.entidadeFinanceira));
            }
            if (criteria.getRubrica() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRubrica(), LinhaOrcamento_.rubrica));
            }
            if (criteria.getDescricaoRubrica() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getDescricaoRubrica(), LinhaOrcamento_.descricaoRubrica));
            }
            if (criteria.getValor() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValor(), LinhaOrcamento_.valor));
            }
            if (criteria.getValorExecutado() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValorExecutado(), LinhaOrcamento_.valorExecutado));
            }
            if (criteria.getSaldo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSaldo(), LinhaOrcamento_.saldo));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), LinhaOrcamento_.createdAt));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), LinhaOrcamento_.createdBy));
            }
            if (criteria.getLastModificationAt() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getLastModificationAt(), LinhaOrcamento_.lastModificationAt));
            }
            if (criteria.getLastModificationBy() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getLastModificationBy(), LinhaOrcamento_.lastModificationBy));
            }
            if (criteria.getAquisicaoId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getAquisicaoId(),
                            root -> root.join(LinhaOrcamento_.aquisicaos, JoinType.LEFT).get(Aquisicao_.id)
                        )
                    );
            }
            if (criteria.getContratoId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getContratoId(),
                            root -> root.join(LinhaOrcamento_.contratoes, JoinType.LEFT).get(Contrato_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
