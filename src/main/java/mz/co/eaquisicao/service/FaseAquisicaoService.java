package mz.co.eaquisicao.service;

import java.util.Optional;
import mz.co.eaquisicao.domain.FaseAquisicao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link FaseAquisicao}.
 */
public interface FaseAquisicaoService {
    /**
     * Save a faseAquisicao.
     *
     * @param faseAquisicao the entity to save.
     * @return the persisted entity.
     */
    FaseAquisicao save(FaseAquisicao faseAquisicao);

    /**
     * Partially updates a faseAquisicao.
     *
     * @param faseAquisicao the entity to update partially.
     * @return the persisted entity.
     */
    Optional<FaseAquisicao> partialUpdate(FaseAquisicao faseAquisicao);

    /**
     * Get all the faseAquisicaos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<FaseAquisicao> findAll(Pageable pageable);

    /**
     * Get the "id" faseAquisicao.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FaseAquisicao> findOne(Long id);

    /**
     * Delete the "id" faseAquisicao.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
