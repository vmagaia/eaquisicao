package mz.co.eaquisicao.service;

import java.util.Optional;
import mz.co.eaquisicao.domain.ModalidadeContratacao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link ModalidadeContratacao}.
 */
public interface ModalidadeContratacaoService {
    /**
     * Save a modalidadeContratacao.
     *
     * @param modalidadeContratacao the entity to save.
     * @return the persisted entity.
     */
    ModalidadeContratacao save(ModalidadeContratacao modalidadeContratacao);

    /**
     * Partially updates a modalidadeContratacao.
     *
     * @param modalidadeContratacao the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ModalidadeContratacao> partialUpdate(ModalidadeContratacao modalidadeContratacao);

    /**
     * Get all the modalidadeContratacaos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ModalidadeContratacao> findAll(Pageable pageable);

    /**
     * Get the "id" modalidadeContratacao.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ModalidadeContratacao> findOne(Long id);

    /**
     * Delete the "id" modalidadeContratacao.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
