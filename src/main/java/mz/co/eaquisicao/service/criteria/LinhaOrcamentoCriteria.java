package mz.co.eaquisicao.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BigDecimalFilter;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link mz.co.eaquisicao.domain.LinhaOrcamento} entity. This class is used
 * in {@link mz.co.eaquisicao.web.rest.LinhaOrcamentoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /linha-orcamentos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LinhaOrcamentoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nome;

    private StringFilter entidadeFinanceira;

    private StringFilter rubrica;

    private StringFilter descricaoRubrica;

    private BigDecimalFilter valor;

    private BigDecimalFilter valorExecutado;

    private BigDecimalFilter saldo;

    private InstantFilter createdAt;

    private StringFilter createdBy;

    private InstantFilter lastModificationAt;

    private StringFilter lastModificationBy;

    private LongFilter aquisicaoId;

    private LongFilter contratoId;

    private Boolean distinct;

    public LinhaOrcamentoCriteria() {}

    public LinhaOrcamentoCriteria(LinhaOrcamentoCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.nome = other.nome == null ? null : other.nome.copy();
        this.entidadeFinanceira = other.entidadeFinanceira == null ? null : other.entidadeFinanceira.copy();
        this.rubrica = other.rubrica == null ? null : other.rubrica.copy();
        this.descricaoRubrica = other.descricaoRubrica == null ? null : other.descricaoRubrica.copy();
        this.valor = other.valor == null ? null : other.valor.copy();
        this.valorExecutado = other.valorExecutado == null ? null : other.valorExecutado.copy();
        this.saldo = other.saldo == null ? null : other.saldo.copy();
        this.createdAt = other.createdAt == null ? null : other.createdAt.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModificationAt = other.lastModificationAt == null ? null : other.lastModificationAt.copy();
        this.lastModificationBy = other.lastModificationBy == null ? null : other.lastModificationBy.copy();
        this.aquisicaoId = other.aquisicaoId == null ? null : other.aquisicaoId.copy();
        this.contratoId = other.contratoId == null ? null : other.contratoId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public LinhaOrcamentoCriteria copy() {
        return new LinhaOrcamentoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNome() {
        return nome;
    }

    public StringFilter nome() {
        if (nome == null) {
            nome = new StringFilter();
        }
        return nome;
    }

    public void setNome(StringFilter nome) {
        this.nome = nome;
    }

    public StringFilter getEntidadeFinanceira() {
        return entidadeFinanceira;
    }

    public StringFilter entidadeFinanceira() {
        if (entidadeFinanceira == null) {
            entidadeFinanceira = new StringFilter();
        }
        return entidadeFinanceira;
    }

    public void setEntidadeFinanceira(StringFilter entidadeFinanceira) {
        this.entidadeFinanceira = entidadeFinanceira;
    }

    public StringFilter getRubrica() {
        return rubrica;
    }

    public StringFilter rubrica() {
        if (rubrica == null) {
            rubrica = new StringFilter();
        }
        return rubrica;
    }

    public void setRubrica(StringFilter rubrica) {
        this.rubrica = rubrica;
    }

    public StringFilter getDescricaoRubrica() {
        return descricaoRubrica;
    }

    public StringFilter descricaoRubrica() {
        if (descricaoRubrica == null) {
            descricaoRubrica = new StringFilter();
        }
        return descricaoRubrica;
    }

    public void setDescricaoRubrica(StringFilter descricaoRubrica) {
        this.descricaoRubrica = descricaoRubrica;
    }

    public BigDecimalFilter getValor() {
        return valor;
    }

    public BigDecimalFilter valor() {
        if (valor == null) {
            valor = new BigDecimalFilter();
        }
        return valor;
    }

    public void setValor(BigDecimalFilter valor) {
        this.valor = valor;
    }

    public BigDecimalFilter getValorExecutado() {
        return valorExecutado;
    }

    public BigDecimalFilter valorExecutado() {
        if (valorExecutado == null) {
            valorExecutado = new BigDecimalFilter();
        }
        return valorExecutado;
    }

    public void setValorExecutado(BigDecimalFilter valorExecutado) {
        this.valorExecutado = valorExecutado;
    }

    public BigDecimalFilter getSaldo() {
        return saldo;
    }

    public BigDecimalFilter saldo() {
        if (saldo == null) {
            saldo = new BigDecimalFilter();
        }
        return saldo;
    }

    public void setSaldo(BigDecimalFilter saldo) {
        this.saldo = saldo;
    }

    public InstantFilter getCreatedAt() {
        return createdAt;
    }

    public InstantFilter createdAt() {
        if (createdAt == null) {
            createdAt = new InstantFilter();
        }
        return createdAt;
    }

    public void setCreatedAt(InstantFilter createdAt) {
        this.createdAt = createdAt;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getLastModificationAt() {
        return lastModificationAt;
    }

    public InstantFilter lastModificationAt() {
        if (lastModificationAt == null) {
            lastModificationAt = new InstantFilter();
        }
        return lastModificationAt;
    }

    public void setLastModificationAt(InstantFilter lastModificationAt) {
        this.lastModificationAt = lastModificationAt;
    }

    public StringFilter getLastModificationBy() {
        return lastModificationBy;
    }

    public StringFilter lastModificationBy() {
        if (lastModificationBy == null) {
            lastModificationBy = new StringFilter();
        }
        return lastModificationBy;
    }

    public void setLastModificationBy(StringFilter lastModificationBy) {
        this.lastModificationBy = lastModificationBy;
    }

    public LongFilter getAquisicaoId() {
        return aquisicaoId;
    }

    public LongFilter aquisicaoId() {
        if (aquisicaoId == null) {
            aquisicaoId = new LongFilter();
        }
        return aquisicaoId;
    }

    public void setAquisicaoId(LongFilter aquisicaoId) {
        this.aquisicaoId = aquisicaoId;
    }

    public LongFilter getContratoId() {
        return contratoId;
    }

    public LongFilter contratoId() {
        if (contratoId == null) {
            contratoId = new LongFilter();
        }
        return contratoId;
    }

    public void setContratoId(LongFilter contratoId) {
        this.contratoId = contratoId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LinhaOrcamentoCriteria that = (LinhaOrcamentoCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(nome, that.nome) &&
            Objects.equals(entidadeFinanceira, that.entidadeFinanceira) &&
            Objects.equals(rubrica, that.rubrica) &&
            Objects.equals(descricaoRubrica, that.descricaoRubrica) &&
            Objects.equals(valor, that.valor) &&
            Objects.equals(valorExecutado, that.valorExecutado) &&
            Objects.equals(saldo, that.saldo) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModificationAt, that.lastModificationAt) &&
            Objects.equals(lastModificationBy, that.lastModificationBy) &&
            Objects.equals(aquisicaoId, that.aquisicaoId) &&
            Objects.equals(contratoId, that.contratoId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            nome,
            entidadeFinanceira,
            rubrica,
            descricaoRubrica,
            valor,
            valorExecutado,
            saldo,
            createdAt,
            createdBy,
            lastModificationAt,
            lastModificationBy,
            aquisicaoId,
            contratoId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LinhaOrcamentoCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (nome != null ? "nome=" + nome + ", " : "") +
            (entidadeFinanceira != null ? "entidadeFinanceira=" + entidadeFinanceira + ", " : "") +
            (rubrica != null ? "rubrica=" + rubrica + ", " : "") +
            (descricaoRubrica != null ? "descricaoRubrica=" + descricaoRubrica + ", " : "") +
            (valor != null ? "valor=" + valor + ", " : "") +
            (valorExecutado != null ? "valorExecutado=" + valorExecutado + ", " : "") +
            (saldo != null ? "saldo=" + saldo + ", " : "") +
            (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (lastModificationAt != null ? "lastModificationAt=" + lastModificationAt + ", " : "") +
            (lastModificationBy != null ? "lastModificationBy=" + lastModificationBy + ", " : "") +
            (aquisicaoId != null ? "aquisicaoId=" + aquisicaoId + ", " : "") +
            (contratoId != null ? "contratoId=" + contratoId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
