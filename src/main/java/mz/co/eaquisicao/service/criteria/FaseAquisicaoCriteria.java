package mz.co.eaquisicao.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link mz.co.eaquisicao.domain.FaseAquisicao} entity. This class is used
 * in {@link mz.co.eaquisicao.web.rest.FaseAquisicaoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /fase-aquisicaos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class FaseAquisicaoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nome;

    private IntegerFilter diasExecucao;

    private IntegerFilter sequencia;

    private InstantFilter createdAt;

    private StringFilter createdBy;

    private InstantFilter lastModificationAt;

    private StringFilter lastModificationBy;

    private LongFilter aquisicaoId;

    private Boolean distinct;

    public FaseAquisicaoCriteria() {}

    public FaseAquisicaoCriteria(FaseAquisicaoCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.nome = other.nome == null ? null : other.nome.copy();
        this.diasExecucao = other.diasExecucao == null ? null : other.diasExecucao.copy();
        this.sequencia = other.sequencia == null ? null : other.sequencia.copy();
        this.createdAt = other.createdAt == null ? null : other.createdAt.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModificationAt = other.lastModificationAt == null ? null : other.lastModificationAt.copy();
        this.lastModificationBy = other.lastModificationBy == null ? null : other.lastModificationBy.copy();
        this.aquisicaoId = other.aquisicaoId == null ? null : other.aquisicaoId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public FaseAquisicaoCriteria copy() {
        return new FaseAquisicaoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNome() {
        return nome;
    }

    public StringFilter nome() {
        if (nome == null) {
            nome = new StringFilter();
        }
        return nome;
    }

    public void setNome(StringFilter nome) {
        this.nome = nome;
    }

    public IntegerFilter getDiasExecucao() {
        return diasExecucao;
    }

    public IntegerFilter diasExecucao() {
        if (diasExecucao == null) {
            diasExecucao = new IntegerFilter();
        }
        return diasExecucao;
    }

    public void setDiasExecucao(IntegerFilter diasExecucao) {
        this.diasExecucao = diasExecucao;
    }

    public IntegerFilter getSequencia() {
        return sequencia;
    }

    public IntegerFilter sequencia() {
        if (sequencia == null) {
            sequencia = new IntegerFilter();
        }
        return sequencia;
    }

    public void setSequencia(IntegerFilter sequencia) {
        this.sequencia = sequencia;
    }

    public InstantFilter getCreatedAt() {
        return createdAt;
    }

    public InstantFilter createdAt() {
        if (createdAt == null) {
            createdAt = new InstantFilter();
        }
        return createdAt;
    }

    public void setCreatedAt(InstantFilter createdAt) {
        this.createdAt = createdAt;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getLastModificationAt() {
        return lastModificationAt;
    }

    public InstantFilter lastModificationAt() {
        if (lastModificationAt == null) {
            lastModificationAt = new InstantFilter();
        }
        return lastModificationAt;
    }

    public void setLastModificationAt(InstantFilter lastModificationAt) {
        this.lastModificationAt = lastModificationAt;
    }

    public StringFilter getLastModificationBy() {
        return lastModificationBy;
    }

    public StringFilter lastModificationBy() {
        if (lastModificationBy == null) {
            lastModificationBy = new StringFilter();
        }
        return lastModificationBy;
    }

    public void setLastModificationBy(StringFilter lastModificationBy) {
        this.lastModificationBy = lastModificationBy;
    }

    public LongFilter getAquisicaoId() {
        return aquisicaoId;
    }

    public LongFilter aquisicaoId() {
        if (aquisicaoId == null) {
            aquisicaoId = new LongFilter();
        }
        return aquisicaoId;
    }

    public void setAquisicaoId(LongFilter aquisicaoId) {
        this.aquisicaoId = aquisicaoId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FaseAquisicaoCriteria that = (FaseAquisicaoCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(nome, that.nome) &&
            Objects.equals(diasExecucao, that.diasExecucao) &&
            Objects.equals(sequencia, that.sequencia) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModificationAt, that.lastModificationAt) &&
            Objects.equals(lastModificationBy, that.lastModificationBy) &&
            Objects.equals(aquisicaoId, that.aquisicaoId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            nome,
            diasExecucao,
            sequencia,
            createdAt,
            createdBy,
            lastModificationAt,
            lastModificationBy,
            aquisicaoId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FaseAquisicaoCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (nome != null ? "nome=" + nome + ", " : "") +
            (diasExecucao != null ? "diasExecucao=" + diasExecucao + ", " : "") +
            (sequencia != null ? "sequencia=" + sequencia + ", " : "") +
            (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (lastModificationAt != null ? "lastModificationAt=" + lastModificationAt + ", " : "") +
            (lastModificationBy != null ? "lastModificationBy=" + lastModificationBy + ", " : "") +
            (aquisicaoId != null ? "aquisicaoId=" + aquisicaoId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
