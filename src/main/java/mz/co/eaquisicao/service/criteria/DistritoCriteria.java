package mz.co.eaquisicao.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link mz.co.eaquisicao.domain.Distrito} entity. This class is used
 * in {@link mz.co.eaquisicao.web.rest.DistritoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /distritos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DistritoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nome;

    private InstantFilter createdAt;

    private StringFilter createdBy;

    private InstantFilter lastModificationAt;

    private StringFilter lastModificationBy;

    private LongFilter provinciaId;

    private Boolean distinct;

    public DistritoCriteria() {}

    public DistritoCriteria(DistritoCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.nome = other.nome == null ? null : other.nome.copy();
        this.createdAt = other.createdAt == null ? null : other.createdAt.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModificationAt = other.lastModificationAt == null ? null : other.lastModificationAt.copy();
        this.lastModificationBy = other.lastModificationBy == null ? null : other.lastModificationBy.copy();
        this.provinciaId = other.provinciaId == null ? null : other.provinciaId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public DistritoCriteria copy() {
        return new DistritoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNome() {
        return nome;
    }

    public StringFilter nome() {
        if (nome == null) {
            nome = new StringFilter();
        }
        return nome;
    }

    public void setNome(StringFilter nome) {
        this.nome = nome;
    }

    public InstantFilter getCreatedAt() {
        return createdAt;
    }

    public InstantFilter createdAt() {
        if (createdAt == null) {
            createdAt = new InstantFilter();
        }
        return createdAt;
    }

    public void setCreatedAt(InstantFilter createdAt) {
        this.createdAt = createdAt;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getLastModificationAt() {
        return lastModificationAt;
    }

    public InstantFilter lastModificationAt() {
        if (lastModificationAt == null) {
            lastModificationAt = new InstantFilter();
        }
        return lastModificationAt;
    }

    public void setLastModificationAt(InstantFilter lastModificationAt) {
        this.lastModificationAt = lastModificationAt;
    }

    public StringFilter getLastModificationBy() {
        return lastModificationBy;
    }

    public StringFilter lastModificationBy() {
        if (lastModificationBy == null) {
            lastModificationBy = new StringFilter();
        }
        return lastModificationBy;
    }

    public void setLastModificationBy(StringFilter lastModificationBy) {
        this.lastModificationBy = lastModificationBy;
    }

    public LongFilter getProvinciaId() {
        return provinciaId;
    }

    public LongFilter provinciaId() {
        if (provinciaId == null) {
            provinciaId = new LongFilter();
        }
        return provinciaId;
    }

    public void setProvinciaId(LongFilter provinciaId) {
        this.provinciaId = provinciaId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DistritoCriteria that = (DistritoCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(nome, that.nome) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModificationAt, that.lastModificationAt) &&
            Objects.equals(lastModificationBy, that.lastModificationBy) &&
            Objects.equals(provinciaId, that.provinciaId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, createdAt, createdBy, lastModificationAt, lastModificationBy, provinciaId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DistritoCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (nome != null ? "nome=" + nome + ", " : "") +
            (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (lastModificationAt != null ? "lastModificationAt=" + lastModificationAt + ", " : "") +
            (lastModificationBy != null ? "lastModificationBy=" + lastModificationBy + ", " : "") +
            (provinciaId != null ? "provinciaId=" + provinciaId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
