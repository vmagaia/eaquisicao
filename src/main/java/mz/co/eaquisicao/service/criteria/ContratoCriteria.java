package mz.co.eaquisicao.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BigDecimalFilter;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LocalDateFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link mz.co.eaquisicao.domain.Contrato} entity. This class is used
 * in {@link mz.co.eaquisicao.web.rest.ContratoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /contratoes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ContratoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter codigo;

    private BigDecimalFilter valor;

    private LocalDateFilter inicioPrevisto;

    private LocalDateFilter terminoPrevisto;

    private LocalDateFilter inicioEfectivo;

    private LocalDateFilter terminoEfectivo;

    private StringFilter descricao;

    private InstantFilter createdAt;

    private StringFilter createdBy;

    private InstantFilter lastModificationAt;

    private StringFilter lastModificationBy;

    private LongFilter propostaFinanceiraId;

    private LongFilter linhaOrcamentoId;

    private Boolean distinct;

    public ContratoCriteria() {}

    public ContratoCriteria(ContratoCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.codigo = other.codigo == null ? null : other.codigo.copy();
        this.valor = other.valor == null ? null : other.valor.copy();
        this.inicioPrevisto = other.inicioPrevisto == null ? null : other.inicioPrevisto.copy();
        this.terminoPrevisto = other.terminoPrevisto == null ? null : other.terminoPrevisto.copy();
        this.inicioEfectivo = other.inicioEfectivo == null ? null : other.inicioEfectivo.copy();
        this.terminoEfectivo = other.terminoEfectivo == null ? null : other.terminoEfectivo.copy();
        this.descricao = other.descricao == null ? null : other.descricao.copy();
        this.createdAt = other.createdAt == null ? null : other.createdAt.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModificationAt = other.lastModificationAt == null ? null : other.lastModificationAt.copy();
        this.lastModificationBy = other.lastModificationBy == null ? null : other.lastModificationBy.copy();
        this.propostaFinanceiraId = other.propostaFinanceiraId == null ? null : other.propostaFinanceiraId.copy();
        this.linhaOrcamentoId = other.linhaOrcamentoId == null ? null : other.linhaOrcamentoId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public ContratoCriteria copy() {
        return new ContratoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getCodigo() {
        return codigo;
    }

    public IntegerFilter codigo() {
        if (codigo == null) {
            codigo = new IntegerFilter();
        }
        return codigo;
    }

    public void setCodigo(IntegerFilter codigo) {
        this.codigo = codigo;
    }

    public BigDecimalFilter getValor() {
        return valor;
    }

    public BigDecimalFilter valor() {
        if (valor == null) {
            valor = new BigDecimalFilter();
        }
        return valor;
    }

    public void setValor(BigDecimalFilter valor) {
        this.valor = valor;
    }

    public LocalDateFilter getInicioPrevisto() {
        return inicioPrevisto;
    }

    public LocalDateFilter inicioPrevisto() {
        if (inicioPrevisto == null) {
            inicioPrevisto = new LocalDateFilter();
        }
        return inicioPrevisto;
    }

    public void setInicioPrevisto(LocalDateFilter inicioPrevisto) {
        this.inicioPrevisto = inicioPrevisto;
    }

    public LocalDateFilter getTerminoPrevisto() {
        return terminoPrevisto;
    }

    public LocalDateFilter terminoPrevisto() {
        if (terminoPrevisto == null) {
            terminoPrevisto = new LocalDateFilter();
        }
        return terminoPrevisto;
    }

    public void setTerminoPrevisto(LocalDateFilter terminoPrevisto) {
        this.terminoPrevisto = terminoPrevisto;
    }

    public LocalDateFilter getInicioEfectivo() {
        return inicioEfectivo;
    }

    public LocalDateFilter inicioEfectivo() {
        if (inicioEfectivo == null) {
            inicioEfectivo = new LocalDateFilter();
        }
        return inicioEfectivo;
    }

    public void setInicioEfectivo(LocalDateFilter inicioEfectivo) {
        this.inicioEfectivo = inicioEfectivo;
    }

    public LocalDateFilter getTerminoEfectivo() {
        return terminoEfectivo;
    }

    public LocalDateFilter terminoEfectivo() {
        if (terminoEfectivo == null) {
            terminoEfectivo = new LocalDateFilter();
        }
        return terminoEfectivo;
    }

    public void setTerminoEfectivo(LocalDateFilter terminoEfectivo) {
        this.terminoEfectivo = terminoEfectivo;
    }

    public StringFilter getDescricao() {
        return descricao;
    }

    public StringFilter descricao() {
        if (descricao == null) {
            descricao = new StringFilter();
        }
        return descricao;
    }

    public void setDescricao(StringFilter descricao) {
        this.descricao = descricao;
    }

    public InstantFilter getCreatedAt() {
        return createdAt;
    }

    public InstantFilter createdAt() {
        if (createdAt == null) {
            createdAt = new InstantFilter();
        }
        return createdAt;
    }

    public void setCreatedAt(InstantFilter createdAt) {
        this.createdAt = createdAt;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getLastModificationAt() {
        return lastModificationAt;
    }

    public InstantFilter lastModificationAt() {
        if (lastModificationAt == null) {
            lastModificationAt = new InstantFilter();
        }
        return lastModificationAt;
    }

    public void setLastModificationAt(InstantFilter lastModificationAt) {
        this.lastModificationAt = lastModificationAt;
    }

    public StringFilter getLastModificationBy() {
        return lastModificationBy;
    }

    public StringFilter lastModificationBy() {
        if (lastModificationBy == null) {
            lastModificationBy = new StringFilter();
        }
        return lastModificationBy;
    }

    public void setLastModificationBy(StringFilter lastModificationBy) {
        this.lastModificationBy = lastModificationBy;
    }

    public LongFilter getPropostaFinanceiraId() {
        return propostaFinanceiraId;
    }

    public LongFilter propostaFinanceiraId() {
        if (propostaFinanceiraId == null) {
            propostaFinanceiraId = new LongFilter();
        }
        return propostaFinanceiraId;
    }

    public void setPropostaFinanceiraId(LongFilter propostaFinanceiraId) {
        this.propostaFinanceiraId = propostaFinanceiraId;
    }

    public LongFilter getLinhaOrcamentoId() {
        return linhaOrcamentoId;
    }

    public LongFilter linhaOrcamentoId() {
        if (linhaOrcamentoId == null) {
            linhaOrcamentoId = new LongFilter();
        }
        return linhaOrcamentoId;
    }

    public void setLinhaOrcamentoId(LongFilter linhaOrcamentoId) {
        this.linhaOrcamentoId = linhaOrcamentoId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ContratoCriteria that = (ContratoCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(codigo, that.codigo) &&
            Objects.equals(valor, that.valor) &&
            Objects.equals(inicioPrevisto, that.inicioPrevisto) &&
            Objects.equals(terminoPrevisto, that.terminoPrevisto) &&
            Objects.equals(inicioEfectivo, that.inicioEfectivo) &&
            Objects.equals(terminoEfectivo, that.terminoEfectivo) &&
            Objects.equals(descricao, that.descricao) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModificationAt, that.lastModificationAt) &&
            Objects.equals(lastModificationBy, that.lastModificationBy) &&
            Objects.equals(propostaFinanceiraId, that.propostaFinanceiraId) &&
            Objects.equals(linhaOrcamentoId, that.linhaOrcamentoId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            codigo,
            valor,
            inicioPrevisto,
            terminoPrevisto,
            inicioEfectivo,
            terminoEfectivo,
            descricao,
            createdAt,
            createdBy,
            lastModificationAt,
            lastModificationBy,
            propostaFinanceiraId,
            linhaOrcamentoId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContratoCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (codigo != null ? "codigo=" + codigo + ", " : "") +
            (valor != null ? "valor=" + valor + ", " : "") +
            (inicioPrevisto != null ? "inicioPrevisto=" + inicioPrevisto + ", " : "") +
            (terminoPrevisto != null ? "terminoPrevisto=" + terminoPrevisto + ", " : "") +
            (inicioEfectivo != null ? "inicioEfectivo=" + inicioEfectivo + ", " : "") +
            (terminoEfectivo != null ? "terminoEfectivo=" + terminoEfectivo + ", " : "") +
            (descricao != null ? "descricao=" + descricao + ", " : "") +
            (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (lastModificationAt != null ? "lastModificationAt=" + lastModificationAt + ", " : "") +
            (lastModificationBy != null ? "lastModificationBy=" + lastModificationBy + ", " : "") +
            (propostaFinanceiraId != null ? "propostaFinanceiraId=" + propostaFinanceiraId + ", " : "") +
            (linhaOrcamentoId != null ? "linhaOrcamentoId=" + linhaOrcamentoId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
