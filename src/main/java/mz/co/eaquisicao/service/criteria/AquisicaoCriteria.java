package mz.co.eaquisicao.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BigDecimalFilter;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link mz.co.eaquisicao.domain.Aquisicao} entity. This class is used
 * in {@link mz.co.eaquisicao.web.rest.AquisicaoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /aquisicaos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AquisicaoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter descricao;

    private BigDecimalFilter valorPrevisto;

    private InstantFilter createdAt;

    private StringFilter createdBy;

    private InstantFilter lastModificationAt;

    private StringFilter lastModificationBy;

    private LongFilter contratoId;

    private LongFilter propostaFinanceiraId;

    private LongFilter categoriaId;

    private LongFilter faseAquisicaoId;

    private LongFilter linhaOrcamentoId;

    private LongFilter modalidadeContratacaoId;

    private Boolean distinct;

    public AquisicaoCriteria() {}

    public AquisicaoCriteria(AquisicaoCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.descricao = other.descricao == null ? null : other.descricao.copy();
        this.valorPrevisto = other.valorPrevisto == null ? null : other.valorPrevisto.copy();
        this.createdAt = other.createdAt == null ? null : other.createdAt.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModificationAt = other.lastModificationAt == null ? null : other.lastModificationAt.copy();
        this.lastModificationBy = other.lastModificationBy == null ? null : other.lastModificationBy.copy();
        this.contratoId = other.contratoId == null ? null : other.contratoId.copy();
        this.propostaFinanceiraId = other.propostaFinanceiraId == null ? null : other.propostaFinanceiraId.copy();
        this.categoriaId = other.categoriaId == null ? null : other.categoriaId.copy();
        this.faseAquisicaoId = other.faseAquisicaoId == null ? null : other.faseAquisicaoId.copy();
        this.linhaOrcamentoId = other.linhaOrcamentoId == null ? null : other.linhaOrcamentoId.copy();
        this.modalidadeContratacaoId = other.modalidadeContratacaoId == null ? null : other.modalidadeContratacaoId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public AquisicaoCriteria copy() {
        return new AquisicaoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDescricao() {
        return descricao;
    }

    public StringFilter descricao() {
        if (descricao == null) {
            descricao = new StringFilter();
        }
        return descricao;
    }

    public void setDescricao(StringFilter descricao) {
        this.descricao = descricao;
    }

    public BigDecimalFilter getValorPrevisto() {
        return valorPrevisto;
    }

    public BigDecimalFilter valorPrevisto() {
        if (valorPrevisto == null) {
            valorPrevisto = new BigDecimalFilter();
        }
        return valorPrevisto;
    }

    public void setValorPrevisto(BigDecimalFilter valorPrevisto) {
        this.valorPrevisto = valorPrevisto;
    }

    public InstantFilter getCreatedAt() {
        return createdAt;
    }

    public InstantFilter createdAt() {
        if (createdAt == null) {
            createdAt = new InstantFilter();
        }
        return createdAt;
    }

    public void setCreatedAt(InstantFilter createdAt) {
        this.createdAt = createdAt;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getLastModificationAt() {
        return lastModificationAt;
    }

    public InstantFilter lastModificationAt() {
        if (lastModificationAt == null) {
            lastModificationAt = new InstantFilter();
        }
        return lastModificationAt;
    }

    public void setLastModificationAt(InstantFilter lastModificationAt) {
        this.lastModificationAt = lastModificationAt;
    }

    public StringFilter getLastModificationBy() {
        return lastModificationBy;
    }

    public StringFilter lastModificationBy() {
        if (lastModificationBy == null) {
            lastModificationBy = new StringFilter();
        }
        return lastModificationBy;
    }

    public void setLastModificationBy(StringFilter lastModificationBy) {
        this.lastModificationBy = lastModificationBy;
    }

    public LongFilter getContratoId() {
        return contratoId;
    }

    public LongFilter contratoId() {
        if (contratoId == null) {
            contratoId = new LongFilter();
        }
        return contratoId;
    }

    public void setContratoId(LongFilter contratoId) {
        this.contratoId = contratoId;
    }

    public LongFilter getPropostaFinanceiraId() {
        return propostaFinanceiraId;
    }

    public LongFilter propostaFinanceiraId() {
        if (propostaFinanceiraId == null) {
            propostaFinanceiraId = new LongFilter();
        }
        return propostaFinanceiraId;
    }

    public void setPropostaFinanceiraId(LongFilter propostaFinanceiraId) {
        this.propostaFinanceiraId = propostaFinanceiraId;
    }

    public LongFilter getCategoriaId() {
        return categoriaId;
    }

    public LongFilter categoriaId() {
        if (categoriaId == null) {
            categoriaId = new LongFilter();
        }
        return categoriaId;
    }

    public void setCategoriaId(LongFilter categoriaId) {
        this.categoriaId = categoriaId;
    }

    public LongFilter getFaseAquisicaoId() {
        return faseAquisicaoId;
    }

    public LongFilter faseAquisicaoId() {
        if (faseAquisicaoId == null) {
            faseAquisicaoId = new LongFilter();
        }
        return faseAquisicaoId;
    }

    public void setFaseAquisicaoId(LongFilter faseAquisicaoId) {
        this.faseAquisicaoId = faseAquisicaoId;
    }

    public LongFilter getLinhaOrcamentoId() {
        return linhaOrcamentoId;
    }

    public LongFilter linhaOrcamentoId() {
        if (linhaOrcamentoId == null) {
            linhaOrcamentoId = new LongFilter();
        }
        return linhaOrcamentoId;
    }

    public void setLinhaOrcamentoId(LongFilter linhaOrcamentoId) {
        this.linhaOrcamentoId = linhaOrcamentoId;
    }

    public LongFilter getModalidadeContratacaoId() {
        return modalidadeContratacaoId;
    }

    public LongFilter modalidadeContratacaoId() {
        if (modalidadeContratacaoId == null) {
            modalidadeContratacaoId = new LongFilter();
        }
        return modalidadeContratacaoId;
    }

    public void setModalidadeContratacaoId(LongFilter modalidadeContratacaoId) {
        this.modalidadeContratacaoId = modalidadeContratacaoId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AquisicaoCriteria that = (AquisicaoCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(descricao, that.descricao) &&
            Objects.equals(valorPrevisto, that.valorPrevisto) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModificationAt, that.lastModificationAt) &&
            Objects.equals(lastModificationBy, that.lastModificationBy) &&
            Objects.equals(contratoId, that.contratoId) &&
            Objects.equals(propostaFinanceiraId, that.propostaFinanceiraId) &&
            Objects.equals(categoriaId, that.categoriaId) &&
            Objects.equals(faseAquisicaoId, that.faseAquisicaoId) &&
            Objects.equals(linhaOrcamentoId, that.linhaOrcamentoId) &&
            Objects.equals(modalidadeContratacaoId, that.modalidadeContratacaoId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            descricao,
            valorPrevisto,
            createdAt,
            createdBy,
            lastModificationAt,
            lastModificationBy,
            contratoId,
            propostaFinanceiraId,
            categoriaId,
            faseAquisicaoId,
            linhaOrcamentoId,
            modalidadeContratacaoId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AquisicaoCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (descricao != null ? "descricao=" + descricao + ", " : "") +
            (valorPrevisto != null ? "valorPrevisto=" + valorPrevisto + ", " : "") +
            (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (lastModificationAt != null ? "lastModificationAt=" + lastModificationAt + ", " : "") +
            (lastModificationBy != null ? "lastModificationBy=" + lastModificationBy + ", " : "") +
            (contratoId != null ? "contratoId=" + contratoId + ", " : "") +
            (propostaFinanceiraId != null ? "propostaFinanceiraId=" + propostaFinanceiraId + ", " : "") +
            (categoriaId != null ? "categoriaId=" + categoriaId + ", " : "") +
            (faseAquisicaoId != null ? "faseAquisicaoId=" + faseAquisicaoId + ", " : "") +
            (linhaOrcamentoId != null ? "linhaOrcamentoId=" + linhaOrcamentoId + ", " : "") +
            (modalidadeContratacaoId != null ? "modalidadeContratacaoId=" + modalidadeContratacaoId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
