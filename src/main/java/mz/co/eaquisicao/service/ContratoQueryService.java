package mz.co.eaquisicao.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import mz.co.eaquisicao.domain.*; // for static metamodels
import mz.co.eaquisicao.domain.Contrato;
import mz.co.eaquisicao.repository.ContratoRepository;
import mz.co.eaquisicao.service.criteria.ContratoCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Contrato} entities in the database.
 * The main input is a {@link ContratoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Contrato} or a {@link Page} of {@link Contrato} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContratoQueryService extends QueryService<Contrato> {

    private final Logger log = LoggerFactory.getLogger(ContratoQueryService.class);

    private final ContratoRepository contratoRepository;

    public ContratoQueryService(ContratoRepository contratoRepository) {
        this.contratoRepository = contratoRepository;
    }

    /**
     * Return a {@link List} of {@link Contrato} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Contrato> findByCriteria(ContratoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Contrato> specification = createSpecification(criteria);
        return contratoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Contrato} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Contrato> findByCriteria(ContratoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Contrato> specification = createSpecification(criteria);
        return contratoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContratoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Contrato> specification = createSpecification(criteria);
        return contratoRepository.count(specification);
    }

    /**
     * Function to convert {@link ContratoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Contrato> createSpecification(ContratoCriteria criteria) {
        Specification<Contrato> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Contrato_.id));
            }
            if (criteria.getCodigo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCodigo(), Contrato_.codigo));
            }
            if (criteria.getValor() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValor(), Contrato_.valor));
            }
            if (criteria.getInicioPrevisto() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getInicioPrevisto(), Contrato_.inicioPrevisto));
            }
            if (criteria.getTerminoPrevisto() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTerminoPrevisto(), Contrato_.terminoPrevisto));
            }
            if (criteria.getInicioEfectivo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getInicioEfectivo(), Contrato_.inicioEfectivo));
            }
            if (criteria.getTerminoEfectivo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTerminoEfectivo(), Contrato_.terminoEfectivo));
            }
            if (criteria.getDescricao() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescricao(), Contrato_.descricao));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), Contrato_.createdAt));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Contrato_.createdBy));
            }
            if (criteria.getLastModificationAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModificationAt(), Contrato_.lastModificationAt));
            }
            if (criteria.getLastModificationBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModificationBy(), Contrato_.lastModificationBy));
            }
            if (criteria.getPropostaFinanceiraId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPropostaFinanceiraId(),
                            root -> root.join(Contrato_.propostaFinanceira, JoinType.LEFT).get(PropostaFinanceira_.id)
                        )
                    );
            }
            if (criteria.getLinhaOrcamentoId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getLinhaOrcamentoId(),
                            root -> root.join(Contrato_.linhaOrcamento, JoinType.LEFT).get(LinhaOrcamento_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
