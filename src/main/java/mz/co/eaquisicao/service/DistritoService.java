package mz.co.eaquisicao.service;

import java.util.Optional;
import mz.co.eaquisicao.domain.Distrito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link Distrito}.
 */
public interface DistritoService {
    /**
     * Save a distrito.
     *
     * @param distrito the entity to save.
     * @return the persisted entity.
     */
    Distrito save(Distrito distrito);

    /**
     * Partially updates a distrito.
     *
     * @param distrito the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Distrito> partialUpdate(Distrito distrito);

    /**
     * Get all the distritos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Distrito> findAll(Pageable pageable);

    /**
     * Get the "id" distrito.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Distrito> findOne(Long id);

    /**
     * Delete the "id" distrito.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
