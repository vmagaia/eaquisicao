package mz.co.eaquisicao.service;

import java.util.Optional;
import mz.co.eaquisicao.domain.Fornecedor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link Fornecedor}.
 */
public interface FornecedorService {
    /**
     * Save a fornecedor.
     *
     * @param fornecedor the entity to save.
     * @return the persisted entity.
     */
    Fornecedor save(Fornecedor fornecedor);

    /**
     * Partially updates a fornecedor.
     *
     * @param fornecedor the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Fornecedor> partialUpdate(Fornecedor fornecedor);

    /**
     * Get all the fornecedors.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Fornecedor> findAll(Pageable pageable);

    /**
     * Get the "id" fornecedor.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Fornecedor> findOne(Long id);

    /**
     * Delete the "id" fornecedor.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
