package mz.co.eaquisicao.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import mz.co.eaquisicao.domain.*; // for static metamodels
import mz.co.eaquisicao.domain.Fornecedor;
import mz.co.eaquisicao.repository.FornecedorRepository;
import mz.co.eaquisicao.service.criteria.FornecedorCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Fornecedor} entities in the database.
 * The main input is a {@link FornecedorCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Fornecedor} or a {@link Page} of {@link Fornecedor} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FornecedorQueryService extends QueryService<Fornecedor> {

    private final Logger log = LoggerFactory.getLogger(FornecedorQueryService.class);

    private final FornecedorRepository fornecedorRepository;

    public FornecedorQueryService(FornecedorRepository fornecedorRepository) {
        this.fornecedorRepository = fornecedorRepository;
    }

    /**
     * Return a {@link List} of {@link Fornecedor} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Fornecedor> findByCriteria(FornecedorCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Fornecedor> specification = createSpecification(criteria);
        return fornecedorRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Fornecedor} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Fornecedor> findByCriteria(FornecedorCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Fornecedor> specification = createSpecification(criteria);
        return fornecedorRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FornecedorCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Fornecedor> specification = createSpecification(criteria);
        return fornecedorRepository.count(specification);
    }

    /**
     * Function to convert {@link FornecedorCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Fornecedor> createSpecification(FornecedorCriteria criteria) {
        Specification<Fornecedor> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Fornecedor_.id));
            }
            if (criteria.getNome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNome(), Fornecedor_.nome));
            }
            if (criteria.getEndereco() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEndereco(), Fornecedor_.endereco));
            }
            if (criteria.getNuit() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNuit(), Fornecedor_.nuit));
            }
            if (criteria.getAreaActividade() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAreaActividade(), Fornecedor_.areaActividade));
            }
            if (criteria.getSaldo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSaldo(), Fornecedor_.saldo));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), Fornecedor_.createdAt));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Fornecedor_.createdBy));
            }
            if (criteria.getLastModificationAt() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getLastModificationAt(), Fornecedor_.lastModificationAt));
            }
            if (criteria.getLastModificationBy() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getLastModificationBy(), Fornecedor_.lastModificationBy));
            }
            if (criteria.getPropostaFinanceiraId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPropostaFinanceiraId(),
                            root -> root.join(Fornecedor_.propostaFinanceiras, JoinType.LEFT).get(PropostaFinanceira_.id)
                        )
                    );
            }
            if (criteria.getPaisId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getPaisId(), root -> root.join(Fornecedor_.pais, JoinType.LEFT).get(Pais_.id))
                    );
            }
        }
        return specification;
    }
}
