package mz.co.eaquisicao.service;

import java.util.Optional;
import mz.co.eaquisicao.domain.LinhaOrcamento;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link LinhaOrcamento}.
 */
public interface LinhaOrcamentoService {
    /**
     * Save a linhaOrcamento.
     *
     * @param linhaOrcamento the entity to save.
     * @return the persisted entity.
     */
    LinhaOrcamento save(LinhaOrcamento linhaOrcamento);

    /**
     * Partially updates a linhaOrcamento.
     *
     * @param linhaOrcamento the entity to update partially.
     * @return the persisted entity.
     */
    Optional<LinhaOrcamento> partialUpdate(LinhaOrcamento linhaOrcamento);

    /**
     * Get all the linhaOrcamentos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LinhaOrcamento> findAll(Pageable pageable);

    /**
     * Get the "id" linhaOrcamento.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LinhaOrcamento> findOne(Long id);

    /**
     * Delete the "id" linhaOrcamento.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
