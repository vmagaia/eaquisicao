package mz.co.eaquisicao.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import mz.co.eaquisicao.domain.*; // for static metamodels
import mz.co.eaquisicao.domain.Provincia;
import mz.co.eaquisicao.repository.ProvinciaRepository;
import mz.co.eaquisicao.service.criteria.ProvinciaCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Provincia} entities in the database.
 * The main input is a {@link ProvinciaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Provincia} or a {@link Page} of {@link Provincia} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProvinciaQueryService extends QueryService<Provincia> {

    private final Logger log = LoggerFactory.getLogger(ProvinciaQueryService.class);

    private final ProvinciaRepository provinciaRepository;

    public ProvinciaQueryService(ProvinciaRepository provinciaRepository) {
        this.provinciaRepository = provinciaRepository;
    }

    /**
     * Return a {@link List} of {@link Provincia} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Provincia> findByCriteria(ProvinciaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Provincia> specification = createSpecification(criteria);
        return provinciaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Provincia} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Provincia> findByCriteria(ProvinciaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Provincia> specification = createSpecification(criteria);
        return provinciaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProvinciaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Provincia> specification = createSpecification(criteria);
        return provinciaRepository.count(specification);
    }

    /**
     * Function to convert {@link ProvinciaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Provincia> createSpecification(ProvinciaCriteria criteria) {
        Specification<Provincia> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Provincia_.id));
            }
            if (criteria.getNome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNome(), Provincia_.nome));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), Provincia_.createdAt));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Provincia_.createdBy));
            }
            if (criteria.getLastModificationAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModificationAt(), Provincia_.lastModificationAt));
            }
            if (criteria.getLastModificationBy() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getLastModificationBy(), Provincia_.lastModificationBy));
            }
            if (criteria.getDistritoId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getDistritoId(),
                            root -> root.join(Provincia_.distritos, JoinType.LEFT).get(Distrito_.id)
                        )
                    );
            }
            if (criteria.getPaisId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getPaisId(), root -> root.join(Provincia_.pais, JoinType.LEFT).get(Pais_.id))
                    );
            }
        }
        return specification;
    }
}
