package mz.co.eaquisicao.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import mz.co.eaquisicao.domain.*; // for static metamodels
import mz.co.eaquisicao.domain.Distrito;
import mz.co.eaquisicao.repository.DistritoRepository;
import mz.co.eaquisicao.service.criteria.DistritoCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Distrito} entities in the database.
 * The main input is a {@link DistritoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Distrito} or a {@link Page} of {@link Distrito} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DistritoQueryService extends QueryService<Distrito> {

    private final Logger log = LoggerFactory.getLogger(DistritoQueryService.class);

    private final DistritoRepository distritoRepository;

    public DistritoQueryService(DistritoRepository distritoRepository) {
        this.distritoRepository = distritoRepository;
    }

    /**
     * Return a {@link List} of {@link Distrito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Distrito> findByCriteria(DistritoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Distrito> specification = createSpecification(criteria);
        return distritoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Distrito} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Distrito> findByCriteria(DistritoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Distrito> specification = createSpecification(criteria);
        return distritoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DistritoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Distrito> specification = createSpecification(criteria);
        return distritoRepository.count(specification);
    }

    /**
     * Function to convert {@link DistritoCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Distrito> createSpecification(DistritoCriteria criteria) {
        Specification<Distrito> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Distrito_.id));
            }
            if (criteria.getNome() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNome(), Distrito_.nome));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), Distrito_.createdAt));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Distrito_.createdBy));
            }
            if (criteria.getLastModificationAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModificationAt(), Distrito_.lastModificationAt));
            }
            if (criteria.getLastModificationBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModificationBy(), Distrito_.lastModificationBy));
            }
            if (criteria.getProvinciaId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getProvinciaId(),
                            root -> root.join(Distrito_.provincia, JoinType.LEFT).get(Provincia_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
