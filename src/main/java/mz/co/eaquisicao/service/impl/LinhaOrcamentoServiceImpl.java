package mz.co.eaquisicao.service.impl;

import java.util.Optional;
import mz.co.eaquisicao.domain.LinhaOrcamento;
import mz.co.eaquisicao.repository.LinhaOrcamentoRepository;
import mz.co.eaquisicao.service.LinhaOrcamentoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link LinhaOrcamento}.
 */
@Service
@Transactional
public class LinhaOrcamentoServiceImpl implements LinhaOrcamentoService {

    private final Logger log = LoggerFactory.getLogger(LinhaOrcamentoServiceImpl.class);

    private final LinhaOrcamentoRepository linhaOrcamentoRepository;

    public LinhaOrcamentoServiceImpl(LinhaOrcamentoRepository linhaOrcamentoRepository) {
        this.linhaOrcamentoRepository = linhaOrcamentoRepository;
    }

    @Override
    public LinhaOrcamento save(LinhaOrcamento linhaOrcamento) {
        log.debug("Request to save LinhaOrcamento : {}", linhaOrcamento);
        return linhaOrcamentoRepository.save(linhaOrcamento);
    }

    @Override
    public Optional<LinhaOrcamento> partialUpdate(LinhaOrcamento linhaOrcamento) {
        log.debug("Request to partially update LinhaOrcamento : {}", linhaOrcamento);

        return linhaOrcamentoRepository
            .findById(linhaOrcamento.getId())
            .map(existingLinhaOrcamento -> {
                if (linhaOrcamento.getNome() != null) {
                    existingLinhaOrcamento.setNome(linhaOrcamento.getNome());
                }
                if (linhaOrcamento.getEntidadeFinanceira() != null) {
                    existingLinhaOrcamento.setEntidadeFinanceira(linhaOrcamento.getEntidadeFinanceira());
                }
                if (linhaOrcamento.getRubrica() != null) {
                    existingLinhaOrcamento.setRubrica(linhaOrcamento.getRubrica());
                }
                if (linhaOrcamento.getDescricaoRubrica() != null) {
                    existingLinhaOrcamento.setDescricaoRubrica(linhaOrcamento.getDescricaoRubrica());
                }
                if (linhaOrcamento.getValor() != null) {
                    existingLinhaOrcamento.setValor(linhaOrcamento.getValor());
                }
                if (linhaOrcamento.getValorExecutado() != null) {
                    existingLinhaOrcamento.setValorExecutado(linhaOrcamento.getValorExecutado());
                }
                if (linhaOrcamento.getSaldo() != null) {
                    existingLinhaOrcamento.setSaldo(linhaOrcamento.getSaldo());
                }
                if (linhaOrcamento.getCreatedAt() != null) {
                    existingLinhaOrcamento.setCreatedAt(linhaOrcamento.getCreatedAt());
                }
                if (linhaOrcamento.getCreatedBy() != null) {
                    existingLinhaOrcamento.setCreatedBy(linhaOrcamento.getCreatedBy());
                }
                if (linhaOrcamento.getLastModificationAt() != null) {
                    existingLinhaOrcamento.setLastModificationAt(linhaOrcamento.getLastModificationAt());
                }
                if (linhaOrcamento.getLastModificationBy() != null) {
                    existingLinhaOrcamento.setLastModificationBy(linhaOrcamento.getLastModificationBy());
                }

                return existingLinhaOrcamento;
            })
            .map(linhaOrcamentoRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<LinhaOrcamento> findAll(Pageable pageable) {
        log.debug("Request to get all LinhaOrcamentos");
        return linhaOrcamentoRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<LinhaOrcamento> findOne(Long id) {
        log.debug("Request to get LinhaOrcamento : {}", id);
        return linhaOrcamentoRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete LinhaOrcamento : {}", id);
        linhaOrcamentoRepository.deleteById(id);
    }
}
