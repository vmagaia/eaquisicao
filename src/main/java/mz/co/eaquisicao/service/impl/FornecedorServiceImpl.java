package mz.co.eaquisicao.service.impl;

import java.util.Optional;
import mz.co.eaquisicao.domain.Fornecedor;
import mz.co.eaquisicao.repository.FornecedorRepository;
import mz.co.eaquisicao.service.FornecedorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Fornecedor}.
 */
@Service
@Transactional
public class FornecedorServiceImpl implements FornecedorService {

    private final Logger log = LoggerFactory.getLogger(FornecedorServiceImpl.class);

    private final FornecedorRepository fornecedorRepository;

    public FornecedorServiceImpl(FornecedorRepository fornecedorRepository) {
        this.fornecedorRepository = fornecedorRepository;
    }

    @Override
    public Fornecedor save(Fornecedor fornecedor) {
        log.debug("Request to save Fornecedor : {}", fornecedor);
        return fornecedorRepository.save(fornecedor);
    }

    @Override
    public Optional<Fornecedor> partialUpdate(Fornecedor fornecedor) {
        log.debug("Request to partially update Fornecedor : {}", fornecedor);

        return fornecedorRepository
            .findById(fornecedor.getId())
            .map(existingFornecedor -> {
                if (fornecedor.getNome() != null) {
                    existingFornecedor.setNome(fornecedor.getNome());
                }
                if (fornecedor.getEndereco() != null) {
                    existingFornecedor.setEndereco(fornecedor.getEndereco());
                }
                if (fornecedor.getNuit() != null) {
                    existingFornecedor.setNuit(fornecedor.getNuit());
                }
                if (fornecedor.getAreaActividade() != null) {
                    existingFornecedor.setAreaActividade(fornecedor.getAreaActividade());
                }
                if (fornecedor.getSaldo() != null) {
                    existingFornecedor.setSaldo(fornecedor.getSaldo());
                }
                if (fornecedor.getCreatedAt() != null) {
                    existingFornecedor.setCreatedAt(fornecedor.getCreatedAt());
                }
                if (fornecedor.getCreatedBy() != null) {
                    existingFornecedor.setCreatedBy(fornecedor.getCreatedBy());
                }
                if (fornecedor.getLastModificationAt() != null) {
                    existingFornecedor.setLastModificationAt(fornecedor.getLastModificationAt());
                }
                if (fornecedor.getLastModificationBy() != null) {
                    existingFornecedor.setLastModificationBy(fornecedor.getLastModificationBy());
                }

                return existingFornecedor;
            })
            .map(fornecedorRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Fornecedor> findAll(Pageable pageable) {
        log.debug("Request to get all Fornecedors");
        return fornecedorRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Fornecedor> findOne(Long id) {
        log.debug("Request to get Fornecedor : {}", id);
        return fornecedorRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Fornecedor : {}", id);
        fornecedorRepository.deleteById(id);
    }
}
