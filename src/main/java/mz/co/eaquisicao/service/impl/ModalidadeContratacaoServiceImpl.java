package mz.co.eaquisicao.service.impl;

import java.util.Optional;
import mz.co.eaquisicao.domain.ModalidadeContratacao;
import mz.co.eaquisicao.repository.ModalidadeContratacaoRepository;
import mz.co.eaquisicao.service.ModalidadeContratacaoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ModalidadeContratacao}.
 */
@Service
@Transactional
public class ModalidadeContratacaoServiceImpl implements ModalidadeContratacaoService {

    private final Logger log = LoggerFactory.getLogger(ModalidadeContratacaoServiceImpl.class);

    private final ModalidadeContratacaoRepository modalidadeContratacaoRepository;

    public ModalidadeContratacaoServiceImpl(ModalidadeContratacaoRepository modalidadeContratacaoRepository) {
        this.modalidadeContratacaoRepository = modalidadeContratacaoRepository;
    }

    @Override
    public ModalidadeContratacao save(ModalidadeContratacao modalidadeContratacao) {
        log.debug("Request to save ModalidadeContratacao : {}", modalidadeContratacao);
        return modalidadeContratacaoRepository.save(modalidadeContratacao);
    }

    @Override
    public Optional<ModalidadeContratacao> partialUpdate(ModalidadeContratacao modalidadeContratacao) {
        log.debug("Request to partially update ModalidadeContratacao : {}", modalidadeContratacao);

        return modalidadeContratacaoRepository
            .findById(modalidadeContratacao.getId())
            .map(existingModalidadeContratacao -> {
                if (modalidadeContratacao.getNome() != null) {
                    existingModalidadeContratacao.setNome(modalidadeContratacao.getNome());
                }
                if (modalidadeContratacao.getDescricao() != null) {
                    existingModalidadeContratacao.setDescricao(modalidadeContratacao.getDescricao());
                }
                if (modalidadeContratacao.getCreatedAt() != null) {
                    existingModalidadeContratacao.setCreatedAt(modalidadeContratacao.getCreatedAt());
                }
                if (modalidadeContratacao.getCreatedBy() != null) {
                    existingModalidadeContratacao.setCreatedBy(modalidadeContratacao.getCreatedBy());
                }
                if (modalidadeContratacao.getLastModificationAt() != null) {
                    existingModalidadeContratacao.setLastModificationAt(modalidadeContratacao.getLastModificationAt());
                }
                if (modalidadeContratacao.getLastModificationBy() != null) {
                    existingModalidadeContratacao.setLastModificationBy(modalidadeContratacao.getLastModificationBy());
                }

                return existingModalidadeContratacao;
            })
            .map(modalidadeContratacaoRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ModalidadeContratacao> findAll(Pageable pageable) {
        log.debug("Request to get all ModalidadeContratacaos");
        return modalidadeContratacaoRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ModalidadeContratacao> findOne(Long id) {
        log.debug("Request to get ModalidadeContratacao : {}", id);
        return modalidadeContratacaoRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ModalidadeContratacao : {}", id);
        modalidadeContratacaoRepository.deleteById(id);
    }
}
