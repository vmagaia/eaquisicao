package mz.co.eaquisicao.service.impl;

import java.util.Optional;
import mz.co.eaquisicao.domain.FaseAquisicao;
import mz.co.eaquisicao.repository.FaseAquisicaoRepository;
import mz.co.eaquisicao.service.FaseAquisicaoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link FaseAquisicao}.
 */
@Service
@Transactional
public class FaseAquisicaoServiceImpl implements FaseAquisicaoService {

    private final Logger log = LoggerFactory.getLogger(FaseAquisicaoServiceImpl.class);

    private final FaseAquisicaoRepository faseAquisicaoRepository;

    public FaseAquisicaoServiceImpl(FaseAquisicaoRepository faseAquisicaoRepository) {
        this.faseAquisicaoRepository = faseAquisicaoRepository;
    }

    @Override
    public FaseAquisicao save(FaseAquisicao faseAquisicao) {
        log.debug("Request to save FaseAquisicao : {}", faseAquisicao);
        return faseAquisicaoRepository.save(faseAquisicao);
    }

    @Override
    public Optional<FaseAquisicao> partialUpdate(FaseAquisicao faseAquisicao) {
        log.debug("Request to partially update FaseAquisicao : {}", faseAquisicao);

        return faseAquisicaoRepository
            .findById(faseAquisicao.getId())
            .map(existingFaseAquisicao -> {
                if (faseAquisicao.getNome() != null) {
                    existingFaseAquisicao.setNome(faseAquisicao.getNome());
                }
                if (faseAquisicao.getDiasExecucao() != null) {
                    existingFaseAquisicao.setDiasExecucao(faseAquisicao.getDiasExecucao());
                }
                if (faseAquisicao.getSequencia() != null) {
                    existingFaseAquisicao.setSequencia(faseAquisicao.getSequencia());
                }
                if (faseAquisicao.getCreatedAt() != null) {
                    existingFaseAquisicao.setCreatedAt(faseAquisicao.getCreatedAt());
                }
                if (faseAquisicao.getCreatedBy() != null) {
                    existingFaseAquisicao.setCreatedBy(faseAquisicao.getCreatedBy());
                }
                if (faseAquisicao.getLastModificationAt() != null) {
                    existingFaseAquisicao.setLastModificationAt(faseAquisicao.getLastModificationAt());
                }
                if (faseAquisicao.getLastModificationBy() != null) {
                    existingFaseAquisicao.setLastModificationBy(faseAquisicao.getLastModificationBy());
                }

                return existingFaseAquisicao;
            })
            .map(faseAquisicaoRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<FaseAquisicao> findAll(Pageable pageable) {
        log.debug("Request to get all FaseAquisicaos");
        return faseAquisicaoRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<FaseAquisicao> findOne(Long id) {
        log.debug("Request to get FaseAquisicao : {}", id);
        return faseAquisicaoRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete FaseAquisicao : {}", id);
        faseAquisicaoRepository.deleteById(id);
    }
}
