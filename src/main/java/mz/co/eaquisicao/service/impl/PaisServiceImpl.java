package mz.co.eaquisicao.service.impl;

import java.util.Optional;
import mz.co.eaquisicao.domain.Pais;
import mz.co.eaquisicao.repository.PaisRepository;
import mz.co.eaquisicao.service.PaisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Pais}.
 */
@Service
@Transactional
public class PaisServiceImpl implements PaisService {

    private final Logger log = LoggerFactory.getLogger(PaisServiceImpl.class);

    private final PaisRepository paisRepository;

    public PaisServiceImpl(PaisRepository paisRepository) {
        this.paisRepository = paisRepository;
    }

    @Override
    public Pais save(Pais pais) {
        log.debug("Request to save Pais : {}", pais);
        return paisRepository.save(pais);
    }

    @Override
    public Optional<Pais> partialUpdate(Pais pais) {
        log.debug("Request to partially update Pais : {}", pais);

        return paisRepository
            .findById(pais.getId())
            .map(existingPais -> {
                if (pais.getNome() != null) {
                    existingPais.setNome(pais.getNome());
                }
                if (pais.getCreatedAt() != null) {
                    existingPais.setCreatedAt(pais.getCreatedAt());
                }
                if (pais.getCreatedBy() != null) {
                    existingPais.setCreatedBy(pais.getCreatedBy());
                }
                if (pais.getLastModificationAt() != null) {
                    existingPais.setLastModificationAt(pais.getLastModificationAt());
                }
                if (pais.getLastModificationBy() != null) {
                    existingPais.setLastModificationBy(pais.getLastModificationBy());
                }

                return existingPais;
            })
            .map(paisRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Pais> findAll(Pageable pageable) {
        log.debug("Request to get all Pais");
        return paisRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Pais> findOne(Long id) {
        log.debug("Request to get Pais : {}", id);
        return paisRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Pais : {}", id);
        paisRepository.deleteById(id);
    }
}
