package mz.co.eaquisicao.service.impl;

import java.util.Optional;
import mz.co.eaquisicao.domain.PropostaFinanceira;
import mz.co.eaquisicao.repository.PropostaFinanceiraRepository;
import mz.co.eaquisicao.service.PropostaFinanceiraService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PropostaFinanceira}.
 */
@Service
@Transactional
public class PropostaFinanceiraServiceImpl implements PropostaFinanceiraService {

    private final Logger log = LoggerFactory.getLogger(PropostaFinanceiraServiceImpl.class);

    private final PropostaFinanceiraRepository propostaFinanceiraRepository;

    public PropostaFinanceiraServiceImpl(PropostaFinanceiraRepository propostaFinanceiraRepository) {
        this.propostaFinanceiraRepository = propostaFinanceiraRepository;
    }

    @Override
    public PropostaFinanceira save(PropostaFinanceira propostaFinanceira) {
        log.debug("Request to save PropostaFinanceira : {}", propostaFinanceira);
        return propostaFinanceiraRepository.save(propostaFinanceira);
    }

    @Override
    public Optional<PropostaFinanceira> partialUpdate(PropostaFinanceira propostaFinanceira) {
        log.debug("Request to partially update PropostaFinanceira : {}", propostaFinanceira);

        return propostaFinanceiraRepository
            .findById(propostaFinanceira.getId())
            .map(existingPropostaFinanceira -> {
                if (propostaFinanceira.getValor() != null) {
                    existingPropostaFinanceira.setValor(propostaFinanceira.getValor());
                }
                if (propostaFinanceira.getDescricao() != null) {
                    existingPropostaFinanceira.setDescricao(propostaFinanceira.getDescricao());
                }
                if (propostaFinanceira.getAnexo() != null) {
                    existingPropostaFinanceira.setAnexo(propostaFinanceira.getAnexo());
                }
                if (propostaFinanceira.getAnexoContentType() != null) {
                    existingPropostaFinanceira.setAnexoContentType(propostaFinanceira.getAnexoContentType());
                }
                if (propostaFinanceira.getEstadoProposta() != null) {
                    existingPropostaFinanceira.setEstadoProposta(propostaFinanceira.getEstadoProposta());
                }
                if (propostaFinanceira.getCreatedAt() != null) {
                    existingPropostaFinanceira.setCreatedAt(propostaFinanceira.getCreatedAt());
                }
                if (propostaFinanceira.getCreatedBy() != null) {
                    existingPropostaFinanceira.setCreatedBy(propostaFinanceira.getCreatedBy());
                }
                if (propostaFinanceira.getLastModificationAt() != null) {
                    existingPropostaFinanceira.setLastModificationAt(propostaFinanceira.getLastModificationAt());
                }
                if (propostaFinanceira.getLastModificationBy() != null) {
                    existingPropostaFinanceira.setLastModificationBy(propostaFinanceira.getLastModificationBy());
                }

                return existingPropostaFinanceira;
            })
            .map(propostaFinanceiraRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PropostaFinanceira> findAll(Pageable pageable) {
        log.debug("Request to get all PropostaFinanceiras");
        return propostaFinanceiraRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PropostaFinanceira> findOne(Long id) {
        log.debug("Request to get PropostaFinanceira : {}", id);
        return propostaFinanceiraRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PropostaFinanceira : {}", id);
        propostaFinanceiraRepository.deleteById(id);
    }
}
