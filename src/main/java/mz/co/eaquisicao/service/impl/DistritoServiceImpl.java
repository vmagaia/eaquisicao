package mz.co.eaquisicao.service.impl;

import java.util.Optional;
import mz.co.eaquisicao.domain.Distrito;
import mz.co.eaquisicao.repository.DistritoRepository;
import mz.co.eaquisicao.service.DistritoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Distrito}.
 */
@Service
@Transactional
public class DistritoServiceImpl implements DistritoService {

    private final Logger log = LoggerFactory.getLogger(DistritoServiceImpl.class);

    private final DistritoRepository distritoRepository;

    public DistritoServiceImpl(DistritoRepository distritoRepository) {
        this.distritoRepository = distritoRepository;
    }

    @Override
    public Distrito save(Distrito distrito) {
        log.debug("Request to save Distrito : {}", distrito);
        return distritoRepository.save(distrito);
    }

    @Override
    public Optional<Distrito> partialUpdate(Distrito distrito) {
        log.debug("Request to partially update Distrito : {}", distrito);

        return distritoRepository
            .findById(distrito.getId())
            .map(existingDistrito -> {
                if (distrito.getNome() != null) {
                    existingDistrito.setNome(distrito.getNome());
                }
                if (distrito.getCreatedAt() != null) {
                    existingDistrito.setCreatedAt(distrito.getCreatedAt());
                }
                if (distrito.getCreatedBy() != null) {
                    existingDistrito.setCreatedBy(distrito.getCreatedBy());
                }
                if (distrito.getLastModificationAt() != null) {
                    existingDistrito.setLastModificationAt(distrito.getLastModificationAt());
                }
                if (distrito.getLastModificationBy() != null) {
                    existingDistrito.setLastModificationBy(distrito.getLastModificationBy());
                }

                return existingDistrito;
            })
            .map(distritoRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Distrito> findAll(Pageable pageable) {
        log.debug("Request to get all Distritos");
        return distritoRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Distrito> findOne(Long id) {
        log.debug("Request to get Distrito : {}", id);
        return distritoRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Distrito : {}", id);
        distritoRepository.deleteById(id);
    }
}
