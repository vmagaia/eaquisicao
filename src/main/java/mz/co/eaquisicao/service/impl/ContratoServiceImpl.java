package mz.co.eaquisicao.service.impl;

import java.util.Optional;
import mz.co.eaquisicao.domain.Contrato;
import mz.co.eaquisicao.repository.ContratoRepository;
import mz.co.eaquisicao.service.ContratoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Contrato}.
 */
@Service
@Transactional
public class ContratoServiceImpl implements ContratoService {

    private final Logger log = LoggerFactory.getLogger(ContratoServiceImpl.class);

    private final ContratoRepository contratoRepository;

    public ContratoServiceImpl(ContratoRepository contratoRepository) {
        this.contratoRepository = contratoRepository;
    }

    @Override
    public Contrato save(Contrato contrato) {
        log.debug("Request to save Contrato : {}", contrato);
        return contratoRepository.save(contrato);
    }

    @Override
    public Optional<Contrato> partialUpdate(Contrato contrato) {
        log.debug("Request to partially update Contrato : {}", contrato);

        return contratoRepository
            .findById(contrato.getId())
            .map(existingContrato -> {
                if (contrato.getCodigo() != null) {
                    existingContrato.setCodigo(contrato.getCodigo());
                }
                if (contrato.getValor() != null) {
                    existingContrato.setValor(contrato.getValor());
                }
                if (contrato.getInicioPrevisto() != null) {
                    existingContrato.setInicioPrevisto(contrato.getInicioPrevisto());
                }
                if (contrato.getTerminoPrevisto() != null) {
                    existingContrato.setTerminoPrevisto(contrato.getTerminoPrevisto());
                }
                if (contrato.getInicioEfectivo() != null) {
                    existingContrato.setInicioEfectivo(contrato.getInicioEfectivo());
                }
                if (contrato.getTerminoEfectivo() != null) {
                    existingContrato.setTerminoEfectivo(contrato.getTerminoEfectivo());
                }
                if (contrato.getDescricao() != null) {
                    existingContrato.setDescricao(contrato.getDescricao());
                }
                if (contrato.getCreatedAt() != null) {
                    existingContrato.setCreatedAt(contrato.getCreatedAt());
                }
                if (contrato.getCreatedBy() != null) {
                    existingContrato.setCreatedBy(contrato.getCreatedBy());
                }
                if (contrato.getLastModificationAt() != null) {
                    existingContrato.setLastModificationAt(contrato.getLastModificationAt());
                }
                if (contrato.getLastModificationBy() != null) {
                    existingContrato.setLastModificationBy(contrato.getLastModificationBy());
                }

                return existingContrato;
            })
            .map(contratoRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Contrato> findAll(Pageable pageable) {
        log.debug("Request to get all Contratoes");
        return contratoRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Contrato> findOne(Long id) {
        log.debug("Request to get Contrato : {}", id);
        return contratoRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Contrato : {}", id);
        contratoRepository.deleteById(id);
    }
}
