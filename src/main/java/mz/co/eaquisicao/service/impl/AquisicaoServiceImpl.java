package mz.co.eaquisicao.service.impl;

import java.util.Optional;
import mz.co.eaquisicao.domain.Aquisicao;
import mz.co.eaquisicao.repository.AquisicaoRepository;
import mz.co.eaquisicao.service.AquisicaoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Aquisicao}.
 */
@Service
@Transactional
public class AquisicaoServiceImpl implements AquisicaoService {

    private final Logger log = LoggerFactory.getLogger(AquisicaoServiceImpl.class);

    private final AquisicaoRepository aquisicaoRepository;

    public AquisicaoServiceImpl(AquisicaoRepository aquisicaoRepository) {
        this.aquisicaoRepository = aquisicaoRepository;
    }

    @Override
    public Aquisicao save(Aquisicao aquisicao) {
        log.debug("Request to save Aquisicao : {}", aquisicao);
        return aquisicaoRepository.save(aquisicao);
    }

    @Override
    public Optional<Aquisicao> partialUpdate(Aquisicao aquisicao) {
        log.debug("Request to partially update Aquisicao : {}", aquisicao);

        return aquisicaoRepository
            .findById(aquisicao.getId())
            .map(existingAquisicao -> {
                if (aquisicao.getDescricao() != null) {
                    existingAquisicao.setDescricao(aquisicao.getDescricao());
                }
                if (aquisicao.getValorPrevisto() != null) {
                    existingAquisicao.setValorPrevisto(aquisicao.getValorPrevisto());
                }
                if (aquisicao.getCreatedAt() != null) {
                    existingAquisicao.setCreatedAt(aquisicao.getCreatedAt());
                }
                if (aquisicao.getCreatedBy() != null) {
                    existingAquisicao.setCreatedBy(aquisicao.getCreatedBy());
                }
                if (aquisicao.getLastModificationAt() != null) {
                    existingAquisicao.setLastModificationAt(aquisicao.getLastModificationAt());
                }
                if (aquisicao.getLastModificationBy() != null) {
                    existingAquisicao.setLastModificationBy(aquisicao.getLastModificationBy());
                }

                return existingAquisicao;
            })
            .map(aquisicaoRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Aquisicao> findAll(Pageable pageable) {
        log.debug("Request to get all Aquisicaos");
        return aquisicaoRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Aquisicao> findOne(Long id) {
        log.debug("Request to get Aquisicao : {}", id);
        return aquisicaoRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Aquisicao : {}", id);
        aquisicaoRepository.deleteById(id);
    }
}
