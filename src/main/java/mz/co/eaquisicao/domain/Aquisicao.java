package mz.co.eaquisicao.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Aquisicao.
 */
@Entity
@Table(name = "aquisicao")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Aquisicao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "descricao")
    private String descricao;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "valor_previsto", precision = 21, scale = 2, nullable = false)
    private BigDecimal valorPrevisto;

    @Column(name = "created_at", nullable = true)
    private Instant createdAt;

    @Column(name = "created_by", nullable = true)
    private String createdBy;

    @Column(name = "last_modification_at", nullable = true)
    private Instant lastModificationAt;

    @Column(name = "last_modification_by", nullable = true)
    private String lastModificationBy;

    @JsonIgnoreProperties(value = { "propostaFinanceira", "linhaOrcamento" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Contrato contrato;

    @OneToMany(mappedBy = "aquisicao")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "fornecedor", "aquisicao" }, allowSetters = true)
    private Set<PropostaFinanceira> propostaFinanceiras = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "aquisicaos" }, allowSetters = true)
    private Categoria categoria;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "aquisicaos" }, allowSetters = true)
    private FaseAquisicao faseAquisicao;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "aquisicaos", "contratoes" }, allowSetters = true)
    private LinhaOrcamento linhaOrcamento;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "aquisicaos" }, allowSetters = true)
    private ModalidadeContratacao modalidadeContratacao;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Aquisicao id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Aquisicao descricao(String descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValorPrevisto() {
        return this.valorPrevisto;
    }

    public Aquisicao valorPrevisto(BigDecimal valorPrevisto) {
        this.setValorPrevisto(valorPrevisto);
        return this;
    }

    public void setValorPrevisto(BigDecimal valorPrevisto) {
        this.valorPrevisto = valorPrevisto;
    }

    public Instant getCreatedAt() {
        return this.createdAt;
    }

    public Aquisicao createdAt(Instant createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Aquisicao createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModificationAt() {
        return this.lastModificationAt;
    }

    public Aquisicao lastModificationAt(Instant lastModificationAt) {
        this.setLastModificationAt(lastModificationAt);
        return this;
    }

    public void setLastModificationAt(Instant lastModificationAt) {
        this.lastModificationAt = lastModificationAt;
    }

    public String getLastModificationBy() {
        return this.lastModificationBy;
    }

    public Aquisicao lastModificationBy(String lastModificationBy) {
        this.setLastModificationBy(lastModificationBy);
        return this;
    }

    public void setLastModificationBy(String lastModificationBy) {
        this.lastModificationBy = lastModificationBy;
    }

    public Contrato getContrato() {
        return this.contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Aquisicao contrato(Contrato contrato) {
        this.setContrato(contrato);
        return this;
    }

    public Set<PropostaFinanceira> getPropostaFinanceiras() {
        return this.propostaFinanceiras;
    }

    public void setPropostaFinanceiras(Set<PropostaFinanceira> propostaFinanceiras) {
        if (this.propostaFinanceiras != null) {
            this.propostaFinanceiras.forEach(i -> i.setAquisicao(null));
        }
        if (propostaFinanceiras != null) {
            propostaFinanceiras.forEach(i -> i.setAquisicao(this));
        }
        this.propostaFinanceiras = propostaFinanceiras;
    }

    public Aquisicao propostaFinanceiras(Set<PropostaFinanceira> propostaFinanceiras) {
        this.setPropostaFinanceiras(propostaFinanceiras);
        return this;
    }

    public Aquisicao addPropostaFinanceira(PropostaFinanceira propostaFinanceira) {
        this.propostaFinanceiras.add(propostaFinanceira);
        propostaFinanceira.setAquisicao(this);
        return this;
    }

    public Aquisicao removePropostaFinanceira(PropostaFinanceira propostaFinanceira) {
        this.propostaFinanceiras.remove(propostaFinanceira);
        propostaFinanceira.setAquisicao(null);
        return this;
    }

    public Categoria getCategoria() {
        return this.categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Aquisicao categoria(Categoria categoria) {
        this.setCategoria(categoria);
        return this;
    }

    public FaseAquisicao getFaseAquisicao() {
        return this.faseAquisicao;
    }

    public void setFaseAquisicao(FaseAquisicao faseAquisicao) {
        this.faseAquisicao = faseAquisicao;
    }

    public Aquisicao faseAquisicao(FaseAquisicao faseAquisicao) {
        this.setFaseAquisicao(faseAquisicao);
        return this;
    }

    public LinhaOrcamento getLinhaOrcamento() {
        return this.linhaOrcamento;
    }

    public void setLinhaOrcamento(LinhaOrcamento linhaOrcamento) {
        this.linhaOrcamento = linhaOrcamento;
    }

    public Aquisicao linhaOrcamento(LinhaOrcamento linhaOrcamento) {
        this.setLinhaOrcamento(linhaOrcamento);
        return this;
    }

    public ModalidadeContratacao getModalidadeContratacao() {
        return this.modalidadeContratacao;
    }

    public void setModalidadeContratacao(ModalidadeContratacao modalidadeContratacao) {
        this.modalidadeContratacao = modalidadeContratacao;
    }

    public Aquisicao modalidadeContratacao(ModalidadeContratacao modalidadeContratacao) {
        this.setModalidadeContratacao(modalidadeContratacao);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Aquisicao)) {
            return false;
        }
        return id != null && id.equals(((Aquisicao) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Aquisicao{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            ", valorPrevisto=" + getValorPrevisto() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModificationAt='" + getLastModificationAt() + "'" +
            ", lastModificationBy='" + getLastModificationBy() + "'" +
            "}";
    }
}
