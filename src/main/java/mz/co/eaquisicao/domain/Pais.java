package mz.co.eaquisicao.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Pais.
 */
@Entity
@Table(name = "pais")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Pais implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "created_at", nullable = true)
    private Instant createdAt;

    @Column(name = "created_by", nullable = true)
    private String createdBy;

    @Column(name = "last_modification_at", nullable = true)
    private Instant lastModificationAt;

    @Column(name = "last_modification_by", nullable = true)
    private String lastModificationBy;

    @OneToMany(mappedBy = "pais")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "distritos", "pais" }, allowSetters = true)
    private Set<Provincia> provincias = new HashSet<>();

    @OneToMany(mappedBy = "pais")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "propostaFinanceiras", "pais" }, allowSetters = true)
    private Set<Fornecedor> fornecedors = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Pais id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public Pais nome(String nome) {
        this.setNome(nome);
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Instant getCreatedAt() {
        return this.createdAt;
    }

    public Pais createdAt(Instant createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Pais createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModificationAt() {
        return this.lastModificationAt;
    }

    public Pais lastModificationAt(Instant lastModificationAt) {
        this.setLastModificationAt(lastModificationAt);
        return this;
    }

    public void setLastModificationAt(Instant lastModificationAt) {
        this.lastModificationAt = lastModificationAt;
    }

    public String getLastModificationBy() {
        return this.lastModificationBy;
    }

    public Pais lastModificationBy(String lastModificationBy) {
        this.setLastModificationBy(lastModificationBy);
        return this;
    }

    public void setLastModificationBy(String lastModificationBy) {
        this.lastModificationBy = lastModificationBy;
    }

    public Set<Provincia> getProvincias() {
        return this.provincias;
    }

    public void setProvincias(Set<Provincia> provincias) {
        if (this.provincias != null) {
            this.provincias.forEach(i -> i.setPais(null));
        }
        if (provincias != null) {
            provincias.forEach(i -> i.setPais(this));
        }
        this.provincias = provincias;
    }

    public Pais provincias(Set<Provincia> provincias) {
        this.setProvincias(provincias);
        return this;
    }

    public Pais addProvincia(Provincia provincia) {
        this.provincias.add(provincia);
        provincia.setPais(this);
        return this;
    }

    public Pais removeProvincia(Provincia provincia) {
        this.provincias.remove(provincia);
        provincia.setPais(null);
        return this;
    }

    public Set<Fornecedor> getFornecedors() {
        return this.fornecedors;
    }

    public void setFornecedors(Set<Fornecedor> fornecedors) {
        if (this.fornecedors != null) {
            this.fornecedors.forEach(i -> i.setPais(null));
        }
        if (fornecedors != null) {
            fornecedors.forEach(i -> i.setPais(this));
        }
        this.fornecedors = fornecedors;
    }

    public Pais fornecedors(Set<Fornecedor> fornecedors) {
        this.setFornecedors(fornecedors);
        return this;
    }

    public Pais addFornecedor(Fornecedor fornecedor) {
        this.fornecedors.add(fornecedor);
        fornecedor.setPais(this);
        return this;
    }

    public Pais removeFornecedor(Fornecedor fornecedor) {
        this.fornecedors.remove(fornecedor);
        fornecedor.setPais(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pais)) {
            return false;
        }
        return id != null && id.equals(((Pais) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Pais{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModificationAt='" + getLastModificationAt() + "'" +
            ", lastModificationBy='" + getLastModificationBy() + "'" +
            "}";
    }
}
