package mz.co.eaquisicao.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import mz.co.eaquisicao.domain.enumeration.EstadoProposta;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A PropostaFinanceira.
 */
@Entity
@Table(name = "proposta_financeira")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PropostaFinanceira implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "valor", precision = 21, scale = 2, nullable = false)
    private BigDecimal valor;

    @NotNull
    @Column(name = "descricao", nullable = false)
    private String descricao;

    @Lob
    @Column(name = "anexo", nullable = false)
    private byte[] anexo;

    @NotNull
    @Column(name = "anexo_content_type", nullable = false)
    private String anexoContentType;

    @Enumerated(EnumType.STRING)
    @Column(name = "estado_proposta")
    private EstadoProposta estadoProposta;

    @Column(name = "created_at", nullable = true)
    private Instant createdAt;

    @Column(name = "created_by", nullable = true)
    private String createdBy;

    @Column(name = "last_modification_at", nullable = true)
    private Instant lastModificationAt;

    @Column(name = "last_modification_by", nullable = true)
    private String lastModificationBy;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "propostaFinanceiras", "pais" }, allowSetters = true)
    private Fornecedor fornecedor;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(
        value = { "contrato", "propostaFinanceiras", "categoria", "faseAquisicao", "linhaOrcamento", "modalidadeContratacao" },
        allowSetters = true
    )
    private Aquisicao aquisicao;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PropostaFinanceira id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValor() {
        return this.valor;
    }

    public PropostaFinanceira valor(BigDecimal valor) {
        this.setValor(valor);
        return this;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public PropostaFinanceira descricao(String descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public byte[] getAnexo() {
        return this.anexo;
    }

    public PropostaFinanceira anexo(byte[] anexo) {
        this.setAnexo(anexo);
        return this;
    }

    public void setAnexo(byte[] anexo) {
        this.anexo = anexo;
    }

    public String getAnexoContentType() {
        return this.anexoContentType;
    }

    public PropostaFinanceira anexoContentType(String anexoContentType) {
        this.anexoContentType = anexoContentType;
        return this;
    }

    public void setAnexoContentType(String anexoContentType) {
        this.anexoContentType = anexoContentType;
    }

    public EstadoProposta getEstadoProposta() {
        return this.estadoProposta;
    }

    public PropostaFinanceira estadoProposta(EstadoProposta estadoProposta) {
        this.setEstadoProposta(estadoProposta);
        return this;
    }

    public void setEstadoProposta(EstadoProposta estadoProposta) {
        this.estadoProposta = estadoProposta;
    }

    public Instant getCreatedAt() {
        return this.createdAt;
    }

    public PropostaFinanceira createdAt(Instant createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public PropostaFinanceira createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModificationAt() {
        return this.lastModificationAt;
    }

    public PropostaFinanceira lastModificationAt(Instant lastModificationAt) {
        this.setLastModificationAt(lastModificationAt);
        return this;
    }

    public void setLastModificationAt(Instant lastModificationAt) {
        this.lastModificationAt = lastModificationAt;
    }

    public String getLastModificationBy() {
        return this.lastModificationBy;
    }

    public PropostaFinanceira lastModificationBy(String lastModificationBy) {
        this.setLastModificationBy(lastModificationBy);
        return this;
    }

    public void setLastModificationBy(String lastModificationBy) {
        this.lastModificationBy = lastModificationBy;
    }

    public Fornecedor getFornecedor() {
        return this.fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public PropostaFinanceira fornecedor(Fornecedor fornecedor) {
        this.setFornecedor(fornecedor);
        return this;
    }

    public Aquisicao getAquisicao() {
        return this.aquisicao;
    }

    public void setAquisicao(Aquisicao aquisicao) {
        this.aquisicao = aquisicao;
    }

    public PropostaFinanceira aquisicao(Aquisicao aquisicao) {
        this.setAquisicao(aquisicao);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PropostaFinanceira)) {
            return false;
        }
        return id != null && id.equals(((PropostaFinanceira) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PropostaFinanceira{" +
            "id=" + getId() +
            ", valor=" + getValor() +
            ", descricao='" + getDescricao() + "'" +
            ", anexo='" + getAnexo() + "'" +
            ", anexoContentType='" + getAnexoContentType() + "'" +
            ", estadoProposta='" + getEstadoProposta() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModificationAt='" + getLastModificationAt() + "'" +
            ", lastModificationBy='" + getLastModificationBy() + "'" +
            "}";
    }
}
