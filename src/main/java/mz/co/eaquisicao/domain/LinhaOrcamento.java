package mz.co.eaquisicao.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A LinhaOrcamento.
 */
@Entity
@Table(name = "linha_orcamento")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class LinhaOrcamento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @Column(name = "entidade_financeira")
    private String entidadeFinanceira;

    @NotNull
    @Column(name = "rubrica", nullable = false)
    private String rubrica;

    @Column(name = "descricao_rubrica")
    private String descricaoRubrica;

    @NotNull
    @Column(name = "valor", precision = 21, scale = 2, nullable = false)
    private BigDecimal valor;

    @Column(name = "valor_executado", precision = 21, scale = 2)
    private BigDecimal valorExecutado;

    @Column(name = "saldo", precision = 21, scale = 2)
    private BigDecimal saldo;

    @Column(name = "created_at", nullable = true)
    private Instant createdAt;

    @Column(name = "created_by", nullable = true)
    private String createdBy;

    @Column(name = "last_modification_at", nullable = true)
    private Instant lastModificationAt;

    @Column(name = "last_modification_by", nullable = true)
    private String lastModificationBy;

    @OneToMany(mappedBy = "linhaOrcamento")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "contrato", "propostaFinanceiras", "categoria", "faseAquisicao", "linhaOrcamento", "modalidadeContratacao" },
        allowSetters = true
    )
    private Set<Aquisicao> aquisicaos = new HashSet<>();

    @OneToMany(mappedBy = "linhaOrcamento")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "propostaFinanceira", "linhaOrcamento" }, allowSetters = true)
    private Set<Contrato> contratoes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public LinhaOrcamento id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public LinhaOrcamento nome(String nome) {
        this.setNome(nome);
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEntidadeFinanceira() {
        return this.entidadeFinanceira;
    }

    public LinhaOrcamento entidadeFinanceira(String entidadeFinanceira) {
        this.setEntidadeFinanceira(entidadeFinanceira);
        return this;
    }

    public void setEntidadeFinanceira(String entidadeFinanceira) {
        this.entidadeFinanceira = entidadeFinanceira;
    }

    public String getRubrica() {
        return this.rubrica;
    }

    public LinhaOrcamento rubrica(String rubrica) {
        this.setRubrica(rubrica);
        return this;
    }

    public void setRubrica(String rubrica) {
        this.rubrica = rubrica;
    }

    public String getDescricaoRubrica() {
        return this.descricaoRubrica;
    }

    public LinhaOrcamento descricaoRubrica(String descricaoRubrica) {
        this.setDescricaoRubrica(descricaoRubrica);
        return this;
    }

    public void setDescricaoRubrica(String descricaoRubrica) {
        this.descricaoRubrica = descricaoRubrica;
    }

    public BigDecimal getValor() {
        return this.valor;
    }

    public LinhaOrcamento valor(BigDecimal valor) {
        this.setValor(valor);
        return this;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getValorExecutado() {
        return this.valorExecutado;
    }

    public LinhaOrcamento valorExecutado(BigDecimal valorExecutado) {
        this.setValorExecutado(valorExecutado);
        return this;
    }

    public void setValorExecutado(BigDecimal valorExecutado) {
        this.valorExecutado = valorExecutado;
    }

    public BigDecimal getSaldo() {
        return this.saldo;
    }

    public LinhaOrcamento saldo(BigDecimal saldo) {
        this.setSaldo(saldo);
        return this;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public Instant getCreatedAt() {
        return this.createdAt;
    }

    public LinhaOrcamento createdAt(Instant createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public LinhaOrcamento createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModificationAt() {
        return this.lastModificationAt;
    }

    public LinhaOrcamento lastModificationAt(Instant lastModificationAt) {
        this.setLastModificationAt(lastModificationAt);
        return this;
    }

    public void setLastModificationAt(Instant lastModificationAt) {
        this.lastModificationAt = lastModificationAt;
    }

    public String getLastModificationBy() {
        return this.lastModificationBy;
    }

    public LinhaOrcamento lastModificationBy(String lastModificationBy) {
        this.setLastModificationBy(lastModificationBy);
        return this;
    }

    public void setLastModificationBy(String lastModificationBy) {
        this.lastModificationBy = lastModificationBy;
    }

    public Set<Aquisicao> getAquisicaos() {
        return this.aquisicaos;
    }

    public void setAquisicaos(Set<Aquisicao> aquisicaos) {
        if (this.aquisicaos != null) {
            this.aquisicaos.forEach(i -> i.setLinhaOrcamento(null));
        }
        if (aquisicaos != null) {
            aquisicaos.forEach(i -> i.setLinhaOrcamento(this));
        }
        this.aquisicaos = aquisicaos;
    }

    public LinhaOrcamento aquisicaos(Set<Aquisicao> aquisicaos) {
        this.setAquisicaos(aquisicaos);
        return this;
    }

    public LinhaOrcamento addAquisicao(Aquisicao aquisicao) {
        this.aquisicaos.add(aquisicao);
        aquisicao.setLinhaOrcamento(this);
        return this;
    }

    public LinhaOrcamento removeAquisicao(Aquisicao aquisicao) {
        this.aquisicaos.remove(aquisicao);
        aquisicao.setLinhaOrcamento(null);
        return this;
    }

    public Set<Contrato> getContratoes() {
        return this.contratoes;
    }

    public void setContratoes(Set<Contrato> contratoes) {
        if (this.contratoes != null) {
            this.contratoes.forEach(i -> i.setLinhaOrcamento(null));
        }
        if (contratoes != null) {
            contratoes.forEach(i -> i.setLinhaOrcamento(this));
        }
        this.contratoes = contratoes;
    }

    public LinhaOrcamento contratoes(Set<Contrato> contratoes) {
        this.setContratoes(contratoes);
        return this;
    }

    public LinhaOrcamento addContrato(Contrato contrato) {
        this.contratoes.add(contrato);
        contrato.setLinhaOrcamento(this);
        return this;
    }

    public LinhaOrcamento removeContrato(Contrato contrato) {
        this.contratoes.remove(contrato);
        contrato.setLinhaOrcamento(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LinhaOrcamento)) {
            return false;
        }
        return id != null && id.equals(((LinhaOrcamento) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LinhaOrcamento{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", entidadeFinanceira='" + getEntidadeFinanceira() + "'" +
            ", rubrica='" + getRubrica() + "'" +
            ", descricaoRubrica='" + getDescricaoRubrica() + "'" +
            ", valor=" + getValor() +
            ", valorExecutado=" + getValorExecutado() +
            ", saldo=" + getSaldo() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModificationAt='" + getLastModificationAt() + "'" +
            ", lastModificationBy='" + getLastModificationBy() + "'" +
            "}";
    }
}
