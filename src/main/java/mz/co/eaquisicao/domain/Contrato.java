package mz.co.eaquisicao.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Contrato.
 */
@Entity
@Table(name = "contrato")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Contrato implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "codigo")
    private Integer codigo;

    @NotNull
    @Column(name = "valor", precision = 21, scale = 2, nullable = false)
    private BigDecimal valor;

    @NotNull
    @Column(name = "inicio_previsto", nullable = false)
    private LocalDate inicioPrevisto;

    @NotNull
    @Column(name = "termino_previsto", nullable = false)
    private LocalDate terminoPrevisto;

    @NotNull
    @Column(name = "inicio_efectivo", nullable = false)
    private LocalDate inicioEfectivo;

    @NotNull
    @Column(name = "termino_efectivo", nullable = false)
    private LocalDate terminoEfectivo;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "created_at", nullable = true)
    private Instant createdAt;

    @Column(name = "created_by", nullable = true)
    private String createdBy;

    @Column(name = "last_modification_at", nullable = true)
    private Instant lastModificationAt;

    @Column(name = "last_modification_by", nullable = true)
    private String lastModificationBy;

    @JsonIgnoreProperties(value = { "fornecedor", "aquisicao" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private PropostaFinanceira propostaFinanceira;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "aquisicaos", "contratoes" }, allowSetters = true)
    private LinhaOrcamento linhaOrcamento;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Contrato id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCodigo() {
        return this.codigo;
    }

    public Contrato codigo(Integer codigo) {
        this.setCodigo(codigo);
        return this;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public BigDecimal getValor() {
        return this.valor;
    }

    public Contrato valor(BigDecimal valor) {
        this.setValor(valor);
        return this;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public LocalDate getInicioPrevisto() {
        return this.inicioPrevisto;
    }

    public Contrato inicioPrevisto(LocalDate inicioPrevisto) {
        this.setInicioPrevisto(inicioPrevisto);
        return this;
    }

    public void setInicioPrevisto(LocalDate inicioPrevisto) {
        this.inicioPrevisto = inicioPrevisto;
    }

    public LocalDate getTerminoPrevisto() {
        return this.terminoPrevisto;
    }

    public Contrato terminoPrevisto(LocalDate terminoPrevisto) {
        this.setTerminoPrevisto(terminoPrevisto);
        return this;
    }

    public void setTerminoPrevisto(LocalDate terminoPrevisto) {
        this.terminoPrevisto = terminoPrevisto;
    }

    public LocalDate getInicioEfectivo() {
        return this.inicioEfectivo;
    }

    public Contrato inicioEfectivo(LocalDate inicioEfectivo) {
        this.setInicioEfectivo(inicioEfectivo);
        return this;
    }

    public void setInicioEfectivo(LocalDate inicioEfectivo) {
        this.inicioEfectivo = inicioEfectivo;
    }

    public LocalDate getTerminoEfectivo() {
        return this.terminoEfectivo;
    }

    public Contrato terminoEfectivo(LocalDate terminoEfectivo) {
        this.setTerminoEfectivo(terminoEfectivo);
        return this;
    }

    public void setTerminoEfectivo(LocalDate terminoEfectivo) {
        this.terminoEfectivo = terminoEfectivo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Contrato descricao(String descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Instant getCreatedAt() {
        return this.createdAt;
    }

    public Contrato createdAt(Instant createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Contrato createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModificationAt() {
        return this.lastModificationAt;
    }

    public Contrato lastModificationAt(Instant lastModificationAt) {
        this.setLastModificationAt(lastModificationAt);
        return this;
    }

    public void setLastModificationAt(Instant lastModificationAt) {
        this.lastModificationAt = lastModificationAt;
    }

    public String getLastModificationBy() {
        return this.lastModificationBy;
    }

    public Contrato lastModificationBy(String lastModificationBy) {
        this.setLastModificationBy(lastModificationBy);
        return this;
    }

    public void setLastModificationBy(String lastModificationBy) {
        this.lastModificationBy = lastModificationBy;
    }

    public PropostaFinanceira getPropostaFinanceira() {
        return this.propostaFinanceira;
    }

    public void setPropostaFinanceira(PropostaFinanceira propostaFinanceira) {
        this.propostaFinanceira = propostaFinanceira;
    }

    public Contrato propostaFinanceira(PropostaFinanceira propostaFinanceira) {
        this.setPropostaFinanceira(propostaFinanceira);
        return this;
    }

    public LinhaOrcamento getLinhaOrcamento() {
        return this.linhaOrcamento;
    }

    public void setLinhaOrcamento(LinhaOrcamento linhaOrcamento) {
        this.linhaOrcamento = linhaOrcamento;
    }

    public Contrato linhaOrcamento(LinhaOrcamento linhaOrcamento) {
        this.setLinhaOrcamento(linhaOrcamento);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Contrato)) {
            return false;
        }
        return id != null && id.equals(((Contrato) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Contrato{" +
            "id=" + getId() +
            ", codigo=" + getCodigo() +
            ", valor=" + getValor() +
            ", inicioPrevisto='" + getInicioPrevisto() + "'" +
            ", terminoPrevisto='" + getTerminoPrevisto() + "'" +
            ", inicioEfectivo='" + getInicioEfectivo() + "'" +
            ", terminoEfectivo='" + getTerminoEfectivo() + "'" +
            ", descricao='" + getDescricao() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModificationAt='" + getLastModificationAt() + "'" +
            ", lastModificationBy='" + getLastModificationBy() + "'" +
            "}";
    }
}
