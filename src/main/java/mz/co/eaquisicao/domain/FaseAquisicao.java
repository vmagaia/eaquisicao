package mz.co.eaquisicao.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A FaseAquisicao.
 */
@Entity
@Table(name = "fase_aquisicao")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FaseAquisicao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @NotNull
    @Column(name = "dias_execucao", nullable = false)
    private Integer diasExecucao;

    @NotNull
    @Column(name = "sequencia", nullable = false)
    private Integer sequencia;

    @Column(name = "created_at", nullable = true)
    private Instant createdAt;

    @Column(name = "created_by", nullable = true)
    private String createdBy;

    @Column(name = "last_modification_at", nullable = true)
    private Instant lastModificationAt;

    @Column(name = "last_modification_by", nullable = true)
    private String lastModificationBy;

    @OneToMany(mappedBy = "faseAquisicao")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "contrato", "propostaFinanceiras", "categoria", "faseAquisicao", "linhaOrcamento", "modalidadeContratacao" },
        allowSetters = true
    )
    private Set<Aquisicao> aquisicaos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public FaseAquisicao id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public FaseAquisicao nome(String nome) {
        this.setNome(nome);
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getDiasExecucao() {
        return this.diasExecucao;
    }

    public FaseAquisicao diasExecucao(Integer diasExecucao) {
        this.setDiasExecucao(diasExecucao);
        return this;
    }

    public void setDiasExecucao(Integer diasExecucao) {
        this.diasExecucao = diasExecucao;
    }

    public Integer getSequencia() {
        return this.sequencia;
    }

    public FaseAquisicao sequencia(Integer sequencia) {
        this.setSequencia(sequencia);
        return this;
    }

    public void setSequencia(Integer sequencia) {
        this.sequencia = sequencia;
    }

    public Instant getCreatedAt() {
        return this.createdAt;
    }

    public FaseAquisicao createdAt(Instant createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public FaseAquisicao createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModificationAt() {
        return this.lastModificationAt;
    }

    public FaseAquisicao lastModificationAt(Instant lastModificationAt) {
        this.setLastModificationAt(lastModificationAt);
        return this;
    }

    public void setLastModificationAt(Instant lastModificationAt) {
        this.lastModificationAt = lastModificationAt;
    }

    public String getLastModificationBy() {
        return this.lastModificationBy;
    }

    public FaseAquisicao lastModificationBy(String lastModificationBy) {
        this.setLastModificationBy(lastModificationBy);
        return this;
    }

    public void setLastModificationBy(String lastModificationBy) {
        this.lastModificationBy = lastModificationBy;
    }

    public Set<Aquisicao> getAquisicaos() {
        return this.aquisicaos;
    }

    public void setAquisicaos(Set<Aquisicao> aquisicaos) {
        if (this.aquisicaos != null) {
            this.aquisicaos.forEach(i -> i.setFaseAquisicao(null));
        }
        if (aquisicaos != null) {
            aquisicaos.forEach(i -> i.setFaseAquisicao(this));
        }
        this.aquisicaos = aquisicaos;
    }

    public FaseAquisicao aquisicaos(Set<Aquisicao> aquisicaos) {
        this.setAquisicaos(aquisicaos);
        return this;
    }

    public FaseAquisicao addAquisicao(Aquisicao aquisicao) {
        this.aquisicaos.add(aquisicao);
        aquisicao.setFaseAquisicao(this);
        return this;
    }

    public FaseAquisicao removeAquisicao(Aquisicao aquisicao) {
        this.aquisicaos.remove(aquisicao);
        aquisicao.setFaseAquisicao(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FaseAquisicao)) {
            return false;
        }
        return id != null && id.equals(((FaseAquisicao) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FaseAquisicao{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", diasExecucao=" + getDiasExecucao() +
            ", sequencia=" + getSequencia() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModificationAt='" + getLastModificationAt() + "'" +
            ", lastModificationBy='" + getLastModificationBy() + "'" +
            "}";
    }
}
