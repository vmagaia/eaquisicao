package mz.co.eaquisicao.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Fornecedor.
 */
@Entity
@Table(name = "fornecedor")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Fornecedor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @NotNull
    @Column(name = "endereco", nullable = false)
    private String endereco;

    @NotNull
    @Column(name = "nuit", nullable = false)
    private Integer nuit;

    @NotNull
    @Column(name = "area_actividade", nullable = false)
    private String areaActividade;

    @Column(name = "saldo", precision = 21, scale = 2)
    private BigDecimal saldo;

    @Column(name = "created_at", nullable = true)
    private Instant createdAt;

    @Column(name = "created_by", nullable = true)
    private String createdBy;

    @Column(name = "last_modification_at", nullable = true)
    private Instant lastModificationAt;

    @Column(name = "last_modification_by", nullable = true)
    private String lastModificationBy;

    @OneToMany(mappedBy = "fornecedor")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "fornecedor", "aquisicao" }, allowSetters = true)
    private Set<PropostaFinanceira> propostaFinanceiras = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "provincias", "fornecedors" }, allowSetters = true)
    private Pais pais;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Fornecedor id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public Fornecedor nome(String nome) {
        this.setNome(nome);
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return this.endereco;
    }

    public Fornecedor endereco(String endereco) {
        this.setEndereco(endereco);
        return this;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public Integer getNuit() {
        return this.nuit;
    }

    public Fornecedor nuit(Integer nuit) {
        this.setNuit(nuit);
        return this;
    }

    public void setNuit(Integer nuit) {
        this.nuit = nuit;
    }

    public String getAreaActividade() {
        return this.areaActividade;
    }

    public Fornecedor areaActividade(String areaActividade) {
        this.setAreaActividade(areaActividade);
        return this;
    }

    public void setAreaActividade(String areaActividade) {
        this.areaActividade = areaActividade;
    }

    public BigDecimal getSaldo() {
        return this.saldo;
    }

    public Fornecedor saldo(BigDecimal saldo) {
        this.setSaldo(saldo);
        return this;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public Instant getCreatedAt() {
        return this.createdAt;
    }

    public Fornecedor createdAt(Instant createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Fornecedor createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModificationAt() {
        return this.lastModificationAt;
    }

    public Fornecedor lastModificationAt(Instant lastModificationAt) {
        this.setLastModificationAt(lastModificationAt);
        return this;
    }

    public void setLastModificationAt(Instant lastModificationAt) {
        this.lastModificationAt = lastModificationAt;
    }

    public String getLastModificationBy() {
        return this.lastModificationBy;
    }

    public Fornecedor lastModificationBy(String lastModificationBy) {
        this.setLastModificationBy(lastModificationBy);
        return this;
    }

    public void setLastModificationBy(String lastModificationBy) {
        this.lastModificationBy = lastModificationBy;
    }

    public Set<PropostaFinanceira> getPropostaFinanceiras() {
        return this.propostaFinanceiras;
    }

    public void setPropostaFinanceiras(Set<PropostaFinanceira> propostaFinanceiras) {
        if (this.propostaFinanceiras != null) {
            this.propostaFinanceiras.forEach(i -> i.setFornecedor(null));
        }
        if (propostaFinanceiras != null) {
            propostaFinanceiras.forEach(i -> i.setFornecedor(this));
        }
        this.propostaFinanceiras = propostaFinanceiras;
    }

    public Fornecedor propostaFinanceiras(Set<PropostaFinanceira> propostaFinanceiras) {
        this.setPropostaFinanceiras(propostaFinanceiras);
        return this;
    }

    public Fornecedor addPropostaFinanceira(PropostaFinanceira propostaFinanceira) {
        this.propostaFinanceiras.add(propostaFinanceira);
        propostaFinanceira.setFornecedor(this);
        return this;
    }

    public Fornecedor removePropostaFinanceira(PropostaFinanceira propostaFinanceira) {
        this.propostaFinanceiras.remove(propostaFinanceira);
        propostaFinanceira.setFornecedor(null);
        return this;
    }

    public Pais getPais() {
        return this.pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public Fornecedor pais(Pais pais) {
        this.setPais(pais);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fornecedor)) {
            return false;
        }
        return id != null && id.equals(((Fornecedor) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Fornecedor{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", endereco='" + getEndereco() + "'" +
            ", nuit=" + getNuit() +
            ", areaActividade='" + getAreaActividade() + "'" +
            ", saldo=" + getSaldo() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModificationAt='" + getLastModificationAt() + "'" +
            ", lastModificationBy='" + getLastModificationBy() + "'" +
            "}";
    }
}
