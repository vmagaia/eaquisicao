package mz.co.eaquisicao.domain.enumeration;

/**
 * The EstadoProposta enumeration.
 */
public enum EstadoProposta {
    Pendente,
    Aprovada,
    Reprovada,
}
