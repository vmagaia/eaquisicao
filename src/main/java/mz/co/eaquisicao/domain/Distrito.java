package mz.co.eaquisicao.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Distrito.
 */
@Entity
@Table(name = "distrito")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Distrito implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "created_at", nullable = true)
    private Instant createdAt;

    @Column(name = "created_by", nullable = true)
    private String createdBy;

    @Column(name = "last_modification_at", nullable = true)
    private Instant lastModificationAt;

    @Column(name = "last_modification_by", nullable = true)
    private String lastModificationBy;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "distritos", "pais" }, allowSetters = true)
    private Provincia provincia;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Distrito id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public Distrito nome(String nome) {
        this.setNome(nome);
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Instant getCreatedAt() {
        return this.createdAt;
    }

    public Distrito createdAt(Instant createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Distrito createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModificationAt() {
        return this.lastModificationAt;
    }

    public Distrito lastModificationAt(Instant lastModificationAt) {
        this.setLastModificationAt(lastModificationAt);
        return this;
    }

    public void setLastModificationAt(Instant lastModificationAt) {
        this.lastModificationAt = lastModificationAt;
    }

    public String getLastModificationBy() {
        return this.lastModificationBy;
    }

    public Distrito lastModificationBy(String lastModificationBy) {
        this.setLastModificationBy(lastModificationBy);
        return this;
    }

    public void setLastModificationBy(String lastModificationBy) {
        this.lastModificationBy = lastModificationBy;
    }

    public Provincia getProvincia() {
        return this.provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    public Distrito provincia(Provincia provincia) {
        this.setProvincia(provincia);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Distrito)) {
            return false;
        }
        return id != null && id.equals(((Distrito) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Distrito{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModificationAt='" + getLastModificationAt() + "'" +
            ", lastModificationBy='" + getLastModificationBy() + "'" +
            "}";
    }
}
