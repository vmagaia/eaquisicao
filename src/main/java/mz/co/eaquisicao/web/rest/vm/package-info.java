/**
 * View Models used by Spring MVC REST controllers.
 */
package mz.co.eaquisicao.web.rest.vm;
