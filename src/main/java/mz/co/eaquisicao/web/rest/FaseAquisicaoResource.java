package mz.co.eaquisicao.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import mz.co.eaquisicao.domain.FaseAquisicao;
import mz.co.eaquisicao.repository.FaseAquisicaoRepository;
import mz.co.eaquisicao.service.FaseAquisicaoQueryService;
import mz.co.eaquisicao.service.FaseAquisicaoService;
import mz.co.eaquisicao.service.criteria.FaseAquisicaoCriteria;
import mz.co.eaquisicao.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link mz.co.eaquisicao.domain.FaseAquisicao}.
 */
@RestController
@RequestMapping("/api")
public class FaseAquisicaoResource {

    private final Logger log = LoggerFactory.getLogger(FaseAquisicaoResource.class);

    private static final String ENTITY_NAME = "faseAquisicao";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FaseAquisicaoService faseAquisicaoService;

    private final FaseAquisicaoRepository faseAquisicaoRepository;

    private final FaseAquisicaoQueryService faseAquisicaoQueryService;

    public FaseAquisicaoResource(
        FaseAquisicaoService faseAquisicaoService,
        FaseAquisicaoRepository faseAquisicaoRepository,
        FaseAquisicaoQueryService faseAquisicaoQueryService
    ) {
        this.faseAquisicaoService = faseAquisicaoService;
        this.faseAquisicaoRepository = faseAquisicaoRepository;
        this.faseAquisicaoQueryService = faseAquisicaoQueryService;
    }

    /**
     * {@code POST  /fase-aquisicaos} : Create a new faseAquisicao.
     *
     * @param faseAquisicao the faseAquisicao to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new faseAquisicao, or with status {@code 400 (Bad Request)} if the faseAquisicao has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fase-aquisicaos")
    public ResponseEntity<FaseAquisicao> createFaseAquisicao(@Valid @RequestBody FaseAquisicao faseAquisicao) throws URISyntaxException {
        log.debug("REST request to save FaseAquisicao : {}", faseAquisicao);
        if (faseAquisicao.getId() != null) {
            throw new BadRequestAlertException("A new faseAquisicao cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FaseAquisicao result = faseAquisicaoService.save(faseAquisicao);
        return ResponseEntity
            .created(new URI("/api/fase-aquisicaos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fase-aquisicaos/:id} : Updates an existing faseAquisicao.
     *
     * @param id the id of the faseAquisicao to save.
     * @param faseAquisicao the faseAquisicao to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated faseAquisicao,
     * or with status {@code 400 (Bad Request)} if the faseAquisicao is not valid,
     * or with status {@code 500 (Internal Server Error)} if the faseAquisicao couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fase-aquisicaos/{id}")
    public ResponseEntity<FaseAquisicao> updateFaseAquisicao(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody FaseAquisicao faseAquisicao
    ) throws URISyntaxException {
        log.debug("REST request to update FaseAquisicao : {}, {}", id, faseAquisicao);
        if (faseAquisicao.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, faseAquisicao.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!faseAquisicaoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        FaseAquisicao result = faseAquisicaoService.save(faseAquisicao);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, faseAquisicao.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /fase-aquisicaos/:id} : Partial updates given fields of an existing faseAquisicao, field will ignore if it is null
     *
     * @param id the id of the faseAquisicao to save.
     * @param faseAquisicao the faseAquisicao to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated faseAquisicao,
     * or with status {@code 400 (Bad Request)} if the faseAquisicao is not valid,
     * or with status {@code 404 (Not Found)} if the faseAquisicao is not found,
     * or with status {@code 500 (Internal Server Error)} if the faseAquisicao couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/fase-aquisicaos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<FaseAquisicao> partialUpdateFaseAquisicao(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody FaseAquisicao faseAquisicao
    ) throws URISyntaxException {
        log.debug("REST request to partial update FaseAquisicao partially : {}, {}", id, faseAquisicao);
        if (faseAquisicao.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, faseAquisicao.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!faseAquisicaoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<FaseAquisicao> result = faseAquisicaoService.partialUpdate(faseAquisicao);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, faseAquisicao.getId().toString())
        );
    }

    /**
     * {@code GET  /fase-aquisicaos} : get all the faseAquisicaos.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of faseAquisicaos in body.
     */
    @GetMapping("/fase-aquisicaos")
    public ResponseEntity<List<FaseAquisicao>> getAllFaseAquisicaos(FaseAquisicaoCriteria criteria, Pageable pageable) {
        log.debug("REST request to get FaseAquisicaos by criteria: {}", criteria);
        Page<FaseAquisicao> page = faseAquisicaoQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /fase-aquisicaos/count} : count all the faseAquisicaos.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/fase-aquisicaos/count")
    public ResponseEntity<Long> countFaseAquisicaos(FaseAquisicaoCriteria criteria) {
        log.debug("REST request to count FaseAquisicaos by criteria: {}", criteria);
        return ResponseEntity.ok().body(faseAquisicaoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /fase-aquisicaos/:id} : get the "id" faseAquisicao.
     *
     * @param id the id of the faseAquisicao to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the faseAquisicao, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fase-aquisicaos/{id}")
    public ResponseEntity<FaseAquisicao> getFaseAquisicao(@PathVariable Long id) {
        log.debug("REST request to get FaseAquisicao : {}", id);
        Optional<FaseAquisicao> faseAquisicao = faseAquisicaoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(faseAquisicao);
    }

    /**
     * {@code DELETE  /fase-aquisicaos/:id} : delete the "id" faseAquisicao.
     *
     * @param id the id of the faseAquisicao to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fase-aquisicaos/{id}")
    public ResponseEntity<Void> deleteFaseAquisicao(@PathVariable Long id) {
        log.debug("REST request to delete FaseAquisicao : {}", id);
        faseAquisicaoService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
