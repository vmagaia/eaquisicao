package mz.co.eaquisicao.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import mz.co.eaquisicao.domain.LinhaOrcamento;
import mz.co.eaquisicao.repository.LinhaOrcamentoRepository;
import mz.co.eaquisicao.service.LinhaOrcamentoQueryService;
import mz.co.eaquisicao.service.LinhaOrcamentoService;
import mz.co.eaquisicao.service.criteria.LinhaOrcamentoCriteria;
import mz.co.eaquisicao.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link mz.co.eaquisicao.domain.LinhaOrcamento}.
 */
@RestController
@RequestMapping("/api")
public class LinhaOrcamentoResource {

    private final Logger log = LoggerFactory.getLogger(LinhaOrcamentoResource.class);

    private static final String ENTITY_NAME = "linhaOrcamento";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LinhaOrcamentoService linhaOrcamentoService;

    private final LinhaOrcamentoRepository linhaOrcamentoRepository;

    private final LinhaOrcamentoQueryService linhaOrcamentoQueryService;

    public LinhaOrcamentoResource(
        LinhaOrcamentoService linhaOrcamentoService,
        LinhaOrcamentoRepository linhaOrcamentoRepository,
        LinhaOrcamentoQueryService linhaOrcamentoQueryService
    ) {
        this.linhaOrcamentoService = linhaOrcamentoService;
        this.linhaOrcamentoRepository = linhaOrcamentoRepository;
        this.linhaOrcamentoQueryService = linhaOrcamentoQueryService;
    }

    /**
     * {@code POST  /linha-orcamentos} : Create a new linhaOrcamento.
     *
     * @param linhaOrcamento the linhaOrcamento to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new linhaOrcamento, or with status {@code 400 (Bad Request)} if the linhaOrcamento has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/linha-orcamentos")
    public ResponseEntity<LinhaOrcamento> createLinhaOrcamento(@Valid @RequestBody LinhaOrcamento linhaOrcamento)
        throws URISyntaxException {
        log.debug("REST request to save LinhaOrcamento : {}", linhaOrcamento);
        if (linhaOrcamento.getId() != null) {
            throw new BadRequestAlertException("A new linhaOrcamento cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LinhaOrcamento result = linhaOrcamentoService.save(linhaOrcamento);
        return ResponseEntity
            .created(new URI("/api/linha-orcamentos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /linha-orcamentos/:id} : Updates an existing linhaOrcamento.
     *
     * @param id the id of the linhaOrcamento to save.
     * @param linhaOrcamento the linhaOrcamento to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated linhaOrcamento,
     * or with status {@code 400 (Bad Request)} if the linhaOrcamento is not valid,
     * or with status {@code 500 (Internal Server Error)} if the linhaOrcamento couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/linha-orcamentos/{id}")
    public ResponseEntity<LinhaOrcamento> updateLinhaOrcamento(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody LinhaOrcamento linhaOrcamento
    ) throws URISyntaxException {
        log.debug("REST request to update LinhaOrcamento : {}, {}", id, linhaOrcamento);
        if (linhaOrcamento.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, linhaOrcamento.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!linhaOrcamentoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        LinhaOrcamento result = linhaOrcamentoService.save(linhaOrcamento);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, linhaOrcamento.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /linha-orcamentos/:id} : Partial updates given fields of an existing linhaOrcamento, field will ignore if it is null
     *
     * @param id the id of the linhaOrcamento to save.
     * @param linhaOrcamento the linhaOrcamento to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated linhaOrcamento,
     * or with status {@code 400 (Bad Request)} if the linhaOrcamento is not valid,
     * or with status {@code 404 (Not Found)} if the linhaOrcamento is not found,
     * or with status {@code 500 (Internal Server Error)} if the linhaOrcamento couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/linha-orcamentos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<LinhaOrcamento> partialUpdateLinhaOrcamento(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody LinhaOrcamento linhaOrcamento
    ) throws URISyntaxException {
        log.debug("REST request to partial update LinhaOrcamento partially : {}, {}", id, linhaOrcamento);
        if (linhaOrcamento.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, linhaOrcamento.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!linhaOrcamentoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<LinhaOrcamento> result = linhaOrcamentoService.partialUpdate(linhaOrcamento);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, linhaOrcamento.getId().toString())
        );
    }

    /**
     * {@code GET  /linha-orcamentos} : get all the linhaOrcamentos.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of linhaOrcamentos in body.
     */
    @GetMapping("/linha-orcamentos")
    public ResponseEntity<List<LinhaOrcamento>> getAllLinhaOrcamentos(LinhaOrcamentoCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LinhaOrcamentos by criteria: {}", criteria);
        Page<LinhaOrcamento> page = linhaOrcamentoQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /linha-orcamentos/count} : count all the linhaOrcamentos.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/linha-orcamentos/count")
    public ResponseEntity<Long> countLinhaOrcamentos(LinhaOrcamentoCriteria criteria) {
        log.debug("REST request to count LinhaOrcamentos by criteria: {}", criteria);
        return ResponseEntity.ok().body(linhaOrcamentoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /linha-orcamentos/:id} : get the "id" linhaOrcamento.
     *
     * @param id the id of the linhaOrcamento to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the linhaOrcamento, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/linha-orcamentos/{id}")
    public ResponseEntity<LinhaOrcamento> getLinhaOrcamento(@PathVariable Long id) {
        log.debug("REST request to get LinhaOrcamento : {}", id);
        Optional<LinhaOrcamento> linhaOrcamento = linhaOrcamentoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(linhaOrcamento);
    }

    /**
     * {@code DELETE  /linha-orcamentos/:id} : delete the "id" linhaOrcamento.
     *
     * @param id the id of the linhaOrcamento to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/linha-orcamentos/{id}")
    public ResponseEntity<Void> deleteLinhaOrcamento(@PathVariable Long id) {
        log.debug("REST request to delete LinhaOrcamento : {}", id);
        linhaOrcamentoService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
