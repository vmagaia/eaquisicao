package mz.co.eaquisicao.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import mz.co.eaquisicao.domain.Aquisicao;
import mz.co.eaquisicao.repository.AquisicaoRepository;
import mz.co.eaquisicao.service.AquisicaoQueryService;
import mz.co.eaquisicao.service.AquisicaoService;
import mz.co.eaquisicao.service.criteria.AquisicaoCriteria;
import mz.co.eaquisicao.security.SecurityUtils;
import mz.co.eaquisicao.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link mz.co.eaquisicao.domain.Aquisicao}.
 */
@RestController
@RequestMapping("/api")
public class AquisicaoResource {

    private final Logger log = LoggerFactory.getLogger(AquisicaoResource.class);

    private static final String ENTITY_NAME = "aquisicao";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AquisicaoService aquisicaoService;

    private final AquisicaoRepository aquisicaoRepository;

    private final AquisicaoQueryService aquisicaoQueryService;

    public AquisicaoResource(
        AquisicaoService aquisicaoService,
        AquisicaoRepository aquisicaoRepository,
        AquisicaoQueryService aquisicaoQueryService
    ) {
        this.aquisicaoService = aquisicaoService;
        this.aquisicaoRepository = aquisicaoRepository;
        this.aquisicaoQueryService = aquisicaoQueryService;
    }

    /**
     * {@code POST  /aquisicaos} : Create a new aquisicao.
     *
     * @param aquisicao the aquisicao to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aquisicao, or with status {@code 400 (Bad Request)} if the aquisicao has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/aquisicaos")
    public ResponseEntity<Aquisicao> createAquisicao(@Valid @RequestBody Aquisicao aquisicao) throws URISyntaxException {
        log.debug("REST request to save Aquisicao : {}", aquisicao);
        if (aquisicao.getId() != null) {
            throw new BadRequestAlertException("A new aquisicao cannot already have an ID", ENTITY_NAME, "idexists");
        }

        aquisicao.setCreatedAt(Instant.now());
        aquisicao.setCreatedBy(SecurityUtils.getCurrentUserLogin().get());
        aquisicao.setLastModificationAt(Instant.now());
        aquisicao.setLastModificationBy(SecurityUtils.getCurrentUserLogin().get());

        Aquisicao result = aquisicaoService.save(aquisicao);
        return ResponseEntity
            .created(new URI("/api/aquisicaos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /aquisicaos/:id} : Updates an existing aquisicao.
     *
     * @param id the id of the aquisicao to save.
     * @param aquisicao the aquisicao to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aquisicao,
     * or with status {@code 400 (Bad Request)} if the aquisicao is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aquisicao couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/aquisicaos/{id}")
    public ResponseEntity<Aquisicao> updateAquisicao(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Aquisicao aquisicao
    ) throws URISyntaxException {
        log.debug("REST request to update Aquisicao : {}, {}", id, aquisicao);
        if (aquisicao.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, aquisicao.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!aquisicaoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        aquisicao.setLastModificationAt(Instant.now());
        aquisicao.setLastModificationBy(SecurityUtils.getCurrentUserLogin().get());

        Aquisicao result = aquisicaoService.save(aquisicao);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, aquisicao.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /aquisicaos/:id} : Partial updates given fields of an existing aquisicao, field will ignore if it is null
     *
     * @param id the id of the aquisicao to save.
     * @param aquisicao the aquisicao to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aquisicao,
     * or with status {@code 400 (Bad Request)} if the aquisicao is not valid,
     * or with status {@code 404 (Not Found)} if the aquisicao is not found,
     * or with status {@code 500 (Internal Server Error)} if the aquisicao couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/aquisicaos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Aquisicao> partialUpdateAquisicao(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Aquisicao aquisicao
    ) throws URISyntaxException {
        log.debug("REST request to partial update Aquisicao partially : {}, {}", id, aquisicao);
        if (aquisicao.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, aquisicao.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!aquisicaoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Aquisicao> result = aquisicaoService.partialUpdate(aquisicao);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, aquisicao.getId().toString())
        );
    }

    /**
     * {@code GET  /aquisicaos} : get all the aquisicaos.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aquisicaos in body.
     */
    @GetMapping("/aquisicaos")
    public ResponseEntity<List<Aquisicao>> getAllAquisicaos(AquisicaoCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Aquisicaos by criteria: {}", criteria);
        Page<Aquisicao> page = aquisicaoQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /aquisicaos/count} : count all the aquisicaos.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/aquisicaos/count")
    public ResponseEntity<Long> countAquisicaos(AquisicaoCriteria criteria) {
        log.debug("REST request to count Aquisicaos by criteria: {}", criteria);
        return ResponseEntity.ok().body(aquisicaoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /aquisicaos/:id} : get the "id" aquisicao.
     *
     * @param id the id of the aquisicao to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aquisicao, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/aquisicaos/{id}")
    public ResponseEntity<Aquisicao> getAquisicao(@PathVariable Long id) {
        log.debug("REST request to get Aquisicao : {}", id);
        Optional<Aquisicao> aquisicao = aquisicaoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aquisicao);
    }

    /**
     * {@code DELETE  /aquisicaos/:id} : delete the "id" aquisicao.
     *
     * @param id the id of the aquisicao to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/aquisicaos/{id}")
    public ResponseEntity<Void> deleteAquisicao(@PathVariable Long id) {
        log.debug("REST request to delete Aquisicao : {}", id);
        aquisicaoService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
