package mz.co.eaquisicao.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import mz.co.eaquisicao.domain.Distrito;
import mz.co.eaquisicao.repository.DistritoRepository;
import mz.co.eaquisicao.service.DistritoQueryService;
import mz.co.eaquisicao.service.DistritoService;
import mz.co.eaquisicao.service.criteria.DistritoCriteria;
import mz.co.eaquisicao.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link mz.co.eaquisicao.domain.Distrito}.
 */
@RestController
@RequestMapping("/api")
public class DistritoResource {

    private final Logger log = LoggerFactory.getLogger(DistritoResource.class);

    private static final String ENTITY_NAME = "distrito";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DistritoService distritoService;

    private final DistritoRepository distritoRepository;

    private final DistritoQueryService distritoQueryService;

    public DistritoResource(
        DistritoService distritoService,
        DistritoRepository distritoRepository,
        DistritoQueryService distritoQueryService
    ) {
        this.distritoService = distritoService;
        this.distritoRepository = distritoRepository;
        this.distritoQueryService = distritoQueryService;
    }

    /**
     * {@code POST  /distritos} : Create a new distrito.
     *
     * @param distrito the distrito to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new distrito, or with status {@code 400 (Bad Request)} if the distrito has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/distritos")
    public ResponseEntity<Distrito> createDistrito(@Valid @RequestBody Distrito distrito) throws URISyntaxException {
        log.debug("REST request to save Distrito : {}", distrito);
        if (distrito.getId() != null) {
            throw new BadRequestAlertException("A new distrito cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Distrito result = distritoService.save(distrito);
        return ResponseEntity
            .created(new URI("/api/distritos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /distritos/:id} : Updates an existing distrito.
     *
     * @param id the id of the distrito to save.
     * @param distrito the distrito to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated distrito,
     * or with status {@code 400 (Bad Request)} if the distrito is not valid,
     * or with status {@code 500 (Internal Server Error)} if the distrito couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/distritos/{id}")
    public ResponseEntity<Distrito> updateDistrito(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Distrito distrito
    ) throws URISyntaxException {
        log.debug("REST request to update Distrito : {}, {}", id, distrito);
        if (distrito.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, distrito.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!distritoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Distrito result = distritoService.save(distrito);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, distrito.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /distritos/:id} : Partial updates given fields of an existing distrito, field will ignore if it is null
     *
     * @param id the id of the distrito to save.
     * @param distrito the distrito to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated distrito,
     * or with status {@code 400 (Bad Request)} if the distrito is not valid,
     * or with status {@code 404 (Not Found)} if the distrito is not found,
     * or with status {@code 500 (Internal Server Error)} if the distrito couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/distritos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Distrito> partialUpdateDistrito(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Distrito distrito
    ) throws URISyntaxException {
        log.debug("REST request to partial update Distrito partially : {}, {}", id, distrito);
        if (distrito.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, distrito.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!distritoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Distrito> result = distritoService.partialUpdate(distrito);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, distrito.getId().toString())
        );
    }

    /**
     * {@code GET  /distritos} : get all the distritos.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of distritos in body.
     */
    @GetMapping("/distritos")
    public ResponseEntity<List<Distrito>> getAllDistritos(DistritoCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Distritos by criteria: {}", criteria);
        Page<Distrito> page = distritoQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /distritos/count} : count all the distritos.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/distritos/count")
    public ResponseEntity<Long> countDistritos(DistritoCriteria criteria) {
        log.debug("REST request to count Distritos by criteria: {}", criteria);
        return ResponseEntity.ok().body(distritoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /distritos/:id} : get the "id" distrito.
     *
     * @param id the id of the distrito to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the distrito, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/distritos/{id}")
    public ResponseEntity<Distrito> getDistrito(@PathVariable Long id) {
        log.debug("REST request to get Distrito : {}", id);
        Optional<Distrito> distrito = distritoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(distrito);
    }

    /**
     * {@code DELETE  /distritos/:id} : delete the "id" distrito.
     *
     * @param id the id of the distrito to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/distritos/{id}")
    public ResponseEntity<Void> deleteDistrito(@PathVariable Long id) {
        log.debug("REST request to delete Distrito : {}", id);
        distritoService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
