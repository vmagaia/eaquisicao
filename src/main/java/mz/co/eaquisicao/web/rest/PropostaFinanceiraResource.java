package mz.co.eaquisicao.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import mz.co.eaquisicao.domain.PropostaFinanceira;
import mz.co.eaquisicao.repository.PropostaFinanceiraRepository;
import mz.co.eaquisicao.service.PropostaFinanceiraQueryService;
import mz.co.eaquisicao.service.PropostaFinanceiraService;
import mz.co.eaquisicao.service.criteria.PropostaFinanceiraCriteria;
import mz.co.eaquisicao.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link mz.co.eaquisicao.domain.PropostaFinanceira}.
 */
@RestController
@RequestMapping("/api")
public class PropostaFinanceiraResource {

    private final Logger log = LoggerFactory.getLogger(PropostaFinanceiraResource.class);

    private static final String ENTITY_NAME = "propostaFinanceira";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PropostaFinanceiraService propostaFinanceiraService;

    private final PropostaFinanceiraRepository propostaFinanceiraRepository;

    private final PropostaFinanceiraQueryService propostaFinanceiraQueryService;

    public PropostaFinanceiraResource(
        PropostaFinanceiraService propostaFinanceiraService,
        PropostaFinanceiraRepository propostaFinanceiraRepository,
        PropostaFinanceiraQueryService propostaFinanceiraQueryService
    ) {
        this.propostaFinanceiraService = propostaFinanceiraService;
        this.propostaFinanceiraRepository = propostaFinanceiraRepository;
        this.propostaFinanceiraQueryService = propostaFinanceiraQueryService;
    }

    /**
     * {@code POST  /proposta-financeiras} : Create a new propostaFinanceira.
     *
     * @param propostaFinanceira the propostaFinanceira to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new propostaFinanceira, or with status {@code 400 (Bad Request)} if the propostaFinanceira has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/proposta-financeiras")
    public ResponseEntity<PropostaFinanceira> createPropostaFinanceira(@Valid @RequestBody PropostaFinanceira propostaFinanceira)
        throws URISyntaxException {
        log.debug("REST request to save PropostaFinanceira : {}", propostaFinanceira);
        if (propostaFinanceira.getId() != null) {
            throw new BadRequestAlertException("A new propostaFinanceira cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PropostaFinanceira result = propostaFinanceiraService.save(propostaFinanceira);
        return ResponseEntity
            .created(new URI("/api/proposta-financeiras/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /proposta-financeiras/:id} : Updates an existing propostaFinanceira.
     *
     * @param id the id of the propostaFinanceira to save.
     * @param propostaFinanceira the propostaFinanceira to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated propostaFinanceira,
     * or with status {@code 400 (Bad Request)} if the propostaFinanceira is not valid,
     * or with status {@code 500 (Internal Server Error)} if the propostaFinanceira couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/proposta-financeiras/{id}")
    public ResponseEntity<PropostaFinanceira> updatePropostaFinanceira(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PropostaFinanceira propostaFinanceira
    ) throws URISyntaxException {
        log.debug("REST request to update PropostaFinanceira : {}, {}", id, propostaFinanceira);
        if (propostaFinanceira.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, propostaFinanceira.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!propostaFinanceiraRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PropostaFinanceira result = propostaFinanceiraService.save(propostaFinanceira);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, propostaFinanceira.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /proposta-financeiras/:id} : Partial updates given fields of an existing propostaFinanceira, field will ignore if it is null
     *
     * @param id the id of the propostaFinanceira to save.
     * @param propostaFinanceira the propostaFinanceira to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated propostaFinanceira,
     * or with status {@code 400 (Bad Request)} if the propostaFinanceira is not valid,
     * or with status {@code 404 (Not Found)} if the propostaFinanceira is not found,
     * or with status {@code 500 (Internal Server Error)} if the propostaFinanceira couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/proposta-financeiras/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PropostaFinanceira> partialUpdatePropostaFinanceira(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PropostaFinanceira propostaFinanceira
    ) throws URISyntaxException {
        log.debug("REST request to partial update PropostaFinanceira partially : {}, {}", id, propostaFinanceira);
        if (propostaFinanceira.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, propostaFinanceira.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!propostaFinanceiraRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PropostaFinanceira> result = propostaFinanceiraService.partialUpdate(propostaFinanceira);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, propostaFinanceira.getId().toString())
        );
    }

    /**
     * {@code GET  /proposta-financeiras} : get all the propostaFinanceiras.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of propostaFinanceiras in body.
     */
    @GetMapping("/proposta-financeiras")
    public ResponseEntity<List<PropostaFinanceira>> getAllPropostaFinanceiras(PropostaFinanceiraCriteria criteria, Pageable pageable) {
        log.debug("REST request to get PropostaFinanceiras by criteria: {}", criteria);
        Page<PropostaFinanceira> page = propostaFinanceiraQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /proposta-financeiras/count} : count all the propostaFinanceiras.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/proposta-financeiras/count")
    public ResponseEntity<Long> countPropostaFinanceiras(PropostaFinanceiraCriteria criteria) {
        log.debug("REST request to count PropostaFinanceiras by criteria: {}", criteria);
        return ResponseEntity.ok().body(propostaFinanceiraQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /proposta-financeiras/:id} : get the "id" propostaFinanceira.
     *
     * @param id the id of the propostaFinanceira to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the propostaFinanceira, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/proposta-financeiras/{id}")
    public ResponseEntity<PropostaFinanceira> getPropostaFinanceira(@PathVariable Long id) {
        log.debug("REST request to get PropostaFinanceira : {}", id);
        Optional<PropostaFinanceira> propostaFinanceira = propostaFinanceiraService.findOne(id);
        return ResponseUtil.wrapOrNotFound(propostaFinanceira);
    }

    /**
     * {@code DELETE  /proposta-financeiras/:id} : delete the "id" propostaFinanceira.
     *
     * @param id the id of the propostaFinanceira to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/proposta-financeiras/{id}")
    public ResponseEntity<Void> deletePropostaFinanceira(@PathVariable Long id) {
        log.debug("REST request to delete PropostaFinanceira : {}", id);
        propostaFinanceiraService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
