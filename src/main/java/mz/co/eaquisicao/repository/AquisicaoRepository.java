package mz.co.eaquisicao.repository;

import mz.co.eaquisicao.domain.Aquisicao;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Aquisicao entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AquisicaoRepository extends JpaRepository<Aquisicao, Long>, JpaSpecificationExecutor<Aquisicao> {}
