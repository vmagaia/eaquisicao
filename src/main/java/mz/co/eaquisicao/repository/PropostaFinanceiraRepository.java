package mz.co.eaquisicao.repository;

import mz.co.eaquisicao.domain.PropostaFinanceira;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the PropostaFinanceira entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PropostaFinanceiraRepository
    extends JpaRepository<PropostaFinanceira, Long>, JpaSpecificationExecutor<PropostaFinanceira> {}
