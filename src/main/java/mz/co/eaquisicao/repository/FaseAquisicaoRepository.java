package mz.co.eaquisicao.repository;

import mz.co.eaquisicao.domain.FaseAquisicao;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the FaseAquisicao entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FaseAquisicaoRepository extends JpaRepository<FaseAquisicao, Long>, JpaSpecificationExecutor<FaseAquisicao> {}
