package mz.co.eaquisicao.repository;

import mz.co.eaquisicao.domain.LinhaOrcamento;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the LinhaOrcamento entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LinhaOrcamentoRepository extends JpaRepository<LinhaOrcamento, Long>, JpaSpecificationExecutor<LinhaOrcamento> {}
