package mz.co.eaquisicao.repository;

import mz.co.eaquisicao.domain.ModalidadeContratacao;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ModalidadeContratacao entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModalidadeContratacaoRepository
    extends JpaRepository<ModalidadeContratacao, Long>, JpaSpecificationExecutor<ModalidadeContratacao> {}
