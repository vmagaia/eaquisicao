package mz.co.eaquisicao.repository;

import mz.co.eaquisicao.domain.Distrito;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Distrito entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DistritoRepository extends JpaRepository<Distrito, Long>, JpaSpecificationExecutor<Distrito> {}
