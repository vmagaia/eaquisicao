import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ModalidadeContratacaoDetailComponent } from './modalidade-contratacao-detail.component';

describe('ModalidadeContratacao Management Detail Component', () => {
  let comp: ModalidadeContratacaoDetailComponent;
  let fixture: ComponentFixture<ModalidadeContratacaoDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ModalidadeContratacaoDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ modalidadeContratacao: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ModalidadeContratacaoDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ModalidadeContratacaoDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load modalidadeContratacao on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.modalidadeContratacao).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
