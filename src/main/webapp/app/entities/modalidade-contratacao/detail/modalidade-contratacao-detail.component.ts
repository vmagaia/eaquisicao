import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IModalidadeContratacao } from '../modalidade-contratacao.model';

@Component({
  selector: 'jhi-modalidade-contratacao-detail',
  templateUrl: './modalidade-contratacao-detail.component.html',
})
export class ModalidadeContratacaoDetailComponent implements OnInit {
  modalidadeContratacao: IModalidadeContratacao | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ modalidadeContratacao }) => {
      this.modalidadeContratacao = modalidadeContratacao;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
