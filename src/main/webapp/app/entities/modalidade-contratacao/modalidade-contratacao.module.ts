import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ModalidadeContratacaoComponent } from './list/modalidade-contratacao.component';
import { ModalidadeContratacaoDetailComponent } from './detail/modalidade-contratacao-detail.component';
import { ModalidadeContratacaoUpdateComponent } from './update/modalidade-contratacao-update.component';
import { ModalidadeContratacaoDeleteDialogComponent } from './delete/modalidade-contratacao-delete-dialog.component';
import { ModalidadeContratacaoRoutingModule } from './route/modalidade-contratacao-routing.module';

@NgModule({
  imports: [SharedModule, ModalidadeContratacaoRoutingModule],
  declarations: [
    ModalidadeContratacaoComponent,
    ModalidadeContratacaoDetailComponent,
    ModalidadeContratacaoUpdateComponent,
    ModalidadeContratacaoDeleteDialogComponent,
  ],
  entryComponents: [ModalidadeContratacaoDeleteDialogComponent],
})
export class ModalidadeContratacaoModule {}
