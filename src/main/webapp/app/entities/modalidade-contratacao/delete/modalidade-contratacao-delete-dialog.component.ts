import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IModalidadeContratacao } from '../modalidade-contratacao.model';
import { ModalidadeContratacaoService } from '../service/modalidade-contratacao.service';

@Component({
  templateUrl: './modalidade-contratacao-delete-dialog.component.html',
})
export class ModalidadeContratacaoDeleteDialogComponent {
  modalidadeContratacao?: IModalidadeContratacao;

  constructor(protected modalidadeContratacaoService: ModalidadeContratacaoService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.modalidadeContratacaoService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
