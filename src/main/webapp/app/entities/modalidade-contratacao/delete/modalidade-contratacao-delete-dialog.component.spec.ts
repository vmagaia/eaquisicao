jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ModalidadeContratacaoService } from '../service/modalidade-contratacao.service';

import { ModalidadeContratacaoDeleteDialogComponent } from './modalidade-contratacao-delete-dialog.component';

describe('ModalidadeContratacao Management Delete Component', () => {
  let comp: ModalidadeContratacaoDeleteDialogComponent;
  let fixture: ComponentFixture<ModalidadeContratacaoDeleteDialogComponent>;
  let service: ModalidadeContratacaoService;
  let mockActiveModal: NgbActiveModal;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ModalidadeContratacaoDeleteDialogComponent],
      providers: [NgbActiveModal],
    })
      .overrideTemplate(ModalidadeContratacaoDeleteDialogComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ModalidadeContratacaoDeleteDialogComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ModalidadeContratacaoService);
    mockActiveModal = TestBed.inject(NgbActiveModal);
  });

  describe('confirmDelete', () => {
    it('Should call delete service on confirmDelete', inject(
      [],
      fakeAsync(() => {
        // GIVEN
        jest.spyOn(service, 'delete').mockReturnValue(of(new HttpResponse({})));

        // WHEN
        comp.confirmDelete(123);
        tick();

        // THEN
        expect(service.delete).toHaveBeenCalledWith(123);
        expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
      })
    ));

    it('Should not call delete service on clear', () => {
      // GIVEN
      jest.spyOn(service, 'delete');

      // WHEN
      comp.cancel();

      // THEN
      expect(service.delete).not.toHaveBeenCalled();
      expect(mockActiveModal.close).not.toHaveBeenCalled();
      expect(mockActiveModal.dismiss).toHaveBeenCalled();
    });
  });
});
