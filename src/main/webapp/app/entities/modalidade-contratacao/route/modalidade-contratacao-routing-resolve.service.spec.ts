jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IModalidadeContratacao, ModalidadeContratacao } from '../modalidade-contratacao.model';
import { ModalidadeContratacaoService } from '../service/modalidade-contratacao.service';

import { ModalidadeContratacaoRoutingResolveService } from './modalidade-contratacao-routing-resolve.service';

describe('ModalidadeContratacao routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: ModalidadeContratacaoRoutingResolveService;
  let service: ModalidadeContratacaoService;
  let resultModalidadeContratacao: IModalidadeContratacao | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(ModalidadeContratacaoRoutingResolveService);
    service = TestBed.inject(ModalidadeContratacaoService);
    resultModalidadeContratacao = undefined;
  });

  describe('resolve', () => {
    it('should return IModalidadeContratacao returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultModalidadeContratacao = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultModalidadeContratacao).toEqual({ id: 123 });
    });

    it('should return new IModalidadeContratacao if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultModalidadeContratacao = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultModalidadeContratacao).toEqual(new ModalidadeContratacao());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as ModalidadeContratacao })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultModalidadeContratacao = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultModalidadeContratacao).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
