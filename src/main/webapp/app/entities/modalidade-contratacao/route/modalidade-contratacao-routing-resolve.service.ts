import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IModalidadeContratacao, ModalidadeContratacao } from '../modalidade-contratacao.model';
import { ModalidadeContratacaoService } from '../service/modalidade-contratacao.service';

@Injectable({ providedIn: 'root' })
export class ModalidadeContratacaoRoutingResolveService implements Resolve<IModalidadeContratacao> {
  constructor(protected service: ModalidadeContratacaoService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IModalidadeContratacao> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((modalidadeContratacao: HttpResponse<ModalidadeContratacao>) => {
          if (modalidadeContratacao.body) {
            return of(modalidadeContratacao.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ModalidadeContratacao());
  }
}
