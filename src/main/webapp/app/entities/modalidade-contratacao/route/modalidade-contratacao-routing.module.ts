import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ModalidadeContratacaoComponent } from '../list/modalidade-contratacao.component';
import { ModalidadeContratacaoDetailComponent } from '../detail/modalidade-contratacao-detail.component';
import { ModalidadeContratacaoUpdateComponent } from '../update/modalidade-contratacao-update.component';
import { ModalidadeContratacaoRoutingResolveService } from './modalidade-contratacao-routing-resolve.service';

const modalidadeContratacaoRoute: Routes = [
  {
    path: '',
    component: ModalidadeContratacaoComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ModalidadeContratacaoDetailComponent,
    resolve: {
      modalidadeContratacao: ModalidadeContratacaoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ModalidadeContratacaoUpdateComponent,
    resolve: {
      modalidadeContratacao: ModalidadeContratacaoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ModalidadeContratacaoUpdateComponent,
    resolve: {
      modalidadeContratacao: ModalidadeContratacaoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(modalidadeContratacaoRoute)],
  exports: [RouterModule],
})
export class ModalidadeContratacaoRoutingModule {}
