import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IModalidadeContratacao, ModalidadeContratacao } from '../modalidade-contratacao.model';

import { ModalidadeContratacaoService } from './modalidade-contratacao.service';

describe('ModalidadeContratacao Service', () => {
  let service: ModalidadeContratacaoService;
  let httpMock: HttpTestingController;
  let elemDefault: IModalidadeContratacao;
  let expectedResult: IModalidadeContratacao | IModalidadeContratacao[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ModalidadeContratacaoService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      nome: 'AAAAAAA',
      descricao: 'AAAAAAA',
      createdAt: currentDate,
      createdBy: 'AAAAAAA',
      lastModificationAt: currentDate,
      lastModificationBy: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a ModalidadeContratacao', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.create(new ModalidadeContratacao()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ModalidadeContratacao', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nome: 'BBBBBB',
          descricao: 'BBBBBB',
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationBy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ModalidadeContratacao', () => {
      const patchObject = Object.assign(
        {
          descricao: 'BBBBBB',
          createdBy: 'BBBBBB',
        },
        new ModalidadeContratacao()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ModalidadeContratacao', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nome: 'BBBBBB',
          descricao: 'BBBBBB',
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationBy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a ModalidadeContratacao', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addModalidadeContratacaoToCollectionIfMissing', () => {
      it('should add a ModalidadeContratacao to an empty array', () => {
        const modalidadeContratacao: IModalidadeContratacao = { id: 123 };
        expectedResult = service.addModalidadeContratacaoToCollectionIfMissing([], modalidadeContratacao);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(modalidadeContratacao);
      });

      it('should not add a ModalidadeContratacao to an array that contains it', () => {
        const modalidadeContratacao: IModalidadeContratacao = { id: 123 };
        const modalidadeContratacaoCollection: IModalidadeContratacao[] = [
          {
            ...modalidadeContratacao,
          },
          { id: 456 },
        ];
        expectedResult = service.addModalidadeContratacaoToCollectionIfMissing(modalidadeContratacaoCollection, modalidadeContratacao);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ModalidadeContratacao to an array that doesn't contain it", () => {
        const modalidadeContratacao: IModalidadeContratacao = { id: 123 };
        const modalidadeContratacaoCollection: IModalidadeContratacao[] = [{ id: 456 }];
        expectedResult = service.addModalidadeContratacaoToCollectionIfMissing(modalidadeContratacaoCollection, modalidadeContratacao);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(modalidadeContratacao);
      });

      it('should add only unique ModalidadeContratacao to an array', () => {
        const modalidadeContratacaoArray: IModalidadeContratacao[] = [{ id: 123 }, { id: 456 }, { id: 50981 }];
        const modalidadeContratacaoCollection: IModalidadeContratacao[] = [{ id: 123 }];
        expectedResult = service.addModalidadeContratacaoToCollectionIfMissing(
          modalidadeContratacaoCollection,
          ...modalidadeContratacaoArray
        );
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const modalidadeContratacao: IModalidadeContratacao = { id: 123 };
        const modalidadeContratacao2: IModalidadeContratacao = { id: 456 };
        expectedResult = service.addModalidadeContratacaoToCollectionIfMissing([], modalidadeContratacao, modalidadeContratacao2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(modalidadeContratacao);
        expect(expectedResult).toContain(modalidadeContratacao2);
      });

      it('should accept null and undefined values', () => {
        const modalidadeContratacao: IModalidadeContratacao = { id: 123 };
        expectedResult = service.addModalidadeContratacaoToCollectionIfMissing([], null, modalidadeContratacao, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(modalidadeContratacao);
      });

      it('should return initial array if no ModalidadeContratacao is added', () => {
        const modalidadeContratacaoCollection: IModalidadeContratacao[] = [{ id: 123 }];
        expectedResult = service.addModalidadeContratacaoToCollectionIfMissing(modalidadeContratacaoCollection, undefined, null);
        expect(expectedResult).toEqual(modalidadeContratacaoCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
