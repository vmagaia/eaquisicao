import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IModalidadeContratacao, getModalidadeContratacaoIdentifier } from '../modalidade-contratacao.model';

export type EntityResponseType = HttpResponse<IModalidadeContratacao>;
export type EntityArrayResponseType = HttpResponse<IModalidadeContratacao[]>;

@Injectable({ providedIn: 'root' })
export class ModalidadeContratacaoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/modalidade-contratacaos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(modalidadeContratacao: IModalidadeContratacao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(modalidadeContratacao);
    return this.http
      .post<IModalidadeContratacao>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(modalidadeContratacao: IModalidadeContratacao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(modalidadeContratacao);
    return this.http
      .put<IModalidadeContratacao>(`${this.resourceUrl}/${getModalidadeContratacaoIdentifier(modalidadeContratacao) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(modalidadeContratacao: IModalidadeContratacao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(modalidadeContratacao);
    return this.http
      .patch<IModalidadeContratacao>(`${this.resourceUrl}/${getModalidadeContratacaoIdentifier(modalidadeContratacao) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IModalidadeContratacao>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IModalidadeContratacao[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addModalidadeContratacaoToCollectionIfMissing(
    modalidadeContratacaoCollection: IModalidadeContratacao[],
    ...modalidadeContratacaosToCheck: (IModalidadeContratacao | null | undefined)[]
  ): IModalidadeContratacao[] {
    const modalidadeContratacaos: IModalidadeContratacao[] = modalidadeContratacaosToCheck.filter(isPresent);
    if (modalidadeContratacaos.length > 0) {
      const modalidadeContratacaoCollectionIdentifiers = modalidadeContratacaoCollection.map(
        modalidadeContratacaoItem => getModalidadeContratacaoIdentifier(modalidadeContratacaoItem)!
      );
      const modalidadeContratacaosToAdd = modalidadeContratacaos.filter(modalidadeContratacaoItem => {
        const modalidadeContratacaoIdentifier = getModalidadeContratacaoIdentifier(modalidadeContratacaoItem);
        if (
          modalidadeContratacaoIdentifier == null ||
          modalidadeContratacaoCollectionIdentifiers.includes(modalidadeContratacaoIdentifier)
        ) {
          return false;
        }
        modalidadeContratacaoCollectionIdentifiers.push(modalidadeContratacaoIdentifier);
        return true;
      });
      return [...modalidadeContratacaosToAdd, ...modalidadeContratacaoCollection];
    }
    return modalidadeContratacaoCollection;
  }

  protected convertDateFromClient(modalidadeContratacao: IModalidadeContratacao): IModalidadeContratacao {
    return Object.assign({}, modalidadeContratacao, {
      createdAt: modalidadeContratacao.createdAt?.isValid() ? modalidadeContratacao.createdAt.toJSON() : undefined,
      lastModificationAt: modalidadeContratacao.lastModificationAt?.isValid()
        ? modalidadeContratacao.lastModificationAt.toJSON()
        : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? dayjs(res.body.createdAt) : undefined;
      res.body.lastModificationAt = res.body.lastModificationAt ? dayjs(res.body.lastModificationAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((modalidadeContratacao: IModalidadeContratacao) => {
        modalidadeContratacao.createdAt = modalidadeContratacao.createdAt ? dayjs(modalidadeContratacao.createdAt) : undefined;
        modalidadeContratacao.lastModificationAt = modalidadeContratacao.lastModificationAt
          ? dayjs(modalidadeContratacao.lastModificationAt)
          : undefined;
      });
    }
    return res;
  }
}
