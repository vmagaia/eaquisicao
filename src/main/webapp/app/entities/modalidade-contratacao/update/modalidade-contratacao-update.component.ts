import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IModalidadeContratacao, ModalidadeContratacao } from '../modalidade-contratacao.model';
import { ModalidadeContratacaoService } from '../service/modalidade-contratacao.service';

@Component({
  selector: 'jhi-modalidade-contratacao-update',
  templateUrl: './modalidade-contratacao-update.component.html',
})
export class ModalidadeContratacaoUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nome: [null, [Validators.required]],
    descricao: [],
    createdAt: [],
    createdBy: [],
    lastModificationAt: [],
    lastModificationBy: [],
  });

  constructor(
    protected modalidadeContratacaoService: ModalidadeContratacaoService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ modalidadeContratacao }) => {
      if (modalidadeContratacao.id === undefined) {
        const today = dayjs().startOf('day');
        modalidadeContratacao.createdAt = today;
        modalidadeContratacao.lastModificationAt = today;
      }

      this.updateForm(modalidadeContratacao);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const modalidadeContratacao = this.createFromForm();
    if (modalidadeContratacao.id !== undefined) {
      this.subscribeToSaveResponse(this.modalidadeContratacaoService.update(modalidadeContratacao));
    } else {
      this.subscribeToSaveResponse(this.modalidadeContratacaoService.create(modalidadeContratacao));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IModalidadeContratacao>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(modalidadeContratacao: IModalidadeContratacao): void {
    this.editForm.patchValue({
      id: modalidadeContratacao.id,
      nome: modalidadeContratacao.nome,
      descricao: modalidadeContratacao.descricao,
      createdAt: modalidadeContratacao.createdAt ? modalidadeContratacao.createdAt.format(DATE_TIME_FORMAT) : null,
      createdBy: modalidadeContratacao.createdBy,
      lastModificationAt: modalidadeContratacao.lastModificationAt
        ? modalidadeContratacao.lastModificationAt.format(DATE_TIME_FORMAT)
        : null,
      lastModificationBy: modalidadeContratacao.lastModificationBy,
    });
  }

  protected createFromForm(): IModalidadeContratacao {
    return {
      ...new ModalidadeContratacao(),
      id: this.editForm.get(['id'])!.value,
      nome: this.editForm.get(['nome'])!.value,
      descricao: this.editForm.get(['descricao'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? dayjs(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      lastModificationAt: this.editForm.get(['lastModificationAt'])!.value
        ? dayjs(this.editForm.get(['lastModificationAt'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastModificationBy: this.editForm.get(['lastModificationBy'])!.value,
    };
  }
}
