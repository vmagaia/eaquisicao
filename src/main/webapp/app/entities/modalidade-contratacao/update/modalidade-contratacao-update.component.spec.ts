jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ModalidadeContratacaoService } from '../service/modalidade-contratacao.service';
import { IModalidadeContratacao, ModalidadeContratacao } from '../modalidade-contratacao.model';

import { ModalidadeContratacaoUpdateComponent } from './modalidade-contratacao-update.component';

describe('ModalidadeContratacao Management Update Component', () => {
  let comp: ModalidadeContratacaoUpdateComponent;
  let fixture: ComponentFixture<ModalidadeContratacaoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let modalidadeContratacaoService: ModalidadeContratacaoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ModalidadeContratacaoUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(ModalidadeContratacaoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ModalidadeContratacaoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    modalidadeContratacaoService = TestBed.inject(ModalidadeContratacaoService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const modalidadeContratacao: IModalidadeContratacao = { id: 456 };

      activatedRoute.data = of({ modalidadeContratacao });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(modalidadeContratacao));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ModalidadeContratacao>>();
      const modalidadeContratacao = { id: 123 };
      jest.spyOn(modalidadeContratacaoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ modalidadeContratacao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: modalidadeContratacao }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(modalidadeContratacaoService.update).toHaveBeenCalledWith(modalidadeContratacao);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ModalidadeContratacao>>();
      const modalidadeContratacao = new ModalidadeContratacao();
      jest.spyOn(modalidadeContratacaoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ modalidadeContratacao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: modalidadeContratacao }));
      saveSubject.complete();

      // THEN
      expect(modalidadeContratacaoService.create).toHaveBeenCalledWith(modalidadeContratacao);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ModalidadeContratacao>>();
      const modalidadeContratacao = { id: 123 };
      jest.spyOn(modalidadeContratacaoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ modalidadeContratacao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(modalidadeContratacaoService.update).toHaveBeenCalledWith(modalidadeContratacao);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
