import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPropostaFinanceira, PropostaFinanceira } from '../proposta-financeira.model';
import { PropostaFinanceiraService } from '../service/proposta-financeira.service';

@Injectable({ providedIn: 'root' })
export class PropostaFinanceiraRoutingResolveService implements Resolve<IPropostaFinanceira> {
  constructor(protected service: PropostaFinanceiraService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPropostaFinanceira> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((propostaFinanceira: HttpResponse<PropostaFinanceira>) => {
          if (propostaFinanceira.body) {
            return of(propostaFinanceira.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PropostaFinanceira());
  }
}
