jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IPropostaFinanceira, PropostaFinanceira } from '../proposta-financeira.model';
import { PropostaFinanceiraService } from '../service/proposta-financeira.service';

import { PropostaFinanceiraRoutingResolveService } from './proposta-financeira-routing-resolve.service';

describe('PropostaFinanceira routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: PropostaFinanceiraRoutingResolveService;
  let service: PropostaFinanceiraService;
  let resultPropostaFinanceira: IPropostaFinanceira | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(PropostaFinanceiraRoutingResolveService);
    service = TestBed.inject(PropostaFinanceiraService);
    resultPropostaFinanceira = undefined;
  });

  describe('resolve', () => {
    it('should return IPropostaFinanceira returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPropostaFinanceira = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPropostaFinanceira).toEqual({ id: 123 });
    });

    it('should return new IPropostaFinanceira if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPropostaFinanceira = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultPropostaFinanceira).toEqual(new PropostaFinanceira());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as PropostaFinanceira })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPropostaFinanceira = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPropostaFinanceira).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
