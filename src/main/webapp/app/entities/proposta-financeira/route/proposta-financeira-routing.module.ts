import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PropostaFinanceiraComponent } from '../list/proposta-financeira.component';
import { PropostaFinanceiraDetailComponent } from '../detail/proposta-financeira-detail.component';
import { PropostaFinanceiraUpdateComponent } from '../update/proposta-financeira-update.component';
import { PropostaFinanceiraRoutingResolveService } from './proposta-financeira-routing-resolve.service';

const propostaFinanceiraRoute: Routes = [
  {
    path: '',
    component: PropostaFinanceiraComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PropostaFinanceiraDetailComponent,
    resolve: {
      propostaFinanceira: PropostaFinanceiraRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PropostaFinanceiraUpdateComponent,
    resolve: {
      propostaFinanceira: PropostaFinanceiraRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PropostaFinanceiraUpdateComponent,
    resolve: {
      propostaFinanceira: PropostaFinanceiraRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(propostaFinanceiraRoute)],
  exports: [RouterModule],
})
export class PropostaFinanceiraRoutingModule {}
