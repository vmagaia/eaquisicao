import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPropostaFinanceira } from '../proposta-financeira.model';
import { PropostaFinanceiraService } from '../service/proposta-financeira.service';

@Component({
  templateUrl: './proposta-financeira-delete-dialog.component.html',
})
export class PropostaFinanceiraDeleteDialogComponent {
  propostaFinanceira?: IPropostaFinanceira;

  constructor(protected propostaFinanceiraService: PropostaFinanceiraService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.propostaFinanceiraService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
