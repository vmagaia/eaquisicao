import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPropostaFinanceira } from '../proposta-financeira.model';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-proposta-financeira-detail',
  templateUrl: './proposta-financeira-detail.component.html',
})
export class PropostaFinanceiraDetailComponent implements OnInit {
  propostaFinanceira: IPropostaFinanceira | null = null;

  constructor(protected dataUtils: DataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ propostaFinanceira }) => {
      this.propostaFinanceira = propostaFinanceira;
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  previousState(): void {
    window.history.back();
  }
}
