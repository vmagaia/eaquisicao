import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPropostaFinanceira, getPropostaFinanceiraIdentifier } from '../proposta-financeira.model';

export type EntityResponseType = HttpResponse<IPropostaFinanceira>;
export type EntityArrayResponseType = HttpResponse<IPropostaFinanceira[]>;

@Injectable({ providedIn: 'root' })
export class PropostaFinanceiraService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/proposta-financeiras');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(propostaFinanceira: IPropostaFinanceira): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(propostaFinanceira);
    return this.http
      .post<IPropostaFinanceira>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(propostaFinanceira: IPropostaFinanceira): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(propostaFinanceira);
    return this.http
      .put<IPropostaFinanceira>(`${this.resourceUrl}/${getPropostaFinanceiraIdentifier(propostaFinanceira) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(propostaFinanceira: IPropostaFinanceira): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(propostaFinanceira);
    return this.http
      .patch<IPropostaFinanceira>(`${this.resourceUrl}/${getPropostaFinanceiraIdentifier(propostaFinanceira) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPropostaFinanceira>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPropostaFinanceira[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPropostaFinanceiraToCollectionIfMissing(
    propostaFinanceiraCollection: IPropostaFinanceira[],
    ...propostaFinanceirasToCheck: (IPropostaFinanceira | null | undefined)[]
  ): IPropostaFinanceira[] {
    const propostaFinanceiras: IPropostaFinanceira[] = propostaFinanceirasToCheck.filter(isPresent);
    if (propostaFinanceiras.length > 0) {
      const propostaFinanceiraCollectionIdentifiers = propostaFinanceiraCollection.map(
        propostaFinanceiraItem => getPropostaFinanceiraIdentifier(propostaFinanceiraItem)!
      );
      const propostaFinanceirasToAdd = propostaFinanceiras.filter(propostaFinanceiraItem => {
        const propostaFinanceiraIdentifier = getPropostaFinanceiraIdentifier(propostaFinanceiraItem);
        if (propostaFinanceiraIdentifier == null || propostaFinanceiraCollectionIdentifiers.includes(propostaFinanceiraIdentifier)) {
          return false;
        }
        propostaFinanceiraCollectionIdentifiers.push(propostaFinanceiraIdentifier);
        return true;
      });
      return [...propostaFinanceirasToAdd, ...propostaFinanceiraCollection];
    }
    return propostaFinanceiraCollection;
  }

  protected convertDateFromClient(propostaFinanceira: IPropostaFinanceira): IPropostaFinanceira {
    return Object.assign({}, propostaFinanceira, {
      createdAt: propostaFinanceira.createdAt?.isValid() ? propostaFinanceira.createdAt.toJSON() : undefined,
      lastModificationAt: propostaFinanceira.lastModificationAt?.isValid() ? propostaFinanceira.lastModificationAt.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? dayjs(res.body.createdAt) : undefined;
      res.body.lastModificationAt = res.body.lastModificationAt ? dayjs(res.body.lastModificationAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((propostaFinanceira: IPropostaFinanceira) => {
        propostaFinanceira.createdAt = propostaFinanceira.createdAt ? dayjs(propostaFinanceira.createdAt) : undefined;
        propostaFinanceira.lastModificationAt = propostaFinanceira.lastModificationAt
          ? dayjs(propostaFinanceira.lastModificationAt)
          : undefined;
      });
    }
    return res;
  }
}
