import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { EstadoProposta } from 'app/entities/enumerations/estado-proposta.model';
import { IPropostaFinanceira, PropostaFinanceira } from '../proposta-financeira.model';

import { PropostaFinanceiraService } from './proposta-financeira.service';

describe('PropostaFinanceira Service', () => {
  let service: PropostaFinanceiraService;
  let httpMock: HttpTestingController;
  let elemDefault: IPropostaFinanceira;
  let expectedResult: IPropostaFinanceira | IPropostaFinanceira[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PropostaFinanceiraService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      valor: 0,
      descricao: 'AAAAAAA',
      anexoContentType: 'image/png',
      anexo: 'AAAAAAA',
      estadoProposta: EstadoProposta.Pendente,
      createdAt: currentDate,
      createdBy: 'AAAAAAA',
      lastModificationAt: currentDate,
      lastModificationBy: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a PropostaFinanceira', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.create(new PropostaFinanceira()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a PropostaFinanceira', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          valor: 1,
          descricao: 'BBBBBB',
          anexo: 'BBBBBB',
          estadoProposta: 'BBBBBB',
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationBy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a PropostaFinanceira', () => {
      const patchObject = Object.assign(
        {
          valor: 1,
          anexo: 'BBBBBB',
          estadoProposta: 'BBBBBB',
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationBy: 'BBBBBB',
        },
        new PropostaFinanceira()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of PropostaFinanceira', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          valor: 1,
          descricao: 'BBBBBB',
          anexo: 'BBBBBB',
          estadoProposta: 'BBBBBB',
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationBy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a PropostaFinanceira', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addPropostaFinanceiraToCollectionIfMissing', () => {
      it('should add a PropostaFinanceira to an empty array', () => {
        const propostaFinanceira: IPropostaFinanceira = { id: 123 };
        expectedResult = service.addPropostaFinanceiraToCollectionIfMissing([], propostaFinanceira);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(propostaFinanceira);
      });

      it('should not add a PropostaFinanceira to an array that contains it', () => {
        const propostaFinanceira: IPropostaFinanceira = { id: 123 };
        const propostaFinanceiraCollection: IPropostaFinanceira[] = [
          {
            ...propostaFinanceira,
          },
          { id: 456 },
        ];
        expectedResult = service.addPropostaFinanceiraToCollectionIfMissing(propostaFinanceiraCollection, propostaFinanceira);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a PropostaFinanceira to an array that doesn't contain it", () => {
        const propostaFinanceira: IPropostaFinanceira = { id: 123 };
        const propostaFinanceiraCollection: IPropostaFinanceira[] = [{ id: 456 }];
        expectedResult = service.addPropostaFinanceiraToCollectionIfMissing(propostaFinanceiraCollection, propostaFinanceira);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(propostaFinanceira);
      });

      it('should add only unique PropostaFinanceira to an array', () => {
        const propostaFinanceiraArray: IPropostaFinanceira[] = [{ id: 123 }, { id: 456 }, { id: 38470 }];
        const propostaFinanceiraCollection: IPropostaFinanceira[] = [{ id: 123 }];
        expectedResult = service.addPropostaFinanceiraToCollectionIfMissing(propostaFinanceiraCollection, ...propostaFinanceiraArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const propostaFinanceira: IPropostaFinanceira = { id: 123 };
        const propostaFinanceira2: IPropostaFinanceira = { id: 456 };
        expectedResult = service.addPropostaFinanceiraToCollectionIfMissing([], propostaFinanceira, propostaFinanceira2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(propostaFinanceira);
        expect(expectedResult).toContain(propostaFinanceira2);
      });

      it('should accept null and undefined values', () => {
        const propostaFinanceira: IPropostaFinanceira = { id: 123 };
        expectedResult = service.addPropostaFinanceiraToCollectionIfMissing([], null, propostaFinanceira, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(propostaFinanceira);
      });

      it('should return initial array if no PropostaFinanceira is added', () => {
        const propostaFinanceiraCollection: IPropostaFinanceira[] = [{ id: 123 }];
        expectedResult = service.addPropostaFinanceiraToCollectionIfMissing(propostaFinanceiraCollection, undefined, null);
        expect(expectedResult).toEqual(propostaFinanceiraCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
