import * as dayjs from 'dayjs';
import { IFornecedor } from 'app/entities/fornecedor/fornecedor.model';
import { IAquisicao } from 'app/entities/aquisicao/aquisicao.model';
import { EstadoProposta } from 'app/entities/enumerations/estado-proposta.model';

export interface IPropostaFinanceira {
  id?: number;
  valor?: number;
  descricao?: string;
  anexoContentType?: string;
  anexo?: string;
  estadoProposta?: EstadoProposta | null;
  createdAt?: dayjs.Dayjs | null;
  createdBy?: string | null;
  lastModificationAt?: dayjs.Dayjs | null;
  lastModificationBy?: string | null;
  fornecedor?: IFornecedor;
  aquisicao?: IAquisicao;
}

export class PropostaFinanceira implements IPropostaFinanceira {
  constructor(
    public id?: number,
    public valor?: number,
    public descricao?: string,
    public anexoContentType?: string,
    public anexo?: string,
    public estadoProposta?: EstadoProposta | null,
    public createdAt?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public lastModificationAt?: dayjs.Dayjs | null,
    public lastModificationBy?: string | null,
    public fornecedor?: IFornecedor,
    public aquisicao?: IAquisicao
  ) {}
}

export function getPropostaFinanceiraIdentifier(propostaFinanceira: IPropostaFinanceira): number | undefined {
  return propostaFinanceira.id;
}
