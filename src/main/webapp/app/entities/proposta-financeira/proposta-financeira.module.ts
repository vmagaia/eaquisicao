import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { PropostaFinanceiraComponent } from './list/proposta-financeira.component';
import { PropostaFinanceiraDetailComponent } from './detail/proposta-financeira-detail.component';
import { PropostaFinanceiraUpdateComponent } from './update/proposta-financeira-update.component';
import { PropostaFinanceiraDeleteDialogComponent } from './delete/proposta-financeira-delete-dialog.component';
import { PropostaFinanceiraRoutingModule } from './route/proposta-financeira-routing.module';

@NgModule({
  imports: [SharedModule, PropostaFinanceiraRoutingModule],
  declarations: [
    PropostaFinanceiraComponent,
    PropostaFinanceiraDetailComponent,
    PropostaFinanceiraUpdateComponent,
    PropostaFinanceiraDeleteDialogComponent,
  ],
  entryComponents: [PropostaFinanceiraDeleteDialogComponent],
})
export class PropostaFinanceiraModule {}
