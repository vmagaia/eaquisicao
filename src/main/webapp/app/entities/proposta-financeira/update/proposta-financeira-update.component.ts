import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IPropostaFinanceira, PropostaFinanceira } from '../proposta-financeira.model';
import { PropostaFinanceiraService } from '../service/proposta-financeira.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';
import { IFornecedor } from 'app/entities/fornecedor/fornecedor.model';
import { FornecedorService } from 'app/entities/fornecedor/service/fornecedor.service';
import { IAquisicao } from 'app/entities/aquisicao/aquisicao.model';
import { AquisicaoService } from 'app/entities/aquisicao/service/aquisicao.service';
import { EstadoProposta } from 'app/entities/enumerations/estado-proposta.model';

@Component({
  selector: 'jhi-proposta-financeira-update',
  templateUrl: './proposta-financeira-update.component.html',
})
export class PropostaFinanceiraUpdateComponent implements OnInit {
  isSaving = false;
  estadoPropostaValues = Object.keys(EstadoProposta);

  fornecedorsSharedCollection: IFornecedor[] = [];
  aquisicaosSharedCollection: IAquisicao[] = [];

  editForm = this.fb.group({
    id: [],
    valor: [null, [Validators.required]],
    descricao: [null, [Validators.required]],
    anexo: [null, [Validators.required]],
    anexoContentType: [],
    estadoProposta: [],
    createdAt: [],
    createdBy: [],
    lastModificationAt: [],
    lastModificationBy: [],
    fornecedor: [null, Validators.required],
    aquisicao: [null, Validators.required],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected propostaFinanceiraService: PropostaFinanceiraService,
    protected fornecedorService: FornecedorService,
    protected aquisicaoService: AquisicaoService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ propostaFinanceira }) => {
      if (propostaFinanceira.id === undefined) {
        const today = dayjs().startOf('day');
        propostaFinanceira.createdAt = today;
        propostaFinanceira.lastModificationAt = today;
      }

      this.updateForm(propostaFinanceira);

      this.loadRelationshipsOptions();
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(new EventWithContent<AlertError>('eaquisicaoApp.error', { ...err, key: 'error.file.' + err.key })),
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const propostaFinanceira = this.createFromForm();
    if (propostaFinanceira.id !== undefined) {
      this.subscribeToSaveResponse(this.propostaFinanceiraService.update(propostaFinanceira));
    } else {
      this.subscribeToSaveResponse(this.propostaFinanceiraService.create(propostaFinanceira));
    }
  }

  trackFornecedorById(index: number, item: IFornecedor): number {
    return item.id!;
  }

  trackAquisicaoById(index: number, item: IAquisicao): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPropostaFinanceira>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(propostaFinanceira: IPropostaFinanceira): void {
    this.editForm.patchValue({
      id: propostaFinanceira.id,
      valor: propostaFinanceira.valor,
      descricao: propostaFinanceira.descricao,
      anexo: propostaFinanceira.anexo,
      anexoContentType: propostaFinanceira.anexoContentType,
      estadoProposta: propostaFinanceira.estadoProposta,
      createdAt: propostaFinanceira.createdAt ? propostaFinanceira.createdAt.format(DATE_TIME_FORMAT) : null,
      createdBy: propostaFinanceira.createdBy,
      lastModificationAt: propostaFinanceira.lastModificationAt ? propostaFinanceira.lastModificationAt.format(DATE_TIME_FORMAT) : null,
      lastModificationBy: propostaFinanceira.lastModificationBy,
      fornecedor: propostaFinanceira.fornecedor,
      aquisicao: propostaFinanceira.aquisicao,
    });

    this.fornecedorsSharedCollection = this.fornecedorService.addFornecedorToCollectionIfMissing(
      this.fornecedorsSharedCollection,
      propostaFinanceira.fornecedor
    );
    this.aquisicaosSharedCollection = this.aquisicaoService.addAquisicaoToCollectionIfMissing(
      this.aquisicaosSharedCollection,
      propostaFinanceira.aquisicao
    );
  }

  protected loadRelationshipsOptions(): void {
    this.fornecedorService
      .query()
      .pipe(map((res: HttpResponse<IFornecedor[]>) => res.body ?? []))
      .pipe(
        map((fornecedors: IFornecedor[]) =>
          this.fornecedorService.addFornecedorToCollectionIfMissing(fornecedors, this.editForm.get('fornecedor')!.value)
        )
      )
      .subscribe((fornecedors: IFornecedor[]) => (this.fornecedorsSharedCollection = fornecedors));

    this.aquisicaoService
      .query()
      .pipe(map((res: HttpResponse<IAquisicao[]>) => res.body ?? []))
      .pipe(
        map((aquisicaos: IAquisicao[]) =>
          this.aquisicaoService.addAquisicaoToCollectionIfMissing(aquisicaos, this.editForm.get('aquisicao')!.value)
        )
      )
      .subscribe((aquisicaos: IAquisicao[]) => (this.aquisicaosSharedCollection = aquisicaos));
  }

  protected createFromForm(): IPropostaFinanceira {
    return {
      ...new PropostaFinanceira(),
      id: this.editForm.get(['id'])!.value,
      valor: this.editForm.get(['valor'])!.value,
      descricao: this.editForm.get(['descricao'])!.value,
      anexoContentType: this.editForm.get(['anexoContentType'])!.value,
      anexo: this.editForm.get(['anexo'])!.value,
      estadoProposta: this.editForm.get(['estadoProposta'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? dayjs(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      lastModificationAt: this.editForm.get(['lastModificationAt'])!.value
        ? dayjs(this.editForm.get(['lastModificationAt'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastModificationBy: this.editForm.get(['lastModificationBy'])!.value,
      fornecedor: this.editForm.get(['fornecedor'])!.value,
      aquisicao: this.editForm.get(['aquisicao'])!.value,
    };
  }
}
