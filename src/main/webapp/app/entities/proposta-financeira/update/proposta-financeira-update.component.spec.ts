jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PropostaFinanceiraService } from '../service/proposta-financeira.service';
import { IPropostaFinanceira, PropostaFinanceira } from '../proposta-financeira.model';
import { IFornecedor } from 'app/entities/fornecedor/fornecedor.model';
import { FornecedorService } from 'app/entities/fornecedor/service/fornecedor.service';
import { IAquisicao } from 'app/entities/aquisicao/aquisicao.model';
import { AquisicaoService } from 'app/entities/aquisicao/service/aquisicao.service';

import { PropostaFinanceiraUpdateComponent } from './proposta-financeira-update.component';

describe('PropostaFinanceira Management Update Component', () => {
  let comp: PropostaFinanceiraUpdateComponent;
  let fixture: ComponentFixture<PropostaFinanceiraUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let propostaFinanceiraService: PropostaFinanceiraService;
  let fornecedorService: FornecedorService;
  let aquisicaoService: AquisicaoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PropostaFinanceiraUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(PropostaFinanceiraUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PropostaFinanceiraUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    propostaFinanceiraService = TestBed.inject(PropostaFinanceiraService);
    fornecedorService = TestBed.inject(FornecedorService);
    aquisicaoService = TestBed.inject(AquisicaoService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Fornecedor query and add missing value', () => {
      const propostaFinanceira: IPropostaFinanceira = { id: 456 };
      const fornecedor: IFornecedor = { id: 61596 };
      propostaFinanceira.fornecedor = fornecedor;

      const fornecedorCollection: IFornecedor[] = [{ id: 30358 }];
      jest.spyOn(fornecedorService, 'query').mockReturnValue(of(new HttpResponse({ body: fornecedorCollection })));
      const additionalFornecedors = [fornecedor];
      const expectedCollection: IFornecedor[] = [...additionalFornecedors, ...fornecedorCollection];
      jest.spyOn(fornecedorService, 'addFornecedorToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ propostaFinanceira });
      comp.ngOnInit();

      expect(fornecedorService.query).toHaveBeenCalled();
      expect(fornecedorService.addFornecedorToCollectionIfMissing).toHaveBeenCalledWith(fornecedorCollection, ...additionalFornecedors);
      expect(comp.fornecedorsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Aquisicao query and add missing value', () => {
      const propostaFinanceira: IPropostaFinanceira = { id: 456 };
      const aquisicao: IAquisicao = { id: 99351 };
      propostaFinanceira.aquisicao = aquisicao;

      const aquisicaoCollection: IAquisicao[] = [{ id: 82368 }];
      jest.spyOn(aquisicaoService, 'query').mockReturnValue(of(new HttpResponse({ body: aquisicaoCollection })));
      const additionalAquisicaos = [aquisicao];
      const expectedCollection: IAquisicao[] = [...additionalAquisicaos, ...aquisicaoCollection];
      jest.spyOn(aquisicaoService, 'addAquisicaoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ propostaFinanceira });
      comp.ngOnInit();

      expect(aquisicaoService.query).toHaveBeenCalled();
      expect(aquisicaoService.addAquisicaoToCollectionIfMissing).toHaveBeenCalledWith(aquisicaoCollection, ...additionalAquisicaos);
      expect(comp.aquisicaosSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const propostaFinanceira: IPropostaFinanceira = { id: 456 };
      const fornecedor: IFornecedor = { id: 67071 };
      propostaFinanceira.fornecedor = fornecedor;
      const aquisicao: IAquisicao = { id: 97535 };
      propostaFinanceira.aquisicao = aquisicao;

      activatedRoute.data = of({ propostaFinanceira });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(propostaFinanceira));
      expect(comp.fornecedorsSharedCollection).toContain(fornecedor);
      expect(comp.aquisicaosSharedCollection).toContain(aquisicao);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PropostaFinanceira>>();
      const propostaFinanceira = { id: 123 };
      jest.spyOn(propostaFinanceiraService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ propostaFinanceira });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: propostaFinanceira }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(propostaFinanceiraService.update).toHaveBeenCalledWith(propostaFinanceira);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PropostaFinanceira>>();
      const propostaFinanceira = new PropostaFinanceira();
      jest.spyOn(propostaFinanceiraService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ propostaFinanceira });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: propostaFinanceira }));
      saveSubject.complete();

      // THEN
      expect(propostaFinanceiraService.create).toHaveBeenCalledWith(propostaFinanceira);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PropostaFinanceira>>();
      const propostaFinanceira = { id: 123 };
      jest.spyOn(propostaFinanceiraService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ propostaFinanceira });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(propostaFinanceiraService.update).toHaveBeenCalledWith(propostaFinanceira);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackFornecedorById', () => {
      it('Should return tracked Fornecedor primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackFornecedorById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackAquisicaoById', () => {
      it('Should return tracked Aquisicao primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackAquisicaoById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
