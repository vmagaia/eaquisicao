import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { FaseAquisicaoComponent } from './list/fase-aquisicao.component';
import { FaseAquisicaoDetailComponent } from './detail/fase-aquisicao-detail.component';
import { FaseAquisicaoUpdateComponent } from './update/fase-aquisicao-update.component';
import { FaseAquisicaoDeleteDialogComponent } from './delete/fase-aquisicao-delete-dialog.component';
import { FaseAquisicaoRoutingModule } from './route/fase-aquisicao-routing.module';

@NgModule({
  imports: [SharedModule, FaseAquisicaoRoutingModule],
  declarations: [FaseAquisicaoComponent, FaseAquisicaoDetailComponent, FaseAquisicaoUpdateComponent, FaseAquisicaoDeleteDialogComponent],
  entryComponents: [FaseAquisicaoDeleteDialogComponent],
})
export class FaseAquisicaoModule {}
