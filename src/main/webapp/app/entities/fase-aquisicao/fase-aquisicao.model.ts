import * as dayjs from 'dayjs';
import { IAquisicao } from 'app/entities/aquisicao/aquisicao.model';

export interface IFaseAquisicao {
  id?: number;
  nome?: string;
  diasExecucao?: number;
  sequencia?: number;
  createdAt?: dayjs.Dayjs | null;
  createdBy?: string | null;
  lastModificationAt?: dayjs.Dayjs | null;
  lastModificationBy?: string | null;
  aquisicaos?: IAquisicao[] | null;
}

export class FaseAquisicao implements IFaseAquisicao {
  constructor(
    public id?: number,
    public nome?: string,
    public diasExecucao?: number,
    public sequencia?: number,
    public createdAt?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public lastModificationAt?: dayjs.Dayjs | null,
    public lastModificationBy?: string | null,
    public aquisicaos?: IAquisicao[] | null
  ) {}
}

export function getFaseAquisicaoIdentifier(faseAquisicao: IFaseAquisicao): number | undefined {
  return faseAquisicao.id;
}
