import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFaseAquisicao } from '../fase-aquisicao.model';

@Component({
  selector: 'jhi-fase-aquisicao-detail',
  templateUrl: './fase-aquisicao-detail.component.html',
})
export class FaseAquisicaoDetailComponent implements OnInit {
  faseAquisicao: IFaseAquisicao | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ faseAquisicao }) => {
      this.faseAquisicao = faseAquisicao;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
