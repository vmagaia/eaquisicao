import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FaseAquisicaoDetailComponent } from './fase-aquisicao-detail.component';

describe('FaseAquisicao Management Detail Component', () => {
  let comp: FaseAquisicaoDetailComponent;
  let fixture: ComponentFixture<FaseAquisicaoDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FaseAquisicaoDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ faseAquisicao: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(FaseAquisicaoDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(FaseAquisicaoDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load faseAquisicao on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.faseAquisicao).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
