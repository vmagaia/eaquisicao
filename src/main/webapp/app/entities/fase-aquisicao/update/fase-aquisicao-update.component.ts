import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IFaseAquisicao, FaseAquisicao } from '../fase-aquisicao.model';
import { FaseAquisicaoService } from '../service/fase-aquisicao.service';

@Component({
  selector: 'jhi-fase-aquisicao-update',
  templateUrl: './fase-aquisicao-update.component.html',
})
export class FaseAquisicaoUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nome: [null, [Validators.required]],
    diasExecucao: [null, [Validators.required]],
    sequencia: [null, [Validators.required]],
    createdAt: [],
    createdBy: [],
    lastModificationAt: [],
    lastModificationBy: [],
  });

  constructor(protected faseAquisicaoService: FaseAquisicaoService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ faseAquisicao }) => {
      if (faseAquisicao.id === undefined) {
        const today = dayjs().startOf('day');
        faseAquisicao.createdAt = today;
        faseAquisicao.lastModificationAt = today;
      }

      this.updateForm(faseAquisicao);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const faseAquisicao = this.createFromForm();
    if (faseAquisicao.id !== undefined) {
      this.subscribeToSaveResponse(this.faseAquisicaoService.update(faseAquisicao));
    } else {
      this.subscribeToSaveResponse(this.faseAquisicaoService.create(faseAquisicao));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFaseAquisicao>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(faseAquisicao: IFaseAquisicao): void {
    this.editForm.patchValue({
      id: faseAquisicao.id,
      nome: faseAquisicao.nome,
      diasExecucao: faseAquisicao.diasExecucao,
      sequencia: faseAquisicao.sequencia,
      createdAt: faseAquisicao.createdAt ? faseAquisicao.createdAt.format(DATE_TIME_FORMAT) : null,
      createdBy: faseAquisicao.createdBy,
      lastModificationAt: faseAquisicao.lastModificationAt ? faseAquisicao.lastModificationAt.format(DATE_TIME_FORMAT) : null,
      lastModificationBy: faseAquisicao.lastModificationBy,
    });
  }

  protected createFromForm(): IFaseAquisicao {
    return {
      ...new FaseAquisicao(),
      id: this.editForm.get(['id'])!.value,
      nome: this.editForm.get(['nome'])!.value,
      diasExecucao: this.editForm.get(['diasExecucao'])!.value,
      sequencia: this.editForm.get(['sequencia'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? dayjs(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      lastModificationAt: this.editForm.get(['lastModificationAt'])!.value
        ? dayjs(this.editForm.get(['lastModificationAt'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastModificationBy: this.editForm.get(['lastModificationBy'])!.value,
    };
  }
}
