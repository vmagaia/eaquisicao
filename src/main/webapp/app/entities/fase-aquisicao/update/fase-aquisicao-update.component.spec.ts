jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { FaseAquisicaoService } from '../service/fase-aquisicao.service';
import { IFaseAquisicao, FaseAquisicao } from '../fase-aquisicao.model';

import { FaseAquisicaoUpdateComponent } from './fase-aquisicao-update.component';

describe('FaseAquisicao Management Update Component', () => {
  let comp: FaseAquisicaoUpdateComponent;
  let fixture: ComponentFixture<FaseAquisicaoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let faseAquisicaoService: FaseAquisicaoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [FaseAquisicaoUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(FaseAquisicaoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(FaseAquisicaoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    faseAquisicaoService = TestBed.inject(FaseAquisicaoService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const faseAquisicao: IFaseAquisicao = { id: 456 };

      activatedRoute.data = of({ faseAquisicao });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(faseAquisicao));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<FaseAquisicao>>();
      const faseAquisicao = { id: 123 };
      jest.spyOn(faseAquisicaoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ faseAquisicao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: faseAquisicao }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(faseAquisicaoService.update).toHaveBeenCalledWith(faseAquisicao);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<FaseAquisicao>>();
      const faseAquisicao = new FaseAquisicao();
      jest.spyOn(faseAquisicaoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ faseAquisicao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: faseAquisicao }));
      saveSubject.complete();

      // THEN
      expect(faseAquisicaoService.create).toHaveBeenCalledWith(faseAquisicao);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<FaseAquisicao>>();
      const faseAquisicao = { id: 123 };
      jest.spyOn(faseAquisicaoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ faseAquisicao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(faseAquisicaoService.update).toHaveBeenCalledWith(faseAquisicao);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
