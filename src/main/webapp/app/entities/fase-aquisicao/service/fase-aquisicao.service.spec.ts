import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IFaseAquisicao, FaseAquisicao } from '../fase-aquisicao.model';

import { FaseAquisicaoService } from './fase-aquisicao.service';

describe('FaseAquisicao Service', () => {
  let service: FaseAquisicaoService;
  let httpMock: HttpTestingController;
  let elemDefault: IFaseAquisicao;
  let expectedResult: IFaseAquisicao | IFaseAquisicao[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(FaseAquisicaoService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      nome: 'AAAAAAA',
      diasExecucao: 0,
      sequencia: 0,
      createdAt: currentDate,
      createdBy: 'AAAAAAA',
      lastModificationAt: currentDate,
      lastModificationBy: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a FaseAquisicao', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.create(new FaseAquisicao()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a FaseAquisicao', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nome: 'BBBBBB',
          diasExecucao: 1,
          sequencia: 1,
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationBy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a FaseAquisicao', () => {
      const patchObject = Object.assign(
        {
          nome: 'BBBBBB',
          diasExecucao: 1,
          createdBy: 'BBBBBB',
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
        },
        new FaseAquisicao()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of FaseAquisicao', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nome: 'BBBBBB',
          diasExecucao: 1,
          sequencia: 1,
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationBy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a FaseAquisicao', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addFaseAquisicaoToCollectionIfMissing', () => {
      it('should add a FaseAquisicao to an empty array', () => {
        const faseAquisicao: IFaseAquisicao = { id: 123 };
        expectedResult = service.addFaseAquisicaoToCollectionIfMissing([], faseAquisicao);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(faseAquisicao);
      });

      it('should not add a FaseAquisicao to an array that contains it', () => {
        const faseAquisicao: IFaseAquisicao = { id: 123 };
        const faseAquisicaoCollection: IFaseAquisicao[] = [
          {
            ...faseAquisicao,
          },
          { id: 456 },
        ];
        expectedResult = service.addFaseAquisicaoToCollectionIfMissing(faseAquisicaoCollection, faseAquisicao);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a FaseAquisicao to an array that doesn't contain it", () => {
        const faseAquisicao: IFaseAquisicao = { id: 123 };
        const faseAquisicaoCollection: IFaseAquisicao[] = [{ id: 456 }];
        expectedResult = service.addFaseAquisicaoToCollectionIfMissing(faseAquisicaoCollection, faseAquisicao);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(faseAquisicao);
      });

      it('should add only unique FaseAquisicao to an array', () => {
        const faseAquisicaoArray: IFaseAquisicao[] = [{ id: 123 }, { id: 456 }, { id: 64342 }];
        const faseAquisicaoCollection: IFaseAquisicao[] = [{ id: 123 }];
        expectedResult = service.addFaseAquisicaoToCollectionIfMissing(faseAquisicaoCollection, ...faseAquisicaoArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const faseAquisicao: IFaseAquisicao = { id: 123 };
        const faseAquisicao2: IFaseAquisicao = { id: 456 };
        expectedResult = service.addFaseAquisicaoToCollectionIfMissing([], faseAquisicao, faseAquisicao2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(faseAquisicao);
        expect(expectedResult).toContain(faseAquisicao2);
      });

      it('should accept null and undefined values', () => {
        const faseAquisicao: IFaseAquisicao = { id: 123 };
        expectedResult = service.addFaseAquisicaoToCollectionIfMissing([], null, faseAquisicao, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(faseAquisicao);
      });

      it('should return initial array if no FaseAquisicao is added', () => {
        const faseAquisicaoCollection: IFaseAquisicao[] = [{ id: 123 }];
        expectedResult = service.addFaseAquisicaoToCollectionIfMissing(faseAquisicaoCollection, undefined, null);
        expect(expectedResult).toEqual(faseAquisicaoCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
