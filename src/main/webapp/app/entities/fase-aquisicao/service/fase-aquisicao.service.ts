import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IFaseAquisicao, getFaseAquisicaoIdentifier } from '../fase-aquisicao.model';

export type EntityResponseType = HttpResponse<IFaseAquisicao>;
export type EntityArrayResponseType = HttpResponse<IFaseAquisicao[]>;

@Injectable({ providedIn: 'root' })
export class FaseAquisicaoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/fase-aquisicaos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(faseAquisicao: IFaseAquisicao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(faseAquisicao);
    return this.http
      .post<IFaseAquisicao>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(faseAquisicao: IFaseAquisicao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(faseAquisicao);
    return this.http
      .put<IFaseAquisicao>(`${this.resourceUrl}/${getFaseAquisicaoIdentifier(faseAquisicao) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(faseAquisicao: IFaseAquisicao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(faseAquisicao);
    return this.http
      .patch<IFaseAquisicao>(`${this.resourceUrl}/${getFaseAquisicaoIdentifier(faseAquisicao) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IFaseAquisicao>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IFaseAquisicao[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addFaseAquisicaoToCollectionIfMissing(
    faseAquisicaoCollection: IFaseAquisicao[],
    ...faseAquisicaosToCheck: (IFaseAquisicao | null | undefined)[]
  ): IFaseAquisicao[] {
    const faseAquisicaos: IFaseAquisicao[] = faseAquisicaosToCheck.filter(isPresent);
    if (faseAquisicaos.length > 0) {
      const faseAquisicaoCollectionIdentifiers = faseAquisicaoCollection.map(
        faseAquisicaoItem => getFaseAquisicaoIdentifier(faseAquisicaoItem)!
      );
      const faseAquisicaosToAdd = faseAquisicaos.filter(faseAquisicaoItem => {
        const faseAquisicaoIdentifier = getFaseAquisicaoIdentifier(faseAquisicaoItem);
        if (faseAquisicaoIdentifier == null || faseAquisicaoCollectionIdentifiers.includes(faseAquisicaoIdentifier)) {
          return false;
        }
        faseAquisicaoCollectionIdentifiers.push(faseAquisicaoIdentifier);
        return true;
      });
      return [...faseAquisicaosToAdd, ...faseAquisicaoCollection];
    }
    return faseAquisicaoCollection;
  }

  protected convertDateFromClient(faseAquisicao: IFaseAquisicao): IFaseAquisicao {
    return Object.assign({}, faseAquisicao, {
      createdAt: faseAquisicao.createdAt?.isValid() ? faseAquisicao.createdAt.toJSON() : undefined,
      lastModificationAt: faseAquisicao.lastModificationAt?.isValid() ? faseAquisicao.lastModificationAt.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? dayjs(res.body.createdAt) : undefined;
      res.body.lastModificationAt = res.body.lastModificationAt ? dayjs(res.body.lastModificationAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((faseAquisicao: IFaseAquisicao) => {
        faseAquisicao.createdAt = faseAquisicao.createdAt ? dayjs(faseAquisicao.createdAt) : undefined;
        faseAquisicao.lastModificationAt = faseAquisicao.lastModificationAt ? dayjs(faseAquisicao.lastModificationAt) : undefined;
      });
    }
    return res;
  }
}
