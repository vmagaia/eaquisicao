jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IFaseAquisicao, FaseAquisicao } from '../fase-aquisicao.model';
import { FaseAquisicaoService } from '../service/fase-aquisicao.service';

import { FaseAquisicaoRoutingResolveService } from './fase-aquisicao-routing-resolve.service';

describe('FaseAquisicao routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: FaseAquisicaoRoutingResolveService;
  let service: FaseAquisicaoService;
  let resultFaseAquisicao: IFaseAquisicao | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(FaseAquisicaoRoutingResolveService);
    service = TestBed.inject(FaseAquisicaoService);
    resultFaseAquisicao = undefined;
  });

  describe('resolve', () => {
    it('should return IFaseAquisicao returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultFaseAquisicao = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultFaseAquisicao).toEqual({ id: 123 });
    });

    it('should return new IFaseAquisicao if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultFaseAquisicao = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultFaseAquisicao).toEqual(new FaseAquisicao());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as FaseAquisicao })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultFaseAquisicao = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultFaseAquisicao).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
