import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { FaseAquisicaoComponent } from '../list/fase-aquisicao.component';
import { FaseAquisicaoDetailComponent } from '../detail/fase-aquisicao-detail.component';
import { FaseAquisicaoUpdateComponent } from '../update/fase-aquisicao-update.component';
import { FaseAquisicaoRoutingResolveService } from './fase-aquisicao-routing-resolve.service';

const faseAquisicaoRoute: Routes = [
  {
    path: '',
    component: FaseAquisicaoComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FaseAquisicaoDetailComponent,
    resolve: {
      faseAquisicao: FaseAquisicaoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FaseAquisicaoUpdateComponent,
    resolve: {
      faseAquisicao: FaseAquisicaoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FaseAquisicaoUpdateComponent,
    resolve: {
      faseAquisicao: FaseAquisicaoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(faseAquisicaoRoute)],
  exports: [RouterModule],
})
export class FaseAquisicaoRoutingModule {}
