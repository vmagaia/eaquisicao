import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IFaseAquisicao } from '../fase-aquisicao.model';
import { FaseAquisicaoService } from '../service/fase-aquisicao.service';

@Component({
  templateUrl: './fase-aquisicao-delete-dialog.component.html',
})
export class FaseAquisicaoDeleteDialogComponent {
  faseAquisicao?: IFaseAquisicao;

  constructor(protected faseAquisicaoService: FaseAquisicaoService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.faseAquisicaoService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
