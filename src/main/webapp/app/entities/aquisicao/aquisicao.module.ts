import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AquisicaoComponent } from './list/aquisicao.component';
import { AquisicaoDetailComponent } from './detail/aquisicao-detail.component';
import { AquisicaoUpdateComponent } from './update/aquisicao-update.component';
import { AquisicaoDeleteDialogComponent } from './delete/aquisicao-delete-dialog.component';
import { AquisicaoRoutingModule } from './route/aquisicao-routing.module';

@NgModule({
  imports: [SharedModule, AquisicaoRoutingModule],
  declarations: [AquisicaoComponent, AquisicaoDetailComponent, AquisicaoUpdateComponent, AquisicaoDeleteDialogComponent],
  entryComponents: [AquisicaoDeleteDialogComponent],
  exports: [AquisicaoComponent],
})
export class AquisicaoModule {}
