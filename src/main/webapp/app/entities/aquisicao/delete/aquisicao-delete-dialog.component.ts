import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAquisicao } from '../aquisicao.model';
import { AquisicaoService } from '../service/aquisicao.service';

@Component({
  templateUrl: './aquisicao-delete-dialog.component.html',
})
export class AquisicaoDeleteDialogComponent {
  aquisicao?: IAquisicao;

  constructor(protected aquisicaoService: AquisicaoService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.aquisicaoService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
