import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IAquisicao, Aquisicao } from '../aquisicao.model';

import { AquisicaoService } from './aquisicao.service';

describe('Aquisicao Service', () => {
  let service: AquisicaoService;
  let httpMock: HttpTestingController;
  let elemDefault: IAquisicao;
  let expectedResult: IAquisicao | IAquisicao[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(AquisicaoService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      descricao: 'AAAAAAA',
      valorPrevisto: 0,
      createdAt: currentDate,
      createdBy: 'AAAAAAA',
      lastModificationAt: currentDate,
      lastModificationBy: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Aquisicao', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.create(new Aquisicao()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Aquisicao', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          descricao: 'BBBBBB',
          valorPrevisto: 1,
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationBy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Aquisicao', () => {
      const patchObject = Object.assign(
        {
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
        },
        new Aquisicao()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Aquisicao', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          descricao: 'BBBBBB',
          valorPrevisto: 1,
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationBy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Aquisicao', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addAquisicaoToCollectionIfMissing', () => {
      it('should add a Aquisicao to an empty array', () => {
        const aquisicao: IAquisicao = { id: 123 };
        expectedResult = service.addAquisicaoToCollectionIfMissing([], aquisicao);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(aquisicao);
      });

      it('should not add a Aquisicao to an array that contains it', () => {
        const aquisicao: IAquisicao = { id: 123 };
        const aquisicaoCollection: IAquisicao[] = [
          {
            ...aquisicao,
          },
          { id: 456 },
        ];
        expectedResult = service.addAquisicaoToCollectionIfMissing(aquisicaoCollection, aquisicao);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Aquisicao to an array that doesn't contain it", () => {
        const aquisicao: IAquisicao = { id: 123 };
        const aquisicaoCollection: IAquisicao[] = [{ id: 456 }];
        expectedResult = service.addAquisicaoToCollectionIfMissing(aquisicaoCollection, aquisicao);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(aquisicao);
      });

      it('should add only unique Aquisicao to an array', () => {
        const aquisicaoArray: IAquisicao[] = [{ id: 123 }, { id: 456 }, { id: 44711 }];
        const aquisicaoCollection: IAquisicao[] = [{ id: 123 }];
        expectedResult = service.addAquisicaoToCollectionIfMissing(aquisicaoCollection, ...aquisicaoArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const aquisicao: IAquisicao = { id: 123 };
        const aquisicao2: IAquisicao = { id: 456 };
        expectedResult = service.addAquisicaoToCollectionIfMissing([], aquisicao, aquisicao2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(aquisicao);
        expect(expectedResult).toContain(aquisicao2);
      });

      it('should accept null and undefined values', () => {
        const aquisicao: IAquisicao = { id: 123 };
        expectedResult = service.addAquisicaoToCollectionIfMissing([], null, aquisicao, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(aquisicao);
      });

      it('should return initial array if no Aquisicao is added', () => {
        const aquisicaoCollection: IAquisicao[] = [{ id: 123 }];
        expectedResult = service.addAquisicaoToCollectionIfMissing(aquisicaoCollection, undefined, null);
        expect(expectedResult).toEqual(aquisicaoCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
