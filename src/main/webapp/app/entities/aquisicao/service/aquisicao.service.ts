import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAquisicao, getAquisicaoIdentifier } from '../aquisicao.model';

export type EntityResponseType = HttpResponse<IAquisicao>;
export type EntityArrayResponseType = HttpResponse<IAquisicao[]>;

@Injectable({ providedIn: 'root' })
export class AquisicaoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/aquisicaos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(aquisicao: IAquisicao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(aquisicao);
    return this.http
      .post<IAquisicao>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(aquisicao: IAquisicao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(aquisicao);
    return this.http
      .put<IAquisicao>(`${this.resourceUrl}/${getAquisicaoIdentifier(aquisicao) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(aquisicao: IAquisicao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(aquisicao);
    return this.http
      .patch<IAquisicao>(`${this.resourceUrl}/${getAquisicaoIdentifier(aquisicao) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAquisicao>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAquisicao[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addAquisicaoToCollectionIfMissing(
    aquisicaoCollection: IAquisicao[],
    ...aquisicaosToCheck: (IAquisicao | null | undefined)[]
  ): IAquisicao[] {
    const aquisicaos: IAquisicao[] = aquisicaosToCheck.filter(isPresent);
    if (aquisicaos.length > 0) {
      const aquisicaoCollectionIdentifiers = aquisicaoCollection.map(aquisicaoItem => getAquisicaoIdentifier(aquisicaoItem)!);
      const aquisicaosToAdd = aquisicaos.filter(aquisicaoItem => {
        const aquisicaoIdentifier = getAquisicaoIdentifier(aquisicaoItem);
        if (aquisicaoIdentifier == null || aquisicaoCollectionIdentifiers.includes(aquisicaoIdentifier)) {
          return false;
        }
        aquisicaoCollectionIdentifiers.push(aquisicaoIdentifier);
        return true;
      });
      return [...aquisicaosToAdd, ...aquisicaoCollection];
    }
    return aquisicaoCollection;
  }

  protected convertDateFromClient(aquisicao: IAquisicao): IAquisicao {
    return Object.assign({}, aquisicao, {
      createdAt: aquisicao.createdAt?.isValid() ? aquisicao.createdAt.toJSON() : undefined,
      lastModificationAt: aquisicao.lastModificationAt?.isValid() ? aquisicao.lastModificationAt.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? dayjs(res.body.createdAt) : undefined;
      res.body.lastModificationAt = res.body.lastModificationAt ? dayjs(res.body.lastModificationAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((aquisicao: IAquisicao) => {
        aquisicao.createdAt = aquisicao.createdAt ? dayjs(aquisicao.createdAt) : undefined;
        aquisicao.lastModificationAt = aquisicao.lastModificationAt ? dayjs(aquisicao.lastModificationAt) : undefined;
      });
    }
    return res;
  }
}
