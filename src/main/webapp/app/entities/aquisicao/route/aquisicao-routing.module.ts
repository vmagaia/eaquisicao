import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { AquisicaoComponent } from '../list/aquisicao.component';
import { AquisicaoDetailComponent } from '../detail/aquisicao-detail.component';
import { AquisicaoUpdateComponent } from '../update/aquisicao-update.component';
import { AquisicaoRoutingResolveService } from './aquisicao-routing-resolve.service';

const aquisicaoRoute: Routes = [
  {
    path: '',
    component: AquisicaoComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AquisicaoDetailComponent,
    resolve: {
      aquisicao: AquisicaoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AquisicaoUpdateComponent,
    resolve: {
      aquisicao: AquisicaoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AquisicaoUpdateComponent,
    resolve: {
      aquisicao: AquisicaoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(aquisicaoRoute)],
  exports: [RouterModule],
})
export class AquisicaoRoutingModule {}
