import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAquisicao, Aquisicao } from '../aquisicao.model';
import { AquisicaoService } from '../service/aquisicao.service';

@Injectable({ providedIn: 'root' })
export class AquisicaoRoutingResolveService implements Resolve<IAquisicao> {
  constructor(protected service: AquisicaoService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAquisicao> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((aquisicao: HttpResponse<Aquisicao>) => {
          if (aquisicao.body) {
            return of(aquisicao.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Aquisicao());
  }
}
