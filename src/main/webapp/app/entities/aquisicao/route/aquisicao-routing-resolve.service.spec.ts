jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IAquisicao, Aquisicao } from '../aquisicao.model';
import { AquisicaoService } from '../service/aquisicao.service';

import { AquisicaoRoutingResolveService } from './aquisicao-routing-resolve.service';

describe('Aquisicao routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: AquisicaoRoutingResolveService;
  let service: AquisicaoService;
  let resultAquisicao: IAquisicao | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(AquisicaoRoutingResolveService);
    service = TestBed.inject(AquisicaoService);
    resultAquisicao = undefined;
  });

  describe('resolve', () => {
    it('should return IAquisicao returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultAquisicao = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultAquisicao).toEqual({ id: 123 });
    });

    it('should return new IAquisicao if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultAquisicao = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultAquisicao).toEqual(new Aquisicao());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Aquisicao })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultAquisicao = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultAquisicao).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
