import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAquisicao } from '../aquisicao.model';

@Component({
  selector: 'jhi-aquisicao-detail',
  templateUrl: './aquisicao-detail.component.html',
})
export class AquisicaoDetailComponent implements OnInit {
  aquisicao: IAquisicao | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ aquisicao }) => {
      this.aquisicao = aquisicao;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
