import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AquisicaoDetailComponent } from './aquisicao-detail.component';

describe('Aquisicao Management Detail Component', () => {
  let comp: AquisicaoDetailComponent;
  let fixture: ComponentFixture<AquisicaoDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AquisicaoDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ aquisicao: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(AquisicaoDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(AquisicaoDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load aquisicao on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.aquisicao).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
