import * as dayjs from 'dayjs';
import { IContrato } from 'app/entities/contrato/contrato.model';
import { IPropostaFinanceira } from 'app/entities/proposta-financeira/proposta-financeira.model';
import { ICategoria } from 'app/entities/categoria/categoria.model';
import { IFaseAquisicao } from 'app/entities/fase-aquisicao/fase-aquisicao.model';
import { ILinhaOrcamento } from 'app/entities/linha-orcamento/linha-orcamento.model';
import { IModalidadeContratacao } from 'app/entities/modalidade-contratacao/modalidade-contratacao.model';

export interface IAquisicao {
  id?: number;
  descricao?: string | null;
  valorPrevisto?: number;
  createdAt?: dayjs.Dayjs | null;
  createdBy?: string | null;
  lastModificationAt?: dayjs.Dayjs | null;
  lastModificationBy?: string | null;
  contrato?: IContrato | null;
  propostaFinanceiras?: IPropostaFinanceira[] | null;
  categoria?: ICategoria;
  faseAquisicao?: IFaseAquisicao;
  linhaOrcamento?: ILinhaOrcamento;
  modalidadeContratacao?: IModalidadeContratacao;
}

export class Aquisicao implements IAquisicao {
  constructor(
    public id?: number,
    public descricao?: string | null,
    public valorPrevisto?: number,
    public createdAt?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public lastModificationAt?: dayjs.Dayjs | null,
    public lastModificationBy?: string | null,
    public contrato?: IContrato | null,
    public propostaFinanceiras?: IPropostaFinanceira[] | null,
    public categoria?: ICategoria,
    public faseAquisicao?: IFaseAquisicao,
    public linhaOrcamento?: ILinhaOrcamento,
    public modalidadeContratacao?: IModalidadeContratacao
  ) {}
}

export function getAquisicaoIdentifier(aquisicao: IAquisicao): number | undefined {
  return aquisicao.id;
}
