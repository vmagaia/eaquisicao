import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IAquisicao, Aquisicao } from '../aquisicao.model';
import { AquisicaoService } from '../service/aquisicao.service';
import { IContrato } from 'app/entities/contrato/contrato.model';
import { ContratoService } from 'app/entities/contrato/service/contrato.service';
import { ICategoria } from 'app/entities/categoria/categoria.model';
import { CategoriaService } from 'app/entities/categoria/service/categoria.service';
import { IFaseAquisicao } from 'app/entities/fase-aquisicao/fase-aquisicao.model';
import { FaseAquisicaoService } from 'app/entities/fase-aquisicao/service/fase-aquisicao.service';
import { ILinhaOrcamento } from 'app/entities/linha-orcamento/linha-orcamento.model';
import { LinhaOrcamentoService } from 'app/entities/linha-orcamento/service/linha-orcamento.service';
import { IModalidadeContratacao } from 'app/entities/modalidade-contratacao/modalidade-contratacao.model';
import { ModalidadeContratacaoService } from 'app/entities/modalidade-contratacao/service/modalidade-contratacao.service';

@Component({
  selector: 'jhi-aquisicao-update',
  templateUrl: './aquisicao-update.component.html',
})
export class AquisicaoUpdateComponent implements OnInit {
  isSaving = false;

  contratoesCollection: IContrato[] = [];
  categoriasSharedCollection: ICategoria[] = [];
  faseAquisicaosSharedCollection: IFaseAquisicao[] = [];
  linhaOrcamentosSharedCollection: ILinhaOrcamento[] = [];
  modalidadeContratacaosSharedCollection: IModalidadeContratacao[] = [];

  editForm = this.fb.group({
    id: [],
    descricao: [],
    valorPrevisto: [null, [Validators.required, Validators.min(0)]],
    createdAt: [],
    createdBy: [],
    lastModificationAt: [],
    lastModificationBy: [],
    contrato: [],
    categoria: [null, Validators.required],
    faseAquisicao: [null, Validators.required],
    linhaOrcamento: [null, Validators.required],
    modalidadeContratacao: [null, Validators.required],
  });

  constructor(
    protected aquisicaoService: AquisicaoService,
    protected contratoService: ContratoService,
    protected categoriaService: CategoriaService,
    protected faseAquisicaoService: FaseAquisicaoService,
    protected linhaOrcamentoService: LinhaOrcamentoService,
    protected modalidadeContratacaoService: ModalidadeContratacaoService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ aquisicao }) => {
      if (aquisicao.id === undefined) {
        const today = dayjs().startOf('day');
        aquisicao.createdAt = today;
        aquisicao.lastModificationAt = today;
      }

      this.updateForm(aquisicao);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const aquisicao = this.createFromForm();
    if (aquisicao.id !== undefined) {
      this.subscribeToSaveResponse(this.aquisicaoService.update(aquisicao));
    } else {
      this.subscribeToSaveResponse(this.aquisicaoService.create(aquisicao));
    }
  }

  trackContratoById(index: number, item: IContrato): number {
    return item.id!;
  }

  trackCategoriaById(index: number, item: ICategoria): number {
    return item.id!;
  }

  trackFaseAquisicaoById(index: number, item: IFaseAquisicao): number {
    return item.id!;
  }

  trackLinhaOrcamentoById(index: number, item: ILinhaOrcamento): number {
    return item.id!;
  }

  trackModalidadeContratacaoById(index: number, item: IModalidadeContratacao): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAquisicao>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(aquisicao: IAquisicao): void {
    this.editForm.patchValue({
      id: aquisicao.id,
      descricao: aquisicao.descricao,
      valorPrevisto: aquisicao.valorPrevisto,
      createdAt: aquisicao.createdAt ? aquisicao.createdAt.format(DATE_TIME_FORMAT) : null,
      createdBy: aquisicao.createdBy,
      lastModificationAt: aquisicao.lastModificationAt ? aquisicao.lastModificationAt.format(DATE_TIME_FORMAT) : null,
      lastModificationBy: aquisicao.lastModificationBy,
      contrato: aquisicao.contrato,
      categoria: aquisicao.categoria,
      faseAquisicao: aquisicao.faseAquisicao,
      linhaOrcamento: aquisicao.linhaOrcamento,
      modalidadeContratacao: aquisicao.modalidadeContratacao,
    });

    this.contratoesCollection = this.contratoService.addContratoToCollectionIfMissing(this.contratoesCollection, aquisicao.contrato);
    this.categoriasSharedCollection = this.categoriaService.addCategoriaToCollectionIfMissing(
      this.categoriasSharedCollection,
      aquisicao.categoria
    );
    this.faseAquisicaosSharedCollection = this.faseAquisicaoService.addFaseAquisicaoToCollectionIfMissing(
      this.faseAquisicaosSharedCollection,
      aquisicao.faseAquisicao
    );
    this.linhaOrcamentosSharedCollection = this.linhaOrcamentoService.addLinhaOrcamentoToCollectionIfMissing(
      this.linhaOrcamentosSharedCollection,
      aquisicao.linhaOrcamento
    );
    this.modalidadeContratacaosSharedCollection = this.modalidadeContratacaoService.addModalidadeContratacaoToCollectionIfMissing(
      this.modalidadeContratacaosSharedCollection,
      aquisicao.modalidadeContratacao
    );
  }

  protected loadRelationshipsOptions(): void {
    this.contratoService
      .query({ 'aquisicaoId.specified': 'false' })
      .pipe(map((res: HttpResponse<IContrato[]>) => res.body ?? []))
      .pipe(
        map((contratoes: IContrato[]) =>
          this.contratoService.addContratoToCollectionIfMissing(contratoes, this.editForm.get('contrato')!.value)
        )
      )
      .subscribe((contratoes: IContrato[]) => (this.contratoesCollection = contratoes));

    this.categoriaService
      .query()
      .pipe(map((res: HttpResponse<ICategoria[]>) => res.body ?? []))
      .pipe(
        map((categorias: ICategoria[]) =>
          this.categoriaService.addCategoriaToCollectionIfMissing(categorias, this.editForm.get('categoria')!.value)
        )
      )
      .subscribe((categorias: ICategoria[]) => (this.categoriasSharedCollection = categorias));

    this.faseAquisicaoService
      .query()
      .pipe(map((res: HttpResponse<IFaseAquisicao[]>) => res.body ?? []))
      .pipe(
        map((faseAquisicaos: IFaseAquisicao[]) =>
          this.faseAquisicaoService.addFaseAquisicaoToCollectionIfMissing(faseAquisicaos, this.editForm.get('faseAquisicao')!.value)
        )
      )
      .subscribe((faseAquisicaos: IFaseAquisicao[]) => (this.faseAquisicaosSharedCollection = faseAquisicaos));

    this.linhaOrcamentoService
      .query()
      .pipe(map((res: HttpResponse<ILinhaOrcamento[]>) => res.body ?? []))
      .pipe(
        map((linhaOrcamentos: ILinhaOrcamento[]) =>
          this.linhaOrcamentoService.addLinhaOrcamentoToCollectionIfMissing(linhaOrcamentos, this.editForm.get('linhaOrcamento')!.value)
        )
      )
      .subscribe((linhaOrcamentos: ILinhaOrcamento[]) => (this.linhaOrcamentosSharedCollection = linhaOrcamentos));

    this.modalidadeContratacaoService
      .query()
      .pipe(map((res: HttpResponse<IModalidadeContratacao[]>) => res.body ?? []))
      .pipe(
        map((modalidadeContratacaos: IModalidadeContratacao[]) =>
          this.modalidadeContratacaoService.addModalidadeContratacaoToCollectionIfMissing(
            modalidadeContratacaos,
            this.editForm.get('modalidadeContratacao')!.value
          )
        )
      )
      .subscribe(
        (modalidadeContratacaos: IModalidadeContratacao[]) => (this.modalidadeContratacaosSharedCollection = modalidadeContratacaos)
      );
  }

  protected createFromForm(): IAquisicao {
    return {
      ...new Aquisicao(),
      id: this.editForm.get(['id'])!.value,
      descricao: this.editForm.get(['descricao'])!.value,
      valorPrevisto: this.editForm.get(['valorPrevisto'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? dayjs(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      lastModificationAt: this.editForm.get(['lastModificationAt'])!.value
        ? dayjs(this.editForm.get(['lastModificationAt'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastModificationBy: this.editForm.get(['lastModificationBy'])!.value,
      contrato: this.editForm.get(['contrato'])!.value,
      categoria: this.editForm.get(['categoria'])!.value,
      faseAquisicao: this.editForm.get(['faseAquisicao'])!.value,
      linhaOrcamento: this.editForm.get(['linhaOrcamento'])!.value,
      modalidadeContratacao: this.editForm.get(['modalidadeContratacao'])!.value,
    };
  }
}
