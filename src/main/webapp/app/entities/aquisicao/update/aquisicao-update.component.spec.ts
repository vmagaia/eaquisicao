jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { AquisicaoService } from '../service/aquisicao.service';
import { IAquisicao, Aquisicao } from '../aquisicao.model';
import { IContrato } from 'app/entities/contrato/contrato.model';
import { ContratoService } from 'app/entities/contrato/service/contrato.service';
import { ICategoria } from 'app/entities/categoria/categoria.model';
import { CategoriaService } from 'app/entities/categoria/service/categoria.service';
import { IFaseAquisicao } from 'app/entities/fase-aquisicao/fase-aquisicao.model';
import { FaseAquisicaoService } from 'app/entities/fase-aquisicao/service/fase-aquisicao.service';
import { ILinhaOrcamento } from 'app/entities/linha-orcamento/linha-orcamento.model';
import { LinhaOrcamentoService } from 'app/entities/linha-orcamento/service/linha-orcamento.service';
import { IModalidadeContratacao } from 'app/entities/modalidade-contratacao/modalidade-contratacao.model';
import { ModalidadeContratacaoService } from 'app/entities/modalidade-contratacao/service/modalidade-contratacao.service';

import { AquisicaoUpdateComponent } from './aquisicao-update.component';

describe('Aquisicao Management Update Component', () => {
  let comp: AquisicaoUpdateComponent;
  let fixture: ComponentFixture<AquisicaoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let aquisicaoService: AquisicaoService;
  let contratoService: ContratoService;
  let categoriaService: CategoriaService;
  let faseAquisicaoService: FaseAquisicaoService;
  let linhaOrcamentoService: LinhaOrcamentoService;
  let modalidadeContratacaoService: ModalidadeContratacaoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [AquisicaoUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(AquisicaoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AquisicaoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    aquisicaoService = TestBed.inject(AquisicaoService);
    contratoService = TestBed.inject(ContratoService);
    categoriaService = TestBed.inject(CategoriaService);
    faseAquisicaoService = TestBed.inject(FaseAquisicaoService);
    linhaOrcamentoService = TestBed.inject(LinhaOrcamentoService);
    modalidadeContratacaoService = TestBed.inject(ModalidadeContratacaoService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call contrato query and add missing value', () => {
      const aquisicao: IAquisicao = { id: 456 };
      const contrato: IContrato = { id: 60437 };
      aquisicao.contrato = contrato;

      const contratoCollection: IContrato[] = [{ id: 23978 }];
      jest.spyOn(contratoService, 'query').mockReturnValue(of(new HttpResponse({ body: contratoCollection })));
      const expectedCollection: IContrato[] = [contrato, ...contratoCollection];
      jest.spyOn(contratoService, 'addContratoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ aquisicao });
      comp.ngOnInit();

      expect(contratoService.query).toHaveBeenCalled();
      expect(contratoService.addContratoToCollectionIfMissing).toHaveBeenCalledWith(contratoCollection, contrato);
      expect(comp.contratoesCollection).toEqual(expectedCollection);
    });

    it('Should call Categoria query and add missing value', () => {
      const aquisicao: IAquisicao = { id: 456 };
      const categoria: ICategoria = { id: 51031 };
      aquisicao.categoria = categoria;

      const categoriaCollection: ICategoria[] = [{ id: 23410 }];
      jest.spyOn(categoriaService, 'query').mockReturnValue(of(new HttpResponse({ body: categoriaCollection })));
      const additionalCategorias = [categoria];
      const expectedCollection: ICategoria[] = [...additionalCategorias, ...categoriaCollection];
      jest.spyOn(categoriaService, 'addCategoriaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ aquisicao });
      comp.ngOnInit();

      expect(categoriaService.query).toHaveBeenCalled();
      expect(categoriaService.addCategoriaToCollectionIfMissing).toHaveBeenCalledWith(categoriaCollection, ...additionalCategorias);
      expect(comp.categoriasSharedCollection).toEqual(expectedCollection);
    });

    it('Should call FaseAquisicao query and add missing value', () => {
      const aquisicao: IAquisicao = { id: 456 };
      const faseAquisicao: IFaseAquisicao = { id: 25746 };
      aquisicao.faseAquisicao = faseAquisicao;

      const faseAquisicaoCollection: IFaseAquisicao[] = [{ id: 44245 }];
      jest.spyOn(faseAquisicaoService, 'query').mockReturnValue(of(new HttpResponse({ body: faseAquisicaoCollection })));
      const additionalFaseAquisicaos = [faseAquisicao];
      const expectedCollection: IFaseAquisicao[] = [...additionalFaseAquisicaos, ...faseAquisicaoCollection];
      jest.spyOn(faseAquisicaoService, 'addFaseAquisicaoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ aquisicao });
      comp.ngOnInit();

      expect(faseAquisicaoService.query).toHaveBeenCalled();
      expect(faseAquisicaoService.addFaseAquisicaoToCollectionIfMissing).toHaveBeenCalledWith(
        faseAquisicaoCollection,
        ...additionalFaseAquisicaos
      );
      expect(comp.faseAquisicaosSharedCollection).toEqual(expectedCollection);
    });

    it('Should call LinhaOrcamento query and add missing value', () => {
      const aquisicao: IAquisicao = { id: 456 };
      const linhaOrcamento: ILinhaOrcamento = { id: 70795 };
      aquisicao.linhaOrcamento = linhaOrcamento;

      const linhaOrcamentoCollection: ILinhaOrcamento[] = [{ id: 10719 }];
      jest.spyOn(linhaOrcamentoService, 'query').mockReturnValue(of(new HttpResponse({ body: linhaOrcamentoCollection })));
      const additionalLinhaOrcamentos = [linhaOrcamento];
      const expectedCollection: ILinhaOrcamento[] = [...additionalLinhaOrcamentos, ...linhaOrcamentoCollection];
      jest.spyOn(linhaOrcamentoService, 'addLinhaOrcamentoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ aquisicao });
      comp.ngOnInit();

      expect(linhaOrcamentoService.query).toHaveBeenCalled();
      expect(linhaOrcamentoService.addLinhaOrcamentoToCollectionIfMissing).toHaveBeenCalledWith(
        linhaOrcamentoCollection,
        ...additionalLinhaOrcamentos
      );
      expect(comp.linhaOrcamentosSharedCollection).toEqual(expectedCollection);
    });

    it('Should call ModalidadeContratacao query and add missing value', () => {
      const aquisicao: IAquisicao = { id: 456 };
      const modalidadeContratacao: IModalidadeContratacao = { id: 87704 };
      aquisicao.modalidadeContratacao = modalidadeContratacao;

      const modalidadeContratacaoCollection: IModalidadeContratacao[] = [{ id: 32642 }];
      jest.spyOn(modalidadeContratacaoService, 'query').mockReturnValue(of(new HttpResponse({ body: modalidadeContratacaoCollection })));
      const additionalModalidadeContratacaos = [modalidadeContratacao];
      const expectedCollection: IModalidadeContratacao[] = [...additionalModalidadeContratacaos, ...modalidadeContratacaoCollection];
      jest.spyOn(modalidadeContratacaoService, 'addModalidadeContratacaoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ aquisicao });
      comp.ngOnInit();

      expect(modalidadeContratacaoService.query).toHaveBeenCalled();
      expect(modalidadeContratacaoService.addModalidadeContratacaoToCollectionIfMissing).toHaveBeenCalledWith(
        modalidadeContratacaoCollection,
        ...additionalModalidadeContratacaos
      );
      expect(comp.modalidadeContratacaosSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const aquisicao: IAquisicao = { id: 456 };
      const contrato: IContrato = { id: 14687 };
      aquisicao.contrato = contrato;
      const categoria: ICategoria = { id: 83505 };
      aquisicao.categoria = categoria;
      const faseAquisicao: IFaseAquisicao = { id: 98227 };
      aquisicao.faseAquisicao = faseAquisicao;
      const linhaOrcamento: ILinhaOrcamento = { id: 51185 };
      aquisicao.linhaOrcamento = linhaOrcamento;
      const modalidadeContratacao: IModalidadeContratacao = { id: 16511 };
      aquisicao.modalidadeContratacao = modalidadeContratacao;

      activatedRoute.data = of({ aquisicao });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(aquisicao));
      expect(comp.contratoesCollection).toContain(contrato);
      expect(comp.categoriasSharedCollection).toContain(categoria);
      expect(comp.faseAquisicaosSharedCollection).toContain(faseAquisicao);
      expect(comp.linhaOrcamentosSharedCollection).toContain(linhaOrcamento);
      expect(comp.modalidadeContratacaosSharedCollection).toContain(modalidadeContratacao);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Aquisicao>>();
      const aquisicao = { id: 123 };
      jest.spyOn(aquisicaoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ aquisicao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: aquisicao }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(aquisicaoService.update).toHaveBeenCalledWith(aquisicao);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Aquisicao>>();
      const aquisicao = new Aquisicao();
      jest.spyOn(aquisicaoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ aquisicao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: aquisicao }));
      saveSubject.complete();

      // THEN
      expect(aquisicaoService.create).toHaveBeenCalledWith(aquisicao);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Aquisicao>>();
      const aquisicao = { id: 123 };
      jest.spyOn(aquisicaoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ aquisicao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(aquisicaoService.update).toHaveBeenCalledWith(aquisicao);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackContratoById', () => {
      it('Should return tracked Contrato primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackContratoById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCategoriaById', () => {
      it('Should return tracked Categoria primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCategoriaById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackFaseAquisicaoById', () => {
      it('Should return tracked FaseAquisicao primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackFaseAquisicaoById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackLinhaOrcamentoById', () => {
      it('Should return tracked LinhaOrcamento primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackLinhaOrcamentoById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackModalidadeContratacaoById', () => {
      it('Should return tracked ModalidadeContratacao primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackModalidadeContratacaoById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
