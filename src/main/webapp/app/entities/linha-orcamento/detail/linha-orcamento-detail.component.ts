import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILinhaOrcamento } from '../linha-orcamento.model';

@Component({
  selector: 'jhi-linha-orcamento-detail',
  templateUrl: './linha-orcamento-detail.component.html',
})
export class LinhaOrcamentoDetailComponent implements OnInit {
  linhaOrcamento: ILinhaOrcamento | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ linhaOrcamento }) => {
      this.linhaOrcamento = linhaOrcamento;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
