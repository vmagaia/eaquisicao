import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { LinhaOrcamentoDetailComponent } from './linha-orcamento-detail.component';

describe('LinhaOrcamento Management Detail Component', () => {
  let comp: LinhaOrcamentoDetailComponent;
  let fixture: ComponentFixture<LinhaOrcamentoDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LinhaOrcamentoDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ linhaOrcamento: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(LinhaOrcamentoDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(LinhaOrcamentoDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load linhaOrcamento on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.linhaOrcamento).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
