import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { LinhaOrcamentoComponent } from './list/linha-orcamento.component';
import { LinhaOrcamentoDetailComponent } from './detail/linha-orcamento-detail.component';
import { LinhaOrcamentoUpdateComponent } from './update/linha-orcamento-update.component';
import { LinhaOrcamentoDeleteDialogComponent } from './delete/linha-orcamento-delete-dialog.component';
import { LinhaOrcamentoRoutingModule } from './route/linha-orcamento-routing.module';

@NgModule({
  imports: [SharedModule, LinhaOrcamentoRoutingModule],
  declarations: [
    LinhaOrcamentoComponent,
    LinhaOrcamentoDetailComponent,
    LinhaOrcamentoUpdateComponent,
    LinhaOrcamentoDeleteDialogComponent,
  ],
  entryComponents: [LinhaOrcamentoDeleteDialogComponent],
})
export class LinhaOrcamentoModule {}
