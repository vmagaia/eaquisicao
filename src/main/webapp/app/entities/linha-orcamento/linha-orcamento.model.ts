import * as dayjs from 'dayjs';
import { IAquisicao } from 'app/entities/aquisicao/aquisicao.model';
import { IContrato } from 'app/entities/contrato/contrato.model';

export interface ILinhaOrcamento {
  id?: number;
  nome?: string;
  entidadeFinanceira?: string | null;
  rubrica?: string;
  descricaoRubrica?: string | null;
  valor?: number;
  valorExecutado?: number | null;
  saldo?: number | null;
  createdAt?: dayjs.Dayjs | null;
  createdBy?: string | null;
  lastModificationAt?: dayjs.Dayjs | null;
  lastModificationBy?: string | null;
  aquisicaos?: IAquisicao[] | null;
  contratoes?: IContrato[] | null;
}

export class LinhaOrcamento implements ILinhaOrcamento {
  constructor(
    public id?: number,
    public nome?: string,
    public entidadeFinanceira?: string | null,
    public rubrica?: string,
    public descricaoRubrica?: string | null,
    public valor?: number,
    public valorExecutado?: number | null,
    public saldo?: number | null,
    public createdAt?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public lastModificationAt?: dayjs.Dayjs | null,
    public lastModificationBy?: string | null,
    public aquisicaos?: IAquisicao[] | null,
    public contratoes?: IContrato[] | null
  ) {}
}

export function getLinhaOrcamentoIdentifier(linhaOrcamento: ILinhaOrcamento): number | undefined {
  return linhaOrcamento.id;
}
