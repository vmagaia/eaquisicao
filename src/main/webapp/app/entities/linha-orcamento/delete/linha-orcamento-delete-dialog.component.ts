import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ILinhaOrcamento } from '../linha-orcamento.model';
import { LinhaOrcamentoService } from '../service/linha-orcamento.service';

@Component({
  templateUrl: './linha-orcamento-delete-dialog.component.html',
})
export class LinhaOrcamentoDeleteDialogComponent {
  linhaOrcamento?: ILinhaOrcamento;

  constructor(protected linhaOrcamentoService: LinhaOrcamentoService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.linhaOrcamentoService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
