import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ILinhaOrcamento, LinhaOrcamento } from '../linha-orcamento.model';
import { LinhaOrcamentoService } from '../service/linha-orcamento.service';

@Injectable({ providedIn: 'root' })
export class LinhaOrcamentoRoutingResolveService implements Resolve<ILinhaOrcamento> {
  constructor(protected service: LinhaOrcamentoService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ILinhaOrcamento> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((linhaOrcamento: HttpResponse<LinhaOrcamento>) => {
          if (linhaOrcamento.body) {
            return of(linhaOrcamento.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new LinhaOrcamento());
  }
}
