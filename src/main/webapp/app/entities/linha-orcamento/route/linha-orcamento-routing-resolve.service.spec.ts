jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { ILinhaOrcamento, LinhaOrcamento } from '../linha-orcamento.model';
import { LinhaOrcamentoService } from '../service/linha-orcamento.service';

import { LinhaOrcamentoRoutingResolveService } from './linha-orcamento-routing-resolve.service';

describe('LinhaOrcamento routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: LinhaOrcamentoRoutingResolveService;
  let service: LinhaOrcamentoService;
  let resultLinhaOrcamento: ILinhaOrcamento | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(LinhaOrcamentoRoutingResolveService);
    service = TestBed.inject(LinhaOrcamentoService);
    resultLinhaOrcamento = undefined;
  });

  describe('resolve', () => {
    it('should return ILinhaOrcamento returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultLinhaOrcamento = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultLinhaOrcamento).toEqual({ id: 123 });
    });

    it('should return new ILinhaOrcamento if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultLinhaOrcamento = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultLinhaOrcamento).toEqual(new LinhaOrcamento());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as LinhaOrcamento })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultLinhaOrcamento = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultLinhaOrcamento).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
