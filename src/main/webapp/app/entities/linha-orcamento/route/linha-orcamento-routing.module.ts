import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { LinhaOrcamentoComponent } from '../list/linha-orcamento.component';
import { LinhaOrcamentoDetailComponent } from '../detail/linha-orcamento-detail.component';
import { LinhaOrcamentoUpdateComponent } from '../update/linha-orcamento-update.component';
import { LinhaOrcamentoRoutingResolveService } from './linha-orcamento-routing-resolve.service';

const linhaOrcamentoRoute: Routes = [
  {
    path: '',
    component: LinhaOrcamentoComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: LinhaOrcamentoDetailComponent,
    resolve: {
      linhaOrcamento: LinhaOrcamentoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: LinhaOrcamentoUpdateComponent,
    resolve: {
      linhaOrcamento: LinhaOrcamentoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: LinhaOrcamentoUpdateComponent,
    resolve: {
      linhaOrcamento: LinhaOrcamentoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(linhaOrcamentoRoute)],
  exports: [RouterModule],
})
export class LinhaOrcamentoRoutingModule {}
