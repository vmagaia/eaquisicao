jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { LinhaOrcamentoService } from '../service/linha-orcamento.service';
import { ILinhaOrcamento, LinhaOrcamento } from '../linha-orcamento.model';

import { LinhaOrcamentoUpdateComponent } from './linha-orcamento-update.component';

describe('LinhaOrcamento Management Update Component', () => {
  let comp: LinhaOrcamentoUpdateComponent;
  let fixture: ComponentFixture<LinhaOrcamentoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let linhaOrcamentoService: LinhaOrcamentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [LinhaOrcamentoUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(LinhaOrcamentoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(LinhaOrcamentoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    linhaOrcamentoService = TestBed.inject(LinhaOrcamentoService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const linhaOrcamento: ILinhaOrcamento = { id: 456 };

      activatedRoute.data = of({ linhaOrcamento });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(linhaOrcamento));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<LinhaOrcamento>>();
      const linhaOrcamento = { id: 123 };
      jest.spyOn(linhaOrcamentoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ linhaOrcamento });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: linhaOrcamento }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(linhaOrcamentoService.update).toHaveBeenCalledWith(linhaOrcamento);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<LinhaOrcamento>>();
      const linhaOrcamento = new LinhaOrcamento();
      jest.spyOn(linhaOrcamentoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ linhaOrcamento });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: linhaOrcamento }));
      saveSubject.complete();

      // THEN
      expect(linhaOrcamentoService.create).toHaveBeenCalledWith(linhaOrcamento);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<LinhaOrcamento>>();
      const linhaOrcamento = { id: 123 };
      jest.spyOn(linhaOrcamentoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ linhaOrcamento });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(linhaOrcamentoService.update).toHaveBeenCalledWith(linhaOrcamento);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
