import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { ILinhaOrcamento, LinhaOrcamento } from '../linha-orcamento.model';
import { LinhaOrcamentoService } from '../service/linha-orcamento.service';

@Component({
  selector: 'jhi-linha-orcamento-update',
  templateUrl: './linha-orcamento-update.component.html',
})
export class LinhaOrcamentoUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nome: [null, [Validators.required]],
    entidadeFinanceira: [],
    rubrica: [null, [Validators.required]],
    descricaoRubrica: [],
    valor: [null, [Validators.required]],
    valorExecutado: [],
    saldo: [],
    createdAt: [],
    createdBy: [],
    lastModificationAt: [],
    lastModificationBy: [],
  });

  constructor(
    protected linhaOrcamentoService: LinhaOrcamentoService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ linhaOrcamento }) => {
      if (linhaOrcamento.id === undefined) {
        const today = dayjs().startOf('day');
        linhaOrcamento.createdAt = today;
        linhaOrcamento.lastModificationAt = today;
      }

      this.updateForm(linhaOrcamento);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const linhaOrcamento = this.createFromForm();
    if (linhaOrcamento.id !== undefined) {
      this.subscribeToSaveResponse(this.linhaOrcamentoService.update(linhaOrcamento));
    } else {
      this.subscribeToSaveResponse(this.linhaOrcamentoService.create(linhaOrcamento));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILinhaOrcamento>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(linhaOrcamento: ILinhaOrcamento): void {
    this.editForm.patchValue({
      id: linhaOrcamento.id,
      nome: linhaOrcamento.nome,
      entidadeFinanceira: linhaOrcamento.entidadeFinanceira,
      rubrica: linhaOrcamento.rubrica,
      descricaoRubrica: linhaOrcamento.descricaoRubrica,
      valor: linhaOrcamento.valor,
      valorExecutado: linhaOrcamento.valorExecutado,
      saldo: linhaOrcamento.saldo,
      createdAt: linhaOrcamento.createdAt ? linhaOrcamento.createdAt.format(DATE_TIME_FORMAT) : null,
      createdBy: linhaOrcamento.createdBy,
      lastModificationAt: linhaOrcamento.lastModificationAt ? linhaOrcamento.lastModificationAt.format(DATE_TIME_FORMAT) : null,
      lastModificationBy: linhaOrcamento.lastModificationBy,
    });
  }

  protected createFromForm(): ILinhaOrcamento {
    return {
      ...new LinhaOrcamento(),
      id: this.editForm.get(['id'])!.value,
      nome: this.editForm.get(['nome'])!.value,
      entidadeFinanceira: this.editForm.get(['entidadeFinanceira'])!.value,
      rubrica: this.editForm.get(['rubrica'])!.value,
      descricaoRubrica: this.editForm.get(['descricaoRubrica'])!.value,
      valor: this.editForm.get(['valor'])!.value,
      valorExecutado: this.editForm.get(['valorExecutado'])!.value,
      saldo: this.editForm.get(['saldo'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? dayjs(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      lastModificationAt: this.editForm.get(['lastModificationAt'])!.value
        ? dayjs(this.editForm.get(['lastModificationAt'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastModificationBy: this.editForm.get(['lastModificationBy'])!.value,
    };
  }
}
