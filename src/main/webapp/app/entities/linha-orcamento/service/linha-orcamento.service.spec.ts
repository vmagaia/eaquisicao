import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { ILinhaOrcamento, LinhaOrcamento } from '../linha-orcamento.model';

import { LinhaOrcamentoService } from './linha-orcamento.service';

describe('LinhaOrcamento Service', () => {
  let service: LinhaOrcamentoService;
  let httpMock: HttpTestingController;
  let elemDefault: ILinhaOrcamento;
  let expectedResult: ILinhaOrcamento | ILinhaOrcamento[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(LinhaOrcamentoService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      nome: 'AAAAAAA',
      entidadeFinanceira: 'AAAAAAA',
      rubrica: 'AAAAAAA',
      descricaoRubrica: 'AAAAAAA',
      valor: 0,
      valorExecutado: 0,
      saldo: 0,
      createdAt: currentDate,
      createdBy: 'AAAAAAA',
      lastModificationAt: currentDate,
      lastModificationBy: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a LinhaOrcamento', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.create(new LinhaOrcamento()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a LinhaOrcamento', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nome: 'BBBBBB',
          entidadeFinanceira: 'BBBBBB',
          rubrica: 'BBBBBB',
          descricaoRubrica: 'BBBBBB',
          valor: 1,
          valorExecutado: 1,
          saldo: 1,
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationBy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a LinhaOrcamento', () => {
      const patchObject = Object.assign(
        {
          entidadeFinanceira: 'BBBBBB',
          valor: 1,
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationBy: 'BBBBBB',
        },
        new LinhaOrcamento()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of LinhaOrcamento', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nome: 'BBBBBB',
          entidadeFinanceira: 'BBBBBB',
          rubrica: 'BBBBBB',
          descricaoRubrica: 'BBBBBB',
          valor: 1,
          valorExecutado: 1,
          saldo: 1,
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationBy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a LinhaOrcamento', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addLinhaOrcamentoToCollectionIfMissing', () => {
      it('should add a LinhaOrcamento to an empty array', () => {
        const linhaOrcamento: ILinhaOrcamento = { id: 123 };
        expectedResult = service.addLinhaOrcamentoToCollectionIfMissing([], linhaOrcamento);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(linhaOrcamento);
      });

      it('should not add a LinhaOrcamento to an array that contains it', () => {
        const linhaOrcamento: ILinhaOrcamento = { id: 123 };
        const linhaOrcamentoCollection: ILinhaOrcamento[] = [
          {
            ...linhaOrcamento,
          },
          { id: 456 },
        ];
        expectedResult = service.addLinhaOrcamentoToCollectionIfMissing(linhaOrcamentoCollection, linhaOrcamento);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a LinhaOrcamento to an array that doesn't contain it", () => {
        const linhaOrcamento: ILinhaOrcamento = { id: 123 };
        const linhaOrcamentoCollection: ILinhaOrcamento[] = [{ id: 456 }];
        expectedResult = service.addLinhaOrcamentoToCollectionIfMissing(linhaOrcamentoCollection, linhaOrcamento);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(linhaOrcamento);
      });

      it('should add only unique LinhaOrcamento to an array', () => {
        const linhaOrcamentoArray: ILinhaOrcamento[] = [{ id: 123 }, { id: 456 }, { id: 66008 }];
        const linhaOrcamentoCollection: ILinhaOrcamento[] = [{ id: 123 }];
        expectedResult = service.addLinhaOrcamentoToCollectionIfMissing(linhaOrcamentoCollection, ...linhaOrcamentoArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const linhaOrcamento: ILinhaOrcamento = { id: 123 };
        const linhaOrcamento2: ILinhaOrcamento = { id: 456 };
        expectedResult = service.addLinhaOrcamentoToCollectionIfMissing([], linhaOrcamento, linhaOrcamento2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(linhaOrcamento);
        expect(expectedResult).toContain(linhaOrcamento2);
      });

      it('should accept null and undefined values', () => {
        const linhaOrcamento: ILinhaOrcamento = { id: 123 };
        expectedResult = service.addLinhaOrcamentoToCollectionIfMissing([], null, linhaOrcamento, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(linhaOrcamento);
      });

      it('should return initial array if no LinhaOrcamento is added', () => {
        const linhaOrcamentoCollection: ILinhaOrcamento[] = [{ id: 123 }];
        expectedResult = service.addLinhaOrcamentoToCollectionIfMissing(linhaOrcamentoCollection, undefined, null);
        expect(expectedResult).toEqual(linhaOrcamentoCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
