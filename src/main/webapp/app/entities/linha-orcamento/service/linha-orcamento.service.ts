import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ILinhaOrcamento, getLinhaOrcamentoIdentifier } from '../linha-orcamento.model';

export type EntityResponseType = HttpResponse<ILinhaOrcamento>;
export type EntityArrayResponseType = HttpResponse<ILinhaOrcamento[]>;

@Injectable({ providedIn: 'root' })
export class LinhaOrcamentoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/linha-orcamentos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(linhaOrcamento: ILinhaOrcamento): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(linhaOrcamento);
    return this.http
      .post<ILinhaOrcamento>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(linhaOrcamento: ILinhaOrcamento): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(linhaOrcamento);
    return this.http
      .put<ILinhaOrcamento>(`${this.resourceUrl}/${getLinhaOrcamentoIdentifier(linhaOrcamento) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(linhaOrcamento: ILinhaOrcamento): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(linhaOrcamento);
    return this.http
      .patch<ILinhaOrcamento>(`${this.resourceUrl}/${getLinhaOrcamentoIdentifier(linhaOrcamento) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ILinhaOrcamento>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ILinhaOrcamento[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addLinhaOrcamentoToCollectionIfMissing(
    linhaOrcamentoCollection: ILinhaOrcamento[],
    ...linhaOrcamentosToCheck: (ILinhaOrcamento | null | undefined)[]
  ): ILinhaOrcamento[] {
    const linhaOrcamentos: ILinhaOrcamento[] = linhaOrcamentosToCheck.filter(isPresent);
    if (linhaOrcamentos.length > 0) {
      const linhaOrcamentoCollectionIdentifiers = linhaOrcamentoCollection.map(
        linhaOrcamentoItem => getLinhaOrcamentoIdentifier(linhaOrcamentoItem)!
      );
      const linhaOrcamentosToAdd = linhaOrcamentos.filter(linhaOrcamentoItem => {
        const linhaOrcamentoIdentifier = getLinhaOrcamentoIdentifier(linhaOrcamentoItem);
        if (linhaOrcamentoIdentifier == null || linhaOrcamentoCollectionIdentifiers.includes(linhaOrcamentoIdentifier)) {
          return false;
        }
        linhaOrcamentoCollectionIdentifiers.push(linhaOrcamentoIdentifier);
        return true;
      });
      return [...linhaOrcamentosToAdd, ...linhaOrcamentoCollection];
    }
    return linhaOrcamentoCollection;
  }

  protected convertDateFromClient(linhaOrcamento: ILinhaOrcamento): ILinhaOrcamento {
    return Object.assign({}, linhaOrcamento, {
      createdAt: linhaOrcamento.createdAt?.isValid() ? linhaOrcamento.createdAt.toJSON() : undefined,
      lastModificationAt: linhaOrcamento.lastModificationAt?.isValid() ? linhaOrcamento.lastModificationAt.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? dayjs(res.body.createdAt) : undefined;
      res.body.lastModificationAt = res.body.lastModificationAt ? dayjs(res.body.lastModificationAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((linhaOrcamento: ILinhaOrcamento) => {
        linhaOrcamento.createdAt = linhaOrcamento.createdAt ? dayjs(linhaOrcamento.createdAt) : undefined;
        linhaOrcamento.lastModificationAt = linhaOrcamento.lastModificationAt ? dayjs(linhaOrcamento.lastModificationAt) : undefined;
      });
    }
    return res;
  }
}
