import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IFornecedor, getFornecedorIdentifier } from '../fornecedor.model';

export type EntityResponseType = HttpResponse<IFornecedor>;
export type EntityArrayResponseType = HttpResponse<IFornecedor[]>;

@Injectable({ providedIn: 'root' })
export class FornecedorService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/fornecedors');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(fornecedor: IFornecedor): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(fornecedor);
    return this.http
      .post<IFornecedor>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(fornecedor: IFornecedor): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(fornecedor);
    return this.http
      .put<IFornecedor>(`${this.resourceUrl}/${getFornecedorIdentifier(fornecedor) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(fornecedor: IFornecedor): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(fornecedor);
    return this.http
      .patch<IFornecedor>(`${this.resourceUrl}/${getFornecedorIdentifier(fornecedor) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IFornecedor>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IFornecedor[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addFornecedorToCollectionIfMissing(
    fornecedorCollection: IFornecedor[],
    ...fornecedorsToCheck: (IFornecedor | null | undefined)[]
  ): IFornecedor[] {
    const fornecedors: IFornecedor[] = fornecedorsToCheck.filter(isPresent);
    if (fornecedors.length > 0) {
      const fornecedorCollectionIdentifiers = fornecedorCollection.map(fornecedorItem => getFornecedorIdentifier(fornecedorItem)!);
      const fornecedorsToAdd = fornecedors.filter(fornecedorItem => {
        const fornecedorIdentifier = getFornecedorIdentifier(fornecedorItem);
        if (fornecedorIdentifier == null || fornecedorCollectionIdentifiers.includes(fornecedorIdentifier)) {
          return false;
        }
        fornecedorCollectionIdentifiers.push(fornecedorIdentifier);
        return true;
      });
      return [...fornecedorsToAdd, ...fornecedorCollection];
    }
    return fornecedorCollection;
  }

  protected convertDateFromClient(fornecedor: IFornecedor): IFornecedor {
    return Object.assign({}, fornecedor, {
      createdAt: fornecedor.createdAt?.isValid() ? fornecedor.createdAt.toJSON() : undefined,
      lastModificationAt: fornecedor.lastModificationAt?.isValid() ? fornecedor.lastModificationAt.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? dayjs(res.body.createdAt) : undefined;
      res.body.lastModificationAt = res.body.lastModificationAt ? dayjs(res.body.lastModificationAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((fornecedor: IFornecedor) => {
        fornecedor.createdAt = fornecedor.createdAt ? dayjs(fornecedor.createdAt) : undefined;
        fornecedor.lastModificationAt = fornecedor.lastModificationAt ? dayjs(fornecedor.lastModificationAt) : undefined;
      });
    }
    return res;
  }
}
