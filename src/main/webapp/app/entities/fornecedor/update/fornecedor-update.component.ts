import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IFornecedor, Fornecedor } from '../fornecedor.model';
import { FornecedorService } from '../service/fornecedor.service';
import { IPais } from 'app/entities/pais/pais.model';
import { PaisService } from 'app/entities/pais/service/pais.service';

@Component({
  selector: 'jhi-fornecedor-update',
  templateUrl: './fornecedor-update.component.html',
})
export class FornecedorUpdateComponent implements OnInit {
  isSaving = false;

  paisSharedCollection: IPais[] = [];

  editForm = this.fb.group({
    id: [],
    nome: [null, [Validators.required]],
    endereco: [null, [Validators.required]],
    nuit: [null, [Validators.required]],
    areaActividade: [null, [Validators.required]],
    saldo: [],
    createdAt: [],
    createdBy: [],
    lastModificationAt: [],
    lastModificationBy: [],
    pais: [],
  });

  constructor(
    protected fornecedorService: FornecedorService,
    protected paisService: PaisService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fornecedor }) => {
      if (fornecedor.id === undefined) {
        const today = dayjs().startOf('day');
        fornecedor.createdAt = today;
        fornecedor.lastModificationAt = today;
      }

      this.updateForm(fornecedor);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fornecedor = this.createFromForm();
    if (fornecedor.id !== undefined) {
      this.subscribeToSaveResponse(this.fornecedorService.update(fornecedor));
    } else {
      this.subscribeToSaveResponse(this.fornecedorService.create(fornecedor));
    }
  }

  trackPaisById(index: number, item: IPais): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFornecedor>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(fornecedor: IFornecedor): void {
    this.editForm.patchValue({
      id: fornecedor.id,
      nome: fornecedor.nome,
      endereco: fornecedor.endereco,
      nuit: fornecedor.nuit,
      areaActividade: fornecedor.areaActividade,
      saldo: fornecedor.saldo,
      createdAt: fornecedor.createdAt ? fornecedor.createdAt.format(DATE_TIME_FORMAT) : null,
      createdBy: fornecedor.createdBy,
      lastModificationAt: fornecedor.lastModificationAt ? fornecedor.lastModificationAt.format(DATE_TIME_FORMAT) : null,
      lastModificationBy: fornecedor.lastModificationBy,
      pais: fornecedor.pais,
    });

    this.paisSharedCollection = this.paisService.addPaisToCollectionIfMissing(this.paisSharedCollection, fornecedor.pais);
  }

  protected loadRelationshipsOptions(): void {
    this.paisService
      .query()
      .pipe(map((res: HttpResponse<IPais[]>) => res.body ?? []))
      .pipe(map((pais: IPais[]) => this.paisService.addPaisToCollectionIfMissing(pais, this.editForm.get('pais')!.value)))
      .subscribe((pais: IPais[]) => (this.paisSharedCollection = pais));
  }

  protected createFromForm(): IFornecedor {
    return {
      ...new Fornecedor(),
      id: this.editForm.get(['id'])!.value,
      nome: this.editForm.get(['nome'])!.value,
      endereco: this.editForm.get(['endereco'])!.value,
      nuit: this.editForm.get(['nuit'])!.value,
      areaActividade: this.editForm.get(['areaActividade'])!.value,
      saldo: this.editForm.get(['saldo'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? dayjs(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      lastModificationAt: this.editForm.get(['lastModificationAt'])!.value
        ? dayjs(this.editForm.get(['lastModificationAt'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastModificationBy: this.editForm.get(['lastModificationBy'])!.value,
      pais: this.editForm.get(['pais'])!.value,
    };
  }
}
