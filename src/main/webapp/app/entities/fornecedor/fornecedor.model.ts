import * as dayjs from 'dayjs';
import { IPropostaFinanceira } from 'app/entities/proposta-financeira/proposta-financeira.model';
import { IPais } from 'app/entities/pais/pais.model';

export interface IFornecedor {
  id?: number;
  nome?: string;
  endereco?: string;
  nuit?: number;
  areaActividade?: string;
  saldo?: number | null;
  createdAt?: dayjs.Dayjs | null;
  createdBy?: string | null;
  lastModificationAt?: dayjs.Dayjs | null;
  lastModificationBy?: string | null;
  propostaFinanceiras?: IPropostaFinanceira[] | null;
  pais?: IPais | null;
}

export class Fornecedor implements IFornecedor {
  constructor(
    public id?: number,
    public nome?: string,
    public endereco?: string,
    public nuit?: number,
    public areaActividade?: string,
    public saldo?: number | null,
    public createdAt?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public lastModificationAt?: dayjs.Dayjs | null,
    public lastModificationBy?: string | null,
    public propostaFinanceiras?: IPropostaFinanceira[] | null,
    public pais?: IPais | null
  ) {}
}

export function getFornecedorIdentifier(fornecedor: IFornecedor): number | undefined {
  return fornecedor.id;
}
