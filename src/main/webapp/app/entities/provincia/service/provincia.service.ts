import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IProvincia, getProvinciaIdentifier } from '../provincia.model';

export type EntityResponseType = HttpResponse<IProvincia>;
export type EntityArrayResponseType = HttpResponse<IProvincia[]>;

@Injectable({ providedIn: 'root' })
export class ProvinciaService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/provincias');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(provincia: IProvincia): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(provincia);
    return this.http
      .post<IProvincia>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(provincia: IProvincia): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(provincia);
    return this.http
      .put<IProvincia>(`${this.resourceUrl}/${getProvinciaIdentifier(provincia) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(provincia: IProvincia): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(provincia);
    return this.http
      .patch<IProvincia>(`${this.resourceUrl}/${getProvinciaIdentifier(provincia) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IProvincia>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IProvincia[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addProvinciaToCollectionIfMissing(
    provinciaCollection: IProvincia[],
    ...provinciasToCheck: (IProvincia | null | undefined)[]
  ): IProvincia[] {
    const provincias: IProvincia[] = provinciasToCheck.filter(isPresent);
    if (provincias.length > 0) {
      const provinciaCollectionIdentifiers = provinciaCollection.map(provinciaItem => getProvinciaIdentifier(provinciaItem)!);
      const provinciasToAdd = provincias.filter(provinciaItem => {
        const provinciaIdentifier = getProvinciaIdentifier(provinciaItem);
        if (provinciaIdentifier == null || provinciaCollectionIdentifiers.includes(provinciaIdentifier)) {
          return false;
        }
        provinciaCollectionIdentifiers.push(provinciaIdentifier);
        return true;
      });
      return [...provinciasToAdd, ...provinciaCollection];
    }
    return provinciaCollection;
  }

  protected convertDateFromClient(provincia: IProvincia): IProvincia {
    return Object.assign({}, provincia, {
      createdAt: provincia.createdAt?.isValid() ? provincia.createdAt.toJSON() : undefined,
      lastModificationAt: provincia.lastModificationAt?.isValid() ? provincia.lastModificationAt.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? dayjs(res.body.createdAt) : undefined;
      res.body.lastModificationAt = res.body.lastModificationAt ? dayjs(res.body.lastModificationAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((provincia: IProvincia) => {
        provincia.createdAt = provincia.createdAt ? dayjs(provincia.createdAt) : undefined;
        provincia.lastModificationAt = provincia.lastModificationAt ? dayjs(provincia.lastModificationAt) : undefined;
      });
    }
    return res;
  }
}
