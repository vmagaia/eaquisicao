import * as dayjs from 'dayjs';
import { IDistrito } from 'app/entities/distrito/distrito.model';
import { IPais } from 'app/entities/pais/pais.model';

export interface IProvincia {
  id?: number;
  nome?: string | null;
  createdAt?: dayjs.Dayjs | null;
  createdBy?: string | null;
  lastModificationAt?: dayjs.Dayjs | null;
  lastModificationBy?: string | null;
  distritos?: IDistrito[] | null;
  pais?: IPais | null;
}

export class Provincia implements IProvincia {
  constructor(
    public id?: number,
    public nome?: string | null,
    public createdAt?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public lastModificationAt?: dayjs.Dayjs | null,
    public lastModificationBy?: string | null,
    public distritos?: IDistrito[] | null,
    public pais?: IPais | null
  ) {}
}

export function getProvinciaIdentifier(provincia: IProvincia): number | undefined {
  return provincia.id;
}
