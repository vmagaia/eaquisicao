import * as dayjs from 'dayjs';
import { IProvincia } from 'app/entities/provincia/provincia.model';
import { IFornecedor } from 'app/entities/fornecedor/fornecedor.model';

export interface IPais {
  id?: number;
  nome?: string | null;
  createdAt?: dayjs.Dayjs | null;
  createdBy?: string | null;
  lastModificationAt?: dayjs.Dayjs | null;
  lastModificationBy?: string | null;
  provincias?: IProvincia[] | null;
  fornecedors?: IFornecedor[] | null;
}

export class Pais implements IPais {
  constructor(
    public id?: number,
    public nome?: string | null,
    public createdAt?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public lastModificationAt?: dayjs.Dayjs | null,
    public lastModificationBy?: string | null,
    public provincias?: IProvincia[] | null,
    public fornecedors?: IFornecedor[] | null
  ) {}
}

export function getPaisIdentifier(pais: IPais): number | undefined {
  return pais.id;
}
