import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPais, getPaisIdentifier } from '../pais.model';

export type EntityResponseType = HttpResponse<IPais>;
export type EntityArrayResponseType = HttpResponse<IPais[]>;

@Injectable({ providedIn: 'root' })
export class PaisService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/pais');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(pais: IPais): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(pais);
    return this.http
      .post<IPais>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(pais: IPais): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(pais);
    return this.http
      .put<IPais>(`${this.resourceUrl}/${getPaisIdentifier(pais) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(pais: IPais): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(pais);
    return this.http
      .patch<IPais>(`${this.resourceUrl}/${getPaisIdentifier(pais) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPais>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPais[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPaisToCollectionIfMissing(paisCollection: IPais[], ...paisToCheck: (IPais | null | undefined)[]): IPais[] {
    const pais: IPais[] = paisToCheck.filter(isPresent);
    if (pais.length > 0) {
      const paisCollectionIdentifiers = paisCollection.map(paisItem => getPaisIdentifier(paisItem)!);
      const paisToAdd = pais.filter(paisItem => {
        const paisIdentifier = getPaisIdentifier(paisItem);
        if (paisIdentifier == null || paisCollectionIdentifiers.includes(paisIdentifier)) {
          return false;
        }
        paisCollectionIdentifiers.push(paisIdentifier);
        return true;
      });
      return [...paisToAdd, ...paisCollection];
    }
    return paisCollection;
  }

  protected convertDateFromClient(pais: IPais): IPais {
    return Object.assign({}, pais, {
      createdAt: pais.createdAt?.isValid() ? pais.createdAt.toJSON() : undefined,
      lastModificationAt: pais.lastModificationAt?.isValid() ? pais.lastModificationAt.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? dayjs(res.body.createdAt) : undefined;
      res.body.lastModificationAt = res.body.lastModificationAt ? dayjs(res.body.lastModificationAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((pais: IPais) => {
        pais.createdAt = pais.createdAt ? dayjs(pais.createdAt) : undefined;
        pais.lastModificationAt = pais.lastModificationAt ? dayjs(pais.lastModificationAt) : undefined;
      });
    }
    return res;
  }
}
