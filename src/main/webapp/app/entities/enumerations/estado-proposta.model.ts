export enum EstadoProposta {
  Pendente = 'Pendente',

  Aprovada = 'Aprovada',

  Reprovada = 'Reprovada',
}
