import * as dayjs from 'dayjs';
import { IAquisicao } from 'app/entities/aquisicao/aquisicao.model';

export interface ICategoria {
  id?: number;
  nome?: string;
  descricao?: string | null;
  createdAt?: dayjs.Dayjs | null;
  createdBy?: string | null;
  lastModificationAt?: dayjs.Dayjs | null;
  lastModificationBy?: string | null;
  aquisicaos?: IAquisicao[] | null;
}

export class Categoria implements ICategoria {
  constructor(
    public id?: number,
    public nome?: string,
    public descricao?: string | null,
    public createdAt?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public lastModificationAt?: dayjs.Dayjs | null,
    public lastModificationBy?: string | null,
    public aquisicaos?: IAquisicao[] | null
  ) {}
}

export function getCategoriaIdentifier(categoria: ICategoria): number | undefined {
  return categoria.id;
}
