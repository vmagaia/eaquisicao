import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IContrato, getContratoIdentifier } from '../contrato.model';

export type EntityResponseType = HttpResponse<IContrato>;
export type EntityArrayResponseType = HttpResponse<IContrato[]>;

@Injectable({ providedIn: 'root' })
export class ContratoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/contratoes');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(contrato: IContrato): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(contrato);
    return this.http
      .post<IContrato>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(contrato: IContrato): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(contrato);
    return this.http
      .put<IContrato>(`${this.resourceUrl}/${getContratoIdentifier(contrato) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(contrato: IContrato): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(contrato);
    return this.http
      .patch<IContrato>(`${this.resourceUrl}/${getContratoIdentifier(contrato) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IContrato>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IContrato[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addContratoToCollectionIfMissing(contratoCollection: IContrato[], ...contratoesToCheck: (IContrato | null | undefined)[]): IContrato[] {
    const contratoes: IContrato[] = contratoesToCheck.filter(isPresent);
    if (contratoes.length > 0) {
      const contratoCollectionIdentifiers = contratoCollection.map(contratoItem => getContratoIdentifier(contratoItem)!);
      const contratoesToAdd = contratoes.filter(contratoItem => {
        const contratoIdentifier = getContratoIdentifier(contratoItem);
        if (contratoIdentifier == null || contratoCollectionIdentifiers.includes(contratoIdentifier)) {
          return false;
        }
        contratoCollectionIdentifiers.push(contratoIdentifier);
        return true;
      });
      return [...contratoesToAdd, ...contratoCollection];
    }
    return contratoCollection;
  }

  protected convertDateFromClient(contrato: IContrato): IContrato {
    return Object.assign({}, contrato, {
      inicioPrevisto: contrato.inicioPrevisto?.isValid() ? contrato.inicioPrevisto.format(DATE_FORMAT) : undefined,
      terminoPrevisto: contrato.terminoPrevisto?.isValid() ? contrato.terminoPrevisto.format(DATE_FORMAT) : undefined,
      inicioEfectivo: contrato.inicioEfectivo?.isValid() ? contrato.inicioEfectivo.format(DATE_FORMAT) : undefined,
      terminoEfectivo: contrato.terminoEfectivo?.isValid() ? contrato.terminoEfectivo.format(DATE_FORMAT) : undefined,
      createdAt: contrato.createdAt?.isValid() ? contrato.createdAt.toJSON() : undefined,
      lastModificationAt: contrato.lastModificationAt?.isValid() ? contrato.lastModificationAt.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.inicioPrevisto = res.body.inicioPrevisto ? dayjs(res.body.inicioPrevisto) : undefined;
      res.body.terminoPrevisto = res.body.terminoPrevisto ? dayjs(res.body.terminoPrevisto) : undefined;
      res.body.inicioEfectivo = res.body.inicioEfectivo ? dayjs(res.body.inicioEfectivo) : undefined;
      res.body.terminoEfectivo = res.body.terminoEfectivo ? dayjs(res.body.terminoEfectivo) : undefined;
      res.body.createdAt = res.body.createdAt ? dayjs(res.body.createdAt) : undefined;
      res.body.lastModificationAt = res.body.lastModificationAt ? dayjs(res.body.lastModificationAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((contrato: IContrato) => {
        contrato.inicioPrevisto = contrato.inicioPrevisto ? dayjs(contrato.inicioPrevisto) : undefined;
        contrato.terminoPrevisto = contrato.terminoPrevisto ? dayjs(contrato.terminoPrevisto) : undefined;
        contrato.inicioEfectivo = contrato.inicioEfectivo ? dayjs(contrato.inicioEfectivo) : undefined;
        contrato.terminoEfectivo = contrato.terminoEfectivo ? dayjs(contrato.terminoEfectivo) : undefined;
        contrato.createdAt = contrato.createdAt ? dayjs(contrato.createdAt) : undefined;
        contrato.lastModificationAt = contrato.lastModificationAt ? dayjs(contrato.lastModificationAt) : undefined;
      });
    }
    return res;
  }
}
