import * as dayjs from 'dayjs';
import { IPropostaFinanceira } from 'app/entities/proposta-financeira/proposta-financeira.model';
import { ILinhaOrcamento } from 'app/entities/linha-orcamento/linha-orcamento.model';

export interface IContrato {
  id?: number;
  codigo?: number | null;
  valor?: number;
  inicioPrevisto?: dayjs.Dayjs;
  terminoPrevisto?: dayjs.Dayjs;
  inicioEfectivo?: dayjs.Dayjs;
  terminoEfectivo?: dayjs.Dayjs;
  descricao?: string | null;
  createdAt?: dayjs.Dayjs | null;
  createdBy?: string | null;
  lastModificationAt?: dayjs.Dayjs | null;
  lastModificationBy?: string | null;
  propostaFinanceira?: IPropostaFinanceira | null;
  linhaOrcamento?: ILinhaOrcamento;
}

export class Contrato implements IContrato {
  constructor(
    public id?: number,
    public codigo?: number | null,
    public valor?: number,
    public inicioPrevisto?: dayjs.Dayjs,
    public terminoPrevisto?: dayjs.Dayjs,
    public inicioEfectivo?: dayjs.Dayjs,
    public terminoEfectivo?: dayjs.Dayjs,
    public descricao?: string | null,
    public createdAt?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public lastModificationAt?: dayjs.Dayjs | null,
    public lastModificationBy?: string | null,
    public propostaFinanceira?: IPropostaFinanceira | null,
    public linhaOrcamento?: ILinhaOrcamento
  ) {}
}

export function getContratoIdentifier(contrato: IContrato): number | undefined {
  return contrato.id;
}
