import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IContrato, Contrato } from '../contrato.model';
import { ContratoService } from '../service/contrato.service';
import { IPropostaFinanceira } from 'app/entities/proposta-financeira/proposta-financeira.model';
import { PropostaFinanceiraService } from 'app/entities/proposta-financeira/service/proposta-financeira.service';
import { ILinhaOrcamento } from 'app/entities/linha-orcamento/linha-orcamento.model';
import { LinhaOrcamentoService } from 'app/entities/linha-orcamento/service/linha-orcamento.service';

@Component({
  selector: 'jhi-contrato-update',
  templateUrl: './contrato-update.component.html',
})
export class ContratoUpdateComponent implements OnInit {
  isSaving = false;

  propostaFinanceirasCollection: IPropostaFinanceira[] = [];
  linhaOrcamentosSharedCollection: ILinhaOrcamento[] = [];

  editForm = this.fb.group({
    id: [],
    codigo: [],
    valor: [null, [Validators.required]],
    inicioPrevisto: [null, [Validators.required]],
    terminoPrevisto: [null, [Validators.required]],
    inicioEfectivo: [null, [Validators.required]],
    terminoEfectivo: [null, [Validators.required]],
    descricao: [],
    createdAt: [],
    createdBy: [],
    lastModificationAt: [],
    lastModificationBy: [],
    propostaFinanceira: [],
    linhaOrcamento: [null, Validators.required],
  });

  constructor(
    protected contratoService: ContratoService,
    protected propostaFinanceiraService: PropostaFinanceiraService,
    protected linhaOrcamentoService: LinhaOrcamentoService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ contrato }) => {
      if (contrato.id === undefined) {
        const today = dayjs().startOf('day');
        contrato.createdAt = today;
        contrato.lastModificationAt = today;
      }

      this.updateForm(contrato);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const contrato = this.createFromForm();
    if (contrato.id !== undefined) {
      this.subscribeToSaveResponse(this.contratoService.update(contrato));
    } else {
      this.subscribeToSaveResponse(this.contratoService.create(contrato));
    }
  }

  trackPropostaFinanceiraById(index: number, item: IPropostaFinanceira): number {
    return item.id!;
  }

  trackLinhaOrcamentoById(index: number, item: ILinhaOrcamento): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IContrato>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(contrato: IContrato): void {
    this.editForm.patchValue({
      id: contrato.id,
      codigo: contrato.codigo,
      valor: contrato.valor,
      inicioPrevisto: contrato.inicioPrevisto,
      terminoPrevisto: contrato.terminoPrevisto,
      inicioEfectivo: contrato.inicioEfectivo,
      terminoEfectivo: contrato.terminoEfectivo,
      descricao: contrato.descricao,
      createdAt: contrato.createdAt ? contrato.createdAt.format(DATE_TIME_FORMAT) : null,
      createdBy: contrato.createdBy,
      lastModificationAt: contrato.lastModificationAt ? contrato.lastModificationAt.format(DATE_TIME_FORMAT) : null,
      lastModificationBy: contrato.lastModificationBy,
      propostaFinanceira: contrato.propostaFinanceira,
      linhaOrcamento: contrato.linhaOrcamento,
    });

    this.propostaFinanceirasCollection = this.propostaFinanceiraService.addPropostaFinanceiraToCollectionIfMissing(
      this.propostaFinanceirasCollection,
      contrato.propostaFinanceira
    );
    this.linhaOrcamentosSharedCollection = this.linhaOrcamentoService.addLinhaOrcamentoToCollectionIfMissing(
      this.linhaOrcamentosSharedCollection,
      contrato.linhaOrcamento
    );
  }

  protected loadRelationshipsOptions(): void {
    this.propostaFinanceiraService
      .query({ 'contratoId.specified': 'false' })
      .pipe(map((res: HttpResponse<IPropostaFinanceira[]>) => res.body ?? []))
      .pipe(
        map((propostaFinanceiras: IPropostaFinanceira[]) =>
          this.propostaFinanceiraService.addPropostaFinanceiraToCollectionIfMissing(
            propostaFinanceiras,
            this.editForm.get('propostaFinanceira')!.value
          )
        )
      )
      .subscribe((propostaFinanceiras: IPropostaFinanceira[]) => (this.propostaFinanceirasCollection = propostaFinanceiras));

    this.linhaOrcamentoService
      .query()
      .pipe(map((res: HttpResponse<ILinhaOrcamento[]>) => res.body ?? []))
      .pipe(
        map((linhaOrcamentos: ILinhaOrcamento[]) =>
          this.linhaOrcamentoService.addLinhaOrcamentoToCollectionIfMissing(linhaOrcamentos, this.editForm.get('linhaOrcamento')!.value)
        )
      )
      .subscribe((linhaOrcamentos: ILinhaOrcamento[]) => (this.linhaOrcamentosSharedCollection = linhaOrcamentos));
  }

  protected createFromForm(): IContrato {
    return {
      ...new Contrato(),
      id: this.editForm.get(['id'])!.value,
      codigo: this.editForm.get(['codigo'])!.value,
      valor: this.editForm.get(['valor'])!.value,
      inicioPrevisto: this.editForm.get(['inicioPrevisto'])!.value,
      terminoPrevisto: this.editForm.get(['terminoPrevisto'])!.value,
      inicioEfectivo: this.editForm.get(['inicioEfectivo'])!.value,
      terminoEfectivo: this.editForm.get(['terminoEfectivo'])!.value,
      descricao: this.editForm.get(['descricao'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? dayjs(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      lastModificationAt: this.editForm.get(['lastModificationAt'])!.value
        ? dayjs(this.editForm.get(['lastModificationAt'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastModificationBy: this.editForm.get(['lastModificationBy'])!.value,
      propostaFinanceira: this.editForm.get(['propostaFinanceira'])!.value,
      linhaOrcamento: this.editForm.get(['linhaOrcamento'])!.value,
    };
  }
}
