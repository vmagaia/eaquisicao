jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ContratoService } from '../service/contrato.service';
import { IContrato, Contrato } from '../contrato.model';
import { IPropostaFinanceira } from 'app/entities/proposta-financeira/proposta-financeira.model';
import { PropostaFinanceiraService } from 'app/entities/proposta-financeira/service/proposta-financeira.service';
import { ILinhaOrcamento } from 'app/entities/linha-orcamento/linha-orcamento.model';
import { LinhaOrcamentoService } from 'app/entities/linha-orcamento/service/linha-orcamento.service';

import { ContratoUpdateComponent } from './contrato-update.component';

describe('Contrato Management Update Component', () => {
  let comp: ContratoUpdateComponent;
  let fixture: ComponentFixture<ContratoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let contratoService: ContratoService;
  let propostaFinanceiraService: PropostaFinanceiraService;
  let linhaOrcamentoService: LinhaOrcamentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ContratoUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(ContratoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ContratoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    contratoService = TestBed.inject(ContratoService);
    propostaFinanceiraService = TestBed.inject(PropostaFinanceiraService);
    linhaOrcamentoService = TestBed.inject(LinhaOrcamentoService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call propostaFinanceira query and add missing value', () => {
      const contrato: IContrato = { id: 456 };
      const propostaFinanceira: IPropostaFinanceira = { id: 33495 };
      contrato.propostaFinanceira = propostaFinanceira;

      const propostaFinanceiraCollection: IPropostaFinanceira[] = [{ id: 13055 }];
      jest.spyOn(propostaFinanceiraService, 'query').mockReturnValue(of(new HttpResponse({ body: propostaFinanceiraCollection })));
      const expectedCollection: IPropostaFinanceira[] = [propostaFinanceira, ...propostaFinanceiraCollection];
      jest.spyOn(propostaFinanceiraService, 'addPropostaFinanceiraToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ contrato });
      comp.ngOnInit();

      expect(propostaFinanceiraService.query).toHaveBeenCalled();
      expect(propostaFinanceiraService.addPropostaFinanceiraToCollectionIfMissing).toHaveBeenCalledWith(
        propostaFinanceiraCollection,
        propostaFinanceira
      );
      expect(comp.propostaFinanceirasCollection).toEqual(expectedCollection);
    });

    it('Should call LinhaOrcamento query and add missing value', () => {
      const contrato: IContrato = { id: 456 };
      const linhaOrcamento: ILinhaOrcamento = { id: 18094 };
      contrato.linhaOrcamento = linhaOrcamento;

      const linhaOrcamentoCollection: ILinhaOrcamento[] = [{ id: 89025 }];
      jest.spyOn(linhaOrcamentoService, 'query').mockReturnValue(of(new HttpResponse({ body: linhaOrcamentoCollection })));
      const additionalLinhaOrcamentos = [linhaOrcamento];
      const expectedCollection: ILinhaOrcamento[] = [...additionalLinhaOrcamentos, ...linhaOrcamentoCollection];
      jest.spyOn(linhaOrcamentoService, 'addLinhaOrcamentoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ contrato });
      comp.ngOnInit();

      expect(linhaOrcamentoService.query).toHaveBeenCalled();
      expect(linhaOrcamentoService.addLinhaOrcamentoToCollectionIfMissing).toHaveBeenCalledWith(
        linhaOrcamentoCollection,
        ...additionalLinhaOrcamentos
      );
      expect(comp.linhaOrcamentosSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const contrato: IContrato = { id: 456 };
      const propostaFinanceira: IPropostaFinanceira = { id: 87420 };
      contrato.propostaFinanceira = propostaFinanceira;
      const linhaOrcamento: ILinhaOrcamento = { id: 31379 };
      contrato.linhaOrcamento = linhaOrcamento;

      activatedRoute.data = of({ contrato });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(contrato));
      expect(comp.propostaFinanceirasCollection).toContain(propostaFinanceira);
      expect(comp.linhaOrcamentosSharedCollection).toContain(linhaOrcamento);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Contrato>>();
      const contrato = { id: 123 };
      jest.spyOn(contratoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ contrato });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: contrato }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(contratoService.update).toHaveBeenCalledWith(contrato);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Contrato>>();
      const contrato = new Contrato();
      jest.spyOn(contratoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ contrato });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: contrato }));
      saveSubject.complete();

      // THEN
      expect(contratoService.create).toHaveBeenCalledWith(contrato);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Contrato>>();
      const contrato = { id: 123 };
      jest.spyOn(contratoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ contrato });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(contratoService.update).toHaveBeenCalledWith(contrato);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackPropostaFinanceiraById', () => {
      it('Should return tracked PropostaFinanceira primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPropostaFinanceiraById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackLinhaOrcamentoById', () => {
      it('Should return tracked LinhaOrcamento primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackLinhaOrcamentoById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
