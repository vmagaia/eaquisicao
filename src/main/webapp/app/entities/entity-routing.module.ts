import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'aquisicao',
        data: { pageTitle: 'eaquisicaoApp.aquisicao.home.title' },
        loadChildren: () => import('./aquisicao/aquisicao.module').then(m => m.AquisicaoModule),
      },
      {
        path: 'categoria',
        data: { pageTitle: 'eaquisicaoApp.categoria.home.title' },
        loadChildren: () => import('./categoria/categoria.module').then(m => m.CategoriaModule),
      },
      {
        path: 'modalidade-contratacao',
        data: { pageTitle: 'eaquisicaoApp.modalidadeContratacao.home.title' },
        loadChildren: () => import('./modalidade-contratacao/modalidade-contratacao.module').then(m => m.ModalidadeContratacaoModule),
      },
      {
        path: 'linha-orcamento',
        data: { pageTitle: 'eaquisicaoApp.linhaOrcamento.home.title' },
        loadChildren: () => import('./linha-orcamento/linha-orcamento.module').then(m => m.LinhaOrcamentoModule),
      },
      {
        path: 'fase-aquisicao',
        data: { pageTitle: 'eaquisicaoApp.faseAquisicao.home.title' },
        loadChildren: () => import('./fase-aquisicao/fase-aquisicao.module').then(m => m.FaseAquisicaoModule),
      },
      {
        path: 'fornecedor',
        data: { pageTitle: 'eaquisicaoApp.fornecedor.home.title' },
        loadChildren: () => import('./fornecedor/fornecedor.module').then(m => m.FornecedorModule),
      },
      {
        path: 'proposta-financeira',
        data: { pageTitle: 'eaquisicaoApp.propostaFinanceira.home.title' },
        loadChildren: () => import('./proposta-financeira/proposta-financeira.module').then(m => m.PropostaFinanceiraModule),
      },
      {
        path: 'contrato',
        data: { pageTitle: 'eaquisicaoApp.contrato.home.title' },
        loadChildren: () => import('./contrato/contrato.module').then(m => m.ContratoModule),
      },
      {
        path: 'pais',
        data: { pageTitle: 'eaquisicaoApp.pais.home.title' },
        loadChildren: () => import('./pais/pais.module').then(m => m.PaisModule),
      },
      {
        path: 'provincia',
        data: { pageTitle: 'eaquisicaoApp.provincia.home.title' },
        loadChildren: () => import('./provincia/provincia.module').then(m => m.ProvinciaModule),
      },
      {
        path: 'distrito',
        data: { pageTitle: 'eaquisicaoApp.distrito.home.title' },
        loadChildren: () => import('./distrito/distrito.module').then(m => m.DistritoModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
