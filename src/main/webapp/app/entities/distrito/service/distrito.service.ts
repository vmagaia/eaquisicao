import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDistrito, getDistritoIdentifier } from '../distrito.model';

export type EntityResponseType = HttpResponse<IDistrito>;
export type EntityArrayResponseType = HttpResponse<IDistrito[]>;

@Injectable({ providedIn: 'root' })
export class DistritoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/distritos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(distrito: IDistrito): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(distrito);
    return this.http
      .post<IDistrito>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(distrito: IDistrito): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(distrito);
    return this.http
      .put<IDistrito>(`${this.resourceUrl}/${getDistritoIdentifier(distrito) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(distrito: IDistrito): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(distrito);
    return this.http
      .patch<IDistrito>(`${this.resourceUrl}/${getDistritoIdentifier(distrito) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDistrito>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDistrito[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDistritoToCollectionIfMissing(distritoCollection: IDistrito[], ...distritosToCheck: (IDistrito | null | undefined)[]): IDistrito[] {
    const distritos: IDistrito[] = distritosToCheck.filter(isPresent);
    if (distritos.length > 0) {
      const distritoCollectionIdentifiers = distritoCollection.map(distritoItem => getDistritoIdentifier(distritoItem)!);
      const distritosToAdd = distritos.filter(distritoItem => {
        const distritoIdentifier = getDistritoIdentifier(distritoItem);
        if (distritoIdentifier == null || distritoCollectionIdentifiers.includes(distritoIdentifier)) {
          return false;
        }
        distritoCollectionIdentifiers.push(distritoIdentifier);
        return true;
      });
      return [...distritosToAdd, ...distritoCollection];
    }
    return distritoCollection;
  }

  protected convertDateFromClient(distrito: IDistrito): IDistrito {
    return Object.assign({}, distrito, {
      createdAt: distrito.createdAt?.isValid() ? distrito.createdAt.toJSON() : undefined,
      lastModificationAt: distrito.lastModificationAt?.isValid() ? distrito.lastModificationAt.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? dayjs(res.body.createdAt) : undefined;
      res.body.lastModificationAt = res.body.lastModificationAt ? dayjs(res.body.lastModificationAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((distrito: IDistrito) => {
        distrito.createdAt = distrito.createdAt ? dayjs(distrito.createdAt) : undefined;
        distrito.lastModificationAt = distrito.lastModificationAt ? dayjs(distrito.lastModificationAt) : undefined;
      });
    }
    return res;
  }
}
