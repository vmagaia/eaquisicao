import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IDistrito, Distrito } from '../distrito.model';

import { DistritoService } from './distrito.service';

describe('Distrito Service', () => {
  let service: DistritoService;
  let httpMock: HttpTestingController;
  let elemDefault: IDistrito;
  let expectedResult: IDistrito | IDistrito[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(DistritoService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      nome: 'AAAAAAA',
      createdAt: currentDate,
      createdBy: 'AAAAAAA',
      lastModificationAt: currentDate,
      lastModificationBy: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Distrito', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.create(new Distrito()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Distrito', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nome: 'BBBBBB',
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationBy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Distrito', () => {
      const patchObject = Object.assign(
        {
          nome: 'BBBBBB',
          createdBy: 'BBBBBB',
          lastModificationBy: 'BBBBBB',
        },
        new Distrito()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Distrito', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nome: 'BBBBBB',
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          lastModificationAt: currentDate.format(DATE_TIME_FORMAT),
          lastModificationBy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          lastModificationAt: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Distrito', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addDistritoToCollectionIfMissing', () => {
      it('should add a Distrito to an empty array', () => {
        const distrito: IDistrito = { id: 123 };
        expectedResult = service.addDistritoToCollectionIfMissing([], distrito);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(distrito);
      });

      it('should not add a Distrito to an array that contains it', () => {
        const distrito: IDistrito = { id: 123 };
        const distritoCollection: IDistrito[] = [
          {
            ...distrito,
          },
          { id: 456 },
        ];
        expectedResult = service.addDistritoToCollectionIfMissing(distritoCollection, distrito);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Distrito to an array that doesn't contain it", () => {
        const distrito: IDistrito = { id: 123 };
        const distritoCollection: IDistrito[] = [{ id: 456 }];
        expectedResult = service.addDistritoToCollectionIfMissing(distritoCollection, distrito);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(distrito);
      });

      it('should add only unique Distrito to an array', () => {
        const distritoArray: IDistrito[] = [{ id: 123 }, { id: 456 }, { id: 64447 }];
        const distritoCollection: IDistrito[] = [{ id: 123 }];
        expectedResult = service.addDistritoToCollectionIfMissing(distritoCollection, ...distritoArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const distrito: IDistrito = { id: 123 };
        const distrito2: IDistrito = { id: 456 };
        expectedResult = service.addDistritoToCollectionIfMissing([], distrito, distrito2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(distrito);
        expect(expectedResult).toContain(distrito2);
      });

      it('should accept null and undefined values', () => {
        const distrito: IDistrito = { id: 123 };
        expectedResult = service.addDistritoToCollectionIfMissing([], null, distrito, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(distrito);
      });

      it('should return initial array if no Distrito is added', () => {
        const distritoCollection: IDistrito[] = [{ id: 123 }];
        expectedResult = service.addDistritoToCollectionIfMissing(distritoCollection, undefined, null);
        expect(expectedResult).toEqual(distritoCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
