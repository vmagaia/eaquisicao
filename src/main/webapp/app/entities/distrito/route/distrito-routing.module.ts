import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DistritoComponent } from '../list/distrito.component';
import { DistritoDetailComponent } from '../detail/distrito-detail.component';
import { DistritoUpdateComponent } from '../update/distrito-update.component';
import { DistritoRoutingResolveService } from './distrito-routing-resolve.service';

const distritoRoute: Routes = [
  {
    path: '',
    component: DistritoComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DistritoDetailComponent,
    resolve: {
      distrito: DistritoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DistritoUpdateComponent,
    resolve: {
      distrito: DistritoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DistritoUpdateComponent,
    resolve: {
      distrito: DistritoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(distritoRoute)],
  exports: [RouterModule],
})
export class DistritoRoutingModule {}
