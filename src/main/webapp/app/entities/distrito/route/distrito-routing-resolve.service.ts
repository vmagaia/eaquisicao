import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDistrito, Distrito } from '../distrito.model';
import { DistritoService } from '../service/distrito.service';

@Injectable({ providedIn: 'root' })
export class DistritoRoutingResolveService implements Resolve<IDistrito> {
  constructor(protected service: DistritoService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDistrito> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((distrito: HttpResponse<Distrito>) => {
          if (distrito.body) {
            return of(distrito.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Distrito());
  }
}
