jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IDistrito, Distrito } from '../distrito.model';
import { DistritoService } from '../service/distrito.service';

import { DistritoRoutingResolveService } from './distrito-routing-resolve.service';

describe('Distrito routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: DistritoRoutingResolveService;
  let service: DistritoService;
  let resultDistrito: IDistrito | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(DistritoRoutingResolveService);
    service = TestBed.inject(DistritoService);
    resultDistrito = undefined;
  });

  describe('resolve', () => {
    it('should return IDistrito returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultDistrito = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultDistrito).toEqual({ id: 123 });
    });

    it('should return new IDistrito if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultDistrito = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultDistrito).toEqual(new Distrito());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Distrito })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultDistrito = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultDistrito).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
