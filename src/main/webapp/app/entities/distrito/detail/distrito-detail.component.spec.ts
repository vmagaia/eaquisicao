import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DistritoDetailComponent } from './distrito-detail.component';

describe('Distrito Management Detail Component', () => {
  let comp: DistritoDetailComponent;
  let fixture: ComponentFixture<DistritoDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DistritoDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ distrito: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(DistritoDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(DistritoDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load distrito on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.distrito).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
