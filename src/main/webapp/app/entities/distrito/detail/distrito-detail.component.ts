import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDistrito } from '../distrito.model';

@Component({
  selector: 'jhi-distrito-detail',
  templateUrl: './distrito-detail.component.html',
})
export class DistritoDetailComponent implements OnInit {
  distrito: IDistrito | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ distrito }) => {
      this.distrito = distrito;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
