jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { DistritoService } from '../service/distrito.service';
import { IDistrito, Distrito } from '../distrito.model';
import { IProvincia } from 'app/entities/provincia/provincia.model';
import { ProvinciaService } from 'app/entities/provincia/service/provincia.service';

import { DistritoUpdateComponent } from './distrito-update.component';

describe('Distrito Management Update Component', () => {
  let comp: DistritoUpdateComponent;
  let fixture: ComponentFixture<DistritoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let distritoService: DistritoService;
  let provinciaService: ProvinciaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [DistritoUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(DistritoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DistritoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    distritoService = TestBed.inject(DistritoService);
    provinciaService = TestBed.inject(ProvinciaService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Provincia query and add missing value', () => {
      const distrito: IDistrito = { id: 456 };
      const provincia: IProvincia = { id: 62240 };
      distrito.provincia = provincia;

      const provinciaCollection: IProvincia[] = [{ id: 97700 }];
      jest.spyOn(provinciaService, 'query').mockReturnValue(of(new HttpResponse({ body: provinciaCollection })));
      const additionalProvincias = [provincia];
      const expectedCollection: IProvincia[] = [...additionalProvincias, ...provinciaCollection];
      jest.spyOn(provinciaService, 'addProvinciaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ distrito });
      comp.ngOnInit();

      expect(provinciaService.query).toHaveBeenCalled();
      expect(provinciaService.addProvinciaToCollectionIfMissing).toHaveBeenCalledWith(provinciaCollection, ...additionalProvincias);
      expect(comp.provinciasSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const distrito: IDistrito = { id: 456 };
      const provincia: IProvincia = { id: 11258 };
      distrito.provincia = provincia;

      activatedRoute.data = of({ distrito });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(distrito));
      expect(comp.provinciasSharedCollection).toContain(provincia);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Distrito>>();
      const distrito = { id: 123 };
      jest.spyOn(distritoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ distrito });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: distrito }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(distritoService.update).toHaveBeenCalledWith(distrito);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Distrito>>();
      const distrito = new Distrito();
      jest.spyOn(distritoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ distrito });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: distrito }));
      saveSubject.complete();

      // THEN
      expect(distritoService.create).toHaveBeenCalledWith(distrito);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Distrito>>();
      const distrito = { id: 123 };
      jest.spyOn(distritoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ distrito });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(distritoService.update).toHaveBeenCalledWith(distrito);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackProvinciaById', () => {
      it('Should return tracked Provincia primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackProvinciaById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
