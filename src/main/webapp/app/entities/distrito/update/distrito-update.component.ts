import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IDistrito, Distrito } from '../distrito.model';
import { DistritoService } from '../service/distrito.service';
import { IProvincia } from 'app/entities/provincia/provincia.model';
import { ProvinciaService } from 'app/entities/provincia/service/provincia.service';

@Component({
  selector: 'jhi-distrito-update',
  templateUrl: './distrito-update.component.html',
})
export class DistritoUpdateComponent implements OnInit {
  isSaving = false;

  provinciasSharedCollection: IProvincia[] = [];

  editForm = this.fb.group({
    id: [],
    nome: [],
    createdAt: [],
    createdBy: [],
    lastModificationAt: [],
    lastModificationBy: [],
    provincia: [null, Validators.required],
  });

  constructor(
    protected distritoService: DistritoService,
    protected provinciaService: ProvinciaService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ distrito }) => {
      if (distrito.id === undefined) {
        const today = dayjs().startOf('day');
        distrito.createdAt = today;
        distrito.lastModificationAt = today;
      }

      this.updateForm(distrito);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const distrito = this.createFromForm();
    if (distrito.id !== undefined) {
      this.subscribeToSaveResponse(this.distritoService.update(distrito));
    } else {
      this.subscribeToSaveResponse(this.distritoService.create(distrito));
    }
  }

  trackProvinciaById(index: number, item: IProvincia): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDistrito>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(distrito: IDistrito): void {
    this.editForm.patchValue({
      id: distrito.id,
      nome: distrito.nome,
      createdAt: distrito.createdAt ? distrito.createdAt.format(DATE_TIME_FORMAT) : null,
      createdBy: distrito.createdBy,
      lastModificationAt: distrito.lastModificationAt ? distrito.lastModificationAt.format(DATE_TIME_FORMAT) : null,
      lastModificationBy: distrito.lastModificationBy,
      provincia: distrito.provincia,
    });

    this.provinciasSharedCollection = this.provinciaService.addProvinciaToCollectionIfMissing(
      this.provinciasSharedCollection,
      distrito.provincia
    );
  }

  protected loadRelationshipsOptions(): void {
    this.provinciaService
      .query()
      .pipe(map((res: HttpResponse<IProvincia[]>) => res.body ?? []))
      .pipe(
        map((provincias: IProvincia[]) =>
          this.provinciaService.addProvinciaToCollectionIfMissing(provincias, this.editForm.get('provincia')!.value)
        )
      )
      .subscribe((provincias: IProvincia[]) => (this.provinciasSharedCollection = provincias));
  }

  protected createFromForm(): IDistrito {
    return {
      ...new Distrito(),
      id: this.editForm.get(['id'])!.value,
      nome: this.editForm.get(['nome'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? dayjs(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      lastModificationAt: this.editForm.get(['lastModificationAt'])!.value
        ? dayjs(this.editForm.get(['lastModificationAt'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastModificationBy: this.editForm.get(['lastModificationBy'])!.value,
      provincia: this.editForm.get(['provincia'])!.value,
    };
  }
}
