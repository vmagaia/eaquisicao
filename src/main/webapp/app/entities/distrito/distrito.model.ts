import * as dayjs from 'dayjs';
import { IProvincia } from 'app/entities/provincia/provincia.model';

export interface IDistrito {
  id?: number;
  nome?: string | null;
  createdAt?: dayjs.Dayjs | null;
  createdBy?: string | null;
  lastModificationAt?: dayjs.Dayjs | null;
  lastModificationBy?: string | null;
  provincia?: IProvincia;
}

export class Distrito implements IDistrito {
  constructor(
    public id?: number,
    public nome?: string | null,
    public createdAt?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public lastModificationAt?: dayjs.Dayjs | null,
    public lastModificationBy?: string | null,
    public provincia?: IProvincia
  ) {}
}

export function getDistritoIdentifier(distrito: IDistrito): number | undefined {
  return distrito.id;
}
