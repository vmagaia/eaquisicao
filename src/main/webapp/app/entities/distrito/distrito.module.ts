import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { DistritoComponent } from './list/distrito.component';
import { DistritoDetailComponent } from './detail/distrito-detail.component';
import { DistritoUpdateComponent } from './update/distrito-update.component';
import { DistritoDeleteDialogComponent } from './delete/distrito-delete-dialog.component';
import { DistritoRoutingModule } from './route/distrito-routing.module';

@NgModule({
  imports: [SharedModule, DistritoRoutingModule],
  declarations: [DistritoComponent, DistritoDetailComponent, DistritoUpdateComponent, DistritoDeleteDialogComponent],
  entryComponents: [DistritoDeleteDialogComponent],
})
export class DistritoModule {}
