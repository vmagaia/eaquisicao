import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDistrito } from '../distrito.model';
import { DistritoService } from '../service/distrito.service';

@Component({
  templateUrl: './distrito-delete-dialog.component.html',
})
export class DistritoDeleteDialogComponent {
  distrito?: IDistrito;

  constructor(protected distritoService: DistritoService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.distritoService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
