import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

import { AquisicaoModule } from 'app/entities/aquisicao/aquisicao.module';
import { LoginModule } from 'app/login/login.module';


@NgModule({
  imports: [SharedModule, RouterModule.forChild([HOME_ROUTE]), AquisicaoModule, LoginModule],
  declarations: [HomeComponent],
})
export class HomeModule {}
